$(function(){
	
	// $("#anggota_investor").hide();
	// $("#Investasi").hide();
	// $("select[name=role_id]").on('change',function(e){
	// 	// $('select[name="role_id"] option:first').attr({selected:true,disabled:true});
	// 	var status = $(this).val();
	// 	if (status == 2) {
	// 		$('#anggota_investor').slideDown(200);
	// 		$('#Investasi').slideUp(200);
	// 	}else{
	//     	$("#anggota_investor").slideDown(200);
	//     	$("#Investasi").slideDown(200);
	// 	}
	// 	// return e;
	// });

	$(document).on('click','.yes',function(){
		var get_id 		= $(this).attr('data-user');
		var desc 		= $(this).attr('desc');
		var checkbox 	= $('input[type="checkbox"][get-user="'+get_id+'"]');
		if(confirm(desc) == true) {
			return true;
		}
		else {
			if(checkbox.attr('checked') == true) {	
				checkbox.prop('checked',true);			
			}
			else {
				checkbox.prop('checked',false);
			}
			return false;
		}
	});

	$("#nomK").hide();
	$("#nomP").hide();
	$("#nomW").hide();
	$("select[name=jbayar]").on('change', function (e) {
		var jenis = $(this).val();
		if (jenis == 3) {
			$('#nomK').slideDown(200);
			$("#nomW").slideUp(200);
			$("#nomP").slideUp(200);
		} else if (jenis== 2) {
			$("#nomK").slideUp(200);
			$("#nomW").slideDown(200);
			$("#nomP").slideUp(200);
		} else {
			$("#nomK").slideUp(200);
			$("#nomW").slideUp(200);
			$("#nomP").slideDown(200);
		}
		// return e;
	});
});

function validateForm() {
	var recaptcha = document.forms["myForm"]["g-recaptcha-response"].value;
	if (recaptcha == "") {
		alert("Centang reCAPTCHA");
		return false;
	}
}

function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

function getOmzet() {
	$.ajax({
		url: '/admin/api/pos',
		method: 'GET',
		dataType: 'json',

	}).done(
		function (r) {
			//alert(r.data.omset_hari_ini);
			$('span#omzet_now').html(convertToRupiah(r.data.omset_hari_ini));
			$('span.omzet_y').html(convertToRupiah(r.data.omset_kemarin));
		}
	);
}

$(document).ready(function (e) {
	getOmzet();
});


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('anggota-check', 'Api\ApiAnggotaController@findAnggota')->name('api.get.anggota-check');
Route::post('penjualan', 'Api\ApiPenjualanController@store')->name('api.penjualan');
Route::get('get-all-anggota', 'AnggotaController@fullAnggota')->name('api.fullAnggota');

<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UtamaController@welcome')->name('/');
Route::get('/profil', 'UtamaController@tentang')->name('/profil');
Route::get('/struktur', 'UtamaController@struktur')->name('/struktur');
Route::get('/faq', 'UtamaController@faq')->name('/faq');
Route::get('/blog', 'UtamaController@blog')->name('/blog');
Route::get('/blog-one/{id}', 'UtamaController@blogOne')->name('blog-one');
Route::get('/page_doc', 'UtamaController@page_doc')->name('/page_doc');
Route::get('resource/download_dokumen', ['as' => 'resource.download', 'uses' => 'SessionController@download']);

Route::get('/login', 'SessionController@form')->name('login');
Route::post('/login', 'SessionController@validasi')->name('process_login');
Route::get('/logout', 'SessionController@logout')->name('logout');
Route::get('/registration', 'SessionController@registration')->name('registration');
Route::post('/registration', 'SessionController@registrationStore', ['middleware' => 'web'])->name('process_registration');

Route::group(['prefix' => 'anggota', 'middleware' => 'AnggotaMiddleware'], function () {
    Route::get('/', 'DashboardController@dashboard_anggota')->name('anggota');
    Route::resource('profil_anggota', 'AnggotaProfilController');
    Route::resource('password_anggota', 'UpdatePassword');
    Route::resource('simpanan_anggota', 'AnggotaPembayaranController', ['except' => ['show', 'edit', 'update', 'destroy']]);
    Route::resource('dokumen_anggota', 'AnggotaDokumenController');
    Route::get('resource/download_anggota', ['as' => 'anggota.resource.download', 'uses' => 'AnggotaDokumenController@download']);
    Route::resource('belanja_anggota', 'BelanjaAnggotaController');
});

Route::group(['prefix' => 'investor', 'middleware' => 'InvestorMiddleware'], function () {
    //omset hari ini
    Route::get('api/pos', 'InvestorProfilController@getOmset');

    Route::get('/', 'DashboardController@dashboard_investor')->name('investor');
    Route::resource('profil', 'InvestorProfilController');
    Route::resource('password', 'UpdatePassword');
    Route::resource('simpanan', 'InvestorPembayaranController', ['except' => ['show', 'edit', 'update', 'destroy']]);
    Route::resource('investasis', 'InvestorInvestasiController', ['except' => ['show', 'edit', 'update', 'destroy']]);
    Route::resource('dokumen_investor', 'InvestorDokumenController');
    Route::get('resource/download_investor', ['as' => 'investor.resource.download', 'uses' => 'InvestorDokumenController@download']);
    Route::resource('belanja_investor', 'BelanjaInvestorController');
});

Route::group(['prefix' => 'admin', 'middleware' => 'AdminMiddleware'], function () {
    //omset hari ini
    Route::get('api/pos', 'AnggotaController@getOmset');

    Route::get('/', 'DashboardController@dashboard_admin')->name('admin');

    //route laporan unit usaha
    Route::get('/download/{jenis}/{id}', array('as' => 'download', 'uses' => 'LaporanController@download'));
    Route::get('/pdfview/{id}', array('as' => 'pdfview', 'uses' => 'LaporanController@pdfview'));
    Route::get('/iniLaporan', 'LaporanController@iniLaporan')->name('iniLaporan');

    Route::resource('role', 'RoleController', ['except' => [
    'create', 'store', 'destroy',
    ]]);

    Route::resource('user', 'UserController');

    Route::resource('anggota', 'AnggotaController');

    Route::get('anggota/simpanan/{id}', 'AdminAnggotaSimpanan@awal')->name('anggota.simpanan');
    Route::get('anggota/investasi/{id}', 'AdminAnggotaInvestasi@awal')->name('anggota.investasi');

    Route::get('/ubah/anggota/{id}/{status}', 'AnggotaController@status');

    Route::resource('jabatan', 'JabatanController');

    Route::resource('jenis_investasi', 'JenisInvestasiController');

    Route::resource('jenis_pembayaran', 'JenisPembayaranController');

    Route::resource('investasi', 'InvestasiController');
    Route::resource('omset', 'OmsetController');
    Route::resource('dokumen', 'DokumenController');
    Route::get('resource/download', ['as' => 'admin.resource.download', 'uses' => 'DokumenController@download']);

    Route::resource('pembayaran', 'PembayaranController');

    Route::get('/ubah/pembayaran/{id}/{status}', 'PembayaranController@status');

    Route::get('/ubah/investasi/{id}/{status}', 'InvestasiController@status');

    Route::get('/ubah/pengambilan/{id}/{status}', 'PengambilanController@status');

    Route::resource('kepengurusan', 'KepengurusanController');

    Route::resource('penjualan', 'PenjualanController', ['except' => ['show', 'edit', 'destroy']]);

    Route::resource('pengambilan', 'PengambilanController');

    Route::resource('visimisi', 'VisimisiController');

    Route::resource('badanhukum', 'BadanhukumController');

    Route::resource('ketentuan', 'KetentuanController');

    Route::resource('faq', 'FaqController');

    Route::resource('berita', 'BeritaController');

    Route::resource('shu', 'ShuController');

    Route::resource('dewan', 'DewanController');
    Route::resource('belanja', 'BelanjaController');

    Route::get('data-omset','DataOmsetController@dataOmset')->name('data.omset');
    Route::get('data-anggota-tahunan','DataOmsetController@dataAnggotaTahunan')->name('data.anggota.tahunan');
    Route::get('data-keuntungan-tahunan','DataOmsetController@dataKeuntunganTahunan')->name('data.keuntungan.tahunan');

});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/kirim-sms', function (){
//    $sms = new \App\Helpers\Zenziva();
  //  $sms->send('085652095696','test kirim zenziva sms');
//});

Route::get('/generate-barcode', function () {
    $anggota = new \App\Model\AnggotaModel();
    //use Milon\Barcode\DNS1D;
    $barcode = new \Milon\Barcode\DNS1D();
    $i = 0;
    foreach ($anggota->get() as $row) {
        ++$i;
        //echo '<img src="data:image/png;base64,' . $barcode->getBarcodePNG($row->nomor_anggota, "C128",2,33,array(1,1,1), true) . '" alt="barcode"   />';

        $barcode->setStorPath(storage_path('/app/images/barcode_anggota/'.$row->nama.'_'));
        $img = $barcode->getBarcodePNGPath($row->nomor_anggota, 'C128', 2, 33);
        //echo $barcode->getBarcodePNGPath($row->nomor_anggota, "C128",3,33,array(255,255,0), true);
        // return response($img.'<br>');
        echo $row->nomor_anggota.'<br>';
    }
});

Route::get('/ajax-chart-anggota', 'DashboardController@ajaxChartAnggota')->name('get.ajaxAnggota');
Route::get('/ajax-chart-line-omzet', 'DashboardController@ajaxLineOmzet')->name('get.ajaxLineOmzet');
Route::get('/ajax-change-toko', 'DashboardController@changeToko')->name('get.changeToko');

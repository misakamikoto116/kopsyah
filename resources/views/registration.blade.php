<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('page_title', 'Registration') | 212 Mart Samarinda</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="{{asset('assets/assets/images/utama/logo1.png')}}">
	<link href="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/morris/morris.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/pages.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/registration.css')}}" rel="stylesheet" type="text/css">	
	<link rel="stylesheet" type="text/css" href="{{asset('component/font-awesome/css/font-awesome.min.css')}}">
	<script src="{{asset('assets/assets/js/modernizr.min.js')}}"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	 <style> 
	 	#rc-imageselect {
			transform:scale(0.77);
			-webkit-transform:scale(0.77);
			transform-origin:0 0;-webkit-transform-origin:0 0;
		} 

		@media screen and (max-height: 575px){ 
			#rc-imageselect, .g-recaptcha {
				transform:scale(0.77);
				-webkit-transform:scale(0.77);
				transform-origin:0 0;
				-webkit-transform-origin:0 0;
				} 
		}
	</style> 
</head>
<body>
	<!-- Begin page -->
	<div id="wrapper">

		<!-- Top Bar Start -->

		<div class="topbar">

			<!-- LOGO -->
			<div class="topbar-left">
				<div class="text-center">
					<a href="{{route('/')}}" class="logo">
						<i class="icon-c-logo"> <img src="{{asset('assets/assets/images/utama/logo1.png')}}" height="40"/> </i>
						<span><img src="{{asset('assets/assets/images/utama/logo1.png')}}" height="50"/></span>
					</a>
				</div>
			</div>

			<!-- Button mobile view to collapse sidebar menu -->
			<div class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="">

						<ul class="nav navbar-nav navbar-right pull-right">
							<li class="hidden-xs">
								<a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
							</li>
							<li class="top-menu-item-xs">
								<a href="{{route('/')}}" id="title-ks" class="profile waves-effect waves-light" aria-expanded="true"><b>Beranda</b></a>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
		
		<!-- Top Bar End -->

		<div class="content-page">
			<div class="navbar navbar-default"></div>
			<!-- Start content -->
			<div class="content">
				<div class="container">
					<div class="panel panel-color panel-primary">
							
						{!! Form::open(['route'=>'process_registration','name'=>'myForm','onsubmit'=>'return validateForm()','class'=>'form-horizontal','files'=>true]) !!}
							@include('registration_form')
							<div class="container">
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-offset-9 text-right">
										<button type="reset" class="btn btn-danger waves-effect waves-light">
										<span class="btn-label"><i class="fa fa-repeat"></i>
										</span>Ulangi</button>
										<button type="submit" class="btn btn-primary effect waves-light">
										<span class="btn-label"><i class="fa fa-plus"></i>
									</span>Simpan</button>
									</div>
								</div>
							</div>
							</div>
						{!! Form::close() !!}
						
					</div><!-- End Panel  -->
				</div><!-- End Container  -->
			</div><!--End content -->

		</div><!-- End Right content here -->

		<footer class="footer text-right" style="position: initial;">
			© 2018. All rights reserved.
		</footer>
	</div><!-- End #Wrapper  -->

	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script src="{{asset('assets/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/detect.js')}}"></script>
	<script src="{{asset('assets/assets/js/fastclick.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.slimscroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.blockUI.js')}}"></script>
	<script src="{{asset('assets/assets/js/waves.js')}}"></script>
	<script src="{{asset('assets/assets/js/wow.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.scrollTo.min.js')}}"></script>

	<!-- jQuery  -->
	<script src="{{asset('assets/assets/plugins/waypoints/lib/jquery.waypoints.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/counterup/jquery.counterup.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/morris/morris.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/raphael/raphael-min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/jquery-knob/jquery.knob.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.core.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.app.js')}}"></script>
	<script src="{{asset('assets/assets/js/custom.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.form-pickers.init.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script> 
	
	<!-- DatePicker  -->
	<script src="{{asset('assets/assets/plugins/moment/moment.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.form-pickers.init.js')}}"></script>
	<!-- End DatePicker  --> 
</body>
</html>

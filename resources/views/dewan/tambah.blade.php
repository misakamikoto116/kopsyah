@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('dewan.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i> </h3></a>
						</div>
						<div class= "container">
							<!-- Alert -->
								@yield('alert')
						</div>
						<div class="panel-body">
							{!! Form::open(['route'=>'dewan.store','class'=>'form-horizontal','files'=>true]) !!}
							<center><h4 class="m-t-0"><b>TAMBAH DEWAN</b></h4></center>
							<br>
							<!--form -->
							<div class="form-group">
									<label class="col-md-2 control-label">Tanggal Menjabat</label>
									<div class="col-md-10">
										<div class="input-group">
											<input type="text" name="tanggal_menjabat" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" value="{{old('tanggal_menjabat')}}" required>
											<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">SK </label>
									<div class="col-md-10">
										<input name = "sk_pengurus" type="file" class="filestyle" required="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Nama</label>
									<div class="col-md-10">
										{!! Form::text('nama',old('nama'),['class'=>'form-control','placeholder'=>"Nama",'required'=>'required']) !!}
									</div>
								</div>

								<div class="form-group">
										<label class="col-md-2 control-label">Foto</label>
										<div class="col-md-10">
											<input name = "foto" type="file" class="filestyle" required="required">
										</div>
									</div>
								<div class="form-group">
										<label class="col-sm-2 control-label">Struktur </label>
										<div class="col-sm-10">
												<select class="form-control" name="struktur" required="required">
														<option selected disabled>-- Pilih Struktur --</option>
														<option value ="1" {{ old('struktur') == 1 ? 'selected' : '' }} >Dewan Pengawas Syariah</option>
														<option value ="2" {{ old('struktur') == 2 ? 'selected' : '' }} >Dewan Pengawas Operasional</option>
														<option value ="3" {{ old('struktur') == 3 ? 'selected' : '' }} >Dewan Penasehat</option>
												</select>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-2 control-label">Jabatan</label>
										<div class="col-sm-10">
												<select class="form-control" name="jabatan" required="required">
														<option selected disabled>-- Pilih Jabatan --</option>
														<option value ="1" {{ old('jabatan') == 1 ? 'selected' : '' }} >Ketua</option>
														<option value ="2" {{ old('jabatan') == 2 ? 'selected' : '' }} >Anggota</option>
												</select>
										</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Status</label>
									<div class="col-sm-10">
											<select class="form-control" name="status_kepengurusan" required="required">
													<option selected disabled>-- Pilih Status --</option>
													<option value ="0" {{ old('jabatan') == 0 ? 'selected' : '' }} >Tidak Aktif</option>
													<option value ="1" {{ old('jabatan') == 1 ? 'selected' : '' }} >Aktif</option>
											</select>
									</div>
							</div>	
								<div class="form-group">
										<label class="col-md-2 control-label">Profil</label>
										<div class="col-md-10">
											{!! Form::textarea('profil',old('profil'),['class'=>'form-control','placeholder'=>"Profil",'required'=>'required']) !!}
										</div>
								</div>

								<div class="form-group">
										<label class="col-md-2 control-label">Deskripsi</label>
										<div class="col-md-10">
											{!! Form::textarea('deskripsi',old('dekripsi'),['class'=>'form-control','placeholder'=>"Deskripsi",'required'=>'required']) !!}
										</div>
								</div>

								

							<!--end -->
							<div class="form-group">
									<div class="col-md-12">
											<div class="form-group">
													<div class="col-md-12">
														 <div class="col-md-offset-9 text-right">
																<button type="reset" class="btn btn-danger waves-effect waves-light">
																		<span class="btn-label"><i class="fa fa-repeat"></i>
																		</span>Ulangi
																</button>
															<button type="submit" class="btn btn-default waves-effect waves-light">
																   <i class="fa fa-save m-r-5"></i>
																   <span>Simpan</span>
														   </button>
			
														 </div>
													</div>
											   </div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
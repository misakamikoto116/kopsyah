@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                   <h3 class="panel-title"> DETAIL DEWAN</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                              <td>Tanggal Menjabat</td>
                                              <td>:</td>
                                              <td>{{DateIndo($dewan->tanggal_menjabat)}}</td>
                                        </tr>
                                        <tr>
                                              <td>SK </td>
                                              <td>:</td>
                                              <td>
                                                @php
                                                $filename = public_path("$dewan->sk_pengurus");
                                                $jln = \File::extension($filename);
                                                @endphp
                                               
                                                @if($jln == "docx" || $jln == "doc")
                                                <a href="{{ route('resource.download', array('file'=>$dewan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px"></a>
                                                @elseif($jln == "xlsx" || $jln == "xls")
                                                      <a href="{{ route('resource.download', array('file'=>$dewan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px"></a>
                                                @elseif($jln == "pdf")
                                                      <a href="{{ route('resource.download', array('file'=>$dewan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px"></a>
                                                @elseif($jln == "ppt")
                                                      <a href="{{ route('resource.download', array('file'=>$dewan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px"></a>
                                                @else
                                                <div class="portfolioContainer">
                                                            <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                            <a href="{{ asset($dewan->sk_pengurus)}}" class="image-popup" title="{{$dewan->sk_pengurus}}">
                                                            <img alt="" src="{{ asset($dewan->sk_pengurus)}}" style="width:50px" height="50px"/>
                                                            </a>
                                                            </div> 
                                                            </div> @endif

                                              </td>
                                        </tr>
                              
                                        <tr>
                                                <td>Nama </td>
                                                <td>:</td>
                                                <td>{{$dewan->nama}}</td>
                                          </tr>
                                          <tr>
                                                      <td>Foto </td>
                                                      <td>:</td>
                                                      <td>
                                                            <div class="portfolioContainer">
                                                            <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                            <a href="{{ asset($dewan->foto)}}" class="image-popup" title="{{$dewan->foto}}">
                                                            <img alt="" src="{{ asset($dewan->foto)}}" style="width:50px" height="50px"/>
                                                            </a>
                                                            </div> 
                                                            </div>
                                                      </td>
                                                </tr>
                                                <tr>
                                                      <td>Struktur</td>
                                                      <td>:</td>
                                                        @if($dewan->struktur == 1)
                                                        <td>{{'Dewan Pengawas Syariah'}}</td>
                                                        @elseif($dewan->struktur == 2)
                                                        <td>{{'Dewan Pengawas Operasional'}}</td>
                                                        @elseif($dewan->struktur == 3)
                                                        <td>{{'Dewan Penasehat'}}</td>
                                                        @endif
                                                </tr>
                                                <tr>
                                                            <td>Jabatan</td>
                                                            <td>:</td>
                                                              @if($dewan->jabatan == 1)
                                                              <td>{{'Ketua'}}</td>
                                                              @elseif($dewan->jabatan == 2)
                                                              <td>{{'Anggota'}}</td>
                                                              @endif
                                                      </tr>
                                                <tr>
                                                            <td>Status</td>
                                                            <td>:</td>
                                                              @if($dewan->status_kepengurusan == 0)
                                                              <td>{{'Tidak Aktif'}}</td>
                                                              @elseif($dewan->status_kepengurusan == 1)
                                                              <td>{{'Aktif'}}</td>
                                                              @endif
                                                      </tr>
                                          <tr>
                                              <td>Profil</td>
                                              <td>:</td>
                                              <td>{{$dewan->profil}}</td>
                                        </tr>
                                        <tr>
                                                <td>Deskripsi</td>
                                                <td>:</td>
                                                <td>{{$dewan->deskripsi}}</td>
                                          </tr>
                                        <tr>
                                              <td class="col-xs-4">Create at</td>
                                              <td class="col-xs-1">:</td>
                                              <td>{{indonesian_date($dewan->created_at)}}</td>
                                        </tr>
                                        <tr>
                                              <td class="col-xs-4">Update at</td>
                                              <td class="col-xs-1">:</td>
                                              <td>{{indonesian_date($dewan->updated_at)}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="col-md-offset-9 text-right">
                                               <a href="{{route('dewan.index')}}">
                                                   <button class="btn btn-warning waves-effect waves-light">
                                                      <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                   </button></a>
                                              </div>
                                          </div>
                                      </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('dewan.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                <a href="#">Kepengurusan</a>
                                            </li>
                                            <li class="active">
                                                Dewan Pengurus
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL DEWAN</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nama Anggota"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table id="datatable" class="table table-striped table-bordered">
                            	<!-- Alert -->
									@yield('alert')
							
                            <thead>
                                <tr>
                                    <th width="10%" scope="row">No</th>
                                    <th width="20%" scope="row">Tanggal Menjabat</th>
                                    <th width="10%" scope="row">SK</th>
                                    <th width="15%" scope="row">Nama Anggota</th>
                                    <th width="20%" scope="row">Struktur</th>
                                    <th width="10%" scope="row">Jabatan</th>
                                    <th width="10%" scope="row">Status</th>
                                    <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($dewan as $dewans)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{DateIndo($dewans->tanggal_menjabat)}}</td>
                                        <td style="text-align:left">
                                            @php
                                                $filename = public_path("$dewans->sk_pengurus");
                                                $jln = \File::extension($filename);
                                            @endphp
                                            @if($jln == "docx" || $jln == "doc")
                                                <a href="{{ route('resource.download', array('file'=>$dewans->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "xlsx" || $jln == "xls")
                                                <a href="{{ route('resource.download', array('file'=>$dewans->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "pdf")
                                                <a href="{{ route('resource.download', array('file'=>$dewans->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "ppt")
                                                <a href="{{ route('resource.download', array('file'=>$dewans->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px"></a>
                                            @else

                                            <div class="portfolioContainer">
                                                    <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                         <a href="{{ asset($dewans->sk_pengurus)}}" class="image-popup" title="{{$dewans->sk_pengurus}}">
                                                                <img alt="" src="{{ asset($dewans->sk_pengurus)}}"  style="width:50px" height="50px"/>
                                                            </a>
                                                    </div> 
                                                </div>   
                                            @endif
                                        </td>
                                        <td>{{$dewans->nama or 'kosong'}}</td>
                                        @if($dewans->struktur == 1)
                                        <td>{{'Dewan Pengawas Syariah'}}</td>
                                        @elseif($dewans->struktur == 2) 
                                        <td>{{'Dewan Pengawas Operasional'}}</td>
                                        @elseif($dewans->struktur == 3) 
                                        <td>{{'Dewan Penasehat'}}</td>
                                        @else
                                        <td>{{'Dewan Pengurus'}}</td>
                                        @endif
                                       
                                        @if($dewans->jabatan == 1)
                                        <td>{{'Ketua'}}</td>
                                        @elseif ($dewans->jabatan == 2)
                                        <td>{{'Anggota'}}</td>
                                        @endif

                                        @if($dewans->status_kepengurusan == 1)
                                        <td>{{'Aktif'}}</td>
                                        @elseif($dewans->status_kepengurusan == 0)
                                        <td>{{' Tidak Aktif'}}</td>
                                        @endif
                                        <td>
                                            {{ Form::open(array('route' => array('dewan.destroy', $dewans->id), 'method' => 'delete'))}}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('dewan.edit', $dewans->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('dewan.show', $dewans->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $dewan->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
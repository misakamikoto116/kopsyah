<div class="form-group">
	<label class="col-md-2 control-label">Tanggal Menjabat</label>
	<div class="col-md-10">
		<div class="input-group">
			@php
			$tgl_conv = date('Y-m-d', strtotime($dewan->tanggal_menjabat));
            $tgl_jadi = date('d-m-Y', strtotime($tgl_conv));
			@endphp
			<input type="text" name="tanggal_menjabat" value="{{ $tgl_jadi }}"class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
			<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">SK</label>
	<div class="col-md-10">
		<input name = "sk_pengurus" type="file" class="filestyle"  value = "{{$dewan->sk_pengurus}}">
		<br>
		@php
        $filename = public_path("$dewan->sk_pengurus");
        $jln = \File::extension($filename);
        @endphp
		
		@if($jln == "docx" || $jln == "doc")
        <img src="../../../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px">
        @elseif($jln == "xlsx" || $jln == "xls")
        <img src="../../../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px">
        @elseif($jln == "pdf")
        <img src="../../../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px">
        @elseif($jln == "ppt")
        <img src="../../../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px">
        @else
        <img alt="" src="{{ asset($dewan->sk_pengurus)}}" style="width:50px" height="50px"/>
        @endif
                                       
        </input>
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-10">
		<input type="text"  required="required" class="form-control"  name="nama" value="{{ $dewan->nama }}" required="required">
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Foto</label>
	<div class="col-md-10">
		<input name = "foto" type="file" class="filestyle"  value = "{{ $dewan->foto}}">
		<br>
		<img id="preview" style="margin-bottom: 5px;" src="{{ asset($dewan->foto) }}" style="width:50px" height="50px">
		</input>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Struktur</label>
	<div class="col-sm-10">
		<select class="form-control" name="struktur" required="required">
			<option value="{{ $dewan->struktur }}">
			@if ($dewan->struktur == 1)
			Dewan Pengawas Syariah</option>
			@elseif  ($dewan->struktur == 2)
			Dewan Pengawas Operasional</option>
			@elseif ($dewan->struktur == 3)
			Dewan Penasehat</option>
			@endif
			<option value ="1">Dewan Pengawas Syariah</option>
			<option value ="2">Dewan Pengawas Operasional</option>
			<option value ="3">Dewan Penasehat</option>
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Jabatan</label>
	<div class="col-sm-10">
		<select class="form-control" name="jabatan" required="required">
			<option value="{{ $dewan->jabatan }}">
			@if ($dewan->jabatan == 1)
			Ketua</option>
			@elseif  ($dewan->jabatan == 2)
			Anggota</option>
			@endif
			<option value ="1">Ketua</option>
			<option value ="2">Anggota</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Status</label>
	<div class="col-sm-10">
		<select class="form-control" name="status_kepengurusan" required="required">
			<option value="{{ $dewan->status_kepengurusan }}">
			@if ($dewan->status_kepengurusan == 0)
			Tidak Aktif</option>
			@elseif  ($dewan->status_kepengurusan == 1)
			Aktif</option>
			@endif
			<option value ="0">Tidak Aktif</option>
			<option value ="1">Aktif</option>
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Profil</label>
	<div class="col-md-10">
		{!! Form::textarea('profil',null,['class'=>'form-control','placeholder'=>"Profil",'required'=>'required']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Deskripsi</label>
	<div class="col-md-10">
		{!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi",'required'=>'required']) !!}
	</div>
</div>
@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        {{-- <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('omset.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div> --}}
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="active">
                                                Omset
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                        <center><h4 class="m-t-0"><b>TABEL OMSET</b></h4></center>
            
                        <table id="datatable" class="table table-striped table-bordered">

                            @if(Session::has('informasi'))
                                <div class="alert alert-info">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <thead>
                                <tr>
                                        <th width="10%" scope="row">No</th>
                                        <th width="20%" scope="row">Tanggal</th>
                                        <th width="20%" scope="row">Anggota</th>
                                        <th width="20%" scope="row">Omset</th>
                                        <th width="20%" scope="row">Non Anggota</th>
                                        <th width="20%" scope="row">Omset</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaOmset as $omset)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$omset->tanggal or 'kosong'}}</td>
                                        <td>{{$omset->omset or 'kosong'}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('omset.destroy', $omset->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('omset.edit', $omset->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('omset.show', $omset->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
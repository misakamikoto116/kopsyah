@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('user.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                <a href="#">User</a>
                                            </li>
                                            <li class="active">
                                                User
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL USER</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('user',null,['class'=>'form-control col-md-3','placeholder'=>"Username"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table id="datatable" class="table table-striped table-bordered">

                            <!-- Alert -->
                            @yield('alert')
                            
                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Username</th> 
                                    <th width="20%" scope="row">Nomor Anggota</th>
                                    <th width="20%" scope="row">Nama</th>
                                    <th width="20%" scope="row">Role</th>
                                    <th width="50%" scope="row">Action</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no =1+($semuaUser->perPage()*($semuaUser->currentPage()-1));
                                @endphp
                                @foreach($semuaUser as $user)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$user->username or 'kosong'}}</td>
                                        <td>{{$user->anggota->nomor_anggota or 'kosong'}}</td>
                                        <td>{{$user->anggota->nama or 'kosong'}}</td>
                                        <td>{{$user->role->nama or 'kosong'}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('user.destroy', $user->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('user.edit', $user->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('user.show', $user->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaUser->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
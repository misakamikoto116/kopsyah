<div class="form-group">
	<label class="col-md-2 control-label">Username</label>
	<div class="col-md-10">
	<input type="text"  required="" class="form-control" value="{{old('username')}}"  name="username"  placeholder="Username"  >
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Password</label>
	<div class="col-md-10">
		<input type="password"  required="" class="form-control"  name="password"  placeholder="Password" >
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Konfirmasi Password</label>
	<div class="col-md-10">
		<input type="password"  required="" class="form-control"  name="password_confirmation"  placeholder="Konfirmasi Password" >
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Role</label>
	<div class="col-md-10">
		<select name="role_id" id="" class="form-control">
			@foreach ($roles as $role )
				<option value="{{$role->id}}">{{ $role->nama}}</option>
			@endforeach
		</select>
	</div>
</div>

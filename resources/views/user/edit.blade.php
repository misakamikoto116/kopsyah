@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"> EDIT USER</h3></a>
						</div>
						
						<div class= "container">
						<!-- Alert -->
							@yield('alert')
						</div>
							
						<div class="panel-body">
     						{{ Form::model($user, array('route' => array('user.update', $user->id), 'files' => true, 'method' => 'PUT','class'=>'form-horizontal')) }}
								 
							 	<!--Form-->
								<div class="form-group">
									<label class="col-md-2 control-label">Username</label>
									<div class="col-md-10">
										<input type="text"  required="" class="form-control" value="{{ $user->username }}" name="username"  placeholder="Username"  >
									</div>
								</div>
								 <div class="form-group">
									<label class="col-md-2 control-label">Password</label>
									<div class="col-md-10">
										<input type="password"  class="form-control"  name="password"  placeholder="Password">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">Konfirmasi Password</label>
									<div class="col-md-10">
										<input type="password"  class="form-control"  name="password_confirmation"  placeholder="Konfirmasi Password">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label">Role</label>
									<div class="col-md-10">
										<select name="role_id" id="" class="form-control">
												@foreach ($roles as $role)
												<option value="{{ $role->id}}"
												@if ($role->id === $user->role_id)
													selected
												@endif
												>{{ $role->nama}}</option>
											@endforeach

										</select>
									</div>
								</div>
	
								 <!--End-->
								 <div class="form-group">
										<div class="col-md-12">
											 <div class="col-md-offset-9 text-right">
												<a href="{{route('user.index')}}" class="btn btn-warning waves-effect waves-light" > 
													<i class="fa fa-arrow-left m-r-5"></i>
													<span>Batal</span>
												</a>
											   <button type="submit" class="btn btn-default waves-effect waves-light">
													   <i class="md-autorenew m-r-5"></i>
													   <span>Perbarui</span>
											   </button>

											 </div>
										</div>
								   </div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
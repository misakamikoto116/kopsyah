@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                   <h3 class="panel-title"> DETAIL PENGURUS</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                              <td>Tanggal Menjabat</td>
                                              <td>:</td>
                                              <td>{{$kepengurusan->tanggal_menjabat}}</td>
                                        </tr>
                                        <tr>
                                              <td>SK Kepengurusan</td>
                                              <td>:</td>
                                              <td>
                                                      @php
                                                      $filename = public_path("$kepengurusan->sk_pengurus");
                                                      $jln = \File::extension($filename);
                                                      @endphp
                                                     
                                                      @if($jln == "docx" || $jln == "doc")
                                                      <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px"></a>
                                                      @elseif($jln == "xlsx" || $jln == "xls")
                                                            <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px"></a>
                                                      @elseif($jln == "pdf")
                                                            <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px"></a>
                                                      @elseif($jln == "ppt")
                                                            <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px"></a>
                                                      @else
                                                      <div class="portfolioContainer">
                                                                  <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                                  <a href="{{ asset($kepengurusan->sk_pengurus)}}" class="image-popup" title="{{$kepengurusan->sk_pengurus}}">
                                                                  <img alt="" src="{{ asset($kepengurusan->sk_pengurus)}}" style="width:50px" height="50px"/>
                                                                  </a>
                                                                  </div> 
                                                                  </div> @endif
      
                                                    </td>   </tr>
                                        <tr>
                                              <td>Status</td>
                                              <td>:</td>
                                                @if($kepengurusan->status_kepengurusan != 0)
                                                <td>{{'Aktif'}}</td>
                                                @else
                                                <td>{{' Tidak Aktif'}}</td>
                                                @endif
                                        </tr>
                                        <tr>
                                              <td>Nomor Anggota</td>
                                              <td>:</td>
                                              <td>{{$kepengurusan->anggota->nomor_anggota}}</td>
                                        </tr>
                                        <tr>
                                                <td>Nama Anggota</td>
                                                <td>:</td>
                                                <td>{{$kepengurusan->anggota->nama}}</td>
                                          </tr>
                                        <tr>
                                              <td>Jabatan</td>
                                              <td>:</td>
                                              <td>{{$kepengurusan->jabatan->posisi}}</td>
                                        </tr>
                                        <tr>
                                              <td class="col-xs-4">Create at</td>
                                              <td class="col-xs-1">:</td>
                                              <td>{{$kepengurusan->created_at}}</td>
                                        </tr>
                                        <tr>
                                              <td class="col-xs-4">Update at</td>
                                              <td class="col-xs-1">:</td>
                                              <td>{{$kepengurusan->updated_at}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                          <div class="col-md-12">
                                              <div class="col-md-offset-9 text-right">
                                               <a href="{{route('kepengurusan.index')}}">
                                                   <button class="btn btn-warning waves-effect waves-light">
                                                      <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                   </button></a>
                                              </div>
                                          </div>
                                      </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
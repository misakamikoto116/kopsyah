<div class="form-group">
	<label class="col-md-2 control-label">Tanggal Menjabat</label>
	<div class="col-md-10">
		<div class="input-group">
			@php
			$tgl_conv = date('Y-m-d', strtotime($kepengurusan->tanggal_menjabat));
            $tgl_jadi = date('d-m-Y', strtotime($tgl_conv));
			@endphp
			<input type="text" name="tanggal_menjabat" value="{{ $tgl_jadi }}" id="tgl_lahir" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">SK Kepengurusan</label>
	<div class="col-md-10">
		<input name = "sk_pengurus" type="file" class="filestyle"  value = "{{ $kepengurusan->sk_pengurus}}">
		<br>@php
        $filename = public_path("$kepengurusan->sk_pengurus");
        $jln = \File::extension($filename);
        @endphp
		
		@if($jln == "docx" || $jln == "doc")
        <img src="../../../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px">
        @elseif($jln == "xlsx" || $jln == "xls")
        <img src="../../../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px">
        @elseif($jln == "pdf")
        <img src="../../../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px">
        @elseif($jln == "ppt")
        <img src="../../../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px">
        @else
        <img alt="" src="{{ asset($kepengurusan->sk_pengurus)}}" style="width:50px" height="50px"/>
        @endif
        </input>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Status Keanggotaan</label>
	<div class="col-sm-10">
		<select class="form-control" name="status_kepengurusan" required="required">
			<option value="{{ $kepengurusan->status_kepengurusan}}">
			@if ($kepengurusan->status_kepengurusan == 1)
			Aktif
			</option>
			<option value ="0">Tidak Aktif</option>
			@else
			Tidak Aktif
			</option>
			<option value ="1">Aktif</option>
			@endif	
		</select>
	</div>
</div>

 
<div class="form-group">
    <label class="col-md-2 control-label">Nama Anggota</label>
    <div class="col-md-10">
        <select class="form-control select2" name="anggota_id" id="">
        	@foreach ($anggotaPluck as $anggota )
           		<option value="{{ $anggota->id}}"
                @if ($anggota->id === $kepengurusan->anggota_id)
                 	selected
                @endif
                  	>{{ $anggota->nama}} - {{ $anggota->nomor_anggota}} </option>
        	@endforeach
         </select>
    </div>
</div> 

<div class="form-group">
	<label class="col-md-2 control-label">Foto Pengurus</label>
	<div class="col-md-10">
		<input name = "foto" type="file" class="filestyle"  value ="{{ $kepengurusan->foto}}">
		<br>
		{{--  @php
        $filename = public_path("$kepengurusan->foto");
        $jln = \File::extension($filename);
        @endphp
		
		@if($jln == "docx" || $jln == "doc")
        <img src="../../../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px">
        @elseif($jln == "xlsx" || $jln == "xls")
        <img src="../../../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px">
        @elseif($jln == "pdf")
        <img src="../../../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px">
        @elseif($jln == "ppt")
        <img src="../../../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px">
        @else
        <img alt="" src="{{ asset($kepengurusan->foto)}}" style="width:50px" height="50px"/>
		@endif  --}}
		<img alt="" src="{{ asset($kepengurusan->foto)}}" style="width:50px" height="50px"/>
        
        </input>
	</div>
</div>


<div class="form-group">
	<label class="col-md-2 control-label">Jabatan</label>
	<div class="col-md-10">
		 {{ Form::select('jabatan_id', $jabatanPluck, null, ['class' => 'form-control']) }}
	</div>
</div>
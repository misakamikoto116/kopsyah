@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('kepengurusan.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i> </h3></a>
						</div>
						<div class="panel-body">
							{!! Form::open(['route'=>'kepengurusan.store','class'=>'form-horizontal','files'=>'true','enctype'=>'multipart/form-data']) !!}
							{!! csrf_field() !!}
							<div class= "container">
									<!-- Alert -->
										@yield('alert')
							</div>
							<center><h4 class="m-t-0"><b>TAMBAH PENGURUS</b></h4></center>
							<br>
							<!--form -->
							<div class="form-group">
								<label class="col-md-2 control-label">Tanggal Menjabat</label>
								<div class="col-md-10">
									<div class="input-group">
										<input type="text" name="tanggal_menjabat" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" value="{{old('tanggal_menjabat')}}" required>
										<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">SK Kepengurusan</label>
								<div class="col-md-10">
									<input name = "sk_pengurus" type="file" class="filestyle" required="required">
								</div>
							</div>
								
							<div class="form-group">
								<label class="col-sm-2 control-label">Status Keanggotaan</label>
								<div class="col-sm-10">
									<select class="form-control" name="status_kepengurusan" required="required">
										<option value ="0" {{ old('status_kepengurusan') == 0 ? 'selected' : '' }} >Tidak Aktif</option>
										<option value ="1" {{ old('status_kepengurusan') == 1 ? 'selected' : '' }} >Aktif</option>
									</select>
								</div>
							</div>
									
							{{-- <div class="form-group">
								<label class="col-md-2 control-label">Nama Pengurus</label>
									<div class="col-md-10">
									<select name="anggota_id" id="" class="form-control">
										@foreach ($anggotaPluck as $anggota )
											<option value="{{$anggota->id}}">{{ $anggota->nama}}</option>
										@endforeach
									</select>
								</div>
							</div> --}}

							<div class="form-group">
								<label class="col-md-2 control-label">Nama Anggota</label>
								<div class="col-md-10">
										<select class="form-control select2"  name="anggota_id" id="" >
									@foreach ($anggotaPluck as $anggota )
										<option value="{{$anggota->id}}">{{ $anggota->nama}} - {{ $anggota->nomor_anggota}}</option>
									@endforeach
								</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Foto Pengurus</label>
								<div class="col-md-10">
									<input name = "foto" type="file" class="filestyle" required="required">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Jabatan</label>
								<div class="col-md-10">
										{{ Form::select('jabatan_id', $jabatanPluck, null, ['class' => 'form-control']) }}
								</div>
							</div>

							<!--end -->
							<div class="form-group">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-md-12">
											<div class="col-md-offset-9 text-right">
												<button type="reset" class="btn btn-danger waves-effect waves-light">
													<span class="btn-label"><i class="fa fa-repeat"></i>
													</span>Ulangi
												</button>
												<button type="submit" class="btn btn-default waves-effect waves-light">
													<i class="fa fa-save m-r-5"></i>
													<span>Simpan</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
</div><!-- End Right content here -->
@stop
@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('kepengurusan.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                <a href="#">Kepengurusan</a>
                                            </li>
                                            <li class="active">
                                                Pengurus
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL PENGURUS</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nama Anggota"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <table id="datatable" class="table table-striped table-bordered">

                           	<!-- Alert -->
								@yield('alert')
							
                            <thead>
                                <tr>
                                    <th width="10%" scope="row">No</th>
                                    <th width="20%" scope="row">Tanggal Menjabat</th>
                                    <th width="15%" scope="row">SK Pengurus</th>
                                    <th width="20%" scope="row">Nama Anggota</th>
                                    <th width="15%" scope="row">Foto Pengurus</th>
                                    <th width="10%" scope="row">Jabatan</th>
                                    <th width="10%" scope="row">Status</th>
                                    <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaKepengurusan as $kepengurusan)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>
                                            {{DateIndo($kepengurusan->tanggal_menjabat)}}
                                        </td>
                                        <td style="text-align:left">
                                            @php
                                                $filename = public_path("$kepengurusan->sk_pengurus");
                                                $jln = \File::extension($filename);
                                            @endphp
                                            @if($jln == "docx" || $jln == "doc")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "xlsx" || $jln == "xls")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "pdf")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "ppt")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->sk_pengurus)) }}"><img src="../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px"></a>
                                            @else

                                            <div class="portfolioContainer">
                                                    <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                            <a href="{{ asset($kepengurusan->sk_pengurus)}}" class="image-popup" title="{{$kepengurusan->sk_pengurus}}">
                                                                <img alt="" src="{{ asset($kepengurusan->sk_pengurus)}}"  style="width:50px" height="50px"/>
                                                            </a>
                                                    </div> 
                                                </div>   
                                            @endif
                                        </td>
                                            
                                        <td>{{$kepengurusan->anggota->nama or 'kosong'}}</td>
                                        <td style="text-align:left">
                                            @php
                                                $filename = public_path("$kepengurusan->foto");
                                                $jln = \File::extension($filename);
                                            @endphp
                                            @if($jln == "docx" || $jln == "doc")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->foto)) }}"><img src="../assets/assets/images/img_file/doc.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "xlsx" || $jln == "xls")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->foto)) }}"><img src="../assets/assets/images/img_file/xls.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "pdf")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->foto)) }}"><img src="../assets/assets/images/img_file/pdf.png" alt="" height="50px" weight="50px"></a>
                                            @elseif($jln == "ppt")
                                                <a href="{{ route('resource.download', array('file'=>$kepengurusan->foto)) }}"><img src="../assets/assets/images/img_file/ppt.png" alt="" height="50px" weight="50px"></a>
                                            @else

                                                <div class="portfolioContainer">
                                                    <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                        <a href="{{ asset($kepengurusan->foto)}}" class="image-popup" title="{{$kepengurusan->foto}}">
                                                            <img alt="" src="{{ asset($kepengurusan->foto)}}"  style="width:50px" height="50px"/>
                                                        </a>
                                                    </div> 
                                                </div>   
                                            @endif
                                        </td>
                                        <td>{{$kepengurusan->jabatan->posisi or 'kosong'}}</td>
                                        @if($kepengurusan->status_kepengurusan != 0)
                                        <td>{{'Aktif'}}</td>
                                        @else
                                        <td>{{' Tidak Aktif'}}</td>
                                        @endif
                                        <td>
                                            {{ Form::open(array('route' => array('kepengurusan.destroy', $kepengurusan->id), 'method' => 'delete'))}}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('kepengurusan.edit', $kepengurusan->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('kepengurusan.show', $kepengurusan->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaKepengurusan->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <div class="row">
                            <div class="col-sm-12">                                   
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>

                                    <li>
                                        <a href="#">User</a>
                                    </li>
                                    
                                    <li class="active">
                                        Role
                                    </li>
                                </ol>
                            </div>
                        </div>

                        <center><h4 class="m-t-0"><b>TABEL ROLE</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">
                            <!-- Alert -->
                            @yield('alert')

                            <thead>
                                <tr>
                                    <th width="10%" scope="row">No</th>
                                    <th width="30%" scope="row">ID Role</th>
                                    <th width="30%" scope="row">Nama Role</th>
                                    <th width="30%" scope="row">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaRole as $role)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$role->id or 'kosong'}}</td>
                                        <td>{{$role->nama or 'kosong'}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{route('role.edit', $role->id)}}">Ubah</a></li>
                                                    <li><a href="{{route('role.show', $role->id)}}">Lihat</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                  <h3 class="panel-title">DETAIL ROLE</h3>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                             <td>Name Role</td>
                                             <td>:</td>
                                             <td>{{$role->nama}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Dibuat Tanggal</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date($role->created_at)}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Diperbarui Tanggal</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date($role->updated_at)}}</td>
                                        </tr>
                                   </table> 
                                   <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                             <a href="{{route('role.index')}}">
                                                 <button class="btn btn-warning waves-effect waves-light">
                                                    <i class="fa fa-arrow-left"></i><span> Kembali</span>
                                                 </button></a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                             
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
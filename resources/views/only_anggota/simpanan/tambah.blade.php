@extends('master_anggota')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('simpanan_anggota.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i></h3></a>
						</div>
						<div class="panel-body">
						@if (!$pembayaran)
								<div class="alert alert-danger">
									<p>
										Anda belum pernah membayar simpanan wajib
									</p>
								</div>
							
							@endif
						
							@php							
								$tgl_wajib_last= $pembayaran->tanggal_pembayaran;
								$format_tgl= \Carbon\Carbon::parse($tgl_wajib_last);
								$tahun_wajib_last= $format_tgl->format('Y');
								
								$tgl_now= \Carbon\Carbon::now();
								$tahun_now= $tgl_now->format('Y');
							@endphp
								@if ($tahun_wajib_last != $tahun_now)
						
								<div class="alert alert-danger">
									<p>Tahun ini belum bayar simpanan wajib</p>
								</div>
								@endif
								<center><h4 class="m-t-0"><b>TAMBAH SIMPANAN</b></h4></center>
								<br>
							{!! Form::open(['route'=>'simpanan_anggota.store','class'=>'form-horizontal','files'=>true]) !!}
							@if(Session::has('informasi'))
								<div class="alert alert-danger">
									<strong>Informasi :</strong>
									{{Session::get('informasi')}}
								</div>
								@endif
							@if(count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif  

							@include('only_anggota.simpanan.form')
							  <div class="form-group">
									<div class="col-md-12">
											<div class="form-group">
													<div class="col-md-12">
														 <div class="col-md-offset-9 text-right">
																<button type="reset" class="btn btn-danger waves-effect waves-light">
																		<span class="btn-label"><i class="fa fa-repeat"></i>
																		</span>Ulangi
																</button>
															<button type="submit" class="btn btn-default waves-effect waves-light">
																   <i class="fa fa-save m-r-5"></i>
																   <span>Simpan</span>
														   </button>
			
														 </div>
													</div>
											   </div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
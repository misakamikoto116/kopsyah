@extends('master_anggota')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                            <a href="{{route('simpanan_anggota.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Anggota</a>
                                            </li>
                                            <li class="active">
                                                Simpanan
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <center><h4 class="m-t-0"><b>TABEL SIMPANAN</b></h4></center>
                        
                                <table id="datatable" class="table table-striped table-bordered">

                                @if(Session::has('informasi'))
                                    <div class="alert alert-info">
                                        <strong>Informasi :</strong>
                                        {{Session::get('informasi')}}
                                    </div>
                                    @endif
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Jenis Simpanan</th>
                                    <th width="10%" scope="row">Jumlah Pembayaran</th>
                                    <th width="20%" scope="row">Tanggal Pembayaran</th>
                                    <th width="10%" scope="row">Tempo</th>
                                    <th width="10%" scope="row">Status</th>
                                    <th width="20%" scope="row">Bukti Pembayaran</th>
                                </tr>
                                
                            </thead>

                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                 @foreach($semuaPembayaran as $pembayaran)
                                    <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$pembayaran->jenis_pembayaran->nama or 'kosong'}}</td>
                                            <td>{{rupiah($pembayaran->jumlah)}}</td>
                                            <td>{{DateIndo($pembayaran->tanggal_pembayaran)}}</td>
                                            <td>{{DateIndo($pembayaran->tanggal_tempo)}}</td>
                                            @if($pembayaran->status == 1)
                                            <td>{{"Sudah tervalidasi"}}</td>
                                            @else
                                            <td>{{"Belum tervalidasi"}}</td>
                                            @endif
                                           
                                            <td>
                                                <div class="portfolioContainer">
                                                    <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                            <a href="{{ asset($pembayaran->file_resi)}}" class="image-popup" title="Resi pembayaran {{$pembayaran->file_resi}}">
                                                            <button class="btn btn-warning waves-effect waves-light"><i class="fa fa-eye m-r-5"></i>
                                                            <span>Resi</span></button></a>
                                                    </div> 
                                                </div>     
                                            </td>
                                    </tr>
                               @endforeach
                               
                            </tbody>
                            <tr>
                                <td></td>
                                <td>Jumlah Total</td>
                                <td colspan = "5">{{rupiah($total)}}</td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop

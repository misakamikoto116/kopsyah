@extends('master_anggota')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Investor</a>
                                            </li>
                                            <li class="active">
                                                Dokumen
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL DOKUMEN</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">

                            @if(Session::has('informasi'))
                                <div class="alert alert-info">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <thead>
                                <tr>
                                        <th width="5%" scope="row">No</th>
                                        <th width="20%" scope="row">Nama</th>
                                        <th width="20%" scope="row">Deskripsi</th>
                                        <th width="40%" scope="row">File</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($semuaDokumen as $dokumen)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$dokumen->judul or 'kosong'}}</td>
                                        <td>{{$dokumen->deskripsi or 'kosong'}}</td>
                                        <td>
                                            <a href="{{ route('anggota.resource.download', array('file'=>$dokumen->file_dokumen)) }}">{{$dokumen->file_dokumen}}</a>
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
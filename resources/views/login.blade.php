<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('page_title', 'Login') | 212 Mart Samarinda</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{asset('component/bootstrap/dist/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/login.css')}}">
	
	<link rel="stylesheet" type="text/css" href="{{asset('component/font-awesome/css/font-awesome.min.css')}}">
	 <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('component/bootstrap/dist/css/skins/_all-skins.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('component/bootstrap/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

</head>
<body style="background:#F8F8F8">
	
	<header>
		<nav class="navbar navbar-light" style="box-shadow:0px 0px 1px #ccc; background: #fff; padding: 25px 0px">
			<img src="{{asset('assets/assets/images/utama/logo1.png')}}" class="mx-auto d-block img-responsive" width="304" height="236" alt="logo">
		</nav>
	</header>

	<section>
		<div class="row" style="margin:0.2rem">
			<div class="col-md-4 offset-md-4 col-sm-12 text-center">
				<h4>LOGIN</h4>
			</div>
				@if(Session::has('informasi'))
					<div class="col-md-4 offset-md-4 col-sm-12 alert alert-info">
						<strong>Informasi :</strong>
						{{Session::get('informasi')}}
					</div>
				@endif
				@if (count($errors) > 0)
				<div class="col-md-4 offset-md-4 col-sm-12 alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			<div class="col-md-4 offset-md-4 col-sm-12">
				{!! Form::open(['route'=>'process_login','class'=>'form-horizontal','file'=>'true']) !!}
				<div class="input-group margin-bottom-sm">
					<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
					{!! Form::text('username',null,['class'=>'form-control','placeholder'=>"Username"])!!}
				</div>
			</div>
			<div class="col-md-4 offset-md-4 col-sm-12" style="margin-top:1rem">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
					{!! Form::password('password',['class'=>'form-control','placeholder'=>"Password"])!!}
				</div>
			</div>
			<div class="col-md-4 offset-md-4 col-sm-12 text-right" style="margin-top:1rem">
				<button type="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Reset</button>
				<button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>
			</div>
			<div class="col-md-4 offset-md-4 text-center" style="margin-top:1rem">
				<p>Tidak Memiliki Akun?
					<a href="{{route('registration')}}">Registrasi</a>
				</p>
			</div>
			{!! Form::close()!!}
		</div>
	</section>

</body>
	<script type="text/javascript" src="{{asset('component/jquery/dist/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('component/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			$('[data-toggle="tooltip"]').tooltip()
		});
	</script>
</html>



		
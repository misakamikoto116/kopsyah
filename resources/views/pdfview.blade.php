<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="{{asset('assets/assets/css/pdfview.css')}}" media="print" rel="stylesheet" type="text/css">
	<title>Laporan Usaha</title>
	<style media="all">

    *{
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif
    }

    @page {
      /* untuk menghilangkan header dan footer saat ngeprint */
      /* margin: 0.5cm auto;  */
      size: A4;
    }
    @page:top{
      margin:0px auto;
    }
		.table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 1rem;
      background-color: transparent;
    }

    .table th,
    .table td {
      padding: 0.75rem;
      vertical-align: top;
      border-top: 1px solid #dee2e6;
    }

    .table thead th {
      vertical-align: bottom;
      border-bottom: 2px solid #dee2e6;
    }

    .table tbody + tbody {
      border-top: 2px solid #dee2e6;
    }

    .table .table {
      background-color: #fff;
    }

    .table-sm th,
    .table-sm td {
      padding: 0.3rem;
    }

    .table-bordered {
      border: 1px solid #dee2e6;
    }

    .table-bordered th,
    .table-bordered td {
      border: 1px solid #dee2e6;
    }

    .table-bordered thead th,
    .table-bordered thead td {
      border-bottom-width: 2px;
    }

    .table-striped tbody tr:nth-of-type(odd) {
      background-color: rgba(0, 0, 0, 0.05);
    }

    .table-hover tbody tr:hover {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-primary,
    .table-primary > th,
    .table-primary > td {
      background-color: #b8daff;
    }

    .table-hover .table-primary:hover {
      background-color: #9fcdff;
    }

    .table-hover .table-primary:hover > td,
    .table-hover .table-primary:hover > th {
      background-color: #9fcdff;
    }

    .table-secondary,
    .table-secondary > th,
    .table-secondary > td {
      background-color: #d6d8db;
    }

    .table-hover .table-secondary:hover {
      background-color: #c8cbcf;
    }

    .table-hover .table-secondary:hover > td,
    .table-hover .table-secondary:hover > th {
      background-color: #c8cbcf;
    }

    .table-success,
    .table-success > th,
    .table-success > td {
      background-color: #c3e6cb;
    }

    .table-hover .table-success:hover {
      background-color: #b1dfbb;
    }

    .table-hover .table-success:hover > td,
    .table-hover .table-success:hover > th {
      background-color: #b1dfbb;
    }

    .table-info,
    .table-info > th,
    .table-info > td {
      background-color: #bee5eb;
    }

    .table-hover .table-info:hover {
      background-color: #abdde5;
    }

    .table-hover .table-info:hover > td,
    .table-hover .table-info:hover > th {
      background-color: #abdde5;
    }

    .table-warning,
    .table-warning > th,
    .table-warning > td {
      background-color: #ffeeba;
    }

    .table-hover .table-warning:hover {
      background-color: #ffe8a1;
    }

    .table-hover .table-warning:hover > td,
    .table-hover .table-warning:hover > th {
      background-color: #ffe8a1;
    }

    .table-danger,
    .table-danger > th,
    .table-danger > td {
      background-color: #f5c6cb;
    }

    .table-hover .table-danger:hover {
      background-color: #f1b0b7;
    }

    .table-hover .table-danger:hover > td,
    .table-hover .table-danger:hover > th {
      background-color: #f1b0b7;
    }

    .table-light,
    .table-light > th,
    .table-light > td {
      background-color: #fdfdfe;
    }

    .table-hover .table-light:hover {
      background-color: #ececf6;
    }

    .table-hover .table-light:hover > td,
    .table-hover .table-light:hover > th {
      background-color: #ececf6;
    }

    .table-dark,
    .table-dark > th,
    .table-dark > td {
      background-color: #c6c8ca;
    }

    .table-hover .table-dark:hover {
      background-color: #b9bbbe;
    }

    .table-hover .table-dark:hover > td,
    .table-hover .table-dark:hover > th {
      background-color: #b9bbbe;
    }

    .table-active,
    .table-active > th,
    .table-active > td {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-hover .table-active:hover {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-hover .table-active:hover > td,
    .table-hover .table-active:hover > th {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table .thead-dark th {
      color: #fff;
      background-color: #212529;
      border-color: #32383e;
    }

    .table .thead-light th {
      color: #495057;
      background-color: #e9ecef;
      border-color: #dee2e6;
    }

		.container{
			margin-right:100px;
			margin-left:100px;
		}
		body{
			box-sizing: border-box;
		}
		h3{
			text-transform: uppercase;
      text-align: center;
      font-size: 20px;
      letter-spacing: 1px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif
		}

		table{
			width: 100%;
			max-width: 100%;
			margin-bottom: 1rem;
			background-color: transparent;
			margin: 0 auto;
		}

		.table{
			border-collapse: collapse;
		}

		.table-bordered{
			border: 1px solid #dee2e6;
		}

		.table-bordered thead td, .table-bordered thead th {
			border-bottom-width: 2px;
		}

		.table thead th {
			vertical-align: bottom;
			border-bottom: 2px solid #dee2e6;
		}
		.table-bordered td, .table-bordered th {
			border: 1px solid #dee2e6;
		}

		.table td, .table th {
    		padding: .75rem;
		}

		th {
			text-align: inherit;
    }
    
    .print{
      cursor: pointer;
      border:none;
      background: #CF000F;
      padding: 5px;
      border-radius: 3px;
      color: white;
      font-size: 17px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif
    }

    .download{
      text-decoration: none;
      background: #34495E;
      padding: 5px;
      border-radius: 3px;
      color: white;
      font-size: 17px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif
    }
	</style>
</head>
<body>
    <br/>
    <div style="display:inline">
      <button class="print" id="noprint" onclick="PagePrint()">Print</button>
      <a class="download" href="{{ route('download',['jenis'=>'pdf','id'=>$id]) }}" id="noprint">Download PDF</a>
    </div>
	
		<h3 class="text-center text-uppercase"><u>laporan unit usaha</u></h3>
		<br>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="1%" scope="col" style="text-align:center">No</th>
					<th width="10%" scope="col">Nomor Anggota</th>
					<th width="10%" scope="col">Nama Investor</th>
					<th width="10%" scope="col">Tanggal Investasi</th>
					<th width="10%" scope="col">Jumlah Investasi</th>
					<th class="unit" width="10%"scope="col">Unit Usaha</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($items as $key => $item)
					<tr>
						<td style="text-align:center" scope="col">{{ ++$key }}</td>
						<td>{{ $item->anggota->nomor_anggota }}</td>
						<td>{{ $item->anggota->nama }}</td>
						<td>{{ DateIndo($item->tanggal_pembayaran) }}</td>
						<td>{{ rupiah($item->jumlah) }}</td>
						<td>{{ $item->jenis_investasi->nama }}</td>
					</tr>
				@endforeach
		</table>
</body>
  <script type="text/javascript" src="{{asset('component/jquery/dist/jquery.min.js')}}"></script>
  <script>
    function PagePrint() 
    {
      window.print();
    }
  </script>
</html>


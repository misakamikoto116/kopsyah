@extends('master')
@section('container')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-15">
                            <a href="{{route('/')}}" > <button class="btn btn-default waves-effect waves-light">
                                <span>Beranda</span></button>
                            </a>
                    </div>

                    <h4 class="page-title">Dashboard</h4>
                    <p class="text-muted page-title-alt">Assalamu’alaikum warahmatullahi wabarakatuh</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 col-lg-5">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-info pull-left">
                            <i class="md  icon-user-following   text-info"></i>
                        </div>
                        <div class="text-right">
                            <ul class="list-inline m-t-15">
                                <li>
                                    <h5 class="m-b-0">{{$jml_biasa}}</h5>
                                    <p class="text-muted">Anggota Biasa</p>
                                </li>
                                <li>
                                    <h5 class="m-b-0">{{$jml_luar_biasa}}</h5>
                                    <p class="text-muted">Anggota Luar Biasa</p>
                                </li>
                            </ul>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-4">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-pink pull-left">
                            <i class="md fa fa-handshake-o  text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_investor}}</b></h3>
                            <p class="text-muted">Jumlah Investor</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md fa fa-vcard-o text-purple"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_pengurus}}</b></h3>
                            <p class="text-muted">Jumlah Pengurus</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <!-- <div class="col-md-3 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md fa fa-vcard-o text-purple"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter"></b></h3>
                            <p class="text-muted">Omset Hari Ini: 
                                <span class="omzet_now" id="omzet_now" ></span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> -->
                <!-- <div class="col-md-3 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md fa fa-vcard-o text-purple"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter"></b></h3>
                            <p class="text-muted">Omset Kemarin 
                                <span class="omzet_y"></span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> -->

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="bar-widget">
                             <div class="table-box">
                                <div class="table-detail">
                                    <div class="iconbox bg-danger">
                                    <i class="fa fa-money"></i>
                                    </div>
                                </div>

                                <div class="table-detail">
                                    <h4 class="m-t-0 m-b-5"><b><span class="omzet_now" id="omzet_now"></span></b></h4>
                                    <p class="text-muted m-b-0 m-t-0">Omset Hari Ini</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="bar-widget">
                             <div class="table-box">
                                <div class="table-detail">
                                    <div class="iconbox bg-custom">
                                    <i class="fa fa-money"></i>
                                    </div>
                                </div>

                                <div class="table-detail">
                                    <h4 class="m-t-0 m-b-5"><b><span class="omzet_y"></span></b></h4>
                                    <p class="text-muted m-b-0 m-t-0">Omset Kemarin</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box" hidden>
                        <h4 class="m-t-0 header-title"><b>GRAFIK PENJUALAN MINGGUAN</b></h4>
                        <p class="text-muted m-b-15 font-13 text-center"><b>Omzet Mingguan</b>
                        </p>

                        <canvas id="lineChart" height="300"></canvas>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title col-lg-6"><b>GRAFIK PENJUALAN MINGGUAN </b></h4>
                        <div class="pull-right">
                            {!! Form::select('list_url_api', $list_url_api, null,['class' => 'form-control','onchange' => 'changeToko(this.value)']) !!}
                        </div>
                        <canvas id="grafik-omzet-weekly" height="195"  ></canvas>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Grafik Pembelian Terbanyak  </b></h4>

                        <canvas id="pie_belanja" height="250"></canvas>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box" hidden>
                        <h4 class="m-t-0 header-title"><b>Pie Chart</b></h4>
                        <p class="text-muted m-b-15 font-13">Pie and doughnut charts are probably the most commonly used chart there are. They are
                            divided into segments, the arc of each segment shows the proportional value of each piece of data.
                        </p>

                        <canvas id="pie" height="260"></canvas>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card-box" hidden>
                        <h4 class="m-t-0 header-title"><b>Donut Chart</b></h4>
                        <p class="text-muted m-b-15 font-13">Pie and doughnut charts are probably the most commonly used chart there are. They are
                            divided into segments, the arc of each segment shows the proportional value of each piece of data.
                        </p>

                        <canvas id="doughnut" height="260"></canvas>
                    </div>
                </div>
            <div class="row">

                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="widget-chart text-center">
                           
                            <h5 class="text-muted m-t-10">Total Simpanan</h5>
                            <h3 class="font-600">{{rupiah($total)}}</h3>
                            <ul class="list-inline m-t-15">
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Pokok</h6>
                                    <h5 class="m-b-0">{{rupiah($total_pokok)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Wajib</h6>
                                    <h5 class="m-b-0">{{rupiah($total_wajib)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Sukarela</h6>
                                    <h5 class="m-b-0">{{rupiah($total_sukarela)}}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="widget-chart text-center">
                            <h5 class="text-muted m-t-10">Total Investasi</h5>
                            <h3 class="font-600"> {{rupiah($investasi)}}</h3>
                            <ul class="list-inline m-t-15">
                                    @foreach ($jenis_investasi as $inv)
                                    <li>
                                    <h6 class="text-muted m-t-10">{{$inv->nama}}</h6>
                                    @php
                                     $jml= App\Model\InvestasiModel::where('status','=','1')
                                                    ->where('jenis_investasi_id','=',$inv->id)    
                                                    ->sum('jumlah');
                                    @endphp
                                    <h5 class="m-b-0">{{rupiah($jml)}}</h5>
                                    </li>
                                    @endforeach
                            </ul>
                        </div>
                    </div>

                </div>


            </div>


            </div>

            <!-- end row -->

            
        </div> <!-- container -->


    </div> <!-- content -->

   

</div>



<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->

    @section('grafik')

    <!-- Chart JS -->
    {{--  atur tampilan chart  --}}
	<script src="{{ asset('assets/assets/plugins/chart.js/chart.min.js')}} "></script>
	<script src="{{ asset('assets/assets/pages/jquery.chartjs.init.js')}} "></script>
	<!-- End Chart JS -->
    <script>
        function getAjaxLine(data) {
            var tanggal = [];
            var omzet = [];

            for(var i in data) {
                tanggal.push(data[i].tanggal);
                omzet.push(data[i].omzet);
            }

            var chartdata = {
                labels: tanggal,
                datasets : [
                    {
                        label: 'Omzet Mingguan',
                        //backgroundColor: ["navy", "red", "orange", "yellow", "green", "blue", "cyan"],
                        //borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        //hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: omzet
                    }
                ]
            };

            var ctx = $("#grafik-omzet-weekly");
            var options = {
                responsive: true,
                fill: false,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            callback: function(value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);

                                // Convert the array to a string and format the output
                                value = value.join('.');
                                return 'Rp. ' + value;
                            },
                            // max: 100,
                            min: 0
                        }
                    }]
                },
                title: {
                    display: true,
                    text: name
                },
                tooltips: {
                    mode: 'label',
                    label: 'Jml Pembelian',
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                }

            };
            var lineGraph = new Chart(ctx, {
                type: 'line',
                data: chartdata,
                options: options
            });
        }

        function getAjaxAnggota(data) {
            var nama = [];
            var jml = [];

            for(var i in data) {
                nama.push(data[i].nama);
                jml.push(data[i].jml);
            }

            var chartdata = {
                labels: nama,
                datasets : [
                    {
                        label: 'Top 20 Anggota Belanja Terbanyak Bulan Ini',
                        //atur warna untuk grafik
                        backgroundColor: [
                        "rgb(5,75,65)","rgb(10,80,75)","rgb(15,95,85)","rgb(20,100,95)","rgb(25,105,105)",
                        "rgb(30,110,115)","rgb(35,115,125)","rgb(40,120,135)","rgb(45,125,145)", "rgb(50,130,155)",
                        "rgb(55,135,165)","rgb(60,140,175)","rgb(65,145,185)","rgb(70,150,195)","rgb(75,155,205)",
                        "rgb(80,160,215)","rgb(85,165,225)","rgb(90,170,235)","rgb(95,175,245)","rgb(100,180,255)"],
                        //borderColor: 'rgba(200, 200, 200, 0.75)',
                        //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        //hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: jml
                    }
                ]
            };

            var ctx = $("#pie_belanja");
            var options = {
                responsive: true,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            callback: function(value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);

                                // Convert the array to a string and format the output
                                value = value.join('.');
                                return 'Rp. ' + value;
                            },
                            // max: 100,
                            min: 0
                        }
                    }]
                },
                title: {
                    display: true,
                    text: name
                },
                tooltips: {
                    mode: 'label',
                    label: 'Jml Pembelian',
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                }

            };
            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata,
                options: options
            });
        }

        function changeToko(val) {
            $.ajax({
                url: '{{ route('get.changeToko') }}',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    url: val
                },
            })
            .done(function(response) {
                getAjaxAnggota(response.anggota);
                getAjaxLine(response.line);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        }

        $(document).ready(function(){
            $.ajax({
                url: "{{ route('get.ajaxAnggota') }}",
                method: "GET",
                success: function(data) {
                    console.log('data');
                    getAjaxAnggota(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
            $.ajax({
                url: "{{ route('get.ajaxLineOmzet')}}",
                method: "GET",
                success: function(data) {
                    console.log(data);
                    getAjaxLine(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });

        });
    </script>

    {{-- ini gak dipakai --}}
    <script type="text/javascript">
		$(document).ready(function () {
            new Chart(document.getElementById("grafik-omzet"), {
                type: 'line',
                data: {
                    labels: ["7 Mei 2018","8 Mei 2018","9 Mei 2018","10 Mei 2018","11 Mei 2018","12 Mei 2018","13 Mei 2018"],
                    datasets: [{
                        data: [3822600,2725100,4583700,5859000,3293300,5258100,35770200],
                        label: "omzet",
                        borderColor: "#3e95cd",
                        fill: false
                    }
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Omzet MIngguan'
                    }
                }
            });
        });
    </script>
    @endsection


@stop

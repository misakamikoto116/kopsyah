{{--  <div class="form-group">
     <label class="col-md-2 control-label">Judul</label>
     <div class="col-md-10">
        {!! Form::text('judul',old('judul'),['class'=>'form-control','placeholder'=>"Judul"]) !!}
     </div>
</div>
<div class="form-group">
     <label class="col-md-2 control-label">Isi</label>
     <div class="col-md-10">
        {!! Form::textarea('isi',old('isi'),['class'=>'form-control','placeholder'=>"Isi",'id'=> 'elm1']) !!}
     </div>
</div>  --}}

    {{--  <body>
        <div class="row-fluid">
            <div class="span6">
                <!-- <form action="index.php" method="post"> -->
                <div class="form-group">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th width="200px">Nama Barang</th>
                                <th width="200px">Merk</th>
                                <th width="100px">Jumlah</th>
                                <th width="100px">Periode</th>
                                <th width="100px">ID Anggota (harus angka)</th>
                                <th width="80px"></th>
                            </tr>
                        </thead>
                        <!--elemet sebagai target append-->
                        <tbody id="itemlist">
                            <tr>
                                <td><input name="jenis_input[1]" class="form-control" /></td>
                                <td><input name="merk_input[1]" class="form-control" /></td>
                                <td><input name="jumlah_input[1]" class="form-control" /></td>
                                <!-- <td><input name="periode_input[1]" class="form-control" /></td> -->
                                <td><select name="periode_input[1]" class="form-control">
                                    <option value="mingguan">Mingguan</option>
                                    <option value="bulanan">Bulanan</option>
                                    </select>
                                </td>
                                <td><input name="anggota_input[1]" class="form-control" /></td>
                            </tr>
                        </tbody>
                   
                    </table>
                <!-- </form> -->
                </div>
            </div>
          
        </div>

        <script>
            var i = 0;
            function additem() {
                var itemlist = document.getElementById('itemlist');
                
        //                membuat element
                var row = document.createElement('tr');
                var jenis = document.createElement('td');
                var merk = document.createElement('td');
                var jumlah = document.createElement('td');
                var periode = document.createElement('td');
                var anggota = document.createElement('td');
                var aksi = document.createElement('td');
        
        //                meng append element
                itemlist.appendChild(row);
                row.appendChild(jenis);
                row.appendChild(merk);
                row.appendChild(jumlah);
                row.appendChild(periode);
                row.appendChild(anggota);

                row.appendChild(aksi);
        
        //                membuat element input
                var jenis_input = document.createElement('input');
                jenis_input.setAttribute('name', 'jenis_input[' + i + ']');
                jenis_input.setAttribute('class', 'form-control');
        
                var merk_input = document.createElement('input');
                merk_input.setAttribute('name', 'merk_input[' + i + ']');
                merk_input.setAttribute('class', 'form-control');
        
                var jumlah_input = document.createElement('input');
                jumlah_input.setAttribute('name', 'jumlah_input[' + i + ']');
                jumlah_input.setAttribute('class', 'form-control');

                var periode_input = document.createElement('input');
                periode_input.setAttribute('name', 'periode_input[' + i + ']');
                periode_input.setAttribute('class', 'form-control');

                var anggota_input = document.createElement('input');
                anggota_input.setAttribute('name', 'anggota_input[' + i + ']');
                anggota_input.setAttribute('class', 'form-control');
        
                var hapus = document.createElement('span');
        
                jenis.appendChild(jenis_input);
                merk.appendChild(merk_input);
                jumlah.appendChild(jumlah_input);
                periode.appendChild(periode_input);
                anggota.appendChild(anggota_input);


                aksi.appendChild(hapus);
        
                hapus.innerHTML = '<button class="btn btn-small btn-default"><i class="glyphicon glyphicon-trash"></i></button>';
        //                Aksi Delete
                hapus.onclick = function () {
                    row.parentNode.removeChild(row);
                };
        
                i++;
            }
        </script>  --}}


        <div class="form-group">
            <table class="table table-condensed"  id="dynamic_field">
                <thead>
                    <tr>
                        <th width="200px">Nama Barang</th>
                        <th width="200px">Merek</th>
                        <th width="50px">Jumlah</th>
                        <th width="100px">Periode</th>
                        <th width="100px">Anggota</th>
                        <th width="80px"></th>
                        </tr>
                </thead>
                <!--elemet sebagai target append-->
                <tbody id="itemlist">
                    <tr>
                        <td> {!! Form::text('nama_barang[]',null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::text('merek[]',null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::text('jumlah[]',null,['class'=>'form-control', 'required']) !!}</td>
                        <td> {!! Form::select('periode[]',['1'=>'Mingguan','2'=>'Bulanan'],null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::select('anggota_id[]', $anggota_pluck, null, ['placeholder' => '-- Pilih --','class'=>'form-control select2', 'required']) !!} </td>    
                        <td> {!! Form::button('+', ['class' => 'btn btn-success', 'id' => 'add']) !!}</td>
                    </tr>
                </tbody>
           
            </table>
          
        </div>
            
  @section('js')        
  <script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           
           $('#dynamic_field')
           .append('<tr id="row'+i+'" class="dynamic-added">'+
                '<td> {!! Form::text('nama_barang[]',null,['class'=>'form-control', 'required'])!!}</td>'+
                '<td> {!! Form::text('merek[]',null,['class'=>'form-control rupiah', 'required'])!!}</td>'+
                '<td> {!! Form::text('jumlah[]',null,['class'=>'form-control', 'required']) !!}</td>'+
                '<td> {!! Form::select('periode[]',['1'=>'Mingguan','2'=>'Bulanan'],null,['class'=>'form-control', 'required'])!!}</td>'+
                '<td> {!! Form::select('anggota_id[]', $anggota_pluck, null, ['placeholder' => '-- Pilih --','class'=>'form-control select2', 'required']) !!}</td>'+ 
                '<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class=" fa fa-trash"></i></button></td>'+
                '</tr>');
          
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                    }
                }  
           });  
      });  


    });  
</script>

@endsection
@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('belanja.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i></h3></a>
						</div>
						<div class="panel-body">
							{!! Form::open(['route'=>'belanja.store','class'=>'form-horizontal','files'=>true]) !!}
								<div class= "container">
								<!-- Alert -->
									@yield('alert')
								</div>
								<center><h4 class="m-t-0"><b>TAMBAH BELANJA BULANAN</b></h4></center>
									<br>
    							@include('belanja.form')
								<div class="form-group">
										<div class="col-md-12">
											<div class="form-group">
												<div class="col-md-12">
													<div class="col-md-offset-9 text-right">
														{{--  <button class="btn btn-small btn-default" onclick="additem(); return false"><i class="glyphicon glyphicon-plus"></i></button>  --}}
														{{-- <button onclick="reset" class="btn btn-danger waves-effect waves-light">
															<span class="btn-label"><i class="fa fa-repeat"></i>
															</span>Ulangi
														</button> --}}
														<button type="submit" class="btn btn-default waves-effect waves-light">
															<i class="fa fa-save m-r-5"></i>
															<span>Simpan</span>
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
</div><!-- End Right content here -->


@section('select2')
<script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>      
<!--File-->
<script src="{{asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/pages/jquery.form-advanced.init.js')}}"></script>
@endsection

@stop
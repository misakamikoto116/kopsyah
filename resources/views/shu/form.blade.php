<div class="form-group">
     <label class="col-md-2 control-label">Nama</label>
     <div class="col-md-10">
        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>"Nama",'required']) !!}
     </div>
</div>
<div class="form-group">
  <label class="col-md-2 control-label">Deskripsi</label>
  <div class="col-md-10">
     {!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi",'required']) !!}
  </div>
</div>
<div class="form-group">
        <label class="col-md-2 control-label">Persentase</label>
        <div class="col-md-10">
           {!! Form::text('persentase',null,['class'=>'form-control','placeholder'=>"Persentase",'required']) !!}
        </div>
</div>
  
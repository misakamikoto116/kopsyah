@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('shu.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="active">
                                                Sisa Hasil Usaha
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL SISA HASIL USAHA</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">
                          
                            <!-- Alert -->
							@yield('alert')
							

                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Nama</th>
                                    <th width="40%" scope="row">Deskripsi</th>
                                    <th width="20%" scope="row">Persentase</th>
                                    <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($shu as $shus)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$shus->nama or 'kosong'}}</td>
                                        <td>{{$shus->deskripsi or 'kosong'}}</td>
                                        <td>{{$shus->persentase or 'kosong'}} %</td>
                                        <td>
                                            {{ Form::open(array('route' => array('shu.destroy', $shus->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('shu.edit', $shus->id)}}">Ubah</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div><!-- End Right content here -->
@stop
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('page_title', 'Home') | 212 Mart Samarinda</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="{{asset('assets/assets/images/utama/logo1.png')}}" rel="icon">
	<link href="{{asset('assets/assets/plugins/morris/morris.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/pages.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	<!--venobox lightbox-->
	<link rel="stylesheet" href="{{asset('assets/assets/plugins/magnific-popup/css/magnific-popup.css')}}"/>

	<!-- Datatables -->
	<link href="{{asset('assets/assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/dataTables.colVis.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/fixedColumns.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">

	<link href="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/morris/morris.css')}}" rel="stylesheet">

	<script src="{{asset('assets/assets/js/modernizr.min.js')}}"></script>
</head>
<body class="fixed-left">

	<!-- Begin page -->
	<div id="wrapper">

		<!-- Top Bar Start -->
		<div class="topbar">

			<!-- LOGO -->
			<div class="topbar-left">
				<div class="text-center">
					<a href="index.html" class="logo">
						<i class="icon-c-logo"> <img src="{{asset('assets/assets/images/utama/logo1.png')}}" width="100%" height="40"/> </i>
						<span><img src="{{asset('assets/assets/images/utama/logo1.png')}}" height="50" width="100%" /></span>
					</a>
				</div>
			</div>

			<!-- Button mobile view to collapse sidebar menu -->
			<div class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="">
						<div class="pull-left">
							<button class="button-menu-mobile open-left waves-effect waves-light">
								<i class="md md-menu"></i>
							</button>
							<span class="clearfix"></span>
						</div>
					@php
						$notif_verif_bayar= \Illuminate\Support\Facades\DB::table('notifications')
								->where('type','=','App\Notifications\VerifikasiPembayaran')
                    			->where('notifiable_id','=',Auth::id())
								->get();
					@endphp

						<ul class="nav navbar-nav navbar-right pull-right">
							<li class="dropdown top-menu-item-xs">
								<a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
									<i class="icon-bell"></i> <span class="badge badge-xs badge-danger">{{count($notif_verif_bayar)}}</span>
								</a>
								<ul class="dropdown-menu dropdown-menu-lg">
									<li class="notifi-title"><span class="label label-default pull-right">New {{count($notif_verif_bayar)}}</span>Notification</li>
									<li class="list-group slimscroll-noti notification-list">

											@if(count($notif_verif_bayar) != 0 )
											<a href="{{route('simpanan_anggota.index')}}" class="list-group-item">
											   <div class="media">
												  <div class="pull-left p-r-10">
													 <em class="fa  fa-money noti-primary"></em>
												  </div>
												  <div class="media-body">
													 <h5 class="media-heading">{{count($notif_verif_bayar)}} payment verified</h5>
													 <p class="m-0">
															<small>see notifications </small>
														</p>
												  </div>
											   </div>
											</a>
											@endif
											

									</li>
									<li>
										<a href="javascript:void(0);" class="list-group-item text-right">
											<small class="font-600">See all notifications</small>
										</a>
									</li>
								</ul>
							</li>
							<li class="hidden-xs">
								<a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
							</li>
							<li class="dropdown top-menu-item-xs">
								<a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{ asset($anggota->file_foto) }}" alt="user-img" class="img-circle"> </a>
								<ul class="dropdown-menu">
									<li><a href="{{route('logout')}}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<!-- Top Bar End -->

		<!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<li class="text-muted menu-title">Navigation</li>

                        <!-- dashboard -->
                            <li class="has_sub">
                                <a href="{{route('anggota')}}" class="waves-effect"><i class="ti-dashboard"></i> <span> Dashboard </span></a>
							</li>

						<!-- Anggota -->
							<li class="has_sub">
								<a href="{{route('profil_anggota.index')}}" class="waves-effect"><i class="fa fa-users"></i> <span> Profil </span></a>
							</li>

						<!-- Simpanan -->
							<li class="has_sub">
                                <a href="{{route('simpanan_anggota.index')}}" class="waves-effect"><i class="fa fa-wpforms"></i> <span> Simpanan </span></a>
							</li>
						
						<!-- Pengambilan -->
							<li class="has_sub">
                                <a href="#" class="waves-effect"><i class="md-autorenew"></i><span class="label label-warning pull-right">soon</span> <span> Pengambilan </span></a>
							</li>
							
						<!-- Belanja Bulanan -->
							<li class="has_sub">
								<a href="{{route('belanja_anggota.index')}}" class="waves-effect"><i class="fa fa-shopping-basket"></i><span> Belanja Bulanan </span></a>
							</li>

						<!-- SHU -->
							<li class="has_sub">
                                <a href="#" class="waves-effect"><i class="fa fa-money"></i><span class="label label-warning pull-right">soon</span> <span> SHU </span></a>
							</li>

                        <!-- Report -->
                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file"></i><span class="label label-warning pull-right">soon</span><span> Report </span></a>
							</li>
							<li class="has_sub">
								<a href="{{route('dokumen_anggota.index')}}" class="waves-effect"><i class="fa fa-file"></i><span> Dokumen </span></a>
							</li>
						
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->

		
		@yield('container')


		<footer class="footer text-right">
			<p class="fl_left">Copyright &copy; 2018 - 212 Mart Samarinda</p>
		</footer>
	</div>
	
	<script>
		var resizefunc = [];
	</script>
	@yield('js')
	<!-- jQuery  -->
	<script src="{{asset('assets/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/detect.js')}}"></script>
	<script src="{{asset('assets/assets/js/fastclick.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/switchery/js/switchery.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.slimscroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.blockUI.js')}}"></script>
	<script src="{{asset('assets/assets/js/waves.js')}}"></script>
	<script src="{{asset('assets/assets/js/wow.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.scrollTo.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/peity/jquery.peity.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/waypoints/lib/jquery.waypoints.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/counterup/jquery.counterup.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/morris/morris.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/raphael/raphael-min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/jquery-knob/jquery.knob.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.dashboard.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.core.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.app.js')}}"></script>
	<script src="{{asset('assets/assets/js/custom.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>

	<!-- jQuery  Datatables-->
	<script src="{{asset('assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/jszip.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/pdfmake.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/vfs_fonts.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.html5.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.print.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.colVis.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.fixedColumns.min.js')}}"></script>
	<script src="{{asset('assets/assets/pages/datatables.init.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.sweet-alert2.init.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script> 
	
	<!-- DatePicker  -->
	<script src="{{asset('assets/assets/plugins/moment/moment.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.form-pickers.init.js')}}"></script>
	<script src="{{ asset('assets/assets/plugins/chart.js/chart.min.js')}} "></script>
	<!--End DatePicker  -->  

	@yield('js')
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#datatable').dataTable();
			$('#datatable-keytable').DataTable({keys: true});
			$('#datatable-responsive').DataTable();
			$('#datatable-colvid').DataTable({
				"dom": 'C<"clear">lfrtip',
				"colVis": {
					"buttonText": "Change columns"
				}
			});
			$('#datatable-scroller').DataTable({
				ajax: "assets/plugins/datatables/json/scroller-demo.json",
				deferRender: true,
				scrollY: 380,
				scrollCollapse: true,
				scroller: true
			});
			var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
			var table = $('#datatable-fixed-col').DataTable({
				scrollY: "300px",
				scrollX: true,
				scrollCollapse: true,
				paging: false,
				fixedColumns: {
					leftColumns: 1,
					rightColumns: 1
				}
			});
		});
		TableManageButtons.init();

	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.counter').counterUp({
				delay: 100,
				time: 1200
			});

			$(".knob").knob();

		});
	</script>

	<!-- Tampil Resi Pembayaran  -->
	<script type="text/javascript" src="{{asset('assets/assets/plugins/isotope/js/isotope.pkgd.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js')}}"></script>
	<script type="text/javascript">
		$(window).load(function(){
			var $container = $('.portfolioContainer');
			$container.isotope({
				filter: '*',
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});

			$('.portfolioFilter a').click(function(){
				$('.portfolioFilter .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
				$container.isotope({
					filter: selector,
					animationOptions: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
				return false;
			}); 
		});
		$(document).ready(function() {
			$('.image-popup').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-fade',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				}
			});
		});
		$(document).ready(function(){
            $.ajax({
                url: "{{ route('get.ajaxAnggota') }}",
                method: "GET",
                success: function(data) {
                    console.log(data);
                    var nama = [];
                    var jml = [];

                    for(var i in data) {
                        nama.push(data[i].nama);
                        jml.push(data[i].jml);
                    }

                    var chartdata = {
                        labels: nama,
                        datasets : [
                            {
                                label: 'Top 5 Anggota Belanja Terbanyak Bulan Ini',
                                backgroundColor: ["navy", "red", "orange", "yellow", "green"],
                                //borderColor: 'rgba(200, 200, 200, 0.75)',
                                //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                //hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                data: jml
                            }
                        ]
                    };

                    var ctx = $("#pie_belanja");
                    var options = {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    callback: function(value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end
                                        value = value.toString();
                                        value = value.split(/(?=(?:...)*$)/);

                                        // Convert the array to a string and format the output
                                        value = value.join('.');
                                        return 'Rp. ' + value;
                                    },
                                    // max: 100,
                                    min: 0
                                }
                            }]
                        },
                        title: {
                            display: true,
                            text: name
                        },
                        tooltips: {
                            mode: 'label',
                            label: 'Jml Pembelian',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        }

                    };
                    var barGraph = new Chart(ctx, {
                        type: 'bar',
                        data: chartdata,
                        options: options
                    });
                },
                error: function(data) {
                    console.log(data);
                }
            });
            $.ajax({
                url: "{{ route('get.ajaxLineOmzet')}}",
                method: "GET",
                success: function(data) {
                    console.log(data);
                    var tanggal = [];
                    var omzet = [];

                    for(var i in data) {
                        tanggal.push(data[i].tanggal);
                        omzet.push(data[i].omzet);
                    }

                    var chartdata = {
                        labels: tanggal,
                        datasets : [
                            {
                                label: 'Omzet Mingguan',
                                //backgroundColor: ["navy", "red", "orange", "yellow", "green", "blue", "cyan"],
                                //borderColor: 'rgba(200, 200, 200, 0.75)',
                                //hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                //hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                data: omzet
                            }
                        ]
                    };

                    var ctx = $("#grafik-omzet-weekly");
                    var options = {
                        responsive: true,
                        fill: false,
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    callback: function(value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end
                                        value = value.toString();
                                        value = value.split(/(?=(?:...)*$)/);

                                        // Convert the array to a string and format the output
                                        value = value.join('.');
                                        return 'Rp. ' + value;
                                    },
                                    // max: 100,
                                    min: 0
                                }
                            }]
                        },
                        title: {
                            display: true,
                            text: name
                        },
                        tooltips: {
                            mode: 'label',
                            label: 'Jml Pembelian',
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        }

                    };
                    var lineGraph = new Chart(ctx, {
                        type: 'line',
                        data: chartdata,
                        options: options
                    });
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
	</script>
</body>
</html>
	
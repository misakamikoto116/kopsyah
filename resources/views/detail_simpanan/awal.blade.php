@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('pembayaran.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                    <a href="#">Anggota</a>
                                            </li>
                                            <li class="active">
                                                Pembayaran Simpanan
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL PEMBAYARAN SIMPANAN</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">
                        
                            @if(Session::has('informasi'))
                                <div class="alert alert-info">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Jenis Pembayaran</th>
                                    <th width="20%" scope="row">Jumlah Pembayaran</th>
                                    <th width="20%" scope="row">Tempo</th>
                                    <th width="20%" scope="row">Tanggal Pembayaran</th>
                                    <th width="20%" scope="row">Status</th>
                                    <th width="20%" scope="row">Bukti Pembayaran</th>
                                    <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($pembayaranAnda as $pembayaran)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$pembayaran->jenis_pembayaran->nama or 'kosong'}}</td>
                                        <td>{{rupiah($pembayaran->jumlah)}}</td>
                                        <td>{{DateIndo($pembayaran->tanggal_tempo)}}</td>
                                        <td>{{DateIndo($pembayaran->tanggal_pembayaran)}}</td>
                                      
                                        <td>
                                            @if($pembayaran->status == 1)
                                            Sudah Tervalidasi
                                            @else
                                            Belum Tervalidasi
                                            @endif
                                        </td>
                                        <td>
                                            <div class="portfolioContainer">
                                                <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                    <a href="{{ asset($pembayaran->file_resi)}}" class="image-popup" title="Resi pembayaran {{$pembayaran->file_resi}}">
                                                    <button class="btn btn-warning waves-effect waves-light"><i class="fa fa-eye m-r-5"></i>
                                                    <span>Resi</span></button></a>
                                                </div> 
                                            </div>     
                                        </td>
                                        <td>
                                            {{ Form::open(array('route' => array('pembayaran.destroy', $pembayaran->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('pembayaran.edit', $pembayaran->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('pembayaran.show', $pembayaran->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                            <tr>
                                <td></td>
                                <td>Jumlah Total</td>
                                <td colspan = "6">{{rupiah($total)}}</td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div><!-- End Right content here -->
@stop
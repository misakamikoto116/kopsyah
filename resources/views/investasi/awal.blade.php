@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                        <a href="{{route('investasi.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                </div>

                                <ol class="breadcrumb">
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>
                                    <li>
                                        <a href="#">Transaksi</a>
                                    </li>
                                    <li class="active">
                                        Investasi
                                    </li>
                                </ol>
                            </div>
                        </div>

                        <center><h4 class="m-t-0"><b>TABEL PEMBAYARAN INVESTASI</b></h4></center>

                        <!-- Form Pencarian -->
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nomor Anggota atau Nama"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        <table id="datatable" class="table table-striped table-bordered">

                      	<!-- Alert -->
						@yield('alert')
					
                            <thead>
                                <tr>
                                    <th width="10%" scope="row">No</th>
                                    <th width="20%" scope="row">Nomor Anggota</th>
                                    <th width="20%" scope="row">Nama Anggota</th>
                                    <th width="30%" scope="row">Jenis Investasi</th>
                                    <th width="30%" scope="row">Jumlah</th>
                                    <th width="30%" scope="row">Tanggal Pembayaran</th>
                                    <th width="30%" scope="row">Validasi</th>
                                    <th width="30%" scope="row">Bukti Pembayaran</th>
                                    <th width="30%" scope="row">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $no =1+($semuaInvestasi->perPage()*($semuaInvestasi->currentPage()-1));
                                @endphp
                                @foreach($semuaInvestasi as $investasi)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$investasi->anggota->nomor_anggota or 'kosong'}}</td>
                                        <td>{{$investasi->anggota->nama or 'kosong'}}</td>
                                        <td>{{$investasi->jenis_investasi->nama or 'kosong'}}</td>
                                        <td>{{rupiah($investasi->jumlah)}}</td>
                                        <td>{{DateIndo($investasi->tanggal_pembayaran )}}</td>
                                        <td>
                                                @if($investasi->status == 1)
                                                <a href="{{ url('admin/ubah/investasi/'.$investasi->id.'/0') }}" class="yes" data-user="{{ $investasi->id }}" desc="Yakin nonaktifkan?">
                                                    <input checked type="checkbox" class="sip" get-user="{{ $investasi->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                                </a>
                                                @else
                                                <a href="{{ url('admin/ubah/investasi/'.$investasi->id.'/1') }}" class="yes" data-user="{{ $investasi->id }}" desc="Yakin ingin memveifikasi?">
                                                    <input type="checkbox" class="sip" get-user="{{ $investasi->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                                </a>
                                                @endif
                                        </td>
                                        <td>
                                            <div class="portfolioContainer">
                                                <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                    <a href="{{ asset($investasi->foto_resi)}}" class="image-popup" title="Resi Pembayaran {{$investasi->anggota->nomor_anggota}}">
                                                    <button class="btn btn-warning waves-effect waves-light"><i class="fa fa-eye m-r-5"></i>
                                                    <span>Resi</span></button></a>
                                                </div> 
                                            </div>     
                                        </td>

                                        <td>
                                            {{ Form::open(array('route' => array('investasi.destroy', $investasi->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('investasi.edit', $investasi->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('investasi.show', $investasi->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaInvestasi->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div><!-- End Right content here -->
@stop
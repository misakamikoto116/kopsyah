@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                   <h3 class="panel-title"> DETAIL PEMBAYARAN INVESTASI</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                            <td>Tanggal Pembayaran</td>
                                            <td>:</td>
                                            <td>{{DateIndo($investasi->tanggal_pembayaran)}}</td>
                                        </tr>

                                        <tr>
                                            <td>Jumlah</td>
                                            <td>:</td>
                                            <td>{{rupiah($investasi->jumlah)}}</td>
                                        </tr>

                                        <tr>
                                            <td>Bukti Pembayaran</td>
                                            <td>:</td>
                                            <td>
                                                <img alt="" src="{{ asset($investasi->foto_resi)}}" height="100px" weight="100px"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            @if($investasi->status != 0)
                                            <td>{{'Sudah tervalidasi'}}</td>
                                            @else
                                            <td>{{'Belum tervalidasi'}}</td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Nomor Anggota</td>
                                            <td>:</td>
                                            <td>{{$investasi->anggota->nomor_anggota}}</td>
                                        </tr>

                                        <tr>
                                            <td>Nama Anggota</td>
                                            <td>:</td>
                                            <td>{{$investasi->anggota->nama}}</td>
                                        </tr>
    
                                        <tr>
                                            <td>Jenis Investasi</td>
                                            <td>:</td>
                                            <td>{{$investasi->jenis_investasi->nama}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="col-xs-4">Create at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$investasi->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td class="col-xs-4">Update at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$investasi->updated_at}}</td>
                                        </tr>
                                    </table>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                                <a href="{{route('investasi.index')}}">
                                                    <button class="btn btn-warning waves-effect waves-light">
                                                    <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
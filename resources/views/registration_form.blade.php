@include('errors.alert')
    <div class="panel-heading">
        <h4 class="panel-title"><b>Informasi Umum</b></h4>
    </div>
    <div class="container">
    
    <div class="form-group">
        <br>
        <div class="container">
       
            @yield('alert')

            <div>
                <strong>Pastikan anda sudah melakukan transfer sebelum registrasi!</strong>
            </div>
            <div>
                <strong class="text-danger">* harus diisi</strong>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">No KTP<span class="text-danger">*</span></label>
            <div class="col-md-7">
               <input  name="no_ktp" value="{{ old('no_ktp') }}" type="number" class="form-control" placeholder="No KTP" required="required">
            </div>
    
            <label class="col-md-1 control-label">Upload KTP<span class="text-danger">*</span></label>
            <div class="col-md-3">
                <input name = "file_ktp" type="file" class="filestyle" required="required">
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Nama<span class="text-danger">*</span></label>
            <div class="col-md-7">
               <input  name="nama" value="{{ old('nama')}} " type="text" class="form-control" placeholder="Nama" required="required">
            </div>
            
            <label class="col-md-1 control-label">Upload Foto<span class="text-danger">*</span></label>
            <div class="col-md-3">
                <input name = "file_foto" type="file" class="filestyle" required="required">
            </div>
        </div>
    </div>
    
     <div class="form-group">
        <div class="container"> 
            <label class="col-md-1">Jenis Kelamin<span class="text-danger">*</span></label>
            <div class="col-md-11">
              <select class="form-control" name="jenis_kelamin" value="{{old('jenis_kelamin')}} " required="required" >
                <option value ="L" {{ old('jenis_kelamin') == "L" ? 'selected' : '' }} >Laki-Laki</option>
                <option value ="P" {{ old('jenis_kelamin') == "P" ? 'selected' : '' }} >Perempuan</option>
              </select>
            </div>
        </div>
     </div>
    
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Agama</label>
            <div class="col-md-11">
            <input  name="agama" type="text" class="form-control" placeholder="Islam" required="required" disabled="">
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Tempat Lahir<span class="text-danger">*</label>
            <div class="col-md-11">
               <input  name="tempat_lahir" value="{{old('tempat_lahir')}}" type="text" class="form-control" placeholder="Tempat Lahir" required="required">
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Tanggal Lahir<span class="text-danger">*</label>
            <div class="col-md-11">
                <div class="input-group">
                    <input type="text" name="tanggal_lahir" id="tgl_lahir" class="form-control datepickerautoclose" value="{{old('tanggal_lahir')}}" placeholder="tgl-bln-tahun" required>
                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                </div><!-- input-group -->
               {{-- <input  name="tanggal_lahir" value="{{ old('tanggal_lahir')}}" type="date" class="form-control" required="required"> --}}
            </div>
        </div>
    </div>
    
     
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Telepon<span class="text-danger">*</span></label>
            <div class="col-md-11">
              <input name="no_telepon" value="{{ old('no_telepon') }}" type="number" class="form-control" required="required">
            </div>
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Kota<span class="text-danger">*</span></label>
            <div class="col-md-11">
                <select class="form-control" name="kota" required="required">
                        <option value ="1" {{ old('kota') == 1 ? 'selected' : '' }} >Samarinda</option>
                        <option value ="2" {{ old('kota') == 2 ? 'selected' : '' }} >Luar Samarinda</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Alamat<span class="text-danger">*</label>
            <div class="col-md-11">
              <input  name="alamat" value="{{ old('alamat') }} " type="text" class="form-control" placeholder="Alamat Sesuai KTP" required="required">
            </div>
        </div>
    </div>           
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Pekerjaan<span class="text-danger">*</label>
            <div class="col-md-11">
                <select class="form-control" name="pekerjaan"   required="required">
                    <option value ="1" {{ old('pekerjaan') == 1 ? 'selected' : '' }} >PNS</option>
                    <option value ="2" {{ old('pekerjaan') == 2 ? 'selected' : '' }} >Wiraswasta</option>
                    <option value ="3" {{ old('pekerjaan') == 3 ? 'selected' : '' }} >Karyawan</option>
                    <option value ="4" {{ old('pekerjaan') == 4 ? 'selected' : '' }} >Pensiunan</option>
                    <option value ="5" {{ old('pekerjaan') == 5 ? 'selected' : '' }} >Petani</option>
                    <option value ="6" {{ old('pekerjaan') == 6 ? 'selected' : '' }} >Lainnya</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Pendapatan 
                <h6>(optional)</h6></label>
            <div class="col-md-11">
                <select class="form-control" name="pendapatan" value="{{ old('pendapatan') }} " >
                    <option value ="1" {{ old('pekerjaan') == 1 ? 'selected' : '' }} ></option>
                    <option value ="2" {{ old('pekerjaan') == 2 ? 'selected' : '' }} >< Rp 500.000,00</option>
                    <option value ="3" {{ old('pekerjaan') == 3 ? 'selected' : '' }} >Rp 500.000,00 - Rp 1.000.000,00</option>
                    <option value ="4" {{ old('pekerjaan') == 4 ? 'selected' : '' }} >Rp 1.000.000,00 - Rp 2.000.000,00</option>
                    <option value ="5" {{ old('pekerjaan') == 5 ? 'selected' : '' }} >Rp 2.000.000,00 - Rp 3.000.000,00</option>
                    <option value ="6" {{ old('pekerjaan') == 6 ? 'selected' : '' }} >Rp 3.000.000,00 - Rp 4.000.000,00</option>
                    <option value ="7" {{ old('pekerjaan') == 7 ? 'selected' : '' }} >Rp 4.000.000,00 - Rp 5.000.000,00</option>
                    <option value ="8" {{ old('pekerjaan') == 8 ? 'selected' : '' }} >Rp 5.000.000,00 - Rp 6.000.000,00</option>
                    <option value ="9" {{ old('pekerjaan') == 9 ? 'selected' : '' }} >> Rp 6.000.000,00</option>
                </select>
            </div>
        </div>
    </div>
    </div>
                    
    
    <div class="panel-heading">
    <h4 class="panel-title"><b>Keanggotaan</b></h4>
    </div>
    
    <br>
    <div class="container">
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Status Keanggotaan<span class="text-danger">*</label>
            <div class="col-md-11">
                <select class="form-control" name="role_id" value="{{ old('role_id') }}" required="required">
                    {{-- <option value =""   disabled="">----- Pilih -----</option> --}}
                    {{-- <option value ="2" disabled="">Anggota</option> --}}
                    <option value ="3" selected="">Anggota dan Investor</option>
                </select>
            </div>
        </div>
    </div>
    </div>
    {{-- <div id="anggota_investor">
    <div class="panel-heading">
    <h4 class="panel-title"><b>Simpanan</b></h4>
    </div>
    
    <br>
    
    
    <div class="container">
        <div class="container">
            <div>
                <strong>Rekening : Bank Syariah Mandiri 7115634385 a.n Kopsyah SMS </strong>
           </div>
        </div>
        <br>
    @foreach($jenis_pembayaran as $pembayaran)
    <div class="form-group">
    @if($pembayaran->nominal != 0)
    <div class="container">
      <label class="col-md-1">{{$pembayaran->nama}}<span class="text-danger">*</label>
      <div class="col-md-11">
              {!! Form::text('pembayaran['.$pembayaran->id.']',$pembayaran->nominal,['class'=>'form-control','placeholder'=>"Jumlah",'readonly']) !!}
      </div>
    
    @else
    <div class="container">
      <label class="col-md-1">{{$pembayaran->nama}}<span class="text-danger">*</label>
      <div class="col-md-11">
              {!! Form::text('pembayaran['.$pembayaran->id.']',0,['class'=>'form-control','placeholder'=>"Jumlah"]) !!}
      </div>
    
    @endif
    </div>
    </div>
    @endforeach --}}
    {{-- <div class="form-group">
        <div class="container">
            <label class="col-md-1">Tanggal Transfer<span class="text-danger">*</label>
            <div class="col-md-7">
                <div class="input-group">
                    <input type="text" name="tanggal_pembayaran" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" required>
                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                </div>input-group
                {{-- <input name = "tanggal_pembayaran" value="{{ old('tanggal_pembayaran') }} " type="date" class="form-control" required="required">
            </div>
    
            <label class="col-md-1">Resi Pembayaran<span class="text-danger">*</label>
             <div class="col-md-3">
                <input name = "file_resi" value="{{ old('file_resi') }} " type="file" class="filestyle" required="required">
            </div>
        </div>
    </div>
    </div>
    </div> --}}
    <hr>
    <div id="Investasi">
    <div class="panel-heading">
    <h4 class="panel-title"><b>Investasi</b></h4>
    </div>
    
    <br>
    <div class="container">
    @foreach ($jenis_investasi as $investasi)
    @if($investasi->status != 1)
    @php
        if($investasi->status == '1'){
            $sts= 'sudah penuh';
        }else{
            $sts= 'belum penuh';
        }
    @endphp
    <div class="container">
        <div>
            <strong>{{$investasi->nama}}</strong>
            <h6>Rekening : {{$investasi->deskripsi}} <span class="text-danger"> ({{$sts}}) </span>
            </h6>
        </div>
    </div>

    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Jumlah<span class="text-danger">*</label>
            <div class="col-md-3">
               {{ Form::text('investasi['.$investasi->id.']',0,['class'=>'form-control','placeholder'=>"Jumlah"]) }}
            </div>
            <label class="col-md-1">Tanggal Transfer<span class="text-danger">*</label>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" id="tgl_inv" name="tanggal_investasi[{{$investasi->id}}]" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                </div><!-- input-group -->
            </div>
            <label class="col-md-1">Resi Pembayaran<span class="text-danger">*</label>
            <div class="col-md-3">
                <input name = "foto_resi[{{$investasi->id}}]" type="file" class="filestyle">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Biaya cetak kartu & sertifikat</label>
            <div class="col-md-3">
                {{ Form::text('biaya_cetak_investasi['.$investasi->id.']', 50000,['class'=>'form-control','placeholder'=>"Biaya cetak kartu & sertifikat","readonly"]) }}
            </div>
        </div>
    </div>  
    <hr>  
    @endif 
    @endforeach
    
    
          
        </div>
    </div>
    
    
    <div class="panel-heading">
    <h4 class="panel-title"><b>Akun Anda</b></h4>
    </div>
    
    <br>
    <div class="container">
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Email<span class="text-danger">*</label>
            <div class="col-md-11">
               <input name="email" value= "{{ old('email') }}" type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Password<span class="text-danger">*</label>
            <div class="col-md-11">
               <input name="password" type="password" class="form-control" required="required">
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="container">
            <label class="col-md-1">Konfirmasi Password<span class="text-danger">*</label>
            <div class="col-md-11">
               <input name="password_confirmation"  type="password" class="form-control" required="required">
            </div>
        </div>
    </div>

    
    <div class="form-group">
        <div class="container">
            <div class="col-md-1">
                &nbsp;
            </div>
            <div class="col-md-11">
                <div class="g-recaptcha" data-sitekey="6LdF700UAAAAALcI2WdCRI87m3zj1pAfmPtKQR28" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                <input type="hidden" name="recaptcha" data-rule-recaptcha="true">
            </div>
        </div>
    </div>
    {{-- <div class='form-group' style="margin-left:12rem">
        <div class="g-recaptcha" data-sitekey="6LdF700UAAAAALcI2WdCRI87m3zj1pAfmPtKQR28"></div>
        <input type="hidden" name="recaptcha" data-rule-recaptcha="true">
    </div>
    </div> --}}
    
  
    
     
  
      

</div>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{asset('assets/assets/images/utama/logo1.png')}}">

        <title>Page not found</title>

        <link href="{{asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/assets/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/assets/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('assets/assets/js/modernizr.min.js')}}"></script>
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var BASE_URL = '{{ url("/") }}/';
    </script>
    <!-- PLUGIN -->
    @yield('custom_css')

</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    
    <div class="wrapper-page">
        <div class="ex-page-content text-center">
            <div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span></div>
            <h2>Who0ps! Page not found</h2><br>
            <p class="text-muted">This page cannot found or is missing.</p>
            <p class="text-muted">Use the navigation above or the button below to get back and track.</p>
            <br>
            <a class="btn btn-default waves-effect waves-light" href="javascript:history.back()"> Return</a>
            
        </div>
    </div>


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('assets/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/assets/js/detect.js')}}"></script>
<script src="{{asset('assets/assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/assets/js/waves.js')}}"></script>
<script src="{{asset('assets/assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/assets/js/jquery.scrollTo.min.js')}}"></script>

<script src="{{asset('assets/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/assets/js/jquery.app.js')}}"></script>

</body>
</html>
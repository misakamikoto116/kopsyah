@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('badanhukum.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                <a href="#">Tentang</a>
                                            </li>
                                            <li class="active">
                                                Badan Hukum
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL BADAN HUKUM</b></h4></center>

                        <!-- Search -->
                            <form action="{{url()->current()}}">
                                <div class="container">
                                    <div class="row">
                                        <div class="input-group col-12 col-md-4 pull-right">
                                            {!! Form::text('judul',null,['class'=>'form-control col-md-3','placeholder'=>"Judul"]) !!}          
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <!-- End Search -->

                        <table id="datatable" class="table table-striped table-bordered">

						<!-- Alert -->
							@yield('alert')
                        <!-- End Alert -->
                        
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Judul</td>
                                    <td>Isi</td>
                                    <td>Action</td>
                                </tr>
                            </thead>

                            @php
                            $no =1;
                            @endphp
                            <tbody>
                                @foreach($semuaTentang as $tentang)
                                    <tr>

                                        <td>{{$no++}}</td>
                                        <td>{{$tentang->judul or 'kosong'}}</td>
                                        <td>{{$tentang->isi or 'kosong'}}</td>
                                        <td>
                                        {{ Form::open(array('route' => array('badanhukum.destroy', $tentang->id), 'method' => 'delete')) }}
                                            <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('badanhukum.edit', $tentang->id)}}">Ubah</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">Hapus</button>
                                                    </ul>
                                                </div>
                                        {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $semuaTentang->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
<div class="form-group">
     <label class="col-md-2 control-label">Judul</label>
     <div class="col-md-10">
        {!! Form::text('judul',old('judul'),['class'=>'form-control','placeholder'=>"Judul"]) !!}
     </div>
</div>
<div class="form-group">
     <label class="col-md-2 control-label">Isi</label>
     <div class="col-md-10">
        {!! Form::textarea('isi',old('isi'),['class'=>'form-control','placeholder'=>"Isi",'id'=> 'elm1']) !!}
     </div>
</div>

@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                   <a href="{{route('tentang.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i> DETAIL ABOUT</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                             <td>Title</td>
                                             <td>:</td>
                                             <td>{{$tentang->judul}}</td>
                                        </tr>
                                        <tr>
                                             <td>Content</td>
                                             <td>:</td>
                                             <td>{{$tentang->isi}}</td>
                                        </tr>
                                        <tr>
                                             <td>Jenis</td>
                                             <td>:</td>
                                             <td>{{$tentang->jenis}}</td>
                                        </tr>
                                        <tr>
                                             <td>Image</td>
                                             <td>:</td>
                                             <td>{{$tentang->foto}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Create at</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{$tentang->created_at}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Update at</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{$tentang->updated_at}}</td>
                                        </tr>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
<div class="form-group">
    <label class="col-md-2 control-label">Judul</label>
    <div class="col-md-10">
    {!! Form::text('judul',old('judul'),['class'=>'form-control','placeholder'=>"Judul",'required']) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Isi</label>
    <div class="col-md-10">
        {!! Form::textarea('isi',old('isi'),['class'=>'form-control','placeholder'=>"Isi",'id'=>'elm1']) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Foto</label>
    <div class="col-md-10">
        <input name = "foto" type="file" class="filestyle" value ="{{$tentang->foto}}">
        <br>
        <img id="preview" style="margin-bottom: 5px;" src="{{ asset('storage/images/tentang/'.$tentang->foto) }}" height="100" alt="">
        </input>
    </div>
</div>
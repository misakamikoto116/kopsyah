@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                 <h3 class="panel-title"> DETAIL Dokumen</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                            <td>Judul</td>
                                            <td>:</td>
                                            <td>{{$dokumen->judul}}</td>
                                        </tr>
                                        <tr>
                                            <td>Deskripsi</td>
                                            <td>:</td>
                                            <td>{{$dokumen->deskripsi}}</td>
                                        </tr>
                                       <tr>
                                             <td>Dokumen</td>
                                             <td>:</td>
                                             <td>
                                                <a href="{{ route('admin.resource.download', array('file'=>$dokumen->file_dokumen)) }}">{{$dokumen->file_dokumen}}</a>
                                            </td>
                                        </tr>
                                        

                                        <tr>
                                            <td class="col-xs-4">Create at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$dokumen->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td class="col-xs-4">Update at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$dokumen->updated_at}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                             <a href="{{route('dokumen.index')}}">
                                                 <button class="btn btn-warning waves-effect waves-light">
                                                    <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                 </button></a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
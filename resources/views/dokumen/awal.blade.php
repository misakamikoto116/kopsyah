@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('dokumen.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="active">
                                                Dokumen
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL DOKUMEN</b></h4></center>

                        <!-- Search -->
                            <form action="{{url()->current()}}">
                                <div class="container">
                                    <div class="row">
                                        <div class="input-group col-12 col-md-4 pull-right">
                                            {!! Form::text('judul',null,['class'=>'form-control col-md-3','placeholder'=>"Nama"]) !!}          
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <!-- End Search -->

                        <table id="datatable" class="table table-striped table-bordered">

                           	<!-- Alert -->
							    @yield('alert')
							<!-- End Alert -->

                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Nama</th>
                                    <th width="20%" scope="row">Deskripsi</th>
                                    <th width="40%" scope="row">File</th>
                                    <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($semuaDokumen as $dokumen)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$dokumen->judul or 'kosong'}}</td>
                                        <td>{{$dokumen->deskripsi or 'kosong'}}</td>
                                        <td>
                                            <a href="{{ route('admin.resource.download', array('file'=>$dokumen->file_dokumen)) }}">{{$dokumen->file_dokumen}}</a>
                                        </td>
                                        <td>
                                            {{ Form::open(array('route' => array('dokumen.destroy', $dokumen->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('dokumen.edit', $dokumen->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('dokumen.show', $dokumen->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!!  $semuaDokumen->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
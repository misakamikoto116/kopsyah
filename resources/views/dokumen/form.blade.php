<div class="form-group">
     <label class="col-md-2 control-label">Judul</label>
     <div class="col-md-10">
        {!! Form::text('judul',old('judul'),['class'=>'form-control','placeholder'=>"Judul"]) !!}
     </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Deskripsi</label>
    <div class="col-md-10">
        {!! Form::textarea('deskripsi',old('deskripsi'),['class'=>'form-control','placeholder'=>"Deskripsi",'id'=> 'elm1']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Upload Dokumen<span class="text-danger">*</label>
    <div class="col-md-10">
        <input name = "file_dokumen" value="<?= Form::old('file_dokumen') ?>" type="file" class="filestyle">
    </div>
</div>
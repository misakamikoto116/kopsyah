{{-- @foreach($jenisInvestasi as $jenis)
<div>{{$jenis->id}}</div>
<a href="{{route('pdfview', $jenis->id)}}">Download PDF</a>
@endforeach --}}

@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="#">Admin</a>
                                        </li>
                                        <li class="active">
                                            Report
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        <center><h4 class="m-t-0"><b>TABEL REPORT</b></h4></center>
                            
                        <form>
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::select('usaha', ['1' => '212 Mart Aws', '2' => '212 Mart Sentosa'], null, ['placeholder' => '-- Pilih Unit Usaha --','class'=>'form-control col-md-3']) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <table id="datatable" class="table table-striped table-bordered">

                            @if(Session::has('informasi'))
                                <div class="alert alert-info">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Jenis Usaha</th>
                                    <th width="40%" scope="row">File</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($jenisInvestasi as $jenis)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$jenis->nama or 'kosong'}}</td>
                                        <td>
                                            <a href="{{route('pdfview', $jenis->id)}}">Lihat Laporan</a>
                                            {{-- <a href="{{ route('investor.resource.download', array('file'=>$dokumen->file_dokumen)) }}">{{$dokumen->file_dokumen}}</a> --}}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- {!! $JenisInvestasi->appends(Request::except('page'))->render() !!} --}}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop 
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <title>212 Mart Samarinda | Beranda</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
      <link href="{{asset('assets/assets/layout/styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('assets/assets/layout/styles/framework.css')}}" rel="stylesheet" type="text/css" media="all">
      <link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all">
      <link rel="icon" href="{{asset('assets/assets/images/utama/logo1.png')}}">  
  </head>
  <body id="top">
            <div class="wrapper row0" style="color: #CBCBCB; background-color: white;">
            <div id="topbar" class="hoc clear"> 
              <div class="fl_right">
                
                @if (Route::has('login'))
                @php
                $user = App\Model\UserModel::where('id', '=', Auth::id())->first();
                @endphp
                @auth
                <ul class="nospace" style="padding:10px;">
                  <li><a href="{{ route('logout') }}" style="color: #4368B0; background-color: white;">Logout</a></li>
                  @if($user->role_id == "2")
                  <li><a href="{{ route('anggota') }}" style="background-color: white;">Dashboard</a></li>
                  @elseif($user->role_id == "3")
                  <li><a href="{{ route('investor') }}" style="background-color: white;">Dashboard</a></li>
                  @elseif($user->role_id == "1")
                  <li><a href="{{ route('admin') }}" style="background-color: white;">Dashboard</a></li>
                  @endif
                </ul>   
        
                @else
                <ul class="nospace" style="padding:10px;">
                  <li><a href="{{ route('login') }}" style="color: #4368B0; background-color: white;">Login</a></li>
                  <li><a href="{{ route('registration') }}" style="background-color: white;">Registrasi</a></li>
                </ul>   
                @endauth
            @endif
             </div>
            </div>
          </div>
          
          <div class="bgded overlay" style="background-image:url('assets/assets/images/utama/islamic2.jpg');"> 
            <div class="wrapper row1">
              <header id="header" class="hoc clear"> 
                <div id="logo" class="fl_left">
                  <h1><a href="{{ route('/') }}"><img src="{{asset('assets/assets/images/utama/logo1.png')}}" style="width: 60%;"></a></h1><br>

                </div>
                <nav id="mainav" class="fl_right" style="margin-top: 100px;">
                  <ul class="clear">
                    <li class="active"><a href="{{ route('/') }}">Beranda</a></li>
                    <li><a href="{{ route('/profil') }}">Tentang Kami</a></li>
                    <li><a href="{{ route('/struktur') }}">Struktur</a></li>
                    <li><a href="{{ route('/faq') }}">FAQ</a></li>
                    <li><a href="{{ route('/blog') }}">Blog</a></li>
                    <li><a href="{{ route('/page_doc') }}">Dokumen</a></li>
                  </ul>
                </nav>
              </header>
            </div>
            <div id="pageintro" class="hoc clear"> 
              <article>
                <h2 class="heading">212 mart samarinda</h2>
                <p style="font-size: 16px">dari kita, oleh kita, untuk kita</p>
                <footer>
                  <ul class="nospace inline pushright">
                    <li><a class="btn" href="{{ route('registration') }}">Registrasi</a></li>
                  </ul>
                </footer>
              </article>
                <p class="col-12 col-md-12 text-white text-left" style="font-size:9px; position:absolute; margin-top:7rem">Sumber Gambar: <a style="color:white"font-size:9px; href="https://www.google.co.id/search?q=islamic+center&oq=islamic+center&aqs=chrome..69i57j69i59.4139j0j1&sourceid=chrome&ie=UTF-8">Islamic Center Samarinda</a></p>                
            </div>
          </div>
          <br> 
          
          
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear" style="padding:20px 0px 70px 0;"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content" style="background: white; color: black;"> 
      <!-- ################################################################################################ -->
      <img class="fl_left" style="margin:0 40px 0px 0px;max-width: 20%" src="{{asset('assets/assets/images/utama/logo1.png')}}" alt="">
      <div style="text-align:justify">
        <p class="nospace font-ms"  style="margin:0 0 10px 0; ">
          <h1 style="text-align:left">212 Mart Samarinda</h1>
          <p>
            Semangat kebangkitan umat terus membara, salah satunya lewat bidang ekonomi. Koperasi Syariah 212 telah menginisiasi lahirnya 212Mart, minimarket Islami dengan konsep sharing economy (ekonomi berbagi). Konsep yang berbeda dengan minimarket yang sudah ada sebelumnya.
          </p>
          
          <p>
            Dalam konsep ekonomi berbagi itu, jaringan ritel 212Mart dimiliki oleh umat, dikelola oleh umat, dan didirikan untuk umat.
          </p>

          <p>
            Gerai pertama didirikan pada 12 Mei 2017, di Ruko Perumahan Taman Yasmin, Kota Bogor, Jawa Barat dengan dana awal sekira Rp300 jutaan yang merupakan patungan dari warga.
          </p>

          <p>
            Sampai hari ini, telah ratusan gerai dibuka dan terus bertambah. Warga bangga menjadi bagian dalam pendirian 212Mart ini. Bahkan banyak yang bertanya juga, di mana kalau mau berbelanja ke 212 Mart. 
          </p>
  
        </p>    
      </div>
    </div>
    <footer><a class="btn inverse" href="{{ route('/profil') }}">Read More &raquo;</a></footer>
                     
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->


<div class="wrapper bgded overlay" style="background:#4368B0;">
    <article class="hoc container" style="padding:30px 0px 60px 0; "> 
      <footer id="footer" class="hoc clear"> 
          <div class="row">
            <div class="col-12 col-md-6 text-center">
              <ul class="nospace contact">
                <li>
                  <h1 class="heading" style="text-align: center; font-size:80px;">{{$jml_anggota}}</h1>
                  <ul class="nospace center">
                    <h1 class="heading" style="text-align: center; font-size:12px;">INVESTOR</h1>
                </ul>
                </li>
              </ul>
            </div>
            <div class="col-12 col-md-6 text-center">
              <ul class="nospace contact">   
                <li>
                  <h6 class="heading font-x1" style="text-align: center;font-size:80px;">{{$jml_usaha}}</h6>
                  <ul class="nospace center">
                    <h1 class="heading" style="text-align: center; font-size:12px;">UNIT USAHA</h1>
                  </ul>
                </li>
             </ul>
            </div>
          </div>
        
       </footer>
       
    </article>
</div>


<div class="wrapper row3">
    <article class="hoc container" style="color: #4368B0;"> 
        <h1 style="text-align: center; margin-top: -3rem">PRINSIP-PRINSIP</h1>
        <div class="row">
          <div class="col-12 col-md-4 text-center">
            <ul class="nospace contact">
              <li>
                <is class="fa fa-users fa-lg"></is>
                  <h1 class="block btmspace-10 font-x4" style="margin-top:10px">Adil</h1>
                <p style="font-size: 12px;">Adil, tidak menguntungkan satu pihak saja.</p>           
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-4 text-center">
            <ul class="nospace contact">
              <li>
                <is class="fa fa-tint fa-lg">  </is> 
                  <h1 class="block btmspace-10 font-x4" style="margin-top:10px">Amanah</h1>
                  <p style="font-size: 12px;">Menjaga amanah yang diberikan.</p>     
                </li>
            </ul>
          </div>
          <div class="col-12 col-md-4 text-center">
            <ul class="nospace contact">
              <li>
                <is class="fa fa-ioxhost fa-lg"></is>
                <h1 class="block btmspace-10 font-x4" style="margin-top:10px">Taawun</h1>
                <p style="font-size: 12px;"> Saling menolong.</p>    
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-6 text-center">
            <ul class="nospace contact">
              <li>
                  <is class="fa fa-black-tie fa-lg"></is>
                  <h1 class="block btmspace-10 font-x4" style="margin-top:10px">Itqan</h1>
                  <p style="font-size: 12px;">Dijalankan secara Profesional.</p>
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-6 text-center">
            <ul class="nospace contact">
              <li>
                <is class="fa fa-graduation-cap fa-lg"></is>
                <h1 class="block btmspace-10 font-x4" style="margin-top:10px">Maslahah</h1>
                <p style="font-size: 12px;">Keberadaannya memberikan manfaat</p>     
              </li>
            </ul>
          </div>
        </div>
    </article>
</div>
      


          
          <div class="wrapper row3">
            <main class="hoc container clear"> 
              <!-- main body -->
              <h1 style="text-align: center; margin-top: -3rem">TERKINI</h1>
              <div class="row">
                @if($berita != null)
                  @foreach ($berita as $terkini)
                  <div class="col-12 col-md-4 text-center terkini">
                    <img class="btmspace-30" src="{{ asset($terkini->foto) }}" style="height:200px" alt=""></a>
                    <h4 class="heading text-left">
                      @php
                      $jdl= $terkini->judul;
                      @endphp
                      {{substr($jdl,0,20)}}
                    </h4>
                    <p class="text-left">
                      @php
                      $kalimat= $terkini->isi;
                      @endphp
                      {{substr($kalimat,0,40)}}
                      ...
                      </p>
                    <p style="font-size:9px; text-align:left">Sumber:<a href="http://koperasisyariah212.co.id/uas-mendukung-gerakan-ekonomi-berjamaah/">Koperasi 212</a></p>
                    <p class="nospace" style="text-align:left"><a href="#">Read More &raquo;</a></p>
                  </div>
                  @endforeach
                @elseif($berita == null)
                <div class="col-12 col-md-4">
                    <article class="one_third" style="height:300px">
                    <h4 class="heading">Judul Berita</h4>
                    <p>Kalimat Berita</p>
                    <p style="font-size:9px">Sumber:Koperasi 212</a></p>
                    <p class="nospace"><a href="#">Read More &raquo;</a></p>
                  </div>
              </div>
              @endif
              </div>
              <!-- / main body -->
              <div class="clear"></div>
            </main>
          </div>




<div class="wrapper bgded overlay" style="background: #4368B0;">
  <article class="hoc container"  style="padding:50px 0px 60px 0;"> 
    <!-- ################################################################################################ -->
    <div class="group btmspace-5">
      <div class="fl_left" style="margin-right:20px; max-width: 10%"><img src="{{asset('assets/assets/images/utama/Arifin-Ilham.png')}}" alt=""></div>
      <div class="fl_left">
        <p class="nospace font-xs">Dewan Penasehat 212 Mart Samarinda</p>
        <h3 class="heading">KH. M. Arifin Ilham</h3>
      </div>
    </div>
    <blockquote>“Semoga bangsa ini selalu diberkahi dan dijauhkan dari bahaya yang memecah-belah persatuan dan kesatuan umat.” </blockquote>
    <em class="block btmspace-0 font-xs">Ilustrasi</em>
    <footer><a class="btn inverse" href="http://koperasisyariah212.co.id/staff/arifin-ilham/">Read More &raquo;</a></footer>
    <!-- ################################################################################################ -->
  </article>
</div>




     
    <div class="wrapper row3">
      <section class="hoc container clear"> 
        <div>
          <h2 class="nospace" style="text-align:center">" 212 Mart Samarinda " </h2>
        </div>
      </section>
    </div>
      <div class="wrapper row5" style="background: #002e5b; height: 120px;">
        <div id="copyright" class="hoc clear"> 
          <p class="fl_left" style="color: white;">Copyright &copy; 2018 - <a href="#">212 Mart Samarinda</a></p>
        </div>
      </div>
    </div>
    <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
    
    <!-- JAVASCRIPTS -->         
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{asset('assets/assets/layout/scripts/jquery.min.js')}}"></script>
    <script src="{{asset('assets/assets/layout/scripts/jquery.backtotop.js')}}"></script>
    <script src="{{asset('assets/assets/layout/scripts/jquery.mobilemenu.js')}}"></script>
  </body>
</html>

<div class="form-group">
     <label class="col-md-2 control-label">Nama</label>
     <div class="col-md-10">
        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>"Nama",'required'=>'required']) !!}
     </div>
</div>
<div class="form-group">
        <label class="col-md-2 control-label">Modal</label>
        <div class="col-md-10">
           {!! Form::text('jumlah_investasi',null,['class'=>'form-control','placeholder'=>"Modal",'required'=>'required']) !!}
        </div>
   </div>
  <div class="form-group">
        <label class="col-md-2 control-label">Status</label>
        <div class="col-md-10">
           {!! Form::select('status',array('0'=>'belum penuh','1'=>'sudah penuh'),null,['class'=>'form-control','required'=>'required']) !!}
        </div>
   </div>
   <div class="form-group">
     <label class="col-md-2 control-label">Deskripsi</label>
     <div class="col-md-10">
        {!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi",'required'=>'required']) !!}
     </div>
</div>
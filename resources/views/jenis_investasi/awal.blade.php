@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('jenis_investasi.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="active">
                                                Jenis Investasi
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL JENIS INVESTASI</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nama Usaha"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <table id="datatable" class="table table-striped table-bordered">

                        		<!-- Alert -->
									@yield('alert')
						

                            <thead>
                                <tr>
                                        <th width="5%" scope="row">No</th>
                                        <th width="20%" scope="row">Nama</th>
                                        <th width="20%" scope="row">Modal</th>
                                        <th width="20%" scope="row">Status</th>
                                        <th width="40%" scope="row">Deskripsi</th>
                                        
                                        <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($semuaJenisInvestasi as $jenis_investasi)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$jenis_investasi->nama or 'kosong'}}</td>
                                        <td>{{rupiah($jenis_investasi->jumlah_investasi)}}</td>
                                        @if($jenis_investasi->status !== "0")
                                        <td>{{'Penuh'}}</td>
                                        @else
                                        <td>{{'Belum Penuh'}}</td>
                                        @endif
                                        <td>{{$jenis_investasi->deskripsi or 'kosong'}}</td>
                                       
                                        <td>
                                            {{ Form::open(array('route' => array('jenis_investasi.destroy', $jenis_investasi->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('jenis_investasi.edit', $jenis_investasi->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('jenis_investasi.show', $jenis_investasi->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaJenisInvestasi->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
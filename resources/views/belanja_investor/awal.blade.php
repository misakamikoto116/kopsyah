@extends('master_investor')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('belanja_investor.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Investor</a>
                                            </li>
                                            <li class="active">
                                                Belanja
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL BELANJA</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">

                           	<!-- Alert -->
							    @yield('alert')
							<!-- End Alert -->

                            <thead>
                                <tr>
                                        <th width="5%" scope="row">No</th>
                                        <th width="30%" scope="row">Nama Barang</th>
                                        <th width="30%" scope="row">Merek</th>
                                        <th width="20%" scope="row">Jumlah</th>
                                        <th width="20%" scope="row">Periode</th>
                                        <th width="20%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                               
                                @foreach($semuaBelanja as $belanja)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$belanja->nama_barang or 'kosong'}}</td>
                                        <td>{{$belanja->merek or 'kosong'}}</td>
                                        <td>{{$belanja->jumlah or 'kosong'}}</td>
                                        <td>{{belanja($belanja->periode)}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('belanja_investor.destroy', $belanja->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
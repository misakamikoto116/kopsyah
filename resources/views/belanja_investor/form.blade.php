
        <div class="form-group">
            <table class="table table-condensed"  id="dynamic_field">
                <thead>
                    <tr>
                        <th width="200px">Nama Barang</th>
                        <th width="200px">Merek</th>
                        <th width="50px">Jumlah</th>
                        <th width="100px">Periode</th>
                        <th width="80px"></th>
                        </tr>
                </thead>
                <!--elemet sebagai target append-->
                <tbody id="itemlist">
                    <tr>
                        <td> {!! Form::text('nama_barang[]',null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::text('merek[]',null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::text('jumlah[]',null,['class'=>'form-control', 'required']) !!}</td>
                        <td> {!! Form::select('periode[]',['1'=>'Mingguan','2'=>'Bulanan'],null,['class'=>'form-control', 'required'])!!}</td>
                        <td> {!! Form::button('+', ['class' => 'btn btn-success', 'id' => 'add']) !!}</td>
                    </tr>
                </tbody>
           
            </table>
          
        </div>
            
  @section('js')        
  <script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           
           $('#dynamic_field')
           .append('<tr id="row'+i+'" class="dynamic-added">'+
                '<td> {!! Form::text('nama_barang[]',null,['class'=>'form-control', 'required'])!!}</td>'+
                '<td> {!! Form::text('merek[]',null,['class'=>'form-control rupiah', 'required'])!!}</td>'+
                '<td> {!! Form::text('jumlah[]',null,['class'=>'form-control', 'required']) !!}</td>'+
                '<td> {!! Form::select('periode[]',['1'=>'Mingguan','2'=>'Bulanan'],null,['class'=>'form-control', 'required'])!!}</td>'+
                '<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class=" fa fa-trash"></i></button></td>'+
                '</tr>');
          
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                    }
                }  
           });  
      });  


    });  
</script>

@endsection
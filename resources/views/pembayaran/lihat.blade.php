@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                  <h3 class="panel-title"> DETAIL PEMBAYARAN SIMPANAN</h3></a>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                            <td>Tanggal Bayar</td>
                                            <td>:</td>
                                            <td>{{DateIndo($pembayaran->tanggal_pembayaran)}}</td>
                                        </tr>       
                                        <tr>
                                             <td>Jumlah</td>
                                             <td>:</td>
                                             <td>{{rupiah($pembayaran->jumlah)}}</td>
                                        </tr>

                                        <tr>
                                             <td>Tempo Pembayaran</td>
                                             <td>:</td>
                                             <td>{{DateIndo($pembayaran->tanggal_tempo)}}</td>
                                        </tr>
                                        <tr>
                                             <td>Bukti Pembayaran</td>
                                             <td>:</td>
                                             <td>
                                                <img alt="" style="width:200" src="{{ asset($pembayaran->file_resi)}}" height="120px"/>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td>Status</td>
                                             <td>:</td>
                                
                                             @if($pembayaran->status != 0)
                                             <td>{{'Sudah tervalidasi'}}</td>
                                             @else
                                             <td>{{'Belum tervalidasi'}}</td>
                                             @endif
                                        </tr>
                                        <tr>
                                              <td>Nomor Anggota</td>
                                              <td>:</td>
                                              <td>{{$pembayaran->anggota->nomor_anggota}}</td>
                                        </tr>
                                        <tr>
                                                <td>Nama Anggota</td>
                                                <td>:</td>
                                                <td>{{$pembayaran->anggota->nama}}</td>
                                          </tr>
                                        <tr>
                                             <td>Jenis Simpanam</td>
                                             <td>:</td>
                                             <td>{{$pembayaran->jenis_pembayaran->nama}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Created at</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date($pembayaran->created_at)}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Updated at</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date($pembayaran->updated_at)}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                                <a href="{{route('pembayaran.index')}}">
                                                    <button class="btn btn-warning waves-effect waves-light">
                                                        <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->
     </div> <!-- content -->
</div><!-- End Right content here -->
@stop
<div class="form-group">
	<label class="col-md-2 control-label">Tanggal Pembayaran</label>
	<div class="col-md-10">
		<div class="input-group">
			@php
			$tgl_conv = date('Y-m-d', strtotime($pembayaran->tanggal_pembayaran));
                        $tgl_jadi = date('d-m-Y', strtotime($tgl_conv));
			@endphp
			<input type="text" name="tanggal_pembayaran" value="{{$tgl_jadi}}" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
			<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-10">
		{!! Form::text('jumlah',null,['class'=>'form-control','placeholder'=>"Jumlah",'required'=>'required']) !!}
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Bukti Pembayaran</label>
	<div class="col-md-10">
		<input name = "file_resi" type="file" class="filestyle"  value = "{{ $pembayaran->file_resi}}">
		<br>
		<img id="preview" style="margin-bottom: 5px;" src="{{ asset($pembayaran->file_resi) }}" height="100" alt="">
		</input>
	</div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Nama Anggota</label>
	<div class="col-md-10">
		<select class="form-control select2" name="anggota_id" id="">
			@foreach ($anggotaPluck as $anggota )
				<option value="{{ $anggota->id}}"
				@if ($anggota->id === $pembayaran->anggota_id)
					selected
				@endif
				>{{ $anggota->nama}} - {{ $anggota->nomor_anggota}} </option>
			@endforeach
		</select>
	</div>
</div>
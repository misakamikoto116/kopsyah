@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                        <a href="{{route('pembayaran.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                </div>

                                <ol class="breadcrumb">
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>

                                    <li>
                                        <a href="#">Transaksi</a>
                                    </li>

                                    <li class="active">
                                        Simpanan
                                    </li>
                                </ol>
                            </div>
                        </div>

                        <center><h4 class="m-t-0"><b>TABEL PEMBAYARAN SIMPANAN</b></h4></center>

                        <!-- Form Pencarian  -->
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nomor Anggota atau Nama"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        
                        <table id="datatable" class="table table-striped table-bordered">
                                
                            <!-- Alert -->
                            @yield('alert')
                                 
                            
                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="15%" scope="row">Nomor Anggota</th>
                                    <th width="15%" scope="row">Nama Anggota</th>
                                    <th width="20%" scope="row">Jenis Simpanan</th>
                                    <th width="10%" scope="row">Jumlah Pembayaran</th>
                                    <th width="10%" scope="row">Tempo</th>
                                    <th width="20%" scope="row">Tanggal Pembayaran</th>
                                    <th width="10%" scope="row">Validasi</th>
                                    <th width="20%" scope="row">Bukti Pembayaran</th>
                                    <th width="10%" scope="row">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $no =1+($allPembayaran->perPage()*($allPembayaran->currentPage()-1));
                                @endphp
                                @foreach($allPembayaran as $pembayaran)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$pembayaran->anggota->nomor_anggota or 'kosong'}}</td>
                                        <td>{{$pembayaran->anggota->nama or 'kosong'}}</td>
                                        <td>{{$pembayaran->jenis_pembayaran->nama or 'kosong'}}</td>
                                        <td>{{rupiah($pembayaran->jumlah)}}</td>
                                        <td>{{DateIndo($pembayaran->tanggal_tempo)}}</td>
                                        <td>{{DateIndo($pembayaran->tanggal_pembayaran)}}</td>
                                        <td>
                                            @if($pembayaran->status == 1)
                                            <a href="{{ url('admin/ubah/pembayaran/'.$pembayaran->id.'/0') }}" class="yes" data-user="{{ $pembayaran->id }}" desc="Yakin batalkan validasi?">
                                                <input checked type="checkbox" class="sip" get-user="{{ $pembayaran->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                            </a>
                                            @else
                                            <a href="{{ url('admin/ubah/pembayaran/'.$pembayaran->id.'/1') }}" class="yes" data-user="{{ $pembayaran->id }}" desc="Yakin ingin memvalidasi?">
                                                <input type="checkbox" class="sip" get-user="{{ $pembayaran->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="portfolioContainer">
                                                <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                     <a href="{{ asset($pembayaran->file_resi)}}" class="image-popup" title="Resi Pembayaran {{$pembayaran->anggota->nomor_anggota}}">
                                                        <button class="btn btn-warning waves-effect waves-light"><i class="fa fa-eye m-r-5"></i>
                                                        <span>Resi</span></button></a>
                                                </div> 
                                            </div>     
                                        </td>
                                        <td>
                                            {{ Form::open(array('route' => array('pembayaran.destroy', $pembayaran->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('pembayaran.edit', $pembayaran->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('pembayaran.show', $pembayaran->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <!-- Render Pagination -->
                        {!! $allPembayaran->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div><!-- End Right content here -->
@stop
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="{{ asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" /> --}}
  <link href="{{ asset('assets/assets/css/papper.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/assets/css/laporan.css')}}" rel="stylesheet" type="text/css" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="{{asset('assets/assets/css/pdfview.css')}}" media="print" rel="stylesheet" type="text/css">
	<title>Laporan Data Omset & Anggota</title>
  
  <style>
      @page {
          size: A4;
          margin: 0.5cm 0.5cm;
      }
      @media print {
          article {
              margin: 0px 15px;
          }
          @page {
              size: A4;
              margin: 0.5cm 0.5cm;
          }
      }
      .font-size-13 {
          font-size: 13px;
      }
      .font-size-16 {
          font-size: 16px;
      }
      .font-size-18 {
          font-size: 18px;
      }
      .font-size-20 {
          font-size: 20px;
      }
      .row-border-bottom-dotted {
          border-bottom : 2px dotted black;
      }
      .row-border {
          border: 2px black solid;
          padding-bottom: 10px;
      }
      .row-border-right {
          border-right: 2px black solid;
      }
      .row-border-left {
          border-left: 2px black solid;
      }
      .row-border-ex-top {
          border: 2px black solid;
          border-top: hidden;
      }
      .row-border-ex-top-bottom {
          border: 2px black solid;
          border-bottom: hidden;
          border-top: hidden;
      }
      .row-border-ex-left-right {
          border: 2px black solid;
          border-left: hidden;
          border-right: hidden;
      }
      .padding-10 {
          padding: 10px 20px;
      }
      .padding-top-5 {
          padding-top: 5px;
      }
      .padding-top-10 {
          padding-top: 10px;
      }
      .padding-top-15 {
          padding-top: 15px;
      }
      .padding-top-20 {
          padding-top: 20px;
      }
      .padding-top-40 {
          padding-top: 40px;
      }
      .padding-bottom-min-5 {
          padding-bottom: -5px;
      }
      .padding-bottom-7-5 {
          padding-bottom: 7.5px;
      }
      .padding-bottom-18 {
          padding-bottom: 18px;
      }
      .padding-bottom-10 {
          padding-bottom: 10px;
      }
      .padding-bottom-25 {
          padding-bottom: 25px;
      }
      .padding-left-40 {
          padding-left: 40px;
      }
      .bordered {
          border: 2px solid black;
      }
      .border-checkbox {
          border: 1px solid black;
          width: 50px !important;
          height: 25px;
      }
      .padding-top-bottom-40 {
          padding: 40px 5px;
      }
      .text-center {
        text-align: center;
      }
  </style>

</head>
<body class="container">
    <section class="sheet padding-0mm">
        <article>
             <div class="row padding-top-5">

                <div class="col-12">
                  <div class="row padding-bottom-min-5">
                    <div class="col-12 text-center">
                      <b class="font-size-20">Laporan Data Omset</b>
                    </div>
                  </div>
                </div>

                <div class="col-12">
                  <div class="row padding-bottom-min-5">
                    <div class="col-12 text-center">
                      <b class="font-size-18">Per Bulan {{ $bulan_awal }} - {{ $bulan_akhir }}</b>
                    </div>
                  </div>
                </div>

                <hr>

                <div class="col-12">
                  <div class="col-xs-3">
                    
                  </div>
                  <div class="col-xs-6">
                    <div class="form-group row">
                      <label for="" class="col-xs-6">Total Omset</label>
                      <label for="" class="col-xs-6">: Rp. {{ number_format($items['total_omset']) }} (100%)</label>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-xs-6">Total Pembelian Anggota</label>
                      <label for="" class="col-xs-6">: Rp. {{ number_format($items['total_anggota']) }} ({{ $items['persen_anggota'] }}%)</label>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-xs-6">Total Pembelian Non Anggota</label>
                      <label for="" class="col-xs-6">: Rp. {{ number_format($items['total_non_anggota']) }} ({{ $items['persen_non_anggota'] }}%)</label>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    
                  </div>
                </div>

                <div class="col-12">
                  <div class=" col-xs-6">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hovered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Bulan</th>
                            <th style="text-align: right;">Total Omset</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($items['omset'] as $key => $data_omset)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $data_omset['bulan'] }}</td>
                              <td style="text-align: right;">{{ number_format($data_omset['total_omset']) }}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div class=" col-xs-6">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hovered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nomor Anggota</th>
                            <th>Nama Anggota</th>
                            <th style="text-align: right;">Jumlah Pembelian</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($items['anggota'] as $key => $data_anggota)
                              <tr>
                                  <td>{{ $loop->iteration }}</td>
                                  <td>{{ $data_anggota['nomer'] }}</td>
                                  <td>{{ $data_anggota['nama'] }}</td>
                                  <td style="text-align: right;">{{ number_format($data_anggota['jml']) }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>

             </div>
        </article>
    </section>
</body>
  <script type="text/javascript" src="{{asset('component/jquery/dist/jquery.min.js')}}"></script>
  <script>
    function PagePrint() 
    {
      window.print();
    }
  </script>
</html>


<div class="form-group">
     <label class="col-md-2 control-label">Nama Jabatan</label>
     <div class="col-md-10">
        {!! Form::text('posisi',null,['class'=>'form-control','placeholder'=>"Nama Jabatan",'required']) !!}
     </div>
</div>
<div class="form-group">
     <label class="col-md-2 control-label">Deskripsi</label>
     <div class="col-md-10">
        {!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi",'required']) !!}
     </div>
</div>
<div class="form-group">
     <label class="col-md-2 control-label">Tupoksi</label>
     <div class="col-md-10">
        {!! Form::textarea('tupoksi',null,['class'=>'form-control','placeholder'=>"Tupoksi",'required']) !!}
     </div>
</div>
@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                  <h3 class="panel-title"> DETAIL JABATAN</h3>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                            <td>Nama Jabatan</td>
                                            <td>:</td>
                                            <td>{{$jabatan->posisi}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Deskripsi</td>
                                            <td>:</td>
                                            <td>{{$jabatan->deskripsi}}</td>
                                        </tr>

                                        <tr>
                                            <td>Tupoksi</td>
                                            <td>:</td>
                                            <td>{{$jabatan->tupoksi}}</td>
                                        </tr>

                                        <tr>
                                            <td class="col-xs-4">Create at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$jabatan->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td class="col-xs-4">Update at</td>
                                            <td class="col-xs-1">:</td>
                                            <td>{{$jabatan->updated_at}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                             <a href="{{route('jabatan.index')}}">
                                                 <button class="btn btn-warning waves-effect waves-light">
                                                    <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                 </button></a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
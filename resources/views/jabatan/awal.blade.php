@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('jabatan.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li>
                                                <a href="#">Kepengurusan</a>
                                            </li>
                                            <li class="active">
                                                Jabatan
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL JABATAN</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('posisi',null,['class'=>'form-control col-md-3','placeholder'=>"Jabatan"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
            
                        <table id="datatable" class="table table-striped table-bordered">

                           	<!-- Alert -->
								@yield('alert')
							
                            <thead>
                                <tr>
                                    <th width="10%" scope="row">No</th>
                                    <th width="30%" scope="row">Jabatan</th>
                                    <th width="30%" scope="row">Deskripsi</th>
                                    <th width="30%" scope="row">Tupoksi</th>
                                    <th width="30%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaJabatan as $jabatan)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$jabatan->posisi or 'kosong'}}</td>
                                        <td>{{$jabatan->deskripsi or 'kosong'}}</td>
                                        <td>{{$jabatan->tupoksi or 'kosong'}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('jabatan.destroy', $jabatan->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('jabatan.edit', $jabatan->id)}}">Ubah</a></li>
                                                        {{-- <li><a href="{{route('jabatan.show', $jabatan->id)}}">Lihat</a></li> --}}
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaJabatan->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
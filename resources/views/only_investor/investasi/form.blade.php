   
{{-- <div class="form-group">
    <br>
    <div class="container">
        @if(Session::has('informasi'))
            <div class="alert alert-danger">
                <strong>Informasi :</strong>
                {{Session::get('informasi')}}
            </div>
        @endif
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div> --}}
<div class="form-group">
    <label class="col-md-2 control-label">Tanggal Pembayaran</label>
    <div class="col-md-10">
        <div class="input-group">
        <input type="text" name="tanggal_pembayaran" value="{{ old('tanggal_pembayaran') }}" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" required>
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
        </div>
    </div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-10">
		{!! Form::text('jumlah',old('jumlah'),['class'=>'form-control','placeholder'=>"Jumlah",'required']) !!}
	</div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Bukti Pembayaran</label>
    <div class="col-md-10">
        <input name = "foto_resi" type="file" class="filestyle" required="required">	
    </div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Jenis Investasi</label>
	<div class="col-md-10">
		 {{ Form::select('jenis_investasi_id', $jenisInvestasiPluck, null, ['class' => 'form-control']) }}
	</div>
</div>
@extends('master_investor')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('investasis.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Investor</a>
                                            </li>
                                            <li class="active">
                                                Investasi
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <center><h4 class="m-t-0"><b>TABEL INVESTASI</b></h4></center>
                        <table id="datatable" class="table table-striped table-bordered">

                            @if(Session::has('informasi'))
                                <div class="alert alert-info">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="20%" scope="row">Investasi</th>
                                    <th width="15%" scope="row">Jumlah Pembayaran</th>
                                    <th width="15%" scope="row">Tanggal Pembayaran</th>
                                    <th width="20%" scope="row">Status</th>
                                    <th width="20%" scope="row">Bukti Pembayaran</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaInvestasi as $investasi)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$investasi->jenis_investasi->nama or 'kosong'}}</td>
                                        <td>{{rupiah($investasi->jumlah)}}</td>
                                        <td>{{DateIndo($investasi->tanggal_pembayaran)}}</td>
                                        @if($investasi->status == 1)
                                            <td>{{"Sudah tervalidasi"}}</td>
                                            @else
                                            <td>{{"Belum tervalidasi"}}</td>
                                            @endif

                                            <td>
                                                <div class="portfolioContainer">
                                                    <div class="col-sm-6 col-lg-3 col-md-4 graphicdesign illustrator photography">
                                                        <a href="{{ asset($investasi->foto_resi)}}" class="image-popup" title="Resi pembayaran {{$investasi->foto_resi}}">
                                                        <button class="btn btn-warning waves-effect waves-light">
                                                            <i class="fa fa-eye m-r-5"></i>
                                                            <span>Resi</span>
                                                        </button>
                                                        </a>
                                                    </div> 
                                                </div>     
                                            </td>
                                    </tr>
                                @endforeach
                              
                            </tbody>
                                    <tr>
                                        <td></td>
                                        <td>Jumlah Total</td>
                                        <td colspan = "5">{{rupiah($total)}}</td>
                                    </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div><!-- End Right content here -->
@stop
@extends('master_investor')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('investasis.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i></h3></a>
						</div>
						<div class="panel-body">
								@if(Session::has('informasi'))
									<div class="alert alert-danger">
										<strong>Informasi :</strong>
										{{Session::get('informasi')}}
									</div>
									@endif
								@if(count($errors) > 0)
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif  
								<center><h4 class="m-t-0"><b>TAMBAH INVESTASI</b></h4></center>
								<br>
							{!! Form::open(['route'=>'investasis.store','class'=>'form-horizontal','files'=>true]) !!}
							@include('only_investor.investasi.form')
							  <div class="form-group">
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12">
												<div class="col-md-offset-9 text-right">
													<button type="reset" class="btn btn-danger waves-effect waves-light">
															<span class="btn-label"><i class="fa fa-repeat"></i>
															</span>Reset
													</button>
													<button type="submit" class="btn btn-default waves-effect waves-light">
															<i class="fa fa-save m-r-5"></i>
															<span>Simpan</span>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
</div><!-- End Right content here -->
@stop
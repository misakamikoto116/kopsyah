<div class="form-group">
<br>
<div class="container">
        @if(Session::has('informasi'))
<div class="alert alert-danger">
        <strong>Informasi :</strong>
        {{Session::get('informasi')}}
</div>
@endif
@if(count($errors) > 0)
<div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
</div>
@endif
        </div>
</div>

<div class="form-group">
        <label class="col-md-2 control-label">No KTP</label>
        <div class="col-md-10">
           <input  name="no_ktp" type="number" class="form-control" placeholder="No KTP" value="{{$anggota->no_ktp}}">
        </div>   
   </div>

   <div class="form-group">
        <label class="col-md-2 control-label">Upload KTP</label>
        <div class="col-md-8">
            <input name = "file_ktp" type="file" class="filestyle">
        </div>
        <div>
                <img id="preview" style="margin-bottom: 5px;" src="{{ asset($anggota->file_ktp) }}" height="70" alt="">
        </div>
   </div>
   
   <div class="form-group">
        <label class="col-md-2 control-label">Nama</label>
        <div class="col-md-10">
           <input  name="nama" type="text" class="form-control" placeholder="Nama" required="required" value="{{ $anggota->nama}}">
        </div>
       
   </div>

   <div class="form-group">
        <label class="col-md-2 control-label">Upload Foto</label>
        <div class="col-md-8">
            <input name = "file_foto" type="file" class="filestyle" >
        </div>
        <div>
                <img id="preview" style="margin-bottom: 5px;" src="{{ asset($anggota->file_foto) }}" height="70" alt="">
        </div>
   
   </div>


    <div class="form-group">
       <label class="col-sm-2 control-label">Jenis Kelamin</label>
       <div class="col-sm-10">
         <select class="form-control" name="jenis_kelamin" required="required">
                <option value="{{ $anggota->jenis_kelamin}}">
                        @if ($anggota->jenis_kelamin == "L")
                        Laki-Laki</option>
                        @elseif  ($anggota->jenis_kelamin == "P")
                        Perempuan</option>
                        @endif
                        <option value ="L">Laki-Laki</option>
                        <option value ="P">Perempuan</option>	
         </select>
       </div>
    </div>
   
 
    <div class="form-group">
        <label class="col-md-2 control-label">Agama</label>
        <div class="col-md-10">
        <input  name="agama" type="text" class="form-control" placeholder="Islam" disabled=""  value="{{$anggota->agama}}">
        </div>
</div>
   
   <div class="form-group">
        <label class="col-md-2 control-label">Tempat Lahir</label>
        <div class="col-md-10">
           <input  name="tempat_lahir" type="text" class="form-control" placeholder="Tempat Lahir" required="required" value="{{ $anggota->tempat_lahir }}">
        </div>
   </div>
   
   <div class="form-group">
        <label class="col-md-2 control-label">Tanggal Lahir</label>
        <div class="col-md-10">
                <div class="input-group">
                        @php
			$tgl_conv = date('Y-m-d', strtotime($anggota->tanggal_lahir));
                        $tgl_jadi = date('d-m-Y', strtotime($tgl_conv));
			@endphp
                <input type="text" name="tanggal_lahir" value="{{$tgl_jadi}}" id="tgl_lahir" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
                <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                </div>
        </div>
</div>
   
    
   <div class="form-group">
        <label class="col-md-2 control-label">Telepon</label>
        <div class="col-md-10">
           {{-- <input name="no_telepon" type="text" data-mask="9999-9999-9999" class="form-control" required="required" value="{{ $anggota->no_telepon }}"> --}}
            <input name="no_telepon" value="{{ $anggota->no_telepon }}" type="number" placeholder="08xxxxxxxxxx" class="form-control" required="required">
        </div>
   </div>
   
   <div class="form-group">
                   <label class="col-sm-2 control-label">Kota</label>
                   <div class="col-sm-10">
                           <select class="form-control" name="kota" required="required">
                            <option value="{{ $anggota->kota}}">
                                @if ($anggota->kota == 1)
                                Samarinda</option>
                                @elseif  ($anggota->kota == 2)
                                Luar Samarinda</option>
                                @endif
                                <option value ="1">Samarinda</option>
                                <option value ="2">Luar Samarinda</option>
                           </select>
                   </div>
           </div>
   
   <div class="form-group">
                   <label class="col-md-2 control-label">Alamat</label>
                   <div class="col-md-10">
                      <input  name="alamat" type="text" class="form-control" placeholder="Alamat" required="required" value="{{ $anggota->alamat }}">
                   </div>
   </div>           
   
   <div class="form-group">
                   <label class="col-sm-2 control-label">Pekerjaan</label>
                   <div class="col-sm-10">
                           <select class="form-control" name="pekerjaan" required="required">
                                <option value="{{ $anggota->pekerjaan }}">
                                        @if ($anggota->pekerjaan == 1)
                                        PNS</option>
                                        @elseif  ($anggota->pekerjaan == 2)
                                        Wiraswasta</option>
                                        @elseif ($anggota->pekerjaan == 3)
                                        Karyawan</option>
                                        @elseif($anggota->pekerjaan == 4)
                                        Pensiunan</option>
                                        @elseif ($anggota->pekerjaan == 5)
                                        Petani</option>
                                        @elseif($anggota->pekerjaan == 6)
                                        Lainnya</option>
                                        @endif
                                        <option value ="1">PNS</option>
                                        <option value ="2">Wiraswasta</option>
                                        <option value ="3">Karyawan</option>
                                        <option value ="4">Pensiunan</option>
                                        <option value ="5">Petani</option>
                                        <option value ="6">Lainnya</option>
                           </select>
                   </div>
           </div>
   
           
           <div class="form-group">
                           <label class="col-sm-2 control-label">Pendapatan</label>
                           <div class="col-sm-10">
                                   <select class="form-control" name="penghasilan">
                                    <option value="{{ $anggota->penghasilan }}">    
                                        @if ($anggota->penghasilan == 1)
                                       -</option>
                                        @elseif ($anggota->penghasilan == 2)
                                        < Rp 500.000,00</option>
                                        @elseif  ($anggota->penghasilan == 3)
                                        Rp 500.000,00 - Rp 1.000.000,00</option>
                                        @elseif ($anggota->penghasilan == 4)
                                        Rp 1.000.000,00 - Rp 2.000.000,00</option>
                                        @elseif($anggota->penghasilan == 5)
                                        Rp 2.000.000,00 - Rp 3.000.000,00</option>
                                        @elseif ($anggota->penghasilan == 6)
                                        Rp 3.000.000,00 - Rp 4.000.000,00</option>
                                        @elseif($anggota->penghasilan == 7)
                                        Rp 4.000.000,00 - Rp 5.000.000,00</option>
                                        @elseif ($anggota->penghasilan == 8)
                                        Rp 5.000.000,00 - Rp 6.000.000,00</option>
                                        @elseif($anggota->penghasilan == 9)
                                        > Rp 6.000.000,00</option>
                                        @endif
                                           <option value ="1">-</option>
                                           <option value ="2">< Rp 500.000,00</option>
                                           <option value ="3">Rp 500.000,00 - Rp 1.000.000,00</option>
                                           <option value ="4">Rp 1.000.000,00 - Rp 2.000.000,00</option>
                                           <option value ="5">Rp 2.000.000,00 - Rp 3.000.000,00</option>
                                           <option value ="6">Rp 3.000.000,00 - Rp 4.000.000,00</option>
                                           <option value ="7">Rp 4.000.000,00 - Rp 5.000.000,00</option>
                                           <option value ="8">Rp 5.000.000,00 - Rp 6.000.000,00</option>
                                           <option value ="9">> Rp 6.000.000,00</option>
                                   </select>
                           </div>
                   </div>
   
                   

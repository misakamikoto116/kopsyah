@extends('master_investor')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">EDIT PASSWORD</h3></a>
						</div>
						@if(Session::has('informasi'))
                                <div class="alert alert-danger">
                                    <strong>Informasi :</strong>
                                    {{Session::get('informasi')}}
                                </div>
                                @endif
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<div class="panel-body">
						{{ Form::model($anggota, array('route' => array('password.update', $anggota->users_id), 'files' => true, 'method' => 'PUT','class'=>'form-horizontal')) }}
						<div class="form-group">
									<label class="col-md-2 control-label">Password Lama</label>
									<div class="col-md-10">
										<input type="password"  class="form-control"  name="password_old"  placeholder="Password Lama" required="required">
									</div>
								</div>
							 <div class="form-group">
									<label class="col-md-2 control-label">Password Baru</label>
									<div class="col-md-10">
										<input type="password"  class="form-control"  name="password"  placeholder="Password Baru" required="required">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">Konfirmasi Password</label>
									<div class="col-md-10">
										<input type="password"  class="form-control"  name="password_confirmation"  placeholder="Konfiramsi Password" required="required">
									</div>
								</div>
								 <div class="form-group">
										<div class="col-md-12">
												<div class="col-md-offset-9 text-right">
											   <button type="submit" class="btn btn-default waves-effect waves-light">
													   <i class="md-autorenew m-r-5"></i>
													   <span>Perbarui</span>
											   </button>
												</div>
								{!! Form::close() !!}
											 </div>
										</div>
								   </div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
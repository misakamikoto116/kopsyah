
        
<div class="form-group">
	<label class="col-md-2 control-label">Tanggal Pembayaran</label>
	<div class="col-md-10">
		<div class="input-group">
		<input type="text" name="tanggal_pembayaran" value="{{ old('tanggal_pembayaran' )}}" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" required>
			<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
		</div>
	</div>
</div>
		
<div class="form-group">
	<label class="col-md-2 control-label">Bukti Pembayaran</label>
	<div class="col-md-10">
		<input name = "file_resi" value="{{ old('file_resi') }}" type="file" class="filestyle" required>	
	</div>
</div>

<div class="form-group">
	<label class="col-md-2 control-label">Jenis Simpanan</label>
	<div class="col-md-10">
			{{ Form::select('jenis_pembayaran_id', $jenisPembayaranPluck, null, ['name'=> 'jbayar','class' => 'form-control']) }}
	</div>
</div>

<div id="nomP" class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-10">
		{!! Form::text('jumlahP',$jenisPokok->nominal,['readonly'=>'readonly','class'=>'form-control','placeholder'=>"Jumlah"]) !!}
	</div>
</div>

<div id="nomW" class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-10">
		{!! Form::text('jumlahW',$jenisWajib->nominal,['readonly'=>'readonly','class'=>'form-control','placeholder'=>"Jumlah"]) !!}
	</div>
</div>

<div id="nomK" class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-10">
		{!! Form::text('jumlah',null,['class'=>'form-control','placeholder'=>"Jumlah"]) !!}
	</div>
</div>


	
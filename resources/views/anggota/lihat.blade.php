@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-color panel-primary">
                            <div class="panel-heading">
                                    
                                    <h3 class="panel-title"><a href="{{route('anggota.index')}}" class="fa fa-arrow-left"></a> PROFIL ANGGOTA</h3>
                            </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="card-box">
                                <div class="widget-chart text-center">
                                    <div class="graphicdesign illustrator photography">
                                        <a href="{{ asset($anggota->file_foto)}}" class="image-popup" title="Profil {{$anggota->file_foto}}">
                                            <img id="preview" style="margin:10px 0 0 0px; border-radius:50%; height:104px; width:108px" src="{{ asset($anggota->file_foto) }}" alt="" >
                                        </a>
                                    </div>
                                    {{-- <img id="preview" style="margin:10px 0 0 0px; border-radius:50%; height:104px; width:108px" src="{{ asset($anggota->file_foto) }}" alt="" > --}}
                                    <h4 class="font-400">{{$anggota->nama}}</h4>
                                    <h5 class="font-300">{{$anggota->nomor_anggota}}</h5>
                                </div>
                            </div>

                            <div class="card-box">
                                <div class="widget-chart ">
                                    <h6 class="text-dark header-title m-t-0" style="font-size:14px;">Data Keanggotaan</h6>
                                    <table class="table" style="font-size:12px; margin-bottom:7px;">
                                        <ul class="list-inline m-t-15">                
                                            <tr>
                                                <td>Status</td>
                                                <td>:</td>
                                                @if($anggota->status_keanggotaan == 2)
                                                <td>{{'Anggota'}}</td>
                                                @else
                                                <td>{{'Anggota Investor'}}</td>
                                                @endif
                                            </tr>

                                            <tr>
                                                <td>Status Aktivasi</td>
                                                <td>:</td>
                                                @if($anggota->status_aktifasi == 0)
                                                <td>{{'Tidak Aktif'}}</td>
                                                @else
                                                <td>{{'Aktif'}}</td>
                                                @endif
                                            </tr>

                                            <tr>
                                                <td class="col-xs-4">Bergabung</td>
                                                <td class="col-xs-1">:</td>
                                                <td>{{indonesian_date($anggota->created_at)}}</td>
                                            </tr>

                                            <tr>
                                                <td class="col-xs-4">Last Update</td>
                                                <td class="col-xs-1">:</td>
                                                <td>{{indonesian_date($anggota->updated_at)}}</td>
                                            </tr> 
                                        </ul>
                                    </table>
                                </div>
                            </div> 
                        </div><!-- End col-lg-4 here -->

                        <div class="col-lg-8">
                            <div class="card-box">
                                <!-- Alert -->
                                @if(Session::has('informasi'))
                                    <div class="alert alert-info">
                                        <strong>Informasi :</strong>
                                        {{Session::get('informasi')}}
                                    </div>
                                @endif
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <h4 class="text-dark header-title m-t-0">Data Diri</h4>
                                <table class="table">
                                  
                                    <tr>
                                        <td>Nomor Anggota</td>
                                        <td>:</td>
                                        <td>{{$anggota->nomor_anggota}}</td>
                                    </tr>

                                    <tr>
                                        <td>Nomor KTP</td>
                                        <td>:</td>
                                        <td>
                                            <div class="portfolioContainer">
                                                <div class="graphicdesign illustrator photography">
                                                    {{$anggota->no_ktp}}  &nbsp;&nbsp;
                                                    <a href="{{ asset($anggota->file_ktp)}}" class="image-popup" title="Foto KTP">
                                                    <i class="fa fa-picture-o m-r-5 "></i>
                                                    </a>
                                                </div> 
                                            </div> 
                                        </td> 
                                    </tr>

                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{$anggota->nama}}</td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:</td>
                                        @if($anggota->jenis_kelamin == 'L')
                                            <td>{{'Laki-Laki'}}</td>
                                        @else
                                            <td>{{'Perempuan'}}</td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <td>Agama</td>
                                        <td>:</td>
                                        <td>{{$anggota->agama}}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Tempat/Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{{$anggota->tempat_lahir}} / {{DateIndo($anggota->tanggal_lahir)}}</td>
                                    </tr>

                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>{{$anggota->no_telepon}}</td>
                                    </tr> 

                                    <tr>
                                        <td>Kota</td>
                                        <td>:</td>
                                        @if($anggota->kota == 1)
                                            <td>{{'Samarinda'}} | <span class="alert-danger">Anggota Biasa</span></td>
                                        @else
                                            <td>{{'Luar Samarinda'}} | <span class="alert-danger">Anggota Luar Biasa</span></td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{$anggota->alamat}}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td>:</td>
                                        @if($anggota->pekerjaan == 1)
                                            <td>{{'PNS'}}</td>
                                        @elseif ($anggota->pekerjaan == 2)
                                            <td>{{'Wiraswasta'}}</td>
                                        @elseif ($anggota->pekerjaan == 3)
                                            <td>{{'Karyawan'}}</td>
                                        @elseif ($anggota->pekerjaan == 4)
                                            <td>{{'Pensiunan'}}</td>
                                        @elseif ($anggota->pekerjaan == 5)
                                            <td>{{'Petani'}}</td>
                                        @elseif ($anggota->pekerjaan == 6)
                                            <td>{{'Lainnya'}}</td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <td>Pendapatan</td>
                                        <td>:</td>
                                        @if($anggota->penghasilan == 1)
                                            <td>{{'-'}}</td>
                                        @elseif ($anggota->penghasilan == 2)
                                            <td>{{'< Rp 500.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 3)
                                            <td>{{'Rp 500.000,00 - Rp 1.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 4)
                                            <td>{{'Rp 1.000.000,00 - Rp 2.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 5)
                                            <td>{{'Rp 2.000.000,00 - Rp 3.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 6)
                                            <td>{{'Rp 3.000.000,00 - Rp 4.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 7)
                                            <td>{{'Rp 4.000.000,00 - Rp 5.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 8)
                                            <td>{{'Rp 5.000.000,00 - Rp 6.000.000,00'}}</td>
                                        @elseif ($anggota->penghasilan == 9)
                                            <td>{{'> Rp 6.000.000,00'}}</td>
                                        @endif
                                    </tr>
                                    @foreach ($anggota->investasi as $investasi)
                                        <tr>
                                            <td>
                                                Biaya Cetak Kartu dan Sertifikat {{ $investasi->jenis_investasi->nama }} 
                                            </td>
                                            <td>:</td>
                                            <td>Rp {{ number_format($investasi->biaya_cetak_investasi) }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div><!-- End card-box here -->
                        </div><!-- End col-lg-8 here -->

                    </div><!-- End Row here -->

                </div><!-- End col-md-12 here -->
            </div><!-- End Row here -->
        </div> <!-- container -->
    </div><!-- End content here -->
@stop
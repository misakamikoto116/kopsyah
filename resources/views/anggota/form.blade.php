        <div class="panel-heading">
            <h5><label>INFORMASI UMUM</label></h5>
        </div>
        <div class="container">      
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">No KTP<span class="text-danger">*</label>
                    <div class="col-md-12">
                       <input  name="no_ktp" value="{{old('no_ktp')}}" type="number" class="form-control" placeholder="No KTP" required="required">
                    </div>
                </div>
            </div>
    
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Upload KTP<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <input name = "file_ktp" type="file" class="filestyle" required="required">
                    </div>
                </div>
            </div>
                
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Nama<span class="text-danger">*</label>
                    <div class="col-md-12">
                       <input  name="nama" value="{{ old('nama')}}" type="text" class="form-control" placeholder="Nama" required="required">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Upload Foto<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <input name = "file_foto" type="file" class="filestyle" required="required">
                    </div>
                </div>
            </div>
                
            <div class="form-group">
                <div class="container"> 
                    <label class="col-md-2">Jenis Kelamin<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <select class="form-control" name="jenis_kelamin" required="required">
                            <option value ="L" {{ old('jenis_kelamin') == "L" ? 'selected' : '' }} >Laki-Laki</option>
                            <option value ="P" {{ old('jenis_kelamin') == "P" ? 'selected' : '' }}>Perempuan</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Agama</label>
                    <div class="col-md-12">
                        <input  name="agama" type="text" class="form-control" placeholder="Islam" required="required" disabled="">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Tempat Lahir<span class="text-danger">*</label>
                    <div class="col-md-12">
                       <input  name="tempat_lahir" value="{{old('tempat_lahir')}}" type="text" class="form-control" placeholder="Tempat Lahir" required="required">
                    </div>
                </div>
            </div>
                
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Tanggal Lahir<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" name="tanggal_lahir" value="{{old('tanggal_lahir')}}" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" required>
                            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                        </div>
                    </div>
                </div>
            </div>
             
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Telepon<span class="text-danger">*</label>
                    <div class="col-md-12">
                      <input name="no_telepon" value="{{ old('no_telepon') }}" type="number" placeholder="08xxxxxxxxxx" class="form-control" required="required">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Kota<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <select class="form-control" name="kota" required="required">
                            <option value ="1" {{ old('kota') == 1 ? 'selected' : '' }} >Samarinda</option>
                            <option value ="2" {{ old('kota') == 2 ? 'selected' : '' }} >Luar Samarinda</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Alamat<span class="text-danger">*</label>
                    <div class="col-md-12">
                      <input  name="alamat" value="{{ old('alamat') }}" type="text" class="form-control" placeholder="Alamat" required="required">
                    </div>
                </div>
            </div>           
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Pekerjaan<span class="text-danger">*</label>
                    <div class="col-md-12">
                        <select class="form-control" name="pekerjaan" required="required">
                            <option value ="1" {{ old('pekerjaan') == 1 ? 'selected' : '' }} >PNS</option>
                            <option value ="2" {{ old('pekerjaan') == 2 ? 'selected' : '' }} >Wiraswasta</option>
                            <option value ="3" {{ old('pekerjaan') == 3 ? 'selected' : '' }} >Karyawan</option>
                            <option value ="4" {{ old('pekerjaan') == 4 ? 'selected' : '' }} >Pensiunan</option>
                            <option value ="5" {{ old('pekerjaan') == 5 ? 'selected' : '' }} >Petani</option>
                            <option value ="6" {{ old('pekerjaan') == 6 ? 'selected' : '' }} >Lainnya</option>
                            <option value ="7" {{ old('pekerjaan') == 7 ? 'selected' : '' }} >Guru</option>
                            <option value ="8" {{ old('pekerjaan') == 8 ? 'selected' : '' }} >Dokter</option>
                            <option value ="9" {{ old('pekerjaan') == 9 ? 'selected' : '' }} >Dosen</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="container">
                    <label class="col-md-2">Pendapatan</label>
                    <div class="col-md-12">
                        <select class="form-control" name="penghasilan" 
                        {{-- value="{{ old('pendapatan') }} " --}}
                        >
                            <option value ="1" {{ old('penghasilan') == 1 ? 'selected' : '' }} ></option>
                            <option value ="2" {{ old('penghasilan') == 2 ? 'selected' : '' }} >< Rp 500.000,00</option>
                            <option value ="3" {{ old('penghasilan') == 3 ? 'selected' : '' }} >Rp 500.000,00 - Rp 1.000.000,00</option>
                            <option value ="4" {{ old('penghasilan') == 4 ? 'selected' : '' }} >Rp 1.000.000,00 - Rp 2.000.000,00</option>
                            <option value ="5" {{ old('penghasilan') == 5 ? 'selected' : '' }} >Rp 2.000.000,00 - Rp 3.000.000,00</option>
                            <option value ="6" {{ old('penghasilan') == 6 ? 'selected' : '' }} >Rp 3.000.000,00 - Rp 4.000.000,00</option>
                            <option value ="7" {{ old('penghasilan') == 7 ? 'selected' : '' }} >Rp 4.000.000,00 - Rp 5.000.000,00</option>
                            <option value ="8" {{ old('penghasilan') == 8 ? 'selected' : '' }} >Rp 5.000.000,00 - Rp 6.000.000,00</option>
                            <option value ="9" {{ old('penghasilan') == 9 ? 'selected' : '' }} >> Rp 6.000.000,00</option>
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- End Container here -->
             <hr>               
            <div class="panel-heading">
                <h5><label>KEANGGOTAAN</label></h5>
            </div>
            <div class="container">
                <div class="form-group">
                    <div class="container">
                        <label class="col-md-12">Status Keanggotaan<span class="text-danger">*</label>
                        <div class="col-md-12">
                            <select class="form-control" name="role_id" required="required">
                                {{-- <option value ="">----- Pilih -----</option> --}}
                                {{-- <option value ="2">Anggota</option> --}}
                                <option value ="3" selected="">Anggota dan Investor</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div id="anggota_investor">
            <div class="panel-heading">
                <h5><label>SIMPANAN</label></h5>
            </div> --}}
            {{-- <div class="container">
                <div class="container">
                    <div>
                        <strong>Rekening : Bank Syariah Mandiri 7115634385 a.n Kopsyah SMS </strong>
                    </div>
                </div>
                <br>

            @foreach($jenis_pembayaran as $pembayaran)
                <div class="form-group">
                    @if($pembayaran->nominal != 0)
                    <div class="container">
                        <label class="col-md-2">{{$pembayaran->nama}}<span class="text-danger">*</label>
                        <div class="col-md-12">
                            {!! Form::text('pembayaran['.$pembayaran->id.']',$pembayaran->nominal,['class'=>'form-control','placeholder'=>"Jumlah",'readonly']) !!}
                        </div>
                
                    @else
                        <div class="container">
                            <label class="col-md-2">{{$pembayaran->nama}}<span class="text-danger">*</label>
                            <div class="col-md-12">
                                {!! Form::text('pembayaran['.$pembayaran->id.']',0,['class'=>'form-control','placeholder'=>"Jumlah"]) !!}
                            </div>
                
                    @endif
                        </div>
                    </div>
            @endforeach

                    <div class="form-group">
                        <div class="container">
                            <label class="col-md-1">Tanggal Transfer<span class="text-danger">*</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <input type="text" name="tanggal_pembayaran" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" value="{{old('tanggal_pembayaran')}}" required>
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                    
                            <label class="col-md-1">Resi <span class="text-danger">*</label>
                                <div class="col-md-3">
                                    <input name = "file_resi" type="file" class="filestyle" required="required">
                                </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Container here -->
            
            <hr> --}}
            <div id="Investasi">
                <div class="panel-heading">
                    <h4 class="panel-title"><b>Investasi</b></h4>
                </div>
                <br>

                <div class="container">
                @foreach ($jenis_investasi as $investasi)
                    @if($investasi->status != 1)
                    @php
                        if($investasi->status == '1'){
                            $sts= 'sudah penuh';
                        }else{
                            $sts= 'belum penuh';
                        }
                    @endphp
                <div class="container">
                    <div>
                        <strong>{{$investasi->nama}}</strong>
                        <h6>Rekening : {{$investasi->deskripsi}} <span class="text-danger"> ({{$sts}}) </span>
                        </h6>
                    </div>
                </div>

                    <div class="form-group">
                        <div class="container">
                            <label class="col-md-1">Jumlah<span class="text-danger">*</label>
                            <div class="col-md-3">
                            {{ Form::text('investasi['.$investasi->id.']',0,['class'=>'form-control','placeholder'=>"Jumlah"]) }}
                            </div>
                    
                            <label class="col-md-1">Tanggal Transfer<span class="text-danger">*</label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" id="tgl_inv" name="tanggal_investasi[{{$investasi->id}}]" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun">
                                    <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                    
                            <label class="col-md-1">Resi<span class="text-danger">*</label>
                            <div class="col-md-3">
                                <input name = "foto_resi[{{$investasi->id}}]" type="file" class="filestyle">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="container">
                            <label class="col-md-1">Biaya cetak kartu & sertifikat</label>
                            <div class="col-md-3">
                                {{ Form::text('biaya_cetak_investasi['.$investasi->id.']', 50000,['class'=>'form-control','placeholder'=>"Biaya cetak kartu & sertifikat","readonly"]) }}
                            </div>
                        </div>
                    </div>  
                    <hr>  
                    @endif 
                @endforeach 
                </div><!-- End Container here -->
            </div><!-- End div #investasi -->
            
            <div class="panel-heading">
                 <h5><label>AKUN</label></h5>
            </div>
            <div class="container">
                <div class="form-group">
                    <div class="container">
                        <label class="col-md-2">Email<span class="text-danger">*</label>
                        <div class="col-md-12">
                        <input name="email" value= "{{ old('email') }}" type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com" required="required">
                        </div>
                    </div>
                </div>
            
                <div class="form-group">
                    <div class="container">
                        <label class="col-md-2">Password<span class="text-danger">*</label>
                        <div class="col-md-12">
                        <input name="password" type="password" class="form-control" required="required">
                        </div>
                    </div>
                </div>
            
                <div class="form-group">
                    <div class="container">
                        <label class="col-md-12">Konfirmasi Password<span class="text-danger">*</label>
                        <div class="col-md-12">
                        <input name="password_confirmation" type="password" class="form-control" required="required">
                        </div>
                    </div>
                </div>
            </div><!-- End Container here -->
            
            
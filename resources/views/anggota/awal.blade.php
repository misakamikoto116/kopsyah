@extends('master')
@include('errors.alert')
@section('container')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                        <a href="{{route('anggota.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                </div>
    
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>
                                    <li class="active">
                                        Anggota
                                    </li>
                                </ol>
                            </div>
                        </div>

                        <center><h4 class="m-t-0"><b>TABEL ANGGOTA</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nomor Anggota atau Nama"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br>
                        
                        <table id="datatable" class="table table-striped table-bordered">
                            
                            <!-- Alert -->
								@yield('alert')
							
                            <thead>
                                <tr>
                                    <th width="5%" scope="row">No</th>
                                    <th width="15%" scope="row">Nomor Anggota</th>
                                    <th width="20%" scope="row">Nama</th>
                                    <th width="10%" scope="row">Data User</th>
                                    <th width="10%" scope="row">Keanggotaan</th>
                                    <th width="10%" scope="row">Aktivasi</th>
                                    <th width="20%" scope="row">Transaksi</th>
                                    <th width="10%" scope="row">Action</th>

                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $no =($semuaAnggota->perPage()*($semuaAnggota->currentPage()-1));
                                @endphp
                                @foreach($semuaAnggota as $index => $anggota)
                                    @if ($anggota->pembayaran->count() <= 0)
                                        <tr style="background-color: #FCFF33 !important;">
                                    @else
                                        <tr>
                                    @endif
                                        <td>{{$reverse_urutan[$no++]}}</td>
                                        <td>{{$anggota->nomor_anggota}}</td>
                                        <td>{{$anggota->nama or 'kosong'}}</td>
                                        <td><a href="{{route('user.show', $anggota->users_id)}}">{{$anggota->user->username or 'kosong'}}</a></td>
                                        @if($anggota->status_keanggotaan == 2)
                                        <td>Anggota</td>
                                        @elseif($anggota->status_keanggotaan == 3)
                                        <td> Anggota Investor </td>
                                        @else
                                        <td> </td>
                                        @endif
                                        
                                        <td>
                                            @if($anggota->status_aktifasi == 1)
                                            <a href="{{ url('admin/ubah/anggota/'.$anggota->id.'/0') }}" class="yes" data-user="{{ $anggota->id }}" desc="Yakin nonaktifkan?">
                                                <input checked type="checkbox" class="sip" get-user="{{ $anggota->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                            </a>
                                            @else
                                            <a href="{{ url('admin/ubah/anggota/'.$anggota->id.'/1') }}" class="yes" data-user="{{ $anggota->id }}" desc="Yakin aktifkan?">
                                                <input type="checkbox" class="sip" get-user="{{ $anggota->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                            </a>
                                            @endif
                                        </td>
                                        
                                        <td>
                                            <div class="form-group" role="grup">
                                                <a href="{{route('anggota.simpanan', $anggota->id)}}" class="btn btn-primary waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Simpanan">
                                                    <i class="fa fa-money fa-2x"></i>
                                                </a>
                                                <a href="{{route('anggota.investasi', $anggota->id)}}" class="btn btn-primary waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Investasi">
                                                    <i class="fa fa-wpforms fa-2x"></i>
                                                </a>                            
                                            </div>
                                        </td>
                                    

                                        <td>
                                        {{ Form::open(array('route' => array('anggota.destroy', $anggota->id), 'method' => 'delete')) }}
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Action <span class="caret"></span> </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{route('anggota.edit', $anggota->id)}}">Ubah</a></li>
                                                    <li><a href="{{route('anggota.show', $anggota->id)}}">Lihat</a></li>
                                                    <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                
                                                </ul>
                                            </div>
                                        </td>
                                        {{ Form::close() }}  
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaAnggota->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">EDIT DATA ANGGOTA</h3></a>
						</div>

						<div class="panel-body">
     						{{ Form::model($anggota, array('route' => array('anggota.update', $anggota->id), 'files' => true, 'method' => 'PUT','class'=>'form-horizontal')) }}
							{{-- alert --}}
							@if(Session::has('informasi'))
								<br>
								<div class="container">
									<div class="alert alert-danger">
										<strong>Informasi :</strong>
										{{Session::get('informasi')}}
									</div>
								</div>
							@endif
							@if(count($errors) > 0)
								<br>
								<div class="container">
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								</div>
							@endif	 
							 @include('anggota.formEdit')
								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-offset-9 text-right">
												<a href="{{route('anggota.index')}}" class="btn btn-warning waves-effect waves-light" > 
														<i class="fa fa-arrow-left m-r-5"></i>
														<span>Batal</span>
												</a>
											<button type="submit" class="btn btn-default waves-effect waves-light">
												<i class="md-autorenew m-r-5"></i>
												<span>Perbarui</span>
											</button>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->
	</div> <!-- content -->
</div><!-- End Right content here -->
@stop
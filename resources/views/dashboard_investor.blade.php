@extends('master_investor')
@section('container')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-15">
                            <a href="{{route('/')}}" > <button class="btn btn-default waves-effect waves-light">
                                    <span>Beranda</span></button>
                                </a>
                          </div>

                    <h4 class="page-title">Dashboard</h4>
                    <p class="text-muted page-title-alt">Assalamu’alaikum warahmatullahi wabarakatuh</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-info pull-left">
                            <i class="md  icon-user-following   text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_anggota}}</b></h3>
                            <p class="text-muted">Jumlah Anggota</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-pink pull-left">
                            <i class="md fa fa-handshake-o  text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_investor}}</b></h3>
                            <p class="text-muted">Jumlah Investor</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md  md-attach-money text-purple"></i>
                        </div>
                        <div class="text-right">
                            {{-- <h5 class="text-dark"><b class="counter">{{rupiah($simpanan_all)}}</b></h5> --}}
                            <h5 class="font-600">{{rupiah($simpanan_all)}}</h5>
                            <p class="text-muted">Total Simpanan</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-success pull-left">
                            <i class="md  md-wb-sunny text-success"></i>
                        </div>
                        <div class="text-right">
                            {{-- <h5 class="text-dark"><b class="counter">{{rupiah($investasi_all)}}</b></h5> --}}
                            <h5 class="font-600">{{rupiah($investasi_all)}}</h5>
                            <p class="text-muted">Total Investasi</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="bar-widget">
                             <div class="table-box">
                                <div class="table-detail">
                                    <div class="iconbox bg-danger">
                                    <i class="fa fa-money"></i>
                                    </div>
                                </div>

                                <div class="table-detail">
                                    <h4 class="m-t-0 m-b-5"><b><span class="omzet_now" id="omzet_now"></span></b></h4>
                                    <p class="text-muted m-b-0 m-t-0">Omset Hari Ini</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="bar-widget">
                             <div class="table-box">
                                <div class="table-detail">
                                    <div class="iconbox bg-custom">
                                    <i class="fa fa-money"></i>
                                    </div>
                                </div>

                                <div class="table-detail">
                                    <h4 class="m-t-0 m-b-5"><b><span class="omzet_y"></span></b></h4>
                                    <p class="text-muted m-b-0 m-t-0">Omset Kemarin</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                {{-- <div class="col-lg-12">
                    <div class="card-box" hidden>
                        <h4 class="m-t-0 header-title"><b>GRAFIK PENJUALAN MINGGUAN</b></h4>
                        <p class="text-muted m-b-15 font-13 text-center"><b>Omzet Mingguan</b>
                        </p>

                        <canvas id="lineChart" height="300"></canvas>
                    </div>
                </div> --}}

                {{--  <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>GRAFIK PENJUALAN MINGGUAN </b></h4>

                        <canvas id="grafik-omzet-weekly" height="195"></canvas>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Grafik Pembelian Terbanyak  </b></h4>

                        <canvas id="pie_belanja" height="250"></canvas>
                    </div>
                </div>  --}}

                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>GRAFIK PENJUALAN MINGGUAN </b></h4>

                        <canvas id="grafik-omzet-weekly" height="195"  ></canvas>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Grafik Pembelian Terbanyak  </b></h4>

                        <canvas id="pie_belanja" height="250"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="widget-chart text-center">
                           
                            <h5 class="text-muted m-t-10">Total Simpanan</h5>
                            <h3 class="font-600">{{rupiah($total)}}</h3>
                            <ul class="list-inline m-t-15">
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Pokok</h6>
                                    <h5 class="m-b-0">{{rupiah($total_pokok)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Wajib</h6>
                                    <h5 class="m-b-0">{{rupiah($total_wajib)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Sukarela</h6>
                                    <h5 class="m-b-0">{{rupiah($total_sukarela)}}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="widget-chart text-center">
                            <h5 class="text-muted m-t-10">Total Investasi</h5>
                            <h3 class="font-600">{{rupiah($investasi)}}</h3>
                            <ul class="list-inline m-t-15">
                            
                            @foreach ($jenis_investasi as $inv)
                            @php
                            $a= App\Model\InvestasiModel::where('jenis_investasi_id','=',$inv->id)
                                                ->where('anggota_id','=',$anggota->id)
                                                ->where('status', '=', '1')
                                                ->sum('jumlah');
                            @endphp
                                    <li>
                                    <h6 class="text-muted m-t-10">{{$inv->nama}}</h6>
                                    <h5 class="m-b-0">{{rupiah($a)}}</h5>          
                                    </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>

                </div>


            </div>
            <!-- end row -->

            
        </div> <!-- container -->


    </div> <!-- content -->

   

</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


@stop
@extends('master_anggota')
@section('container')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-15">
                            <a href="{{route('/')}}" > <button class="btn btn-default waves-effect waves-light">
                                    <span>Beranda</span></button>
                                </a>
                           </div>

                    <h4 class="page-title">Dashboard</h4>
                    <p class="text-muted page-title-alt">Assalamu’alaikum warahmatullahi wabarakatuh</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box fadeInDown animated">
                        <div class="bg-icon bg-icon-info pull-left">
                            <i class="md  icon-user-following   text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_anggota}}</b></h3>
                            <p class="text-muted">Anggota</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-pink pull-left">
                            <i class="md fa fa-handshake-o  text-pink"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_investor}}</b></h3>
                            <p class="text-muted">Investor</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-purple pull-left">
                            <i class="md fa fa-vcard-o text-purple"></i>
                        </div>
                        <div class="text-right">
                            <h3 class="text-dark"><b class="counter">{{$jml_pengurus}}</b></h3>
                            <p class="text-muted">Pengurus</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon bg-icon-success pull-left">
                            <i class="md md-attach-money text-success"></i>
                        </div>
                        <div class="text-right">
                            {{-- <h5 class="text-dark"><b class="counter">{{rupiah($simpanan_all)}}</b></h5> --}}
                            <h5 class="font-600">{{rupiah($simpanan_all)}}</h5>
                            <p class="text-muted">Total Simpanan</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="card-box">
                        <div class="widget-chart text-center">
                           
                            <h5 class="text-muted m-t-10">Total Simpanan</h5>
                            <h3 class="font-600">{{rupiah($total)}}</h3>
                            <ul class="list-inline m-t-15">
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Pokok</h6>
                                    <h5 class="m-b-0">{{rupiah($total_pokok)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Wajib</h6>
                                    <h5 class="m-b-0">{{rupiah($total_wajib)}}</h5>
                                </li>
                                <li>
                                    <h6 class="text-muted m-t-10">Simpanan Sukarela</h6>
                                    <h5 class="m-b-0">{{rupiah($total_sukarela)}}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

            

            </div>
            <!-- end row -->

            
        </div> <!-- container -->


    </div> <!-- content -->

   

</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->



@stop
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('page_title', 'Home') | 212 Mart Samarinda</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="{{asset('assets/assets/images/utama/logo1.png')}}" rel="icon">
	<link href="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/plugins/morris/morris.css')}}" rel="stylesheet">
	<link href="{{asset('assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/pages.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	<!--venobox lightbox-->
	<link rel="stylesheet" href="{{asset('assets/assets/plugins/magnific-popup/css/magnific-popup.css')}}"/>
	
	<!--Select-->
	<link href="{{asset('assets/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
	
	<!-- Datatables -->
	<link href="{{asset('assets/assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/dataTables.colVis.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/datatables/fixedColumns.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('assets/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/assets/plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">

	<script src="{{asset('assets/assets/js/modernizr.min.js')}}"></script>
</head>
<body class="fixed-left">

	<!-- Begin page -->
	<div id="wrapper">

		<!-- Top Bar Start -->
		<div class="topbar">

			<!-- LOGO -->
			<div class="topbar-left">
				<div class="text-center">
					<a href="{{route('/')}}" class="logo">
						<i class="icon-c-logo"> <img src="{{asset('assets/assets/images/utama/logo1.png')}}" width="100%" height="40"/> </i>
						<span><img src="{{asset('assets/assets/images/utama/logo1.png')}}" height="50" width="100%" /></span>
					</a>
				</div>
			</div>

			<!-- Button mobile view to collapse sidebar menu -->
			<div class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="">
						<div class="pull-left">
							<button class="button-menu-mobile open-left waves-effect waves-light">
								<i class="md md-menu"></i>
							</button>
							<span class="clearfix"></span>
						</div>
					@php
					
					$notif_regis= \Illuminate\Support\Facades\DB::table('notifications')
								->where('type','=','App\Notifications\NewRegistrasi')
								->get();
					$notif_simpanan= \Illuminate\Support\Facades\DB::table('notifications')
								->where('type','=','App\Notifications\NewPembayaran')
								->get();
					$notif_investasi= \Illuminate\Support\Facades\DB::table('notifications')
								->where('type','=','App\Notifications\NewInvestasi')
								->get();

					@endphp

						<ul class="nav navbar-nav navbar-right pull-right">
							<li class="dropdown top-menu-item-xs">
								<a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
									<i class="icon-bell"></i> <span class="badge badge-xs badge-danger">{{count($notif_regis)+count($notif_simpanan)+count($notif_investasi)}}</span>
								</a>
								<ul class="dropdown-menu dropdown-menu-lg">
									<li class="notifi-title"><span class="label label-default pull-right">New {{count($notif_regis)+count($notif_simpanan)+count($notif_investasi)}}</span>Notification</li>
									<li class="list-group slimscroll-noti notification-list">
											
											@if(count($notif_regis) != 0 ) 
											<a href="{{route('anggota.index')}}" class="list-group-item">
											   <div class="media">
												  <div class="pull-left p-r-10">
													 <em class="fa  fa-user-plus noti-primary"></em>
												  </div>
												  <div class="media-body">
													 <h5 class="media-heading">{{count($notif_regis)}} new user registered</h5>
													 <p class="m-0">
														 <small>see notifications </small>
													 </p>
												  </div>
											   </div>
											</a>
											@endif
											
											@if(count($notif_simpanan) != 0 )
											<a href="{{route('pembayaran.index')}}" class="list-group-item">
											   <div class="media">
												  <div class="pull-left p-r-10">
													 <em class="fa  fa-money noti-primary"></em>
												  </div>
												  <div class="media-body">
													 <h5 class="media-heading">{{count($notif_simpanan)}} new payment unverified</h5>
													 <p class="m-0">
															<small>see notifications </small>
														</p>
												  </div>
											   </div>
											</a>
											@endif
											@if(count($notif_investasi) != 0 )
											<a href="{{route('investasi.index')}}" class="list-group-item">
											   <div class="media">
												  <div class="pull-left p-r-10">
													 <em class="fa fa-wpforms noti-primary"></em>
												  </div>
												  <div class="media-body">
													 <h5 class="media-heading">{{count($notif_investasi)}} new investation unverified</h5>
													 <p class="m-0">
															<small>see notifications </small>
														</p>
												  </div>
											   </div>
											</a>
											@endif
									</li>
									<li>
										<a href="javascript:void(0);" class="list-group-item text-right">
											<small class="font-600">See all notifications</small>
										</a>
									</li>
								</ul>
							</li>
							<li class="hidden-xs">
								<a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
							</li>
							<li class="dropdown top-menu-item-xs">
								<a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{asset('assets/assets/images/utama/logo1.png')}}" width="100%" alt="user-img" class="img-circle"> </a>
								<ul class="dropdown-menu">
									<li><a href="{{route('logout')}}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<!-- Top Bar End -->

		<!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<li class="text-muted menu-title">Navigation</li>

                        <!-- dashboard -->
                            <li class="has_sub">
                                <a href="{{route('admin')}}" class="waves-effect"><i class="ti-dashboard"></i> <span> Dashboard </span></a>
							</li>
							
                        <!-- User -->
                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user"></i><span> User </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{{route('role.index')}}">Role</a></li>
									<li><a href="{{route('user.index')}}">User</a></li>
									<li><a href="{{route('anggota.index')}}">Anggota</a></li>
								</ul>
							</li>

                        <!-- Tentang -->
                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-th-large"></i><span> Tentang </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
									<li><a href="{{route('visimisi.index')}}">Visi & Misi</a></li>
									<li><a href="{{route('badanhukum.index')}}">Badan Hukum</a></li>
									<li><a href="{{route('ketentuan.index')}}">Ketentuan</a></li>
									<li><a href="{{route('faq.index')}}">FAQ</a></li>
									<li><a href="{{route('berita.index')}}">Berita</a></li>
									<li><a href="{{route('dokumen.index')}}">Dokumen</a></li>
								</ul>
							</li>

                        <!-- Kepengurusan -->
                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-sitemap"></i><span> Kepengurusan </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
									<li><a href="{{route('dewan.index')}}">Dewan</a></li>
									<li><a href="{{route('jabatan.index')}}">Jabatan</a></li>
									<li><a href="{{route('kepengurusan.index')}}">Pengurus</a></li>
								</ul>
							</li>

						<!-- Jenis simpanan -->
							<li class="has_sub">
                                <a href="{{route('jenis_pembayaran.index')}}" class="waves-effect"><i class="fa fa-wpforms"></i> <span> Jenis Simpanan </span></a>
							</li>
						
						<!-- Jenis Investasi -->
							<li class="has_sub">
                                <a href="{{route('jenis_investasi.index')}}" class="waves-effect"><i class="fa fa-dropbox"></i> <span> Jenis Investasi </span></a>
							</li>

                        <!-- Transaksi -->
                            <li class="has_sub">
								<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"></i><span> Transaksi </span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled">
									<li><a href="{{route('pembayaran.index')}}">Simpanan</a></li>
									<li><a href="{{route('investasi.index')}}">Investasi</a></li>
									<li><a href="{{route('pengambilan.index')}}">Pengambilan</a></li>
								</ul>
							</li>

						<!-- Belanja Bulanan -->
							<li class="has_sub">
								<a href="{{route('belanja.index')}}" class="waves-effect"><i class="fa fa-shopping-basket"></i><span> Belanja Bulanan </span></a>
							</li>

                        <!-- SHU -->
                            <li class="has_sub">
								<a href="#" data-toggle="modal" data-target="#data-keuntungan" class="waves-effect"><i class="fa fa-repeat"></i><span> Keuntungan Toko </span></a>
							</li>

                        <!-- OMSET -->
                            <li class="has_sub">
								<a href="#" data-toggle="modal" data-target="#data-omset-modal" class="waves-effect"><i class="fa fa-money"></i><span> Data Omset </span></a>
							</li>
						<!-- OMSET -->
                        <li class="has_sub">
							<a href="#" data-toggle="modal" data-target="#data-anggota-tahunan" class="waves-effect"><i class="fa fa-money"></i><span> Data Omset & Anggota </span></a>
						</li>  
						<!-- UMKM -->
                            <li class="has_sub">
								<a href="#" class="waves-effect"><i class="fa fa-money"></i><span> Data UMKM </span></a>
							</li>  

                        <!-- Report -->
                            <li class="has_sub">
								<a href="{{route('iniLaporan')}}" class="waves-effect"><i class="fa fa-file"></i><span> Report </span></a>
							</li>
					
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->

		
		@yield('container')


		<footer class="footer text-right">
			<p class="fl_left">Copyright &copy; 2018 - 212 Mart Samarinda</p>
		</footer>
	</div>

	{{-- modal --}}
	{{-- Modal Rincian Pembayaran Pembelian Pemasok --}}
	<div id="data-omset-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Data Omset</h4>
	      </div>
	        {!! Form::open(['route' => 'data.omset', 'method' => 'get', 'target' => '_blank']) !!}
	        <div class="modal-body">
	            <div class="form-group row">
	                {!! Form::label('Tanggal Awal',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
	                    {!! Form::date('tanggal_awal', null, ['class' => 'form-control','onchange' => 'var end = document.getElementById("end-date-rincian-buku-besar"); end.setAttribute("min", this.value); end.value = (this.value);']) !!}          
	                </div>
	            </div>
	            <div class="form-group row">
	                {!! Form::label('Tanggal Akhir',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
	                    {!! Form::date('tanggal_akhir', null, ['class' => 'form-control','id' => 'end-date-rincian-buku-besar']) !!}          
	                </div>
	            </div>
	        </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Lanjutkan</button>
	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>

	{{-- Modal Anggota Tahunan --}}
	<div id="data-anggota-tahunan" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Pilih Tanggal Untuk Laporan Data Omset & Anggota</h4>
	      </div>
	        {!! Form::open(['route' => 'data.anggota.tahunan', 'method' => 'get', 'target' => '_blank']) !!}
	        <div class="modal-body">
	            <div class="form-group row">
	                {!! Form::label('Bulan Awal',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
						<input type="month" name="bulan_awal" class="form-control">
	                </div>
	            </div>
	            <div class="form-group row">
	                {!! Form::label('Bulan Akhir',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
						<input type="month" name="bulan_akhir" class="form-control">
	                </div>
	            </div>
	        </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Lanjutkan</button>
	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>

	{{-- Modal Keuntungan --}}
	<div id="data-keuntungan" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Pilih Bulan Untuk Laporan Keuntungan Toko</h4>
	      </div>
	        {!! Form::open(['route' => 'data.keuntungan.tahunan', 'method' => 'get', 'target' => '_blank']) !!}
	        <div class="modal-body">
	            <div class="form-group row">
	                {!! Form::label('Bulan Awal',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
						<input type="month" name="bulan_awal" class="form-control">
	                </div>
	            </div>
	            <div class="form-group row">
	                {!! Form::label('Bulan Akhir',null,['class' => 'col-3 col-form-label']) !!}
	                <div class="col-9">
						<input type="month" name="bulan_akhir" class="form-control">
	                </div>
	            </div>
	        </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Lanjutkan</button>
	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>

	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script src="{{asset('assets/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/detect.js')}}"></script>
	<script src="{{asset('assets/assets/js/fastclick.js')}}"></script>
	<script src="{{asset('assets/assets/js/custom.js')}}"></script>

	@yield('grafik')

	@yield('js')
	
	<!-- DatePicker  -->
	<script src="{{asset('assets/assets/plugins/moment/moment.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.form-pickers.init.js')}}"></script>

	<!-- TinyMce  -->
	<script src="{{asset('assets/assets/plugins/tinymce/tinymce.min.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
            // new Chart(document.getElementById("grafik-omzet"), {
            //     type: 'line',
            //     data: {
            //         labels: ["7 Mei 2018","8 Mei 2018","9 Mei 2018","10 Mei 2018","11 Mei 2018","12 Mei 2018","13 Mei 2018"],
            //         datasets: [{
            //             data: [3822600,2725100,4583700,5859000,3293300,5258100,35770200],
            //             label: "omzet",
            //             borderColor: "#3e95cd",
            //             fill: false
            //         }
            //         ]
            //     },
            //     options: {
            //         title: {
            //             display: true,
            //             text: 'Omzet MIngguan'
            //         }
            //     }
            // });
		if($("#elm1").length > 0){
			tinymce.init({
				force_br_newlines : true,
				force_p_newlines : false,
				allow_conditional_comments: true,
				forced_root_block : "",
				selector: "textarea#elm1",
				theme: "modern",
				height:300,
				plugins: [
					"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
					"save table contextmenu directionality emoticons template paste textcolor"
				],
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
				style_formats: [
					{title: 'Bold text', inline: 'b'},
					{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
					{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
					{title: 'Example 1', inline: 'span', classes: 'example1'},
					{title: 'Example 2', inline: 'span', classes: 'example2'},
					{title: 'Table styles'},
					{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
				]
			});    
		}  
		});
	</script>
	<!-- End TinyMce  -->

	<script src="{{asset('assets/assets/plugins/switchery/js/switchery.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.slimscroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.blockUI.js')}}"></script>
	<script src="{{asset('assets/assets/js/waves.js')}}"></script>
	<script src="{{asset('assets/assets/js/wow.min.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.scrollTo.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/peity/jquery.peity.min.js')}}"></script>
	
	<!-- jQuery  -->
	<script src="{{asset('assets/assets/plugins/waypoints/lib/jquery.waypoints.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/counterup/jquery.counterup.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/morris/morris.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/raphael/raphael-min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/jquery-knob/jquery.knob.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.dashboard.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.core.js')}}"></script>
	<script src="{{asset('assets/assets/js/jquery.app.js')}}"></script>

	<script src="{{asset('assets/assets/js/custom.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
	
	<!-- jQuery  Datatables-->
	<script src="{{asset('assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/jszip.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/pdfmake.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/vfs_fonts.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.html5.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/buttons.print.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.colVis.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/datatables/dataTables.fixedColumns.min.js')}}"></script>
	<script src="{{asset('assets/assets/pages/datatables.init.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/autoNumeric/autoNumeric.js')}}"></script>

    <script type="text/javascript">  	  
		jQuery(function($) {
			$('.autonumber').autoNumeric('init');    
		});
	</script> 
	<script type="text/javascript">
		$(document).ready(function () {
			$('#datatable').dataTable({paginate:false, searching: false});
			$('#datatable-keytable').DataTable({keys: true});
			$('#datatable-responsive').DataTable();
			$('#datatable-colvid').DataTable({
				"dom": 'C<"clear">lfrtip',
				"colVis": {
					"buttonText": "Change columns"
				}
			});
			$('#datatable-scroller').DataTable({
				ajax: "assets/plugins/datatables/json/scroller-demo.json",
				deferRender: true,
				scrollY: 380,
				scrollCollapse: true,
				scroller: true
			});
			var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
			var table = $('#datatable-fixed-col').DataTable({
				scrollY: "300px",
				scrollX: true,
				scrollCollapse: true,
				paging: false,
				fixedColumns: {
					leftColumns: 1,
					rightColumns: 1
				}
			});
		});
		TableManageButtons.init();

	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.counter').counterUp({
				delay: 100,
				time: 1200
			});

			$(".knob").knob();

		});
	</script>

{{--tampil resi pembayaran--}}
	<script type="text/javascript" src="{{asset('assets/assets/plugins/isotope/js/isotope.pkgd.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js')}}"></script>
	  
	<script type="text/javascript">
		$(window).load(function(){
			var $container = $('.portfolioContainer');
			$container.isotope({
				filter: '*',
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});

			$('.portfolioFilter a').click(function(){
				$('.portfolioFilter .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
				$container.isotope({
					filter: selector,
					animationOptions: {
						duration: 750,
						easing: 'linear',
						queue: false
					}
				});
				return false;
			}); 
		});
		$(document).ready(function() {
			$('.image-popup').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-fade',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				}
			});
		});
	</script>

	<script src="{{asset('assets/assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
	<script src="{{asset('assets/assets/plugins/select2/js/select2.min.js')}}"></script>
	<script src="{{asset('assets/assets/pages/jquery.form-advanced.init.js')}}"></script>
</body>
</html>
	


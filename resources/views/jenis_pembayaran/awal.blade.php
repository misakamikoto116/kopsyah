@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right m-t-15">
                                                <a href="{{route('jenis_pembayaran.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                                        </div>
        
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="active">
                                                Jenis Simpanan
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                        <center><h4 class="m-t-0"><b>TABEL JENIS SIMPANAN</b></h4></center>
                        <form action="{{url()->current()}}">
                            <div class="container">
                                <div class="row">
                                    <div class="input-group col-12 col-md-4 pull-right">
                                        {!! Form::text('nama',null,['class'=>'form-control col-md-3','placeholder'=>"Nama"]) !!}          
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Cari</button>                                        
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    
                        <table id="datatable" class="table table-striped table-bordered">

                        	<!-- Alert -->
							@yield('alert')
						

                            <thead>
                                <tr>
                                        <th width="5%" scope="row">No</th>
                                        <th width="20%" scope="row">Nama</th>
                                        <th width="20%" scope="row">Nominal</th>
                                        <th width="20%" scope="row">Lama Tempo (bulan)</th>
                                        <th width="30%" scope="row">Deskripsi</th>
                                        <th width="10%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaJenisPembayaran as $jenis_pembayaran)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$jenis_pembayaran->nama or 'kosong'}}</td>
                                        <td>{{rupiah($jenis_pembayaran->nominal)}}</td>
                                        <td>{{$jenis_pembayaran->lama_tempo or 'kosong'}}</td>
                                        <td>{{$jenis_pembayaran->deskripsi or 'kosong'}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('jenis_pembayaran.destroy', $jenis_pembayaran->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('jenis_pembayaran.edit', $jenis_pembayaran->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('jenis_pembayaran.show', $jenis_pembayaran->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $semuaJenisPembayaran->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
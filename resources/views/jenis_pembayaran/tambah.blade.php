@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('jenis_pembayaran.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i> </h3></a>
						</div>
						 <div class= "container">
							<!-- Alert -->
							@yield('alert')
						 </div>
						<div class="panel-body">
							<center><h4 class="m-t-0"><b>TAMBAH JENIS SIMPANAN</b></h4></center>
							<br>
							{!! Form::open(['route'=>'jenis_pembayaran.store','class'=>'form-horizontal','files'=>true]) !!}
    							@include('jenis_pembayaran.form')
								<div class="form-group">
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12">
												<div class="col-md-offset-9 text-right">
													<button type="reset" class="btn btn-danger waves-effect waves-light">
														<span class="btn-label"><i class="fa fa-repeat"></i>
														</span>Ulangi
													</button>
													<button type="submit" class="btn btn-default waves-effect waves-light">
														<i class="fa fa-save m-r-5"></i>
														<span>Simpan</span>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
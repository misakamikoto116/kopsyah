<div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-10">
        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>"Nama",'required'=>'required']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Nominal</label>
	<div class="col-md-10">
        {!! Form::text('nominal',null,['class'=>'form-control','placeholder'=>"0",'required'=>'required']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Lama Tempo (bulan)</label>
	<div class="col-md-10">
        {!! Form::text('lama_tempo',null,['class'=>'form-control','placeholder'=>"0",'required'=>'required']) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Deskripsi</label>
	<div class="col-md-10">
        {!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi",'required'=>'required']) !!}
	</div>
</div>
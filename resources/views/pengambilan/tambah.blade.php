@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-color panel-primary">
						<div class="panel-heading">
							<a href="{{route('pengambilan.index')}}"><h3 class="panel-title"><i class="fa  fa-arrow-left"></i></h3></a>
						</div>
						<div class= "container">
						<!-- Alert -->
						@yield('alert')
						</div>
						<div class="panel-body">
							{!! Form::open(['route'=>'pengambilan.store','class'=>'form-horizontal','files'=>true]) !!}
							<center><h4 class="m-t-0"><b>TAMBAH PENGAMBILAN</b></h4></center>
							<br>
							<!--Form-->
								<div class="form-group">
									<label class="col-md-2 control-label">Tanggal</label>
									<div class="col-md-10">
										<div class="input-group">
											<input type="text" name="tanggal" class="form-control datepickerautoclose" placeholder="tgl-bln-tahun" value="{{old('tanggal')}}" required>
											<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">Jumlah</label>
									<div class="col-md-10">
										{!! Form::text('jumlah',old('jumlah'),['class'=>'form-control','placeholder'=>"Jumlah",'required']) !!}
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label">Deskripsi</label>
									<div class="col-md-10">
										{!! Form::textarea('deskripsi',old('deskripsi'),['class'=>'form-control','placeholder'=>"Deskripsi",'required']) !!}
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label">Status</label>
									<div class="col-md-10">
										<select class="form-control" name="status" required="required">
											<option  selected disabled>-- Pilih --</option>
											<option value ="0" {{ old('status') == 0 ? 'selected' : '' }} >Belum tervalidasi</option>
											<option value ="1" {{ old('status') == 1 ? 'selected' : '' }} >Sudah tervalidasi</option>
										</select> 
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">Anggota</label>
									<div class="col-md-10">
										 <select class="form-control select2"  name="anggota_id" id="">
										@foreach ($anggotaPluck as $anggota )
											<option value="{{$anggota->id}}">{{ $anggota->nama}} - {{ $anggota->nomor_anggota}}</option>
										@endforeach
									</select>
									</div>
								</div>
							<!--End-->

							<div class="form-group">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-md-12">
											<div class="col-md-offset-9 text-right">
												<button type="reset" class="btn btn-danger waves-effect waves-light">
													<span class="btn-label"><i class="fa fa-repeat"></i>
													</span>Ulangi
												</button>
												<button type="submit" class="btn btn-default waves-effect waves-light">
													<i class="fa fa-save m-r-5"></i>
													<span>Simpan</span>
												</button>

											</div>
										</div>
									</div>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->

</div><!-- End Right content here -->
@stop
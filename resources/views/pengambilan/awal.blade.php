@extends('master')
@include('errors.alert')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="btn-group pull-right m-t-15">
                                    <a href="{{route('pengambilan.create')}}"> <button type="button" class="btn btn-default ">Tambah <span class="m-l-5"><i class=" fa fa-plus fa fa-cog"></i></span></button></a>
                            </div>

                            <ol class="breadcrumb">
                                <li>
                                    <a href="#">Admin</a>
                                </li>
                                <li>
                                    <a href="#">Transaksi</a>
                                </li>
                                <li class="active">
                                    Pengambilan
                                </li>
                            </ol>
                        </div>
                    </div>
                    <center><h4 class="m-t-0"><b>TABEL PENGAMBILAN</b></h4></center>

                        <table id="datatable" class="table table-striped table-bordered">

                        <!-- Alert -->
						@yield('alert')
						
                            <thead>
                                <tr>
                                        <th width="5%" scope="row">No</th>
                                        <th width="15%" scope="row">Nomor Anggota</th>
                                        <th width="15%" scope="row">Nama Anggota</th>
                                        <th width="15%" scope="row">Deskripsi</th>
                                        <th width="10%" scope="row">Jumlah</th>
                                        <th width="20%" scope="row">Tanggal Pengambilan</th>
                                        <th width="10%" scope="row">Validasi</th>
                                        <th width="20%" scope="row">User</th>
                                        <th width="10%" scope="row">Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach($semuaPengambilan as $pengambilan)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$pengambilan->anggota->nomor_anggota or 'kosong'}}</td>
                                        <td>{{$pengambilan->anggota->nama or 'kosong'}}</td>
                                        <td>{{$pengambilan->deskripsi or 'kosong'}}</td>
                                        <td>{{rupiah($pengambilan->jumlah)}}</td>
                                        <td>{{DateIndo($pengambilan->tanggal)}}</td>
                                        <td>
                                                @if($pengambilan->status == 1)
                                                <a href="{{ url('admin/ubah/pengambilan/'.$pengambilan->id.'/0') }}" class="yes" data-user="{{ $pengambilan->id }}" desc="Yakin batalkan validasi?">
                                                    <input checked type="checkbox" class="sip" get-user="{{ $pengambilan->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                                </a>
                                                @else
                                                <a href="{{ url('admin/ubah/pengambilan/'.$pengambilan->id.'/1') }}" class="yes" data-user="{{ $pengambilan->id }}" desc="Yakin ingin memvalidasi?">
                                                    <input type="checkbox" class="sip" get-user="{{ $pengambilan->id }}" data-color="#f05050" data-plugin="switchery" data-size="small"/>
                                                </a>
                                                @endif
                                        </td>
                                        <td>{{$pengambilan->user->username}}</td>
                                        <td>
                                            {{ Form::open(array('route' => array('pengambilan.destroy', $pengambilan->id), 'method' => 'delete')) }}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle  waves-effect" data-toggle="dropdown" aria-expanded="false"> Aksi <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('pengambilan.edit', $pengambilan->id)}}">Ubah</a></li>
                                                        <li><a href="{{route('pengambilan.show', $pengambilan->id)}}">Lihat</a></li>
                                                        <button id="btn-action" class="btn btn-white waves-effect waves-light btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus">Hapus</button>
                                                    </ul>
                                                </div>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div><!-- End Right content here -->
@stop
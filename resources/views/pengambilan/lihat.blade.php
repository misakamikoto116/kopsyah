@extends('master')
@section('container')
<!-- Start right Content here -->
<div class="content-page">
     <!-- Start content -->
     <div class="content">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-color panel-primary">
                              <div class="panel-heading">
                                  <h3 class="panel-title">DETAIL PENGAMBILAN</h3>
                              </div>
                              <div class="panel-body">
                                   <table class="table">
                                        <tr>
                                             <td>Jumlah</td>
                                             <td>:</td>
                                             <td>{{rupiah($pengambilan->jumlah)}}</td>
                                        </tr>
                                        <tr>
                                             <td>Deskripsi</td>
                                             <td>:</td>
                                             <td>{{$pengambilan->deskripsi}}</td>
                                        </tr>

                                        <tr>
                                             <td>Tanggal Pengambilan</td>
                                             <td>:</td>
                                             <td>{{DateIndo($pengambilan->tanggal)}}</td>
                                        </tr>

                                        <tr>
                                             <td>Status</td>
                                             <td>:</td>
                                             <td>{{svalidasi($pengambilan->status)}}</td>
                                        </tr>
                                        <tr>
                                             <td>Nomor Anggota</td>
                                             <td>:</td>
                                             <td>{{$pengambilan->anggota->nomor_anggota}}</td>
                                        </tr>
                                        <tr>
                                             <td>User</td>
                                             <td>:</td>
                                             {{-- <td>{{$pengambilan->anggota->user->username}}</td> --}}
                                             <td>{{$pengambilan->user->username}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Dibuat</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date(indonesian_date($pengambilan->created_at))}}</td>
                                        </tr>
                                        <tr>
                                             <td class="col-xs-4">Diubah</td>
                                             <td class="col-xs-1">:</td>
                                             <td>{{indonesian_date(indonesian_date($pengambilan->updated_at))}}</td>
                                        </tr>
                                   </table>
                                   <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-9 text-right">
                                             <a href="{{route('pengambilan.index')}}">
                                                 <button class="btn btn-warning waves-effect waves-light">
                                                    <i class="fa fa-arrow-left m-r-5"></i><span>Kembali</span>
                                                 </button></a>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div> <!-- container -->

     </div> <!-- content -->

</div><!-- End Right content here -->
@stop
@extends('pages.partial.app')

@section('front_css')

@endsection

@section('front_content')

  <div class="row">
    <div class="col-12 col-md-9">
        <h1 style="text-align: center;">FREQUENTLY ASKED QUESTIONS</h1>
        {{-- @if($faq != null)
          @foreach ($faq as $fa)
            <div class="col-12 col-md-12 bg-light" id="comments">
                <header>
                  <address>
                    <a href="#">{{$fa->judul}}</a>
                  </address>
                </header>
                <div class="comcont">
                  <p>{!! $fa->isi !!}</p>
                </div>
            </div>
          @endforeach
        @elseif($faq == null)
          <div class="col-12 col-md-12 bg-light" id="comments">
              <header>
                <address>
                  <a href="#">Pertanyaan</a>
                </address>
              </header>
              <div class="comcont">
                <p>Jawaban</p>
              </div>
          </div>
        @endif --}}
    </div>
    <div class="sidebar col-12 col-md-3">
        <nav class="sdb_holder" style="padding-top:40px">
          {{-- @if($faq != null)
          <ul>
            <li><a href="#">Seputar Kopsyah</a>
              @foreach ($faq as $fa)
              <ul>
                <li><a href="#">{{$fa->judul}}</a></li>
              </ul>
              @endforeach
            </li>
          </ul>
          @elseif($faq == null)
          <ul>
              <li><a href="#">Seputar Kopsyah</a>
                <ul>
                  <li><a href="#">-</a></li>
                </ul>
              </li>
            </ul>
          @endif --}}
        </nav>
    </div>
  </div>
  <div class="clear"></div>

@endsection

@section('front_js')

@endsection

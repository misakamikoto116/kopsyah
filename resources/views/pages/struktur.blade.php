@extends('pages.partial.app')

@section('front_css')

@endsection

@section('front_content')

  <div class="content"> 
      <header class="heading" style="text-align: center; font-size: 20px">STRUKTUR DEWAN DAN PENGURUS<br>212 MART SAMARINDA</header> 
       <br>
       <br>
      <div id="gallery">
        <figure>
          <header class="heading" style="font-size: 18px;">DEWAN PENASEHAT</header>
            <div class="row">
                @if($ketua_Penasehat != null)
                    <div class="col-12 col-md-4 text-center">
                        <a href="#"><img src="{{ asset($ketua_Penasehat->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                        <p class="nospace font-xs">Ketua</p>
                        <h3 class="heading">{{$ketua_Penasehat->nama}}</h3>
                    </div>
                @endif

                @if($dewan_Penasehat != null)
                    @foreach ($dewan_Penasehat as $penasehat)
                        <div class="col-12 col-md-4 text-center">
                            <a href="#"><img src="{{ asset($penasehat->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                            <p class="nospace font-xs">Anggota</p>
                            <h3 class="heading">{{$penasehat->nama}}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </figure>
        <br>

        <figure>
            <header class="heading" style="font-size: 18px">DEWAN PENGAWAS  OPERASIONAL</header>
            <div class="row">
                @if($ketua_Operasional != null)
                    <div class="col-12 col-md-4 text-center">
                        <a href="#"><img src="{{ asset($ketua_Operasional->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                        <p class="nospace font-xs">Ketua</p>
                        <h3 class="heading">{{$ketua_Operasional->nama}}</h3>
                    </div>
                @endif

                @if($dewan_Operasional != null)
                    @foreach ($dewan_Operasional as $operasional)
                        <div class="col-12 col-md-4 text-center">
                            <a href="#"><img src="{{ asset($operasional->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                            <p class="nospace font-xs">Anggota</p>
                            <h3 class="heading">{{$operasional->nama}}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </figure> 
        <br>

        <figure>
            <header class="heading" style="font-size: 18px">DEWAN PENGAWAS SYARIAH</header>
            <div class="row">
                @if($ketua_Syariah != null)
                    <div class="col-12 col-md-4 text-center">
                        <a href="#"><img src="{{ asset($ketua_Syariah->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                        <p class="nospace font-xs">Ketua</p>
                        <h3 class="heading">{{$ketua_Syariah->nama}}</h3>
                    </div>
                @endif

                @if($dewan_Syariah != null)
                    @foreach ($dewan_Syariah as $syariah)
                        <div class="col-12 col-md-4 text-center">
                            <a href="#"><img src="{{ asset($syariah->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                            <p class="nospace font-xs">Anggota</p>
                            <h3 class="heading">{{$syariah->nama}}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </figure>
        <br>
        
        <figure>
            <header class="heading" style="font-size: 18px">DEWAN PENGURUS</header>
            <div class="row">
                @if($ketua_Pengurus != null)
                    <div class="col-12 col-md-4 text-center">
                        <a href="#"><img src="{{ asset($ketua_Pengurus->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                        <p class="nospace font-xs">Ketua</p>
                        <h3 class="heading">{{$ketua_Pengurus->anggota->nama}}</h3>
                    </div>
                @endif

                @if($dewan_Pengurus != null)
                    @foreach ($dewan_Pengurus as $pengurus)
                        <div class="col-12 col-md-4 text-center">
                            <a href="#"><img src="{{ asset($pengurus->foto)}}" alt="" style="max-width: 50%; height:150px"></a>
                            <p class="nospace font-xs">{{$pengurus->jabatan->posisi}}</p>
                            <h3 class="heading">{{$pengurus->anggota->nama}}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </figure>
    </div>
  <div class="clear"></div>

@endsection

@section('front_js')

@endsection

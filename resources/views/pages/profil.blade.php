@extends('pages.partial.app')

@section('front_css')

@endsection

@section('front_content')
  <div class="row">
    <div class="content col-12 col-md-9 tentang">
        <h6 style="font-size: 18px">PROFIL 212 MART SAMARINDA</h6>
        <p style="border-left: 3px solid #4368B0; padding: 15px ;">
          212 Mart adalah merek minimarket Koperasi Syariah 212. 212 Mart menjual barang kebutuhan sehari-hari masyarakat seperti bahan pokok, perlengkapan rumah tangga, alat tulis, dll. Berbeda dengan minimarket pada umumnya, 212 Mart tidak menjual rokok, minuman keras, alat kontrasepsi dan produk yang tidak halal. 
        </p>
    
        <br>
        <h6 style="font-size: 18px">VISI DAN MISI <br>212 MART SAMARINDA SAMARINDA</h6>
        @if($visis != null)
        <form style="border-left: 3px solid #4368B0; padding: 15px">
            Visi: <br>
            {!!$visis->judul!!}
        </form>
        </p>
        <form style="border-left: 3px solid #4368B0; padding: 15px">
            Misi: <br>
            {!!$visis->isi!!}
        </form>
        @endif

      <br>
      <h6 style="font-size: 18px">TUJUAN 212 MART SAMARINDA</h6>
      <form style="border-left: 3px solid #4368B0; padding: 15px">
        <ul>
          <li>Koperasi bertujuan meningkatkan pendapatan dan kesejahteraan para anggota secara berkelanjutan.</li>
          <li>Dalam mencapai tujuan yang telah ditetapkan, Koperasi menyusun Rencana Strategis.</li>
        </ul>
      </form>

      <br>
      <h6 style="font-size: 18px">LANDASAN 212 MART SAMARINDA</h6>
      <form style="border-left: 3px solid #4368B0; padding: 15px">
          @if($badanhukum != null)
            {!! $badanhukum->isi !!}
          @endif
      </form>
      <br>
      
      <h6 style="font-size: 18px">SYARAT, HAK, DAN KEWAJIBAN ANGGOTA 212 MART SAMARINDA</h6>
      @if($ketentuan != null)
        @foreach($ketentuan as $keten)
          <form style="border-left: 3px solid #4368B0; padding: 15px">
            {!! $keten ->judul !!} <br>
            {!! $keten->isi !!}
          </form>
        @endforeach 
      @endif
    </div>
    <div class="sidebar col-12 col-md-3">
        <nav class="sdb_holder" style="border: 5px ; background: #f2e132; padding:10px 10px 10px 10px; padding:10px 10px 10px 10px; height:180px; width:300px;">  
            <form style="padding: 10px 10px 10px 10px ;">
              <h6 style="font-size: 18px">Alamat</h6>
              <ul>
                  <li>Jalan Abdul Wahab Syahranie No. 20 RT. 25, Kelurahan Air Hitam, 
                    Kecamatan Samarinda Ulu, Kota Samarinda, Provinsi Kalimantan Timur</li>
                </ul> 
            </form>
        </nav>
        <nav class="sdb_holder" style="border: 5px solid #f2e132; padding:10px 10px 10px 10px; padding:10px 10px 10px 10px; height:180px; width:300px;">
          <form style="padding: 10px 10px 10px 10px ;">
            <h6 style="font-size: 18px">Kontak</h6>
            <ul>
                <li><i class="fa fa-phone"></i> &nbsp;08125572467 (Pono)</a></li>
                <li><i class="fa fa-phone"></i> &nbsp;08164300702 (Mustriono)</a></li> 
                <li> <i class="fa fa-phone"></i> &nbsp;085250719075 (Rudi)</a></li>
              </ul> 
          </form>
        </nav>
    </div>
  </div>
  <div class="clear"></div>
@endsection

@section('front_js')

@endsection

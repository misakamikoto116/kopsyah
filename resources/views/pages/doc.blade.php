@extends('pages.partial.app')

@section('front_css')

@endsection

@section('front_content')

    <div class="content"> 
      <header class="heading" style="text-align: center; font-size: 20px">FILE DOKUMEN<br>212 MART SAMARINDA</header> 
       <br>
       <br>
        <div class="row">
            @if($semuaDokumen != null)
                @foreach($semuaDokumen as $dokumen)
                    @php
                    $dok = $dokumen->file_dokumen;
                    
                    $substr= substr($dok,15);
                    @endphp
            <div class="col-12 col-md-2 text-center dokumen">
                @if($substr == ".docx" || $substr == ".doc")
                        <div class="card" style="background:#EEEEEE">
                            <a href="{{ route('resource.download', array('file'=>$dokumen->file_dokumen)) }}"><img src="assets/assets/images/img_file/doc.png" alt="" style="max-width: 60%; padding:5px; margin-top:1.7rem"></a>
                            <hr style="width:100%;">
                                <span style="font-size:10px; margin-top:-1rem; text-transform:uppercase">{{$dokumen->judul}}</span>
                                <span style="font-size:10px; margin-bottom:0.3rem">{{$dokumen->deskripsi}}</span>
                        </div>
                    @elseif($substr == ".xlsx" || $substr == ".xls")
                        <div class="card" style="background:#EEEEEE">
                            <a href="{{ route('resource.download', array('file'=>$dokumen->file_dokumen)) }}"><img src="assets/assets/images/img_file/xls.png" alt="" style="max-width: 60%; padding:5px; margin-top:1.7rem"></a>
                            <hr style="width:100%;">
                                <span style="font-size:10px; margin-top:-1rem; text-transform:uppercase">{{$dokumen->judul}}</span>
                                <span style="font-size:10px; margin-bottom:0.3rem">{{$dokumen->deskripsi}}</span>
                        </div>
                    @elseif($substr == ".pdf")
                        <div class="card" style="background:#EEEEEE">
                            <a href="{{ route('resource.download', array('file'=>$dokumen->file_dokumen)) }}"><img src="assets/assets/images/img_file/pdf.png" alt="" style="max-width: 60%; padding:5px; margin-top:1.7rem"></a>
                            <hr style="width:100%;">
                                <span style="font-size:10px; margin-top:-1rem; text-transform:uppercase">{{$dokumen->judul}}</span>
                                <span style="font-size:10px; margin-bottom:0.3rem">{{$dokumen->deskripsi}}</span>
                        </div>
                    @else
                        <div class="card" style="background:#EEEEEE">
                            <a href="{{ route('resource.download', array('file'=>$dokumen->file_dokumen)) }}"><img src="assets/assets/images/img_file/ppt.png" alt="" style="max-width: 60%; padding:5px; margin-top:1.7rem"></a>
                            <hr style="width:100%;">
                                <span style="font-size:10px; margin-top:-1rem; text-transform:uppercase">{{$dokumen->judul}}</span>
                                <span style="font-size:10px; margin-bottom:0.3rem">{{$dokumen->deskripsi}}</span>
                        </div>
                    @endif
            </div>
                @endforeach 
            @endif
        </div>
  <div class="clear"></div>

@endsection

@section('front_js')

@endsection

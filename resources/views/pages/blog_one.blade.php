@extends('pages.partial.app')

@section('front_css')

@endsection

@section('front_content')

  <div class="row">
    <div class="sidebar col-12 col-md-3 col-sm-4">
      <h6>BERITA</h6>
      <nav class="sdb_holder">
        @if($listberita != null)
          <ul>
            @foreach ($listberita as $listb)
              <li><a href="{{ route('blog-one', $listb->id) }}">{{$listb->judul}}</a></li>
            @endforeach
          </ul>
        @endif
      </nav>
    </div>
    <div class="col-12 col-md-9 col-sm-4">
      @if($terkini != null)
        <h1 style="font-size: 30px; margin-bottom:0px">{{$terkini->judul}}</h1>
        <time datetime="2045-04-06T08:15+00:00">{{$tgl}}</time>           
        <img class="btmspace-30" src="{{asset('storage/images/tentang/'.$terkini->foto)}}" alt="">
      @elseif($terkini == null)
        <h1 style="font-size: 30px; margin-bottom:0px">Tidak Ada Berita</h1>
       @endif
      
      @if ($terkini != null)
        <p>
          {!!$terkini->isi!!}
        </p>
      @elseif($terkini == null)
        <p>
         -
        </p>
      @endif

      <main class="hoc container clear" style="padding-top:10px;"> 
        <a href="#"><h5 class="font-x1 font-x4" style="color: black;" >TERKINI</h5>
        @if($terbaru !== null)
        <div class="row">
        @foreach ($terbaru as $baru)
          <div class="col-12 col-md-4">
            <img class="btmspace-30" src="{{asset('storage/images/tentang/'.$baru->foto)}}" alt=""></a>
            <h6 class="heading" style="font-size: 16px">{{$baru->judul}}</h6>
          </div>
        @endforeach
        </div>
        @endif
        <div class="clear"></div>
      </main>
    </div>

@endsection

@section('front_js')

@endsection

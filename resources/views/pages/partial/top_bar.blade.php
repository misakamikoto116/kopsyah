<div class="bgded overlay" style="background-image:url('assets/assets/images/utama/islamic.jpg'); background-position: center;"> 
  <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      <div id="logo" class="fl_left">
        <h1><a href="{{ route('/') }}"><img src="{{asset('assets/assets/images/utama/logo1.png')}}" style="width: 60%;"></a></h1><br>
      </div>
      <nav id="mainav" class="fl_right" style="margin-top: 100px;">
        <ul class="clear">
          <li><a href="{{ route('/') }}">Beranda</a></li>
          <li class="{{ isset($bar_profil) ? 'active' : '' }}"><a href="{{ route('/profil') }}">Tentang Kami</a></li>
          <li class="{{ isset($bar_struktur) ? 'active' : '' }}"><a href="{{ route('/struktur') }}">Struktur</a></li>
          <li class="{{ isset($bar_faq) ? 'active' : '' }}"><a href="{{ route('/faq') }}">FAQ</a></li>
          <li class="{{ isset($bar_blog) ? 'active' : '' }}"><a href="{{ route('/blog') }}">Blog</a></li>
          <li class="{{ isset($bar_page_doc) ? 'active' : '' }}"><a href="{{ route('/page_doc') }}">Dokumen</a></li>
        </ul>
      </nav>
    </header>
  </div>

  <div id="breadcrumb" class="hoc clear"> 
    <ul>
      @isset ($breadcrumbs)
        @foreach ($breadcrumbs as $breadcrumb)
          <li><a href="#">{{ $breadcrumb }}</a></li>
        @endforeach
      @endisset
    </ul>
  </div>
  
</div>
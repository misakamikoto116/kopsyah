<footer>
  <div class="bgded overlay" style="background-image:url('../images/demo/backgrounds/04.png');"> 
    <div class="wrapper row5" style="background: #002e5b; height: 120px;">
      <div id="copyright" class="hoc clear"> 
        <p class="fl_left" style="color: white;">Copyright &copy; 2018 - <a href="#">212 Mart Samarinda</a></p>
      </div>
    </div>
  </div>

  <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>

  <!-- JAVASCRIPTS -->
  <script src="{{asset('assets/assets/layout/scripts/jquery.min.js')}}"></script>
  <script src="{{asset('assets/assets/layout/scripts/jquery.backtotop.js')}}"></script>
  <script src="{{asset('assets/assets/layout/scripts/jquery.mobilemenu.js')}}"></script>
</footer>
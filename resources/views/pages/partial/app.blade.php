<!DOCTYPE html>
<html>
<head>
  <title>212 Mart Samarinda | {{ $title }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  @include('pages.partial.header')
  @yield('front_css')
</head>
{{-- Header --}}

<body id="top">
      
      {{-- Second Header --}}
      @include('pages.partial.second_header')
      
      {{-- top bar --}}
      @include('pages.partial.top_bar')
      
      <div class="wrapper row3">
        <main class="hoc container clear">  
        @yield('front_content')
        </main>
      </div>
      
      {{-- Footer --}}
      @include('pages.partial.footer')
      @yield('front_js')
</body>      

</html>
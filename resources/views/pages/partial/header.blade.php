<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
<link href="{{asset('assets/assets/layout/styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
<link href="{{asset('assets/assets/layout/styles/framework.css')}}" rel="stylesheet" type="text/css" media="all">
<link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all">
<link rel="icon" href="{{asset('assets/assets/images/utama/logo1.png')}}">
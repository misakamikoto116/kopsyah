<div class="wrapper row0" style="color: #CBCBCB; background-color: white;">
  <div id="topbar" class="hoc clear"> 
    <div class="fl_right">
      
      @if (Route::has('login'))
      @php
      $user = App\Model\UserModel::where('id', '=', Auth::id())->first();
      @endphp
      @auth
      <ul class="nospace" style="padding:10px;">
        <li><a href="{{ route('logout') }}" style="color: #4368B0; background-color: white;">Logout</a></li>
        @if($user->role_id == "2")
        <li><a href="{{ route('anggota') }}" style="background-color: white;">Dashboard</a></li>
        @elseif($user->role_id == "3")
        <li><a href="{{ route('investor') }}" style="background-color: white;">Dashboard</a></li>
        @elseif($user->role_id == "1")
        <li><a href="{{ route('admin') }}" style="background-color: white;">Dashboard</a></li>
        @endif
      </ul>   

      @else
      <ul class="nospace" style="padding:10px;">
        <li><a href="{{ route('login') }}" style="color: #4368B0; background-color: white;">Login</a></li>
        <li><a href="{{ route('registration') }}" style="background-color: white;">Registrasi</a></li>
      </ul>   
      @endauth
  @endif
   </div>
  </div>
</div>
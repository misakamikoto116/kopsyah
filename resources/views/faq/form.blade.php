<div class="form-group">
     <label class="col-md-2 control-label">Pertanyaan</label>
     <div class="col-md-10">
        {!! Form::text('judul',old('judul'),['class'=>'form-control','placeholder'=>"Pertanyaan"]) !!}
     </div>
</div>
<div class="form-group">
     <label class="col-md-2 control-label">Jawaban</label>
     <div class="col-md-10">
        {!! Form::textarea('isi',old('isi'),['class'=>'form-control','placeholder'=>"Jawaban",'id'=>'elm1']) !!}
     </div>
</div>

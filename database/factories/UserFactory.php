<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** 
* $factory->define(App\User::class, function (Faker $faker) {
*   static $password;
*
*   return [
*      'name' => $faker->name,
*     'email' => $faker->unique()->safeEmail,
*    'password' => $password ?: $password = bcrypt('secret'),
*   'remember_token' => str_random(10),
*  ];
* });

*/

//user
$factory->define(App\Model\UserModel::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
        'remember_token' => str_random(10),
        'role_id' => $faker->randomElement($array = array ('1','2','3')),
      //  'created_by' => '1',
      //  'updated_by' => '1'
    ];
});


<?php

use Faker\Generator as Faker;

//pembayaran
$factory->define(App\Model\JenisInvestasiModel::class, function (Faker $faker) {
    return [
        'jumlah' => factory(App\Model\JenisSimpananModel::class)->create()->nominal,
        'tanggal_tempo' =>  $faker->dateTimeInInterval($startDate = 'now', $interval = '+ 5 months'),
        'tanggal_pembayaran' =>  $faker->date,
        'file_resi' =>  $faker->image($dir = '/tmp', $width = 640, $height = 480),
        'status' =>  $faker->randomElement($array = array('0','1')),  
        'anggota_id' => factory(App\Model\AnggotaModel::class)->create()->id,
        'jenis_pembayaran_id' => factory(App\Model\JenisInvestasiModel::class)->create()->id,
        'created_by' => factory(App\Model\UserModel::class)->create()->id,
        'updated_by' => factory(App\Model\UserModel::class)->create()->id    
    ];
});
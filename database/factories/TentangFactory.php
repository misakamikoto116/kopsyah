<?php

use Faker\Generator as Faker;

//tentang
$factory->define(App\Model\TentangModel::class, function (Faker $faker) {
    return [
        'jenis' => $faker->randomElement($array = array('1','2','3','4','5')),
        'judul' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'isi' => $faker->text($maxNbChars = 200) , 
        'foto' => $faker->image($dir = '/tmp', $width = 640, $height = 480),
        'created_by' => '1',
        'updated_by' => '1' 
    ];
});


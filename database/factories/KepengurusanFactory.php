<?php

use Faker\Generator as Faker;

//kepengurusan
$factory->define(App\Model\KepengurusanModel::class, function (Faker $faker) {
    return [
        'tanggal_menjabat' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'sk_pengurus' =>  $faker->sentence($nbWords = 3, $variableNbWords = true) ,
        'status_kepengurusan' =>  $faker->randomElement($array = array('0','1')),
        'anggota_id' => factory(App\Model\AnggotaModel::class)->create()->id,
        'jabatan_id' => $faker->randomElement($array = array('1','2','3','4','5')),
        'created_by' => '1',
        'updated_by' => '1'
  
    ];
});

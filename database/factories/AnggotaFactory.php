<?php

use Faker\Generator as Faker;

//anggota
$factory->define(App\Model\AnggotaModel::class, function (Faker $faker) {
    return [
        'no_ktp' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'nama' =>  $faker->name ,
        'agama' =>  'islam',
        'nama_ibu_kandung' =>  $faker->name ,
        'tempat_lahir' =>  $faker->city,
        'tanggal_lahir' =>  $faker->date ,
        'email' =>  $faker->unique()->safeEmail,
        'no_telepon' =>  $faker->phoneNumber,
        'jenis_kelamin' =>  $faker->randomElement($array = array('L','P')),
        'kota' =>  $faker->randomElement($array = array('1','2')),
        'alamat' =>  $faker->address,
        'pendapatan' =>  $faker->randomElement($array = array('1','2','3','4','5','6','7','8')),
        'pekerjaan' =>  $faker->randomElement($array = array('PNS','Wiraswasta','Karyawan','Pensiunan','Petani','Lainnya')),
        'file_ktp' =>   $faker->image($dir = '/tmp', $width = 640, $height = 480), 
        'file_foto' =>  $faker->image($dir = '/tmp', $width = 640, $height = 480), 
        'status_keanggotaan' =>  $faker->randomElement($array = array('2','3')),
        'status_aktifasi' =>  $faker->randomElement($array = array('0','1')), 
        'users_id' => factory(App\Model\UserModel::class)->create()->id,
        'created_by' => factory(App\Model\UserModel::class)->create()->id,
        'updated_by' => factory(App\Model\UserModel::class)->create()->id
  
    ];
});

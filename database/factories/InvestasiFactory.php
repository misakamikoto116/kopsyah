<?php

use Faker\Generator as Faker;

//investasi
$factory->define(App\Model\InvestasiModel::class, function (Faker $faker) {
    return [
        'jumlah' => $faker->numberBetween($min = 5000000, $max = 200000000),
        'tanggal_pembayaran' =>  $faker->date($format = 'Y-m-d', $max = 'now'),
        'foto_resi' =>  $faker->image($dir = '/tmp', $width = 640, $height = 480),
        'status' =>  $faker->randomElement($array = array('0','1')),
        'anggota_id' => factory(App\Model\AnggotaModel::class)->create()->id,
        'jenis_investasi_id' => $faker->randomElement($array = array('1','2')),
        //'created_by' => factory(App\Model\UserModel::class)->create()->id,
        //'updated_by' => factory(App\Model\UserModel::class)->create()->id
  
    ];
});


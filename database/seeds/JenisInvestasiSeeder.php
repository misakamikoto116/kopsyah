<?php

use App\Repositories\CsvSeeder;

class JenisInvestasiSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'jenis_investasi';
        $this->filename = base_path().'/database/seeds/jenisinvestasi_seeder.csv';
    }

    public function run()
    {
        DB::disableQueryLog(); //jk besar pastikan pakai diSableQueryLog
        //DB::table($this->table)->truncate();
        parent::run();
    }
}

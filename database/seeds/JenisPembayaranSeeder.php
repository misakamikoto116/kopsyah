<?php

use Illuminate\Database\Seeder;

class JenisPembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        \App\Model\JenisPembayaranModel::insert([
            [
              'id'  => '1',
              'nama' => 'Simpanan Pokok',
              'nominal' => '30000',
              'deskripsi' => 'Rekening atas nama Kopsyah SMS 7115634385',
              'lama_tempo' => '0',
              'created_by' => '1',
              'updated_by' => '1',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
           
            ],
            [
                'id'  => '2',
                'nama' => 'Simpanan Wajib',
                'nominal' => '120000',
                'deskripsi' => 'Simpanan Pertahun',
                'lama_tempo' => '12',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
           
              ],
            [
                'id'  => '3',
                'nama' => 'Simpanan Sukarela',
                'nominal' => '0',
                'deskripsi' => 'Simpanan sukarela',
                'lama_tempo' => '0',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
           
              ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\JabatanModel::insert([
            [
              'id'  => '1',
              'posisi' => 'Ketua',
              'deskripsi' => 'Pimpinan',
              'tupoksi' => 'Bertanggung jawab penuh',
              'created_by' => '1',
              'updated_by' => '1',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
           
            ],
            [
                'id'  => '2',
                'posisi' => 'Wakil ketua',
                'deskripsi' => 'Wakil Pimpinan',
                'tupoksi' => 'Mendampingi dan menggantikan tugas ketua',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
              ],
              [
                'id'  => '3',
                'posisi' => 'Sekretaris',
                'deskripsi' => '-',
                'tupoksi' => 'Mengatur rapat',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
              ],

              [
                'id'  => '4',
                'posisi' => 'Bendahara',
                'deskripsi' => '-',
                'tupoksi' => 'Mengelola seluruh keuangan',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
              ],

              [
                'id'  => '5',
                'posisi' => 'Pengurus',
                'deskripsi' => '-',
                'tupoksi' => 'Anggota biasa',
                'created_by' => '1',
                'updated_by' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
              ],
        ]);
    
    }
}

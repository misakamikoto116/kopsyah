<?php

use Illuminate\Database\Seeder;

class KepengurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\KepengurusanModel::class, 20)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\RoleModel::insert([
            [
              'id'  => '1',
              'nama' => 'Admin',
              'created_by' => '1',
              'updated_by' => '1',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'id'  => '2',
              'nama' => 'Anggota',
              'created_by' => '1',
              'updated_by' => '1',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
         
            ],
            [
              'id'  => '3',
              'nama' => 'Anggota Investor',
              'created_by' => '1',
              'updated_by' => '1',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
        ]);
    }
}

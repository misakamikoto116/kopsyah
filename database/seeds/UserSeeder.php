<?php

use App\Repositories\CsvSeeder;

class UserSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'users';
        $this->filename = base_path().'/database/seeds/user_seeder.csv';
    }

    public function run()
    {
        DB::disableQueryLog(); //jk besar pastikan pakai diSableQueryLog
        //DB::table($this->table)->truncate();
        parent::run();
    }
}


<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class); 
        $this->call(UserSeeder::class);
        $this->call(AnggotaSeeder::class);
        $this->call(JenisPembayaranSeeder::class);
        $this->call(JenisInvestasiSeeder::class);
        //$this->call(TentangSeeder::class);
        $this->call(JabatanSeeder::class);
        //$this->call(KepengurusanSeeder::class);
        $this->call(PembayaranSeeder::class);
        $this->call(InvestasiSeeder::class);
    }
}

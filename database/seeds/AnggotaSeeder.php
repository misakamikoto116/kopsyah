<?php

use App\Repositories\CsvSeeder;

class AnggotaSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'anggota';
        $this->filename = base_path().'/database/seeds/anggota_seeder.csv';
    }

    public function run()
    {
        DB::disableQueryLog(); //jk besar pastikan pakai diSableQueryLog
        //DB::table($this->table)->truncate();
        parent::run();
    }
}
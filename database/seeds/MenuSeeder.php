<?php
//untuk role

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listMenu() as $role => $menus) {
            $role =Role::where('name',$role)->firstOrFail();
            $this->makeLink($menus,$role);
        }
    }
    function makeLink($menus,$role,$parentRoute = null,$parentMenuId){
        //looping semua array
        foreach ($menus as $menu) {
            $route='';
            //kita cek apakah route ini punya parent routing
            if(null !== $parentRoute){
                $route.=''!==$route ? ".$parentRoute":$parentRoute;
            }
            //sambung semua text route menjadi satu
            $route .= '' !== $route ? ".$menu[route]" : $menu['route'];

            if (array_key_exists('child', $menu)) {
                $this->makeLink($menu, $role, true, $menu['route']);

            } else {
                if ($parentRoute !== null) {
                    $route.=".$parentRoute";
                }
                $route.=$menu['route'];

            }
        $role->menu()->save(new Menu([
                'name' => $menu['name'],
                'icon' => $menu['icon'],
                'route' =>'' !== $route?: '#',
        ]));
        }
        }
    
    function listMenu(){
        return[
            'admission' => [ //user level
                ['name' =>'Dashboard', 'icon'=>'fa-home','route'=>'home'], //link admission.home
                [
                    'name' => 'Register', 
                    'icon' => 'fa-heart', 
                    'route' => 'regist',
                    'child'=>[
                                ['name' => 'IRJA', 'icon' => 'fa-home', 'route' => 'irja'],
                                ['name' => 'IGD', 'icon' => 'fa-home', 'route' => 'igd'],
                            ],
                ], //link admission.home
                ['name' => 'Pasien', 'icon' => 'fa-user', 'route' => 'home'], //link admission.home
            ],
            ];

    }
}

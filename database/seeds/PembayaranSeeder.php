<?php

use App\Repositories\CsvSeeder;

class PembayaranSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'pembayaran';
        $this->filename = base_path().'/database/seeds/pembayaran_seeder.csv';
    }

    public function run()
    {
        DB::disableQueryLog(); //jk besar pastikan pakai diSableQueryLog
        //DB::table($this->table)->truncate();
        parent::run();
    }
}

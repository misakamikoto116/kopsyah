<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('posisi', 100)->nullable(false);
            $table->text('deskripsi')->nullable(true);
            $table->text('tupoksi')->nullable(true);
            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatan');
    }
}

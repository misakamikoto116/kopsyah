<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePenjualan extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal')->nullable(true);
            $table->double('total')->nullable(true);
            $table->double('jml_umkm')->nullable(true)->comment('nominal umkm');
            $table->double('jml_non')->nullable(true)->comment('nominal non-umkm');
            $table->string('nomor_anggota')->nullable(true);
            $table->string('anggota_id')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('penjualan');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteTableDewan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dewan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal_menjabat')->nullable(false);
            $table->string('sk_pengurus',100)->nullable(false);
            $table->string('nama',100)->nullable(false);
            $table->string('foto',100)->nullable(false);
            $table->string('struktur',100)->nullable(false);
            $table->string('jabatan',100)->nullable(false);
            $table->boolean('status_kepengurusan')->nullable(false);
            $table->text('profil')->nullable(false);
            $table->text('deskripsi')->nullable(false);
           
            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dewan');
    }
}

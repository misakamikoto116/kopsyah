<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBiayaCetakInvestasiOnTableInvestasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('investasi', 'biaya_cetak_investasi')) {
            Schema::table('investasi', function (Blueprint $table) {
                $table->decimal('biaya_cetak_investasi',15,2)->after('jumlah');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investasi', function (Blueprint $table) {
            //
        });
    }
}

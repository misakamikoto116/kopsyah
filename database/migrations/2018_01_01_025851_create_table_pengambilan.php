<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengambilan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengambilan', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('jumlah',15,2)->nullable(false);
            $table->date('tanggal')->nullable(false);
            $table->text('deskripsi')->nullable(false);
            $table->boolean('status')->nullable(false);
            $table->integer('anggota_id',false,true);
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade');
            $table->integer('users_id',false,true);
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            
            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengambilan');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('jumlah',15,2)->nullable(false);
            $table->date('tanggal_tempo')->nullable(false);
            $table->date('tanggal_pembayaran')->nullable(false);
            $table->string('file_resi')->nullable(false);
            $table->boolean('status')->nullable(true);
            $table->integer('anggota_id',false,true);
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade');
            $table->integer('jenis_pembayaran_id',false,true);
            $table->foreign('jenis_pembayaran_id')->references('id')->on('jenis_pembayaran')->onUpdate('cascade');

            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran');
    }
}

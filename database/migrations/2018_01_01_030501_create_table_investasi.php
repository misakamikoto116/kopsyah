<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvestasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investasi', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('jumlah',15,2)->nullable(false);
            $table->date('tanggal_pembayaran')->nullable(false);
            $table->string('foto_resi')->nullable(true);
            $table->boolean('status')->nullable(false);
            $table->integer('anggota_id',false,true);
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade');
            $table->integer('jenis_investasi_id',false,true);
            $table->foreign('jenis_investasi_id')->references('id')->on('jenis_investasi')->onUpdate('cascade');

            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investasi');
    }
}

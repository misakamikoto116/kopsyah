<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKepengurusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepengurusan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal_menjabat')->nullable(false);
            $table->string('sk_pengurus',100)->nullable(false);
            $table->boolean('status_kepengurusan')->nullable(false);
            $table->integer('anggota_id',false,true);
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade');
            $table->integer('jabatan_id',false,true);
            $table->foreign('jabatan_id')->references('id')->on('jabatan')->onUpdate('cascade');

            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepengurusan');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBelanja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('belanja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_barang')->nullable(true);
            $table->string('merek')->nullable(true);
            $table->string('jumlah')->nullable(true);
            $table->string('periode')->nullable(true);
            $table->integer('anggota_id', false, true);
            $table->foreign('anggota_id')->references('id')->on('anggota')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('belanja');
    }
}

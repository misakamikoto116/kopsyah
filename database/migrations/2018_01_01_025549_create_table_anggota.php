<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_ktp', 16)->nullable(false);
            $table->string('nama', 100)->nullable(false);
            $table->string('agama', 50)->nullable(false);
            $table->string('nama_ibu_kandung', 100)->nullable(false);
            $table->string('tempat_lahir', 100)->nullable(false);
            $table->date('tanggal_lahir')->nullable(false);
            $table->string('email')->nullable(false)->unique();
            $table->string('no_telepon')->nullable(false);
            $table->string('jenis_kelamin')->nullable(false);
            $table->string('kota')->nullable(false);
            $table->string('alamat')->nullable(false);
            $table->string('pendapatan')->nullable(false);
            $table->string('pekerjaan')->nullable(false);
            $table->string('file_ktp')->nullable(false);
            $table->string('file_foto')->nullable(false);
            $table->string('status_keanggotaan')->nullable(false);
            $table->boolean('status_aktifasi')->nullable(false);
            $table->integer('users_id',false,true);
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            

            $table->integer('created_by')->nullable(true)->comment('Dibuat oleh siapa');
            $table->integer('updated_by')->nullable(true)->comment('Diupdate oleh siapa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota');
    }
}

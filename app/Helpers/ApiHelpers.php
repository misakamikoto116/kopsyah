<?php

/**
 * Created by PhpStorm.
 * User: efendihariyadi
 * Date: 07/09/18
 * Time: 10.13
 */

namespace App\Helpers;


use DB;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class ApiHelpers
{
    //API_POS=http://localhost:8000/
    //API_POS_CLIENT_ID=8
    //API_POS_CLIENT_SCREET=dRIIufv8I5146yL1iUHKFSjNKki3pyTxINiARJE6
    //API_POS_GRANT_TYPE=password
    //API_POS_USERNAME=client_kopsyah@koperasisyariahsms.com
    //API_POS_PASSWORD=123456
    //API_POS_SCOPE=*
    protected $pos_api_url;
    protected $pos_clientId;
    protected $pos_client_screet;
    protected $pos_client_grant_type;
    protected $pos_username;
    protected $pos_userpass;
    public function __construct()
    {
        $this->pos_api_url = env('API_POS_AKUNTASI', null);
        $this->pos_clientId = env('API_POS_CLIENT_ID', null);
        $this->pos_client_screet = env('API_POS_CLIENT_SCREET', null);;
        $this->pos_username = env('API_POS_USERNAME', null);
        $this->pos_userpass = env('API_POS_PASSWORD', null);
        $this->pos_client_grant_type = env('API_POS_GRANT_TYPE', null);;
    }

    private function _getToken()
    {
        $client = new Client;
        $token = '';
        try {
            $response = $client->post($this->pos_api_url . '/oauth/token', [
                'form_params' => [
                    'client_id' => $this->pos_clientId,
                    'client_secret' => $this->pos_client_screet,
                    'grant_type' => $this->pos_client_grant_type,
                    'username' => $this->pos_username,
                    'password' => $this->pos_userpass,
                    'scope' => '*',
                ]
            ]);

            $resBody = json_decode((string) $response->getBody());
            $token = $resBody->access_token;
        } catch (BadResponseException $e) {
            throw new Exception($e->getMessage());
        }
        return $token;
    }
    /**
     * @param $data array
     * @return mixed
     */
    public function sendAnggotaApi($data)
    {
        DB::beginTransaction();
        $client = new Client;
        try {
            $response = $client->post($this->pos_api_url . '/api/anggota', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->_getToken(),
                    'Accept' => 'application/json'
                ],
                'form_params' => [
                    'anggota' => [
                        'no_pelanggan' => $data['no_pelanggan'],
                        'nama' => $data['nama'],
                        'alamat' => $data['alamat'],
                        'alamat_pajak' => $data['alamat_pajak'],
                        'kota' => $data['kota'],
                        'telepon' => $data['telepon'],
                        'personal_kontak' => $data['telepon'],
                        'email' => $data['email']
                    ]
                ]
            ]);
            $res = json_decode((string) $response->getBody());
            DB::commit();
        } catch (BadResponseException $e) {
            DB::rollback();
            throw new Exception($e->getMessage());
        }
        //$res = $this->_getToken();
        return $res;
    }
    public function sendAnggotaApi212($data)
    {
        DB::beginTransaction();
        $client = new Client;
        try {
            $response = $client->post($this->pos_api_url . '/api/anggota', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->_getToken(),
                    'Accept' => 'application/json'
                ],
                'form_params' => [
                    'anggota' => [
                        'nomor_anggota' => $data['no_pelanggan'],
                        'jenis_kelamin' => $data['jenis_kelamin'],
                        'tanggal_lahir' => $data['tanggal_lahir'],
                        'kota' => $data['kota'],
                        'status_keanggotaan' => $data['status_keanggotaan'],
                        'status_aktifasi' => $data['status_aktifasi'],
                        'pekerjaan' => $data['pekerjaan'],
                        'nama' => $data['nama'],
                        'alamat' => $data['alamat'],
                        //'alamat_pajak' => $data['alamat_pajak'],
                        //'telepon' => $data['telepon'],
                        'no_telepon' => $data['telepon'],
                        'email' => $data['email']
                    ]
                ]
            ]);
            $res = json_decode((string) $response->getBody());
            DB::commit();
        } catch (BadResponseException $e) {
            DB::rollback();
            throw new Exception($e->getMessage());
        }
        //$res = $this->_getToken();
        return $res;
    }


    // Anggota Create To POS API
    public function sendPostAnggota($data)
    {
        DB::beginTransaction();
        $client = new Client;
        try {
            $response = $client->post($this->pos_api_url . '/api/anggota/', [
                'form_params' => [
                    'anggota' => $data
                ]
            ]);
            $res = json_decode($response->getBody()->getContents());
            DB::commit();
        } catch (BadResponseException $e) {
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $res;
    }


    // Anggota Update To POS API
    public function sendUpdateAnggota($data)
    {
        DB::beginTransaction();
        $client = new Client;
        try {
            $response = $client->put($this->pos_api_url . '/api/anggota/' . $data['oldNoAnggota'], [
                'form_params' => [
                    'anggota' => $data
                ]
            ]);

            $res = json_decode($response->getBody()->getContents());
            DB::commit();
        } catch (BadResponseException $e) {
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $res;
    }


    // Anggota Delete To POS API
    public function sendDeleteAnggota($no_anggota)
    {
        DB::beginTransaction();
        $client = new Client;
        try {
            $response = $client->delete($this->pos_api_url . '/api/anggota/' . $no_anggota);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }

        return true;
    }


    public function testing()
    {
        $data['no_pelanggan'] = 71627673;
        $data['jenis_kelamin'] = 'L';
        $data['tanggal_lahir'] = '1960-05-16';
        $data['kota'] = 'Samarinda';
        $data['status_keanggotaan'] = 3;
        $data['status_aktifasi'] = true;
        $data['pekerjaan'] = 'swasta';
        $data['nama'] = 'seseorang';
        $data['alamat'] = 'samarinda';
        $data['no_ktp'] = '63726376273';
        $data['telepon'] = '167261763271';
        $data['email'] = 'anggta@mail.com';
        return self::sendAnggotaApi212($data);
    }
}

<?php
namespace App\Helpers;

use GuzzleHttp\Client;

class Zenziva
{
    // https://reguler.zenziva.net/apps/smsapi.php?userkey=xxxxxx&passkey=xxxxxx&nohp=081234567890&pesan=test+kirim+sms+dengan+zenzivaapi

    /*
    *
    * Pastikan di .env harus ada
    *
    * ZENZIVA_USERNAME=hnzc9b
    * ZENZIVA_PASSWORD=thortechasia4321
    *
    ****/

    /*
    * Penggunaan
    *
    * $zenziva = New Zenziva;
    *
    * $zenziva->number('0812xxxxx')->message('Pesan Zenziva Test Api')->send();
    *
    * Atau
    *
    * $zenziva->send('0812xxxx', 'Pesan Zenziva Test Api);
    *
    ***/

    protected $type = 'reguler';
    /**
     * Zenziva api.
     *
     * @var string
     */
    protected $url = 'https://reguler.zenziva.net/apps/smsapi.php';

    /**
     * Zenziva username.
     *
     * @var string
     */
    protected $username;

    /**
     * Zenziva password.
     *
     * @var string
     */
    protected $password;

    /**
     * Phone number.
     *
     * @var string
     */
    public $number;

    /**
     * Message.
     *
     * @var string
     */
    public $message;

    public function __construct()
    {
        $this->username = env('ZENZIVA_USERNAME') ?? '';
        $this->password = env('ZENZIVA_PASSWORD') ?? '';
    }

    /**
     * Set destination phone number.
     *
     * @param $number Phone number
     *
     * @return self
     */
    public function number($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Set messages.
     *
     * @param $message  Message
     *
     * @return self
     */
    public function message($message)
    {
        if (!is_string($message)) {
            throw new \Exception('Pesan harus berupa karakter');
        }
        $this->message = $message;

        return $this;
    }

    /**
     * @param $number  Phone number
     * @param $message  Message
     *
     * @throws \Exception
     */
    public function send($number = '', $message = ''): bool
    {
        if (!is_string($message)) {
            throw new \Exception('Pesan harus berupa karakter');
        }

        $this->number = !empty($number) ? $number : $this->number;
        $this->message = !empty($message) ? $message : $this->message;

        if (empty($this->number)) {
            throw new \Exception('Nomor telepon belum diisi');
        }

        if (is_null($this->message)) {
            throw new \Exception('Pesan belum diisi');
        }

        if (empty($this->username) || empty($this->password)) {
            throw new \Exception('Userkey dan Passkey belum dideklarasikan, Tolong Perikas Config Env');
        }

        $params = http_build_query([
            'userkey' => $this->username,
            'passkey' => $this->password,
            'nohp'    => $this->number,
            'pesan'   => $this->message,
        ]);

        $url = $this->url.'?'.$params;

        $client = new Client();
        $req = $client->request('GET', $url, [
                'connect_timeout' => 0,
            ]);

        if ($req) {
            return true;
        }

        return false;
    }
}

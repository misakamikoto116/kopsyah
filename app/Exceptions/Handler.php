<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }
    
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
    if ($request->segment(1) == 'api') {
        return response()->json([
                    'error' => 'not_found',
                    'sukses' => false,
                    'resp_num' => 403], $exception->getStatusCode());
    } else {
        $data['title'] = '404';
        $data['html_msg'] = '<div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">3</span></div>';
        $data['message'] = 'Tidak  !';
        $data['detail_message'] = null;
        return response()
                    ->view('errors.request_error', $data, 403);
    }
}
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                // not found
                case 404:
                    if ($request->segment(1) == 'api') {
                        return response()->json([
                            'error' => 'not_found',
                            'sukses' => false,
                            'resp_num' => 404], $exception->getStatusCode());
                    } else {
                        $data['title'] = '404';
                        $data['html_msg'] = '<div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span></div>';
                        $data['message'] = 'Halaman Tidak di Temukan !';
                        $data['detail_message'] = null;
                        return response()
                            ->view('errors.request_error', $data, 404);
                    }
                    break;
                case 401:
                    if ($request->segment(1) == 'api') {
                        return response()->json(['error' => 'not_authorized', 'sukses' => false, 'resp_num' => 401], $exception->getStatusCode());
                    } else {
                        $data['title'] = '401';
                        $data['html_msg'] = '<div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">1</span></div>';
                        $data['message'] = 'Tidak di ijinkan akses !';
                        $data['detail_message'] = 'Anda tidak di izinkan akses halaman ini';
                        return response()
                            ->view('errors.request_error', $data, 401);
                    }
                    break;
                // internal error
                case 500:
                    return response()->view('errors.500', array(), 500);
                    break;
                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        }

        return parent::render($request, $exception);
    }
}

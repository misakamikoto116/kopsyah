<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TentangModel;
use App\Http\Requests\FaqRequest;

class FaqController extends Controller
{
    public function index(Request $request){
        // $semuaTentang= TentangModel::where('jenis','=','4')->get();
        $semuaTentang= TentangModel::where('jenis','=','4')->Cari($request)->paginate(5);
        return view('faq.awal',compact('semuaTentang'));
    }

    public function create(){
        return view('faq.tambah');
    }

    public function store(FaqRequest $input){
        $tentang= new TentangModel();
        $tentang->judul=$input->judul;
        $tentang->isi=$input->isi;
        $tentang->jenis = '4';
        $informasi=$tentang->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('faq.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $tentang= TentangModel::findOrFail($id);
        return view('faq.edit')->with(array('tentang'=>$tentang));
    }

    public function update($id, FaqRequest $input){
        $tentang= TentangModel::findOrFail($id);
        $tentang->judul=$input->judul;
        $tentang->isi=$input->isi;
        $informasi=$tentang->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('faq.index')->with(['informasi'=>$informasi]);
    }

    public function destroy($id){
        $tentang= TentangModel::findOrFail($id);
        $informasi=$tentang->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('faq.index')->with(['informasi'=>$informasi]);

    }
}

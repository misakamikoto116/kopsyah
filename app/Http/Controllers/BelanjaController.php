<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\BelanjaModel;

class BelanjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semuaBelanja = BelanjaModel::get();

        return view('belanja.awal', compact('semuaBelanja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $anggota = AnggotaModel::all();

        $anggota_pluck = AnggotaModel::pluck('nama', 'id');

        return view('belanja.tambah', compact('anggota_pluck', 'semuaBelanja'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $nama_barang = $request->jenis_input;
        // $merek = $request->merk_input;
        // $jumlah = $request->jumlah_input;
        // $periode = $request->periode_input;
        // $anggota = $request->anggota_input;

        // $count = count($nama_barang);
        // $count1 = $count + 1;

        // for ($i = 1; $i < $count1; ++$i) {
        //     $belanja = new BelanjaModel();
        //     $belanja->nama_barang = $nama_barang[$i];
        //     $belanja->merek = $merek[$i];
        //     $belanja->jumlah = $jumlah[$i];
        //     $belanja->periode = $periode[$i];
        //     $belanja->anggota_id = $anggota[$i];

        //     $belanja->save();
        // }

        $belanja_bulanan = [];
        foreach ($request->nama_barang as $key => $belanja) {
            $belanja_bulanan[] = ([
                'nama_barang' => $request->nama_barang[$key],
                'merek' => $request->merek[$key],
                'jumlah' => $request->jumlah[$key],
                'periode' => $request->periode[$key],
                'anggota_id' => $request->anggota_id[$key],
            ]);
        }

        $save = BelanjaModel::insert($belanja_bulanan);

        $informasi = $belanja_bulanan = $save ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('belanja.index')->with(['informasi' => $informasi]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $belanja = BelanjaModel::findOrFail($id);
        $informasi = $belanja->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('belanja.index')->with(['informasi' => $informasi]);
    }
}

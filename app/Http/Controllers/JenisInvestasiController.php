<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JenisInvestasiModel;
use App\Http\Requests\JenisInvestasiRequest;

class JenisInvestasiController extends Controller
{
    public function index(Request $request){
        $semuaJenisInvestasi= JenisInvestasiModel::Cari($request)->orderBy('created_at', 'DESC')->paginate(5);
        return view('jenis_investasi.awal',compact('semuaJenisInvestasi'));
    }

    public function create(){
        return view('jenis_investasi.tambah');
    }

    public function store(JenisInvestasiRequest $input){
        $jenis_investasi= new JenisInvestasiModel();
        $jenis_investasi->nama=$input->nama;
        $jenis_investasi->jumlah_investasi = $input->jumlah_investasi;
        $jenis_investasi->deskripsi=$input->deskripsi;
        $jenis_investasi->status= $input->status;
        $informasi=$jenis_investasi->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('jenis_investasi.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $jenis_investasi= JenisInvestasiModel::findOrFail($id);
        return view('jenis_investasi.edit')->with(array('jenis_investasi'=>$jenis_investasi));
    }

    public function update($id, JenisInvestasiRequest $input){
        $jenis_investasi= JenisInvestasiModel::findOrFail($id);
        $jenis_investasi->nama=$input->nama;
        $jenis_investasi->jumlah_investasi = $input->jumlah_investasi;
        $jenis_investasi->deskripsi=$input->deskripsi;
        $jenis_investasi->status= $input->status;
        $informasi=$jenis_investasi->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('jenis_investasi.index')->with(['informasi'=>$informasi]);
    }

    public function show($id){
        $jenis_investasi = JenisInvestasiModel::findOrFail($id);
        return view('jenis_investasi.lihat')->with(array('jenis_investasi'=>$jenis_investasi));
    }

    public function destroy($id){
        $jenis_investasi= JenisInvestasiModel::findOrFail($id);
        $informasi=$jenis_investasi->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('jenis_investasi.index')->with(['informasi'=>$informasi]);

    }
}

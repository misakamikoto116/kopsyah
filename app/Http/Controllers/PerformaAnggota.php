<?php
namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

trait PerformaAnggota
{
    public function getOmset(Request $request)
    {
        $tanggal_now= \Carbon\Carbon::now()->format('Y-m-d');
        $tanggal_yesterday= \Carbon\Carbon::yesterday()->format('Y-m-d');

        $client = new Client([
            'base_uri' => 'https://pos.kelontongku.com/',
            'headers' => ['app-token' => '323e7deb6725b189d7665c0159160cba'],
            'http_errors' => false,
            'timeout' => 0,
        ]);
        $response = $client->request('POST', 'api/omzet', [
            'form_params' => [
                'tgl_hari_ini' => $tanggal_now,
                'tgl_kemarin' => $tanggal_yesterday
            ]
        ]);
        $body = $response->getBody();
        $arr = json_decode($body, true);
        return response()->json($arr, 200);
        
    }
}


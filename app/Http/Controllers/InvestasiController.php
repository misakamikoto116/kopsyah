<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\InvestasiModel;
use App\Model\AnggotaModel;
use App\Model\JenisInvestasiModel;
use App\Model\NotificationsModel;
use App\Model\UserModel;
use App\Http\Requests\InvestasiRequest;
use \App\Notifications\VerifikasiInvestasi;
use Intervention\Image\ImageManagerStatic as Image;
class InvestasiController extends Controller
{
    public function index(Request $request){

        //list semua investasi
        $semuaInvestasi= InvestasiModel::Cari($request)->orderBy('id', 'DESC')->paginate(10);

        //hapus notifikasi new investasi di navbr
        $new_inves= NotificationsModel::where('type','=','App\Notifications\NewInvestasi')->delete();
        return view('investasi.awal',compact('semuaInvestasi'));
    }

    public function create(){
        $jenisInvestasiPluck = JenisInvestasiModel::where('status','=','0')->pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_keanggotaan','=','3')
                                    ->where('status_aktifasi','=','1')->get();
        return view('investasi.tambah', compact('jenisInvestasiPluck','anggotaPluck'));
    }

    public function store(InvestasiRequest $input){

        $investasi = new InvestasiModel($input->only('jenis_investasi_id','anggota_id'));

        //prngaturan format tanggal dari datepicker ke database format
        $tgl_pem = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        $pembayaran = date('Y-m-d', strtotime($tgl_pem));   

        $investasi->jumlah = $input->jumlah;
        $investasi->tanggal_pembayaran = $pembayaran;
        $investasi->status = $input->status;
        $investasi->jenis_investasi_id = $input->jenis_investasi_id;
        $investasi->biaya_cetak_investasi = $input->biaya_cetak_investasi;

        //pengaturan path file
        $file = $input->file('foto_resi');
        $pic = time().'.'.$file->getClientOriginalExtension();
        $path = public_path('images/investasi/'.$pic);
        Image::make($file->getRealPath())->resize(468,249)->save($path);
        $investasi->foto_resi = 'images/investasi/'.$pic;             
        
        $informasi=$investasi->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('investasi.index')->with(['informasi'=>$informasi]);

    }

    public function edit($id){
        $investasi= InvestasiModel::findOrFail($id);
        $jenisInvestasiPluck = JenisInvestasiModel::pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_keanggotaan','=','3')
                                    ->where('status_aktifasi','=','1')->get();
        return view('investasi.edit',compact('investasi','jenisInvestasiPluck', 'anggotaPluck'))->with(array('investasi'=>$investasi));
    }

    public function update($id, InvestasiRequest $input){
        // $validator = \Validator::make($input->all(), [
        //     'jumlah' => 'required|numeric',
        //     'anggota_id' => 'required|numeric',
        //     'jenis_investasi_id' => 'required|numeric',
        // ]);

        // if ($validator->fails()) {

        //     $informasi = 'Jumlah harus disi angka, nama anggota dan jenis investasi harus diisi';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }
        $investasi = InvestasiModel::findOrFail($id);
        $investasi->anggota_id = $input->anggota_id;
        $investasi->jenis_investasi_id = $input->jenis_investasi_id;
        $investasi->jumlah = $input->jumlah;
        $investasi->biaya_cetak_investasi = $input->biaya_cetak_investasi;
        
            //jika foto resi tidak null
           if ($input->foto_resi != null) {
                // $validator = \Validator::make($input->all(), [
                // 'foto_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                // ]);

                // if ($validator->fails()) {

                //     $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,atau PNG ';
                //     return redirect()->back()->with(['informasi' => $informasi]);

                // }

            //pengaturan path file
            $file = $input->file('foto_resi');
            $pic = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/investasi/' . $pic);
            Image::make($file->getRealPath())->resize(468, 249)->save($path);
            $investasi->foto_resi = 'images/investasi/' . $pic;
           }

           //jika tanggal pembayaran tidak null
           if ($input->tanggal_pembayaran != null) {
                // $validator = \Validator::make($input->all(), [
                //     'tanggal_pembayaran' => 'date_format:"d-m-Y"|required',
                // ]);

                // if ($validator->fails()) {

                //     $informasi = 'Format tanggal pembayaran salah';
                //     return redirect()->back()->with(['informasi' => $informasi]);

                // }

                //pengaturan format tanggal dari datepicker ke database
                $tgl_pem = date('d-m-Y', strtotime($input->tanggal_pembayaran));
                $pembayaran = date('Y-m-d', strtotime($tgl_pem));  
                $investasi->tanggal_pembayaran = $pembayaran;
           }

            if ($investasi->save()) {
                $informasi = 'Berhasil update Data';
            } else {
                $informasi = 'Gagal update Data';
            }
            return redirect()->route('investasi.index')->with(['informasi'=>$informasi]);
    
}

    public function show($id){
        $investasi = InvestasiModel::findOrFail($id);
        return view('investasi.lihat')->with(array('investasi'=>$investasi));
    }

    public function destroy($id){
        $investasi= InvestasiModel::findOrFail($id);
        $informasi=$investasi->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('investasi.index')->with(['informasi'=>$informasi]);

    }

    public function status($id,$status) {

        //mengubah status verifikasi investasi melalui klik tombol
        $investasi = InvestasiModel::findOrFail($id);
        $anggota= AnggotaModel::where('id','=',$investasi->anggota_id)->first();       
        $user= UserModel::where('id','=',$anggota->users_id)->first();        
        $update = $investasi->where('id',$id)->firstOrFail()->update(['status'=>$status]);
        if($status == 1) {
            $informasi = 'Investasi Berhasil Divalidasi';
            $message = 'Assalamualaikum wr. wb , Pembayaran investasi di '.$investasi->jenis_investasi->nama.' anda pada tanggal : '.$investasi->tanggal_pembayaran.' dengan jumlah : '.$investasi->jumlah.' telah tervalidasi. 212 Mart Samarinda';
            $sms = new \App\Helpers\Zenziva();
            $sms->send($anggota->no_telepon, $message);
        }
        else {
            $informasi = 'Investasi Batal Divalidasi';
        }

        //notifikasi ke navbar investor bahwa investasi dia telah di verifikasi
        $user->notify(new VerifikasiInvestasi());
        return redirect()->route('investasi.index')->with(['informasi'=>$informasi]);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\PembayaranModel;
use App\Model\JenisPembayaranModel;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use \App\Notifications\NewPembayaran;
use \App\Http\Requests\AnggotaPembayaranRequest;
use App\Model\NotificationsModel;
use Intervention\Image\ImageManagerStatic as Image;

class AnggotaPembayaranController extends Controller
{
    public function index()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        
        //hapus notifikasi adanya pembayaran baru dari navbar
        $del_verif_bayar= NotificationsModel::where('type','=','App\Notifications\VerifikasiPembayaran')
                    ->Where('notifiable_id','=',Auth::id())->delete();

        $semuaPembayaran = PembayaranModel::where('anggota_id', '=', $anggota->id)->get()->sortByDesc('id');
        $total= PembayaranModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');

        $tmp= new PembayaranModel();
        if($tmp->status == 0){
            $tmp->status = "Belum diverifikasi";
        }else{
            $tmp->status= "Sudah diverifikasi";
        }

        return view('only_anggota.simpanan.awal', compact('semuaPembayaran','tmp','total','anggota'));
    }
    public function create()
    {
        //menampilkan jenis pembayaran kecuali simpanan pokok
        $jenisPembayaranPluck = JenisPembayaranModel::where('id','!=','1')->pluck('nama', 'id');

        //menampilkan pembayaran simpanan wajib terakhir
        $pembayaran= PembayaranModel::where('jenis_pembayaran_id', '=', '2')->orderBy('id', 'DESC')->first();

        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        $jenisPokok= JenisPembayaranModel::where('id','=','1')->first();
        $jenisWajib= JenisPembayaranModel::where('id','=','2')->first();
        return view('only_anggota.simpanan.tambah', compact('jenisPembayaranPluck',
                                    'jenisPokok','jenisWajib','pembayaran','anggota'));
    }

    public function store(AnggotaPembayaranRequest $input)
    {
        //validasi tanggal pembayaran
        $input->validated();
        $pembayaran = new PembayaranModel();

        $tgl_bayar = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        $bayarpembayaran = date('Y-m-d', strtotime($tgl_bayar));


        $pembayaran->jenis_pembayaran_id= $input->jbayar;
        $jenis_pembayaran=  JenisPembayaranModel::where('id', '=', $input->jbayar)->first();

        // input tempo pembayaran
        $lama_tempo=$jenis_pembayaran->lama_tempo;
        $tgl=date('Y-m-d', strtotime("+$lama_tempo months"));

        //pengaturan path file resi
        $file = $input->file('file_resi');
        $pic = time().'.'.$file->getClientOriginalExtension();
        $path = public_path('images/pembayaran/'.$pic);
        Image::make($file->getRealPath())->resize(836,498)->save($path);
        
        $anggota = AnggotaModel::where('users_id','=', Auth::id())->first();
        $pembayaran->anggota_id= $anggota->id;
        if($input->jbayar== 1){
                 $pembayaran->jumlah = $input->jumlahP;
             }else if ($input->jbayar== 2){
                 $pembayaran->jumlah = $input->jumlahW;
             }else {
                    $validator = \Validator::make($input->all(), [
                        'jumlah' => 'numeric|required',

                    ]);
                    if ($validator->fails()) {

                        $informasi = 'Jumlah Harus Angka';
                        return redirect()->back()->with(['informasi' => $informasi]);

                    }
                 $pembayaran->jumlah = $input->jumlah;
        }
        $pembayaran->tanggal_tempo = $tgl;
        $pembayaran->tanggal_pembayaran = $bayarpembayaran;
        $pembayaran->file_resi = 'images/pembayaran/'.$pic;
        $pembayaran->status = '0';
        
        $informasi=$pembayaran->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        $anggota= AnggotaModel::where('id','=',$pembayaran->anggota_id)->first();       
        $user= UserModel::where('id','=',$anggota->users_id)->first();

        //notifikasi ke navbar admin untuk input pembayaran yang baru saja dilakukan
        $user->notify(new NewPembayaran());
        return redirect()->route('simpanan_anggota.index')->with(['informasi'=>$informasi]);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JabatanModel;

class JabatanController extends Controller
{
    public function index(Request $request){
        $semuaJabatan= JabatanModel::Cari($request)->orderBy('created_at', 'DESC')->paginate(5);
        return view('jabatan.awal',compact('semuaJabatan'));
    }

    public function create(){
        return view('jabatan.tambah');
    }

    public function store(Request $input){
        $this->validate($input,[
            'posisi'=> 'required',
            'deskripsi'=> 'required',
            'tupoksi'=>'required'
            ]);
        $jabatan= new JabatanModel();
        $jabatan->posisi=$input->posisi;
        $jabatan->deskripsi=$input->deskripsi;
        $jabatan->tupoksi=$input->tupoksi;
        $informasi=$jabatan->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('jabatan.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $jabatan= JabatanModel::findOrFail($id);
        return view('jabatan.edit')->with(array('jabatan'=>$jabatan));
    }

    public function update($id, Request $input){
        $jabatan= JabatanModel::findOrFail($id);
        $jabatan->posisi=$input->posisi;
        $jabatan->deskripsi=$input->deskripsi;
        $jabatan->tupoksi=$input->tupoksi;
        $informasi=$jabatan->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('jabatan.index')->with(['informasi'=>$informasi]);
    }

    public function show($id){
        $jabatan = JabatanModel::findOrFail($id);
        return view('jabatan.lihat')->with(array('jabatan'=>$jabatan));
    }

    public function destroy($id){
        $jabatan= JabatanModel::findOrFail($id);
        $informasi=$jabatan->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('jabatan.index')->with(['informasi'=>$informasi]);

    }
}

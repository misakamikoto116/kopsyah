<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PembayaranModel;
use App\Model\JenisPembayaranModel;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use App\Model\NotificationsModel;
use App\Notifications\VerifikasiPembayaran;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\PembayaranRequest;

class PembayaranController extends Controller
{
    public function index(Request $request)
    {
        $allPembayaran = PembayaranModel::Cari($request)->orderBy('id', 'DESC')->paginate(10);

        //notifikasi pembayaran baru di navbar dihapus
        $del_new_bayar = NotificationsModel::where('type', '=', 'App\Notifications\NewPembayaran')->delete();

        return view('pembayaran.awal', compact('allPembayaran'));
    }

    public function create()
    {
        $jenisPembayaranPluck = JenisPembayaranModel::pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_aktifasi', '=', '1')->get();
        $jenisPokok = JenisPembayaranModel::where('id', '=', '1')->first();
        $jenisWajib = JenisPembayaranModel::where('id', '=', '2')->first();

        return view('pembayaran.tambah', compact('jenisPembayaranPluck','anggotaPluck',
                                    'jenisPokok', 'jenisWajib'));
    }

    public function store(Request $input)
    {
        $pembayaran = new PembayaranModel($input->only('anggota_id'));

        //jika jenis pembayaran yang diinput simpanan pokok
        if ($input->jbayar == 1) {
            $validator = \Validator::make($input->all(), [
                'jumlahP' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $informasi = 'Jumlah harus angka';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            $pembayaran->jumlah = $input->jumlahP;

            //jika jenis pembayaran yang diinput simpanan wajib
        } elseif ($input->jbayar == 2) {
            $validator = \Validator::make($input->all(), [
                'jumlahW' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $informasi = 'Jumlah harus angka';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            $pembayaran->jumlah = $input->jumlahW;

            //jika jenis pembayaran yang diinput simpanan sukarela
        } else {
            $validator = \Validator::make($input->all(), [
                'jumlah' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $informasi = 'Jumlah harus angka';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            $pembayaran->jumlah = $input->jumlah;
        }

        //format tanggal datepicker diubah ke database format
        $tgl_bayar = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        $tglpembayaran = date('Y-m-d', strtotime($tgl_bayar));

        $pembayaran->jenis_pembayaran_id = $input->jbayar;
        $pembayaran->tanggal_pembayaran = $input->tanggal_pembayaran;
        $pembayaran->status = $input->status;

        // tempo ambil dari tempo sesuai jenis pembayaran yang diinput
        $jenis_pembayaran = JenisPembayaranModel::where('id', '=', $input->jbayar)->first();
        $lama_tempo = $jenis_pembayaran->lama_tempo;
        $pembayaran->tanggal_pembayaran = $tglpembayaran;
        $tgl = date('Y-m-d', strtotime("$tglpembayaran+$lama_tempo months"));
        $pembayaran->tanggal_tempo = $tgl;

        //input file resi
        $file = $input->file('file_resi');
        $pic = time().'.'.$file->getClientOriginalExtension();
        $path = public_path('images/pembayaran/'.$pic);
        Image::make($file->getRealPath())->resize(468, 249)->save($path);
        $pembayaran->file_resi = 'images/pembayaran/'.$pic;
        $informasi = $pembayaran->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('pembayaran.index')->with(['informasi' => $informasi]);
    }

    public function edit($id)
    {
        $pembayaran = PembayaranModel::findOrFail($id);
        $jenisPembayaranPluck = JenisPembayaranModel::pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_aktifasi', '=', '1')->get();

        return view('pembayaran.edit', compact('pembayaran', 'jenisPembayaranPluck', 'anggotaPluck'))->with(array('pembayaran' => $pembayaran));
    }

    public function update($id, PembayaranRequest $input)
    {
        $pembayaran = PembayaranModel::findOrFail($id);

        // $validator = \Validator::make($input->all(), [
        //     'status' => 'required|numeric',
        //     'jumlah' => 'required|numeric',
        //     'anggota_id' => 'required|numeric',
        // ]);
        // if ($validator->fails()) {
        //     $informasi = 'Jumlah harus diisi angka, status pembayaran dan nama anggota harus diisi';

        //     return redirect()->back()->with(['informasi' => $informasi]);
        // }

        $pembayaran->anggota_id = $input->anggota_id;
        $pembayaran->jumlah = $input->jumlah;
        $pembayaran->status = $input->status;

        if ($input->tanggal_pembayaran != null) {
            $tgl_bayar = date('d-m-Y', strtotime($input->tanggal_pembayaran));
            $tglpembayaran = date('Y-m-d', strtotime($tgl_bayar));
            $pembayaran->tanggal_pembayaran = $tglpembayaran;
            $tmp = $pembayaran->jenis_pembayaran_id;

            //tempo
            $jenis_pembayaran = JenisPembayaranModel::where('id', '=', $tmp)->first();
            $lama_tempo = $jenis_pembayaran->lama_tempo;
            $bayar = $pembayaran->tanggal_pembayaran;
            $tgl = date('Y-m-d', strtotime("$bayar+$lama_tempo months"));
            $pembayaran->tanggal_tempo = $tgl;
        }

        //jika inputan file resi tidak null
        if ($input->file_resi != null) {
            $validator = \Validator::make($input->all(), [
                'file_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);

            if ($validator->fails()) {
                $informasi = 'File resi harus jpeg,png,jpg,JPEG,JPG, atau PNG dan file resi maksimal 2048kb';

                return redirect()->back()->with(['informasi' => $informasi]);
            }

            //pengaturan path gambar
            $file = $input->file('file_resi');
            $pic = time().'.'.$file->getClientOriginalExtension();
            $path = public_path('images/pembayaran/'.$pic);
            Image::make($file->getRealPath())->resize(468, 249)->save($path);
            $pembayaran->file_resi = 'images/pembayaran/'.$pic;
        }

        if ($pembayaran->save()) {
            $informasi = 'Berhasil update Data';
        } else {
            $informasi = 'Gagal update Data';
        }

        return redirect()->route('pembayaran.index')->with(['informasi' => $informasi]);
    }

    public function show($id)
    {
        $pembayaran = PembayaranModel::findOrFail($id);

        return view('pembayaran.lihat')->with(array('pembayaran' => $pembayaran));
    }

    public function destroy($id)
    {
        $pembayaran = PembayaranModel::findOrFail($id);
        $informasi = $pembayaran->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('pembayaran.index')->with(['informasi' => $informasi]);
    }

    public function status($id, $status)
    {
        //status pembayaran diubah melalui klik tombol
        $pembayaran = PembayaranModel::findOrFail($id);
        $anggota = AnggotaModel::where('id', '=', $pembayaran->anggota_id)->first();
        $user = UserModel::where('id', '=', $anggota->users_id)->first();

        $update = $pembayaran->where('id', $id)->firstOrFail()->update(['status' => $status]);
        if ($status == 1) {
            $informasi = 'Pembayaran Berhasil Divalidasi';
            $message = 'Assalamualaikum wr. wb , Pembayaran '.$pembayaran->jenis_pembayaran->nama.' anda pada tanggal : '.$pembayaran->tanggal_pembayaran.' dengan jumlah : '.$pembayaran->jumlah.' telah tervalidasi. 212 Mart Samarinda';
            $sms = new \App\Helpers\Zenziva();
            $sms->send($anggota->no_telepon, $message);
        } else {
            $informasi = 'Pembayaran Batal Divalidasi';
        }

        //notif ke halaman anggota jika pembayaran telah diverifikasi
        $user->notify(new VerifikasiPembayaran());

        return redirect()->route('pembayaran.index')->with(['informasi' => $informasi]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShuRequest;
use App\Model\SisaModel;

class ShuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shu = SisaModel::all();

        return view('shu.awal', compact('shu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shu.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ShuRequest $input)
    {
        // $this->validate($input,[
        //     'nama'=> 'required',
        //     'deskripsi'=> 'required',
        //     'persentase'=> 'required'
        //     ]);
        $shu = new SisaModel();
        $shu->nama = $input->nama;
        $shu->deskripsi = $input->deskripsi;
        $shu->persentase = $input->persentase;
        $informasi = $shu->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('shu.index')->with(['informasi' => $informasi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shu = SisaModel::findOrFail($id);

        return view('shu.edit')->with(array('shu' => $shu));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\shu                 $shu
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, ShuRequest $input)
    {
        // $validator = \Validator::make($input->all(), [
        //     'persentase' => 'numeric|required',
        // ]);
        // if ($validator->fails()) {
        //     $informasi = 'Persentase harus angka';

        //     return redirect()->back()->with(['informasi' => $informasi]);
        // }
        $shu = SisaModel::findOrFail($id);
        $shu->nama = $input->nama;
        $shu->deskripsi = $input->deskripsi;
        $shu->persentase = $input->persentase;
        $informasi = $shu->save() ? 'Berhasil update data' : 'Gagal update data';

        return redirect()->route('shu.index')->with(['informasi' => $informasi]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\shu $shu
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shu = SisaModel::findOrFail($id);
        $informasi = $shu->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('shu.index')->with(['informasi' => $informasi]);
    }
}

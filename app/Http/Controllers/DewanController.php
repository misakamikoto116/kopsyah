<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\DewanPengurusModel;
use App\Http\Requests\DewanRequest;
use Intervention\Image\ImageManagerStatic as Image;

class DewanController extends Controller
{
    public function index(Request $request)
    {
        $dewan = DewanPengurusModel::Cari($request)->orderBy('created_at', 'DESC')->paginate(5);

        return view('dewan.awal', compact('dewan'));
    }

    public function download(Request $request)
    {
        return response()->download(public_path('/'.$request['images/kepengurusan']));
    }

    public function create()
    {
        return view('dewan.tambah');
    }

    public function store(DewanRequest $input)
    {
        $dewan = new DewanPengurusModel();
        $tgl_jbt = date('d-m-Y', strtotime($input->tanggal_menjabat));
        $menjabat = date('Y-m-d', strtotime($tgl_jbt));
        $dewan->tanggal_menjabat = $menjabat;
        $dewan->struktur = $input->struktur;
        $dewan->jabatan = $input->jabatan;
        $dewan->nama = $input->nama;
        $dewan->profil = $input->profil;
        $dewan->deskripsi = $input->deskripsi;
        $dewan->status_kepengurusan = $input->status_kepengurusan;

        $file = $input->file('sk_pengurus');
        $pic = 'sk'.time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path('images/kepengurusan/'), $pic);
        $dewan->sk_pengurus = 'images/kepengurusan/'.$pic;

        $file_foto = $input->file('foto');
        $pic = 'f'.time().'.'.$file_foto->getClientOriginalExtension();
        $path = public_path('images/kepengurusan/'.$pic);
        Image::make($file_foto->getRealPath())->resize(836, 498)->save($path);
        $dewan->foto = 'images/kepengurusan/'.$pic;

        $informasi = $dewan->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('dewan.index')->with(['informasi' => $informasi]);
    }

    public function show($id)
    {
        $dewan = DewanPengurusModel::findOrFail($id);

        return view('dewan.lihat')->with(array('dewan' => $dewan));
    }

    public function edit($id)
    {
        $dewan = DewanPengurusModel::findOrFail($id);

        return view('dewan.edit')->with(array('dewan' => $dewan));
    }

    public function update($id, DewanRequest $input)
    {
        $dewan = DewanPengurusModel::findOrFail($id);
        // $validator = \Validator::make($input->all(), [
        //     'struktur' => 'required|numeric',
        //     'jabatan' => 'required|numeric',
        //     'status_kepengurusan' => 'required|numeric',
        //     'deskripsi' => 'required',
        //     'profil' => 'required',
        // ]);

        // if ($validator->fails()) {

        //     $informasi = 'Struktur, Jabatan, Status Kepengurusan, Deskripsi, dan Profil tidak boleh kosong';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }
        $dewan->struktur = $input->struktur;
        $dewan->jabatan = $input->jabatan;
        $dewan->nama = $input->nama;
        $dewan->profil = $input->profil;
        $dewan->deskripsi = $input->deskripsi;
        $dewan->status_kepengurusan = $input->status_kepengurusan;

        if ($input->sk_pengurus != null) {
            // $validator = \Validator::make($input->all(), [
            //     'sk_pengurus' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx|max:2048|required',
            // ]);

            // if ($validator->fails()) {

            //     $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx';
            //     return redirect()->back()->with(['informasi' => $informasi]);

            // }

            $file = $input->file('sk_pengurus');
            $pic = 'sk'.time().'.'.$file->getClientOriginalExtension();
            $pic->move(public_path('images/kepengurusan/'), $pic);
            $dewan->sk_pengurus = 'images/kepengurusan/'.$pic;
        }

        if ($input->foto != null) {
            // $validator = \Validator::make($input->all(), [
            //     'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            // ]);

            // if ($validator->fails()) {

            //     $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,atau PNG ';
            //     return redirect()->back()->with(['informasi' => $informasi]);

            // }
            $file_foto = $input->file('foto');
            $pic = 'f'.time().'.'.$file_foto->getClientOriginalExtension();
            $path = public_path('images/kepengurusan/'.$pic);
            Image::make($file_foto->getRealPath())->resize(836, 498)->save($path);
            $dewan->foto = 'images/kepengurusan/'.$pic;
        }

        if ($input->tanggal_menjabat != null) {
            // $validator = \Validator::make($input->all(), [
            // 'tanggal_menjabat' => 'date_format:"d-m-Y"|required',
            // ]);

            // if ($validator->fails()) {

            //     $informasi = 'Format tanggal salah';
            //     return redirect()->back()->with(['informasi' => $informasi]);

            // }
            $tgl_jbt = date('d-m-Y', strtotime($input->tanggal_menjabat));
            $menjabat = date('Y-m-d', strtotime($tgl_jbt));
            $dewan->tanggal_menjabat = $menjabat;
        }

        if ($dewan->save()) {
            $informasi = 'Berhasil update Data';
        } else {
            $informasi = 'Gagal update Data';
        }

        return redirect()->route('dewan.index')->with(['informasi' => $informasi]);
    }

    public function destroy($id)
    {
        $dewan = DewanPengurusModel::findOrFail($id);
        $informasi = $dewan->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('dewan.index')->with(['informasi' => $informasi]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use DB;
use Validator;
use Carbon\Carbon;
use App\Model\AnggotaModel;
use Illuminate\Http\Request;
use App\Model\PenjualanModel;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ApiPenjualanController extends Controller
{
    
    public function store(Request $request, AnggotaModel $anggota)
    {   
        \DB::beginTransaction();
        try{
            $carbonNow = Carbon::now()->format('Y-m-d');

            $messages = [
                'total.required'            => 'Jotal tidak boleh kosong',
                'jml_umkm.required'         => 'Jumlah umkm tidak boleh kosong',
                'jml_non.required'          => 'Jumlah non tidak boleh kosong',
                'nomor_anggota.required'    => 'Nomor_anggota tidak boleh kosong',                      
            ];

            $validator = Validator::make($request->all(), [
                'total'         =>'required|string',
                'jml_umkm'      =>'required|string',
                'jml_non'       =>'required|string',
                'nomor_anggota' =>'required|string'            			
            ], $messages);

            if ($validator->fails()) {
                return response()->json([
                'sukses'           => 'Validasi Gagal cek kelengkapan input',
                'nomor_anggota'    => $request->nomor_anggota,
                'response'         => 400
                ], 400);    
            }        

            $Anggota = AnggotaModel::where('nomor_anggota', $anggota->nomor_anggota)->first();
            $data = new PenjualanModel();
            $data->tanggal          = $carbonNow;
            $data->total            = floatval(preg_replace('/[^\d.]/', '', $request->total));
            $data->jml_umkm         = floatval(preg_replace('/[^\d.]/', '', $request->jml_umkm));
            $data->jml_non          = floatval(preg_replace('/[^\d.]/', '', $request->jml_non));
            $data->nomor_anggota    = $request->nomor_anggota;
            $data->anggota_id       = $anggota->id;
            
            if($data->save()){
                $sukses         = "data saved";
                $nomor_anggota  = $request->nomor_anggota;
                $response       = 200;
            }else{
                $sukses         = "bad request !";
                $nomor_anggota  = $request->nomor_anggota;
                $response       = 400;
            }
            \DB::commit();
        }catch(\Exeception $exc){
            DB::rollBack();
            $r = ['error' => $exc->getMessage()];
            \Log::error('insert penjualan',[$exc->getMessage()]); 
        }

        return response()->json([
            'sukses'        => $sukses,
            'nomor_anggota' => $nomor_anggota,
            'response'      => $response
        ], $response);
    }


}

<?php

namespace App\Http\Controllers\Api;
use App\Model\JenisInvestasiModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AnggotaModel as Anggota;
class ApiAnggotaController extends Controller
{
    public function findAnggota(Request $request)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',

        ];

        if(!$request->hasHeader('unit-token')){
            return response()->json([
                'sukses'        => false,
                'nomer_anggota' => $request->nomer_anggota,
                'data'          => 'token tidak tepat'], 401, $headers);
        }
        $token_unit = $request->header('unit-token');
        $ada = JenisInvestasiModel::where('api_token','=',$token_unit)->count();
        if($ada < 1){
            return response()->json([
                'sukses'        => false, 
                'nomer_anggota' => $request->nomer_anggota,
                'data'          => 'tidak ditemukan'], 404, $headers);
        }
        $anggota = Anggota::where('nomor_anggota', $request->nomer_anggota)->first();
        if(null === $anggota){
            return response()->json([
                'sukses'        => false,
                'nomer_anggota' => $request->nomer_anggota,
                'data'          => 'tidak ditemukan'], 404,$headers);
        }
        $anggota_resp = [
            'sukses' => true,
            'nomer_anggota' => $request->nomer_anggota,
            'data' => $anggota,
        ];
        return response()->json($anggota_resp, 200, $headers);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JenisPembayaranModel;
use App\Http\Requests\JenisPembayaranRequest;

class JenisPembayaranController extends Controller
{
    public function index(Request $request)
    {
        $semuaJenisPembayaran= JenisPembayaranModel::Cari($request)->orderBy('created_at', 'DESC')->paginate(5);
        return view('jenis_pembayaran.awal', compact('semuaJenisPembayaran'));
    }

    public function create()
    {
        return view('jenis_pembayaran.tambah');
    }

    public function store(JenisPembayaranRequest $input)
    {
        $jenis_pembayaran= new JenisPembayaranModel();
        $jenis_pembayaran->nama=$input->nama;
        if (!$input->nominal) {
            $jenis_pembayaran->nominal='0';
        } else {
            $jenis_pembayaran->nominal=$input->nominal;
        }
        if (!$input->lama_tempo) {
            $jenis_pembayaran->lama_tempo='0';
        } else {
            $jenis_pembayaran->lama_tempo=$input->lama_tempo;
        }

        $jenis_pembayaran->deskripsi=$input->deskripsi;
        $informasi=$jenis_pembayaran->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('jenis_pembayaran.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id)
    {
        $jenis_pembayaran= JenisPembayaranModel::findOrFail($id);
        return view('jenis_pembayaran.edit')->with(array('jenis_pembayaran'=>$jenis_pembayaran));
    }

    public function update($id, JenisPembayaranRequest $input)
    {
        $jenis_pembayaran= JenisPembayaranModel::findOrFail($id);
        $jenis_pembayaran->nama=$input->nama;
        $jenis_pembayaran->nominal=$input->nominal;
        $jenis_pembayaran->lama_tempo=$input->lama_tempo;
        $jenis_pembayaran->deskripsi=$input->deskripsi;
        $informasi=$jenis_pembayaran->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('jenis_pembayaran.index')->with(['informasi'=>$informasi]);
    }

    public function show($id)
    {
        $jenis_pembayaran = JenisPembayaranModel::findOrFail($id);
        return view('jenis_pembayaran.lihat')->with(array('jenis_pembayaran'=>$jenis_pembayaran));
    }

    public function destroy($id)
    {
        $jenis_pembayaran= JenisPembayaranModel::findOrFail($id);
        $informasi=$jenis_pembayaran->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('jenis_pembayaran.index')->with(['informasi'=>$informasi]);
    }
}

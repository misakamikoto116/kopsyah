<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\InvestasiModel;
use App\Model\JenisInvestasiModel;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use \App\Notifications\NewInvestasi;
use App\Http\Requests\InvestorInvestasiRequest;
use App\Model\NotificationsModel;
use Intervention\Image\ImageManagerStatic as Image;

class InvestorInvestasiController extends Controller
{
    public function index()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        //notifikasi verifikasi investasi di navbar dihapus
        $del_verif_bayar= NotificationsModel::where('type','=','App\Notifications\VerifikasiInvestasi')
                    ->Where('notifiable_id','=',Auth::id())->delete();
        
        //list semua investasi dia
        $semuaInvestasi = InvestasiModel::where('anggota_id', '=', $anggota->id)->get()->sortByDesc('id');

        $tmp= new InvestasiModel();
        if ($tmp->status !== 1) {
            $tmp->status = "Belum diverifikasi";
        } else {
            $tmp->status= "Sudah diverifikasi";
        }

        $total= InvestasiModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');
        return view('only_investor.investasi.awal', compact('anggota','semuaInvestasi','tmp','total'));
    }
    public function create()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        $jenisInvestasiPluck = JenisInvestasiModel::where('status','=','0')->pluck('nama', 'id');
        return view('only_investor.investasi.tambah', compact('anggota','jenisInvestasiPluck'));
    }
    public function store(InvestorInvestasiRequest $input)
    {
        $validator = \Validator::make($input->all(), [
            'jumlah' => 'numeric|required',

        ]);
        if ($validator->fails()) {

            $informasi = 'Jumlah Harus Angka';
            return redirect()->back()->with(['informasi' => $informasi]);

        }

            //pengaturan path foto resi
            $file = $input->file('foto_resi');
            $pic = time().'.'.$file->getClientOriginalExtension();
            $path = public_path('images/investasi/'.$pic);
            Image::make($file->getRealPath())->resize(468,249)->save($path);
            
            $investasi = new InvestasiModel($input->only('jenis_investasi_id'));

        //pengaturan format dari datepicker ke database format
        $tgl_inves = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        $bayarinvestasi = date('Y-m-d', strtotime($tgl_inves));

            $anggota = AnggotaModel::where('users_id','=', Auth::id())->first();
            $id_anggota= $anggota->id;

            $investasi->anggota_id= $id_anggota;
            $investasi->jumlah = $input->jumlah;
            $investasi->tanggal_pembayaran = $bayarinvestasi;
            $investasi->foto_resi = 'images/investasi/'.$pic;
            $investasi->status = '0';
            
            $informasi=$investasi->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

            //notifikasi ke admin bahwa telah ada penambahan investasi dari investor ini
            $anggota= AnggotaModel::where('id','=',$investasi->anggota_id)->first();       
            $user= UserModel::where('id','=',$anggota->users_id)->first();
            $user->notify(new NewInvestasi());
            return redirect()->route('investasis.index')->with(['informasi'=>$informasi]);
        }
    
}

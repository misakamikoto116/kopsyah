<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\DokumenModel;
use Intervention\Image\ImageManagerStatic as Image;
use \Intervention\Image\File;
use App\Http\Requests\DokumenRequest;


class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $semuaDokumen = DokumenModel::all();
        $semuaDokumen = DokumenModel::Cari($request)->paginate(5);
        return view('dokumen.awal', compact('semuaDokumen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dokumen.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DokumenRequest $input)
    {
        // $validator = \Validator::make($input->all(), [
        //     'file_dokumen' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required'
        // ]);

        // if ($validator->fails()) {

        //     $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah pdf,doc,docx,ppt,pptx,xls,atau xlsx ';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }
        $dokumen = new DokumenModel($input->only('judul','deskripsi'));
        $file = $input->file('file_dokumen');
        // $pic = time() . '.' . $file->getClientOriginalExtension();
        $pic = $file->getClientOriginalName();
        
        $file->move(public_path('file/'), $pic);



        // $dokumen->file_dokumen = 'file/'.$pic;
        $dokumen->file_dokumen = $pic;

        $informasi = $dokumen->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('dokumen.index')->with(['informasi' => $informasi]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dokumen = DokumenModel::findOrFail($id);
        return view('dokumen.lihat')->with(array('dokumen' => $dokumen));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokumen = DokumenModel::findOrFail($id);
        return view('dokumen.edit')->with(array('dokumen' => $dokumen));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DokumenRequest $input, $id)
    {
        $dokumen = DokumenModel::findOrFail($id);
        $dokumen->judul=$input->judul;
        $dokumen->deskripsi = $input->deskripsi;
        if($input->file('file_dokumen') != null){
        $file = $input->file('file_dokumen');
        $pic = time() . '.' . $file->getClientOriginalExtension();
        // $path = public_path('file/' . $pic);

        $file->move(public_path('file/'), $pic);
        // $dokumen->file_dokumen = 'file/' . $pic;
        $dokumen->file_dokumen = $pic;
        }

        $informasi = $dokumen->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('dokumen.index')->with(['informasi' => $informasi]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokumen = DokumenModel::findOrFail($id);
        $informasi = $dokumen->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('dokumen.index')->with(['informasi' => $informasi]);
    }

    public function download(Request $request)
    {
        return response()->download(public_path('/file/' . $request['file']));  
    }
}

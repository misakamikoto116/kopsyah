<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\BelanjaModel;
use Auth;

class BelanjaInvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        $semuaBelanja = BelanjaModel::where('anggota_id', '=', $anggota->id)->get();

        return view('belanja_investor.awal', compact('semuaBelanja', 'anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        return view('belanja_investor.tambah', compact('anggota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $belanja = new BelanjaModel();
        // $belanja->nama_barang = $request->nama_barang;
        // $belanja->merek = $request->merek;
        // $belanja->jumlah = $request->jumlah;
        // $belanja->periode = $request->periode;

        // $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        // $belanja->anggota_id = $anggota->id;

        //     $nama_barang =  $request->jenis_input;
        //     $merek  =  $request->merk_input;
        //     $jumlah         = $request->jumlah_input;
        //     $periode         = $request->periode_input;
        //     $count= count($nama_barang);
        //     $count1=$count+1;

        //     for ($i = 1; $i < $count1; $i++) {
        //         $belanja = new BelanjaModel();
        //         $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        //         $belanja->nama_barang = $nama_barang[$i];
        //         $belanja->merek = $merek[$i];
        //         $belanja->jumlah = $jumlah[$i];
        //         $belanja->periode = $periode[$i];
        //         $belanja->anggota_id = $anggota->id;
        //         $belanja->save();
        //     }

        //     $informasi='Berhasil simpan data';

        //     $informasi = $belanja->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        //     return redirect()->route('belanja_investor.index')->with(['informasi' => $informasi]);
        // }

        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        $belanja_bulanan = [];
        foreach ($request->nama_barang as $key => $belanja) {
            $belanja_bulanan[] = ([
                'nama_barang' => $request->nama_barang[$key],
                'merek' => $request->merek[$key],
                'jumlah' => $request->jumlah[$key],
                'periode' => $request->periode[$key],
                'anggota_id' => $anggota->id,
            ]);
        }

        $save = BelanjaModel::insert($belanja_bulanan);

        $informasi = $belanja_bulanan = $save ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('belanja_investor.index')->with(['informasi' => $informasi]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $belanja = BelanjaModel::findOrFail($id);
        $informasi = $belanja->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('belanja_investor.index')->with(['informasi' => $informasi]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\UserModel;
use App\Model\RoleModel;
use App\Model\AnggotaModel;
use \App\Http\Requests\UserRequest;
use Hash;

class UserController extends Controller
{
    public function index(Request $request){
        $semuaUser= UserModel::Cari($request)->with('anggota')->orderBy('id','DESC')->paginate(10);
        return view('user.awal',compact('semuaUser'));
    }

    public function create(){
       
        $roles = RoleModel::all();
        return view('user.tambah', compact('roles'));
    }

    public function store(UserRequest $input){

        $user = new UserModel($input->only('role_id'));
        $user->username = $input->username;
        $user->password = Hash::make($input->password);
        
        $informasi=$user->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('user.index')->with(['informasi'=>$informasi]);

    }

    public function edit($id){
        $user= UserModel::findOrFail($id);
        $roles = RoleModel::all();
        
        return view('user.edit',compact('user','roles'))->with(array('user'=>$user));
    }

    public function update($id, UserRequest $input){
        $user = UserModel::findOrFail($id);
        $user->username= $input->username;
        $user->password = Hash::make($input->password);
        $user->role_id= $input->role_id;


        if ($user->save()) {
            if($user_anggota= UserModel::findOrFail($id)->whereHas('anggota') == true)
            {
            $anggota= AnggotaModel::where('users_id','=',$user->id)
                                ->update(['email'=> $user->username, 
                                            'status_keanggotaan' => $user->role_id]);
            }
            
            $informasi = 'Berhasil update Data';
        } else {
            $informasi = 'Gagal update Data';
        }
        return redirect()->route('user.index')->with(['informasi'=>$informasi]);
    }

    public function show($id){
        $user = UserModel::findOrFail($id);
        return view('user.lihat')->with(array('user'=>$user));
    }

    public function destroy($id){
        $user= UserModel::findOrFail($id);
        $informasi=$user->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('user.index')->with(['informasi'=>$informasi]);

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\KepengurusanModel;
use App\Model\AnggotaModel;
use App\Model\JabatanModel;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\KepengurusanRequest;


class KepengurusanController extends Controller
{
    public function index(Request $request){
        $semuaKepengurusan= KepengurusanModel::Cari($request)->orderBy('created_at', 'DESC')->paginate(5);
        return view('kepengurusan.awal',compact('semuaKepengurusan'));
    }

    public function download(Request $request)
    {
        return response()->download(public_path('/' . $request['images/kepengurusan']));
    }

    public function create(){
        $jabatanPluck = JabatanModel::pluck('posisi', 'id');
        $anggotaPluck = AnggotaModel::where('kota','=','1')->get();
        return view('kepengurusan.tambah', compact('jabatanPluck','anggotaPluck'));
    }

    public function store(KepengurusanRequest $input){
        // $validator = \Validator::make($input->all(), [
        //     'tanggal_menjabat' => 'date_format:"d-m-Y"|required',
        //     'sk_pengurus' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required',
        //     'status_kepengurusan' => 'required',
        //     'anggota_id' => 'required',
        //     'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required',
        //     'jabatan_id' => 'required'
        // ]);
        // if ($validator->fails()) {

        //     $informasi = 'Format file SK dan file Foto anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx ';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }
        
            $tgl_jbt = date('d-m-Y', strtotime($input->tanggal_menjabat));
            $menjabat = date('Y-m-d', strtotime($tgl_jbt));
            $kepengurusan = new KepengurusanModel($input->only('jabatan_id','anggota_id'));
            $kepengurusan->tanggal_menjabat = $menjabat;
            $kepengurusan->status_kepengurusan = $input->status_kepengurusan;

            // $file = $input->file('sk_pengurus');
            // $pic = time() . '.' . $file->getClientOriginalExtension();
            // $file->move(public_path('images/kepengurusan/'), $pic);
            // $kepengurusan->sk_pengurus = 'images/kepengurusan/' . $pic;

            // $file = $input->file('foto');
            // $pic = time() . '.' . $file->getClientOriginalExtension();
            // $file->move(public_path('images/kepengurusan/'), $pic);
            // $kepengurusan->foto = 'images/kepengurusan/' . $pic;

            $file = $input->file('foto');
            $pic = 'foto'.time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/kepengurusan/' . $pic);
            Image::make($file->getRealPath())->resize(836, 498)->save($path);
            $kepengurusan->foto = 'images/kepengurusan/' . $pic;

            $file = $input->file('sk_pengurus');
            $pic = 'sk_kepengurusan'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images/kepengurusan/'), $pic);
            $kepengurusan->sk_pengurus = 'images/kepengurusan/' . $pic;


            // $file = $input->file('sk_pengurus');
            // $pic = time() . '.' . $file->getClientOriginalExtension();
            // $path = public_path('images/kepengurusan/' . $pic);
            // Image::make($file->getRealPath())->resize(836, 498)->save($path);
            // $kepengurusan->sk_pengurus = 'images/kepengurusan/' . $pic;

            $informasi=$kepengurusan->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
            return redirect()->route('kepengurusan.index')->with(['informasi'=>$informasi]);

    }

    public function edit($id){
        $kepengurusan= KepengurusanModel::findOrFail($id);
        $jabatanPluck = JabatanModel::pluck('posisi', 'id');
        $anggotaPluck = AnggotaModel::where('kota','=','1')->get();
        return view('kepengurusan.edit',compact('kepengurusan','jabatanPluck', 'anggotaPluck'))->with(array('kepengurusan'=>$kepengurusan));
    }

    public function update($id, KepengurusanRequest $input){
    
        $kepengurusan = KepengurusanModel::findOrFail($id);
        
        // $validator = \Validator::make($input->all(), [
        //     'tanggal_menjabat' => 'date_format:"d-m-Y"|required',
        //     'status_kepengurusan' => 'required',
        //     'anggota_id' => 'required',
        //     'jabatan_id' => 'required'
        // ]);

        // if ($validator->fails()) {

        //     $informasi = 'Format file SK dan file Foto anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx ';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }            
                $kepengurusan->anggota_id = $input->anggota_id;
                $kepengurusan->jabatan_id = $input->jabatan_id;
                $kepengurusan->status_kepengurusan = $input->status_kepengurusan;
                
                if($input->sk_pengurus != null){
                // $validator = \Validator::make($input->all(), [
                //     'sk_pengurus' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required',
                //     ]);
                //     if ($validator->fails()) {
                //         $informasi = 'Format file SK anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx ';
                //         return redirect()->back()->with(['informasi' => $informasi]);

                //     }
                                
                    $file = $input->file('sk_pengurus');
                    $pic = 'sk_pengurus'.time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('images/kepengurusan/'), $pic);
                    $kepengurusan->sk_pengurus = 'images/kepengurusan/' . $pic;
                    }
                // elseif($input->file('sk_pengurus') == null ){
                //     $kepengurusan->sk_pengurus = $kepengurusan->sk_pengurus;
                // }

                
                if($input->foto != null){
                // $validator = \Validator::make($input->all(), [
                //     'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required',
                //     ]);
                //     if ($validator->fails()) {
                //         $informasi = 'Format file SK anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,xls,xlsx ';
                //         return redirect()->back()->with(['informasi' => $informasi]);

                //     }
                    $file = $input->file('foto');
                    $pic = 'foto'.time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('images/kepengurusan/'), $pic);
                    $kepengurusan->foto = 'images/kepengurusan/' . $pic;
                    }
                
                
                //if ($input->tanggal_menjabat != null) {
                //     $validator = \Validator::make($input->all(), [
                //         'tanggal_menjabat' => 'date_format:"d-m-Y"|required',    
                //     ]);
                //     if ($validator->fails()) {
                //         $informasi = 'Format tanggal salah';
                //         return redirect()->back()->with(['informasi' => $informasi]);
                    
                //     }
                //     $tgl_jbt = date('d-m-Y', strtotime($input->tanggal_menjabat));
                //     $menjabat = date('Y-m-d', strtotime($tgl_jbt));
                //     $kepengurusan->tanggal_menjabat = $menjabat;
                // }
                 
                    if ($kepengurusan->save()) {
                        $informasi = 'Berhasil update Data';
                    } else {
                        $informasi = 'Gagal update Data';
                    }
                return redirect()->route('kepengurusan.index')->with(['informasi'=>$informasi]);
                
}

    public function show($id){
        $kepengurusan = KepengurusanModel::findOrFail($id);
        return view('kepengurusan.lihat')->with(array('kepengurusan'=>$kepengurusan));
    }

    public function destroy($id){
        $kepengurusan= KepengurusanModel::findOrFail($id);
        $informasi=$kepengurusan->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('kepengurusan.index')->with(['informasi'=>$informasi]);

    }
}



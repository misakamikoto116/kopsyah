<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\InvestasiModel;

class AdminAnggotaInvestasi extends Controller
{
    public function awal($id)
    {
        $anggota= AnggotaModel::findOrFail($id);
        $investasiAnda= InvestasiModel::where('anggota_id','=',$anggota->id)->get();
        $total= InvestasiModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');

        return view('detail_investasi.awal',compact('anggota','investasiAnda','total'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\BelanjaModel;
use Auth;
use DB;

class BelanjaAnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        $semuaBelanja = BelanjaModel::where('anggota_id', '=', $anggota->id)->get();

        return view('belanja_anggota.awal', compact('semuaBelanja', 'anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        return view('belanja_anggota.tambah', compact('anggota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // //dd($request->all());

        // $sqlData = array();
        // $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        // for ($i=1; $i < count($request->jenis_input); $i++) {
        //     $sqlData[] = array(
        //         'nama_barang'         => $request->jenis_input[$i],
        //         'merek'         => $request->merk_input[$i],
        //         'jumlah'         => $request->jumlah_input[$i],
        //         'periode'         => $request->periode_input[$i],
        //         'anggota_id'    => $anggota->id,
        //         'created_at'    => \Carbon\Carbon::now(),
        //         'updated_at'    => \Carbon\Carbon::now()
        //     );
        // DB::table('belanja')->insert($sqlData);

        // }

        //     $nama_barang =  $request->jenis_input;
        //     $merek  =  $request->merk_input;
        //     $jumlah         = $request->jumlah_input;
        //     $periode         = $request->periode_input;
        //     $count= count($nama_barang);
        //     $count1=$count+1;

        //     for ($i = 1; $i < $count1; $i++) {
        //         $belanja = new BelanjaModel();
        //         $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        //         $belanja->nama_barang = $nama_barang[$i];
        //         $belanja->merek = $merek[$i];
        //         $belanja->jumlah = $jumlah[$i];
        //         $belanja->periode = $periode[$i];
        //         $belanja->anggota_id = $anggota->id;
        //         $belanja->save();
        //     }

        //     $informasi='Berhasil simpan data';

        //     return redirect()->route('belanja_anggota.index')->with(['informasi' => $informasi]);
        // }

        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        $belanja_bulanan = [];
        foreach ($request->nama_barang as $key => $belanja) {
            $belanja_bulanan[] = ([
                'nama_barang' => $request->nama_barang[$key],
                'merek' => $request->merek[$key],
                'jumlah' => $request->jumlah[$key],
                'periode' => $request->periode[$key],
                'anggota_id' => $anggota->id,
            ]);
        }
        $save = BelanjaModel::insert($belanja_bulanan);

        $informasi = $belanja_bulanan = $save ? 'Berhasil simpan data' : 'Gagal simpan data';

        return redirect()->route('belanja_anggota.index')->with(['informasi' => $informasi]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $belanja = BelanjaModel::findOrFail($id);
        $informasi = $belanja->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

        return redirect()->route('belanja_anggota.index')->with(['informasi' => $informasi]);
    }
}

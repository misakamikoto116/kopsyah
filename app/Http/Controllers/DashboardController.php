<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use App\Model\InvestasiModel;
use App\Model\PembayaranModel;
use App\Model\JenisInvestasiModel;
use App\Model\KepengurusanModel;
use Auth;

class DashboardController extends Controller
{
    protected $api_url;

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->api_url = env('API_POS');
    }

    public function dashboard_admin()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        $jml_anggota = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '!=', '1')
        ->count();
        $jml_investor = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '=', '3')
        ->count();

        $jml_pengurus = KepengurusanModel::all()->count();

        $jml_biasa = AnggotaModel::where('status_aktifasi', '=', '1')->where('kota', '=', '1')->count();
        $jml_luar_biasa = AnggotaModel::where('status_aktifasi', '=', '1')->where('kota', '=', '2')->count();

        $total = PembayaranModel::where('status', '=', '1')->sum('jumlah');

        $total_pokok = PembayaranModel::where('jenis_pembayaran_id', '=', '1')
                               ->where('status', '=', '1')
                               ->sum('jumlah');

        $total = PembayaranModel::where('status', '=', '1')->sum('jumlah');

        $total_pokok = PembayaranModel::where('jenis_pembayaran_id', '=', '1')
                                ->where('status', '=', '1')
                                ->sum('jumlah');
        $total_wajib = PembayaranModel::where('jenis_pembayaran_id', '=', '2')
                                ->where('status', '=', '1')
                                ->sum('jumlah');
        $total_sukarela = PembayaranModel::where('jenis_pembayaran_id', '=', '3')
                                ->where('status', '=', '1')
                                ->sum('jumlah');

        $jenis_investasi = JenisInvestasiModel::with('investasi')->get();
        $investasi = InvestasiModel::where('status', '=', '1')->sum('jumlah');
        
        $list_url_api = [
            'https://pos.kelontongku.com'                   => '212 Mart AWS',
            'https://212-gerilya.thortech.asia'             => '212 Mart Gerilya',
        ];

        return view('dashboard_admin', compact('total_pokok','total_wajib','total_sukarela',
                                                'jenis_investasi','anggota','jml_pengurus','jml_anggota',
                                                'jml_investor','investasi', 'total',
                                            'jml_biasa', 'jml_luar_biasa','list_url_api'));
    }

    public function dashboard_anggota()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        $jml_anggota = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '!=', '1')
        ->count();
        $jml_investor = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '=', '3')
        ->count();
        $jml_pengurus = KepengurusanModel::all()->count();

        $simpanan_all = PembayaranModel::where('status', '=', '1')->sum('jumlah');

        $total = PembayaranModel::where('anggota_id', '=', $anggota->id)
                                ->where('status', '=', '1')
                                ->sum('jumlah');
        $total_pokok = PembayaranModel::where('jenis_pembayaran_id', '=', '1')
                                        ->where('status', '=', '1')
                                       ->where('anggota_id', '=', $anggota->id)->sum('jumlah');
        $total_wajib = PembayaranModel::where('jenis_pembayaran_id', '=', '2')
                                        ->where('status', '=', '1')
                                       ->where('anggota_id', '=', $anggota->id)->sum('jumlah');
        $total_sukarela = PembayaranModel::where('jenis_pembayaran_id', '=', '3')
                                        ->where('status', '=', '1')
                                       ->where('anggota_id', '=', $anggota->id)->sum('jumlah');

        return view('dashboard_anggota', compact('total_pokok','total_wajib','total_sukarela',
                                                'anggota','jml_anggota','jml_pengurus',
                                                'jml_investor', 'investasi', 'total', 'simpanan_all'));
    }

    public function dashboard_investor()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        $jml_anggota = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '!=', '1')
        ->count();
        $jml_investor = UserModel::whereHas('anggota', function ($a) {
            $a->where('status_aktifasi', '=', '1');
        })
        ->where('role_id', '=', '3')
        ->count();
        $jml_pengurus = KepengurusanModel::all()->count();

        $simpanan_all = PembayaranModel::where('status', '=', '1')->sum('jumlah');
        $total = PembayaranModel::where('anggota_id', '=', $anggota->id)
                                ->where('status', '=', '1')
                                ->sum('jumlah');
        $total_pokok = PembayaranModel::where('jenis_pembayaran_id', '=', '1')
                                        ->where('status', '=', '1')
                                        ->where('anggota_id', '=', $anggota->id)->sum('jumlah');
        $total_wajib = PembayaranModel::where('jenis_pembayaran_id', '=', '2')
                                        ->where('status', '=', '1')
                                        ->where('anggota_id', '=', $anggota->id)->sum('jumlah');
        $total_sukarela = PembayaranModel::where('jenis_pembayaran_id', '=', '3')
                                        ->where('status', '=', '1')
                                        ->where('anggota_id', '=', $anggota->id)->sum('jumlah');

        $investasi_all = InvestasiModel::where('status', '=', '1')->sum('jumlah');
        $jenis_investasi = JenisInvestasiModel::with('investasi')->get();
        $investasi = InvestasiModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');

        return view('dashboard_investor', compact('total_pokok','total_wajib','total_sukarela',
                                                'jenis_investasi','anggota','jml_pengurus','jml_anggota',
                                                'jml_investor', 'investasi', 'total', 'investasi_all', 'simpanan_all'));
    }

    public function ajaxChartAnggota(Request $request)
    {
        if ($request->has('url')) {
            $this->api_url = $request->url;
        }

        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => $this->api_url,
                'headers' => ['app-token' => '323e7deb6725b189d7665c0159160cba'],
                'http_errors' => false,
                'timeout' => 0,
            ]);
            $request = $client->post('api/performa-anggota', [
                'form_params' => [
                    'limit_row' => 20,
                ],
            ]);
            $status = $request->getStatusCode();
            $body = $request->getBody();
            if ($status == 200) {
                return json_decode($body, true);
            } else {
                return json_decode($body);
            }
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }
    }

    public function ajaxLineOmzet(Request $request)
    {
        if ($request->has('url')) {
            $this->api_url = $request->url;
        }

        $tgl = new \Carbon\Carbon();
        $kemarin = $tgl->yesterday()->format('Y-m-d');
        $kemarin_week = $tgl->yesterday()->subDays(6)->format('Y-m-d');

        try {
            $client = new \GuzzleHttp\Client(['base_uri' => $this->api_url]);
            $request = $client->post('api/omzet-weekly', [
                'form_params' => [
                    'tgl_start' => $kemarin_week,
                    'tgl_end' => $kemarin,
                ],
            ]);
            $status = $request->getStatusCode();
            $body = $request->getBody();
            if ($status == 200) {
                return json_decode($body, true);
            } else {
                return json_decode($body);
            }
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }
    }

    public function changeToko(Request $request)
    {
        $data = [
            'anggota'       => $this->ajaxChartAnggota($request),
            'line'          => $this->ajaxLineOmzet($request),
        ];

        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Model\InvestasiModel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DataOmsetController extends Controller
{
    protected $api_url;

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->api_url = env('API_POS');
    }	

    public function dataOmset(Request $request)
    {
    	$data = $request->all();

    	$tanggal_awal_formatted 	= \Carbon\Carbon::parse($data['tanggal_awal'])->format('d F Y');
    	$tanggal_akhir_formatted 	= \Carbon\Carbon::parse($data['tanggal_akhir'])->format('d F Y');

    	try {

    	    $client = new Client([
    	        'base_uri' 		=> 'https://pos.kelontongku.com/',
    	        'headers' 		=> ['app-token' => '323e7deb6725b189d7665c0159160cba'],
    	        'http_errors' 	=> false,
    	        'timeout' 		=> 0,
    	    ]);

    	    $response = $client->request('POST', 'api/omzet-weekly', [
    	        'form_params' => [
    	            'tgl_start' 	=> $data['tanggal_awal'],
    	            'tgl_end' 		=> $data['tanggal_akhir']
    	        ]
    	    ]);

    	    $body 	= $response->getBody();
    	    $arr 	= json_decode($body, true);

    	    $view 			= [];
    	    $total_omset 	= 0;

    	    foreach ($arr as $key => $array) {
    	    	
    	    	$tanggal_new = str_replace('/', '-', $array['tanggal']);

    	    	$total_omset += $array['omzet'];
    	    	
    	    	$view[] = [
    	    		'tanggal'			=> \Carbon\Carbon::parse($tanggal_new)->format('d F Y'),
    	    		'omset_sekarang'	=> number_format($array['omzet']),
    	    		'total_omset'		=> number_format($total_omset),
    	    	];

    	    }

    	    $views = [
    	    	'items'			=> $view,
    	    	'tanggal_awal'	=> $tanggal_awal_formatted,
    	    	'tanggal_akhir'	=> $tanggal_akhir_formatted,
                'total_omset'   => $total_omset,
    	    ];

    	    return view('data_omset.laporan_data_omset')->with($views);

    	} catch (\GuzzleHttp\Exception\ConnectException $e) {
    	    return false;
    	}


    }

    public function dataAnggotaTahunan(Request $request)
    {
        $data   = $request->all();      

        try {

            $client = new Client([
                'base_uri'      => 'https://pos.kelontongku.com/',
                // 'base_uri'      => 'http://localhost:8080/',
                'headers'       => ['app-token' => '323e7deb6725b189d7665c0159160cba'],
                'http_errors'   => false,
                'timeout'       => 0,
            ]);

            $response = $client->request('POST', 'api/performa-anggota-yearly', [
                'form_params' => [
                    'bulan_awal'     => $data['bulan_awal'],
                    'bulan_akhir'    => $data['bulan_akhir'],
                ]
            ]);

            $body   = $response->getBody();
            $arr    = json_decode($body, true);

            $view = [
                'items'         => $arr,
                'bulan_awal'    => Carbon::parse($data['bulan_awal'])->format('F Y'),
                'bulan_akhir'   => Carbon::parse($data['bulan_akhir'])->format('F Y'),
            ];

            return view('data_anggota_tahunan.laporan_data_anggota_tahunan')->with($view);

        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }


    }

    public function dataKeuntunganTahunan(Request $request)
    {
        $data = $request->all();
        
        try {
            
            $total_invest = InvestasiModel::where('status', 1)
                                          ->whereHas('jenis_investasi', function ($query)
                                          {
                                            $query->where('nama','212 Mart AWS Air Hitam');
                                          })
                                          ->sum('jumlah');

            $client = new Client([
                'base_uri'      => 'https://pos.kelontongku.com/',
                // 'base_uri'      => 'http://localhost:8080/',
                'headers'       => ['app-token' => '323e7deb6725b189d7665c0159160cba'],
                'http_errors'   => false,
                'timeout'       => 0,
            ]);

            $response = $client->request('POST', 'api/performa-anggota-yearly', [
                'form_params' => [
                    'bulan_awal'     => $data['bulan_awal'],
                    'bulan_akhir'    => $data['bulan_akhir'],
                ]
            ]);

            $body   = $response->getBody();
            $arr    = json_decode($body, true);
            $dari_omset = $arr['total_omset'] * (10/100);

            $arr_omset = [];

            foreach ($arr['omset'] as $key => $omset) {

                $dari_omset = $omset['total_omset'] * (10 / 100);
                
                $arr_omset[] = [
                    'bulan'             => $omset['bulan'],
                    'total_omset'       => $omset['total_omset'],
                    'dari_omset'        => $dari_omset,
                    'dana_cadangan'     => $dari_omset * (20/100),
                    'koperasi'          => $dari_omset * (10/100),
                    'dana_sosial'       => $dari_omset * (5/100),
                ];

            }

            $view = [
                'items'         => $arr,
                'total_invest'  => $total_invest,
                'arr_omset'     => $arr_omset,
                'bulan_awal'    => Carbon::parse($data['bulan_awal'])->format('F Y'),
                'bulan_akhir'   => Carbon::parse($data['bulan_akhir'])->format('F Y'),
            ];

            return view('data_keuntungan_tahunan.laporan_data_keuntungan_tahunan')->with($view);

        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TentangModel;
use App\Http\Requests\KententuanRequest;

class KetentuanController extends Controller
{
    public function index(Request $request){
        // $semuaTentang= TentangModel::where('jenis','=','3')->get();;
        $semuaTentang= TentangModel::where('jenis','=','3')->Cari($request)->paginate(5);
        return view('ketentuan.awal',compact('semuaTentang'));
    }

    public function create(){
        return view('ketentuan.tambah');
    }

    public function store(KententuanRequest $input){
            $tentang= new TentangModel();
            $tentang->judul=$input->judul;
            $tentang->isi=$input->isi;
            $tentang->jenis = '3';
            $informasi=$tentang->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
            return redirect()->route('ketentuan.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $tentang= TentangModel::findOrFail($id);
        return view('ketentuan.edit')->with(array('tentang'=>$tentang));
    }

    public function update($id, KententuanRequest $input){
        $tentang= TentangModel::findOrFail($id);
        $tentang->judul=$input->judul;
        $tentang->isi=$input->isi;
        $informasi=$tentang->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('ketentuan.index')->with(['informasi'=>$informasi]);
    }

    public function destroy($id){
        $tentang= TentangModel::findOrFail($id);
        $informasi=$tentang->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('ketentuan.index')->with(['informasi'=>$informasi]);

    }
}

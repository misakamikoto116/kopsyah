<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrasiRequest;
use App\Model\AnggotaModel;
use App\Model\InvestasiModel;
use App\Model\JenisInvestasiModel;
use App\Model\JenisPembayaranModel;
use App\Model\PembayaranModel;
use App\Model\UserModel;
use App\Http\Requests\LoginRequest;
use App\Notifications\NewRegistrasi;
use DB;
use Hash;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;

class SessionController extends Controller
{
    public function form()
    {
        if (Auth::check()) {
            return redirect('/');
        }

        return view('login');
    }

    public function validasi(LoginRequest $input)
    {
        //validasi login
        $user = UserModel::where('username', $input->username)->first();
        if ($user) {
            if (Hash::check($input->password, $user->password)) {
                if (!is_null($user)) {
                    Auth::login($user);
                }

                if (Auth::check()) {
                    //jika role admin
                    if (Auth::user()->role_id == '1') {
                        $this->informasi = 'Selamat Datang '.Auth::user()->username;

                        return redirect('admin')->with(['informasi' => $this->informasi]);

                        //jika role anggota atau anggota investor
                    } else {
                        $anggota = AnggotaModel::where('users_id', '=', Auth::user()->id)->first();
                        $aktifasi = $anggota->status_aktifasi;

                        //jika status aktifasi anggota non aktif
                        if ($aktifasi == '0') {
                            Auth::logout();

                            return redirect('login')->withErrors(['Akun anda belum diaktifasi']);

                            //jika status aktifasi anggota aktif
                        } else {
                            //jika user anggota
                            if (Auth::user()->role_id == '2') {
                                $this->informasi = 'Selamat Datang '.Auth::user()->username;

                                return redirect()->route('anggota')->with(['informasi' => $this->informasi]);

                                //jika user anggota sekaligus investor
                            } elseif (Auth::user()->role_id == '3') {
                                $this->informasi = 'Selamat Datang '.Auth::user()->username;

                                return redirect()->route('investor')->with(['informasi' => $this->informasi]);
                            }
                        }
                    }
                }
            }
        }

        return redirect('login')->withErrors(['Username atau Password Anda Salah!']);
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();

            return redirect('/')->withErrors(['Anda Logout! Silahkan Login Untuk Masuk']);
        } else {
            return redirect('/')->withErrors(['Silahkan Login Terlebih Dahulu']);
        }
    }

    public function registration()
    {
        $jenis_investasi = JenisInvestasiModel::all();
        $jenis_pembayaran = JenisPembayaranModel::all();

        return view('registration', compact('jenis_investasi', 'jenis_pembayaran'));
    }

    public function registrationStore(RegistrasiRequest $input)
    {
        try {
            
            DB::beginTransaction();

            // format
            $tgl_lhr            = date('d-m-Y', strtotime($input->tanggal_lahir));
            $ini                = date('Y-m-d', strtotime($tgl_lhr));
            $tgl_bayar          = date('d-m-Y', strtotime($input->tanggal_pembayaran));
            $bayarpembayaran    = date('Y-m-d', strtotime($tgl_bayar));

            // model
            $user           = new UserModel();

            $user->role_id  = $input->role_id;
            $user->username = $input->email;
            $user->password = Hash::make($input->password);

            if ($user->save()) {

                //notifikasi di navbar admin bahwa ada pendaftaran baru
                $user->notify(new NewRegistrasi());

                // foto anggota
                //input pict file ktp
                $file               = $input->file('file_ktp');
                $pic_ktp            = 'ktp'.time().'.'.$file->getClientOriginalExtension();
                $path               = public_path('images/anggota/'.$pic_ktp);
                Image::make($file->getRealPath())->resize(836, 498)->save($path);

                //input pict file foto
                $file_profil        = $input->file('file_foto');
                $pic_profil         = 'foto'.time().'.'.$file_profil->getClientOriginalExtension();
                $path               = public_path('images/anggota/'.$pic_profil);
                Image::make($file_profil->getRealPath())->resize(836, 498)->save($path);

                // get anggota sebelum nya
                $last_anggota = AnggotaModel::orderBy('id','DESC')->first();

                // Dapatkan angka dari tanggal untuk nomor anggota di komen berikutnya
                $tgl_now        = date('dm');
                $tahun_now      = date('Y');
                $nomor          = substr($tahun_now, 2);
                $new_id         = $last_anggota->id + 1;
                $nomor_anggota  = '212'.'.'.$tgl_now.$nomor.'.'.sprintf('%06d', $new_id);
                

                // anggota
                $anggota = AnggotaModel::create([
                    'no_ktp'                => $input->no_ktp,
                    'nama'                  => $input->nama,
                    'agama'                 => 'Islam',
                    'tempat_lahir'          => $input->tempat_lahir,
                    'tanggal_lahir'         => $ini,
                    'email'                 => $input->email,
                    'no_telepon'            => $input->no_telepon,
                    'jenis_kelamin'         => $input->jenis_kelamin,
                    'kota'                  => $input->kota,
                    'alamat'                => $input->alamat,
                    'penghasilan'           => $input->pendapatan,
                    'pekerjaan'             => $input->pekerjaan,
                    'status_keanggotaan'    => $input->role_id,
                    'status_aktifasi'       => '0',
                    'users_id'              => $user->id,
                    'file_ktp'              => 'images/anggota/'.$pic_ktp,
                    'file_foto'             => 'images/anggota/'.$pic_profil,
                    'nomor_anggota'         => $nomor_anggota,
                ]);

                // dd($input->all());

                foreach ($input->investasi as $jenis_investasi_id => $value) {
                    //jika ada investasi yang diiputkan
                    if ($value != 0) {
                        //format tanggal dari datepicker ke database format
                        $tgl_inves      = date('d-m-Y', strtotime($input->tanggal_investasi[$jenis_investasi_id]));
                        $bayarinvestasi = date('Y-m-d', strtotime($tgl_inves));

                        //pengaturan path foto
                        $file   = $input->file('foto_resi')[$jenis_investasi_id];
                        $pic    = 'fresi'.time().'.'.$file->getClientOriginalExtension();
                        $path   = public_path('images/investasi/'.$pic);
                        Image::make($file->getRealPath())->resize(836, 498)->save($path);

                        $inves_bro = $anggota->investasi()->save(new InvestasiModel([
                            'jumlah'                => $value,
                            'biaya_cetak_investasi' => $input->biaya_cetak_investasi[$jenis_investasi_id],
                            'tanggal_pembayaran'    => $bayarinvestasi,
                            'status'                => '0',
                            'jenis_investasi_id'    => $jenis_investasi_id,
                            'foto_resi'             => 'images/investasi/'.$pic,
                        ]));
                    }
                }

            }

            DB::commit();

            $informasi  = 'Selamat Anda Berhasil Registrasi. Silahkan Tunggu SMS Notifikasi!';
            $message    = 'Terima kasih telah melakukan registrasi. Data anda akan kami proses selambat-lambatnya 1x24 Jam. Bila dalam 1 x 24 Jam anda tidak menerima informasi aktivasi akun silahkan menghubungi call center kami. 212 Mart Samarinda';

            //sms notif hanya jika production
            if (env('APP_ENV') != 'production') {
                $sms = new \App\Helpers\Zenziva();
                $sms->send($anggota->no_telepon, $message);
            }

            return redirect()->route('login')->with(['informasi' => $informasi]);

        } catch (Exception $e) {
            
            DB::rollback();

            return redirect()->back()->withInput()->withErrors($e->getMessage());

        }

        //store user
        // $user = new UserModel($input->only('role_id'));
        // $user->role_id = $input->role_id;
        // $user->username = $input->email;
        // $user->password = Hash::make($input->password);
        // $anggota = new AnggotaModel();
        // $tgl_lhr = date('d-m-Y', strtotime($input->tanggal_lahir));
        // $ini = date('Y-m-d', strtotime($tgl_lhr));
        // $tgl_bayar = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        // $bayarpembayaran = date('Y-m-d', strtotime($tgl_bayar));

        // if ($user->save()) {
        //     //notifikasi di navbar admin bahwa ada pendaftaran baru
        //     $user->notify(new NewRegistrasi());
        //     //store anggota
        //     $anggota->no_ktp = $input->no_ktp;
        //     $anggota->nama = $input->nama;
        //     $anggota->agama = 'Islam';
        //     $anggota->tempat_lahir = $input->tempat_lahir;
        //     $anggota->tanggal_lahir = $ini;
        //     $anggota->email = $input->email;
        //     $anggota->no_telepon = $input->no_telepon;
        //     $anggota->jenis_kelamin = $input->jenis_kelamin;
        //     $anggota->kota = $input->kota;
        //     $anggota->alamat = $input->alamat;
        //     $anggota->penghasilan = $input->pendapatan;
        //     $anggota->pekerjaan = $input->pekerjaan;
        //     $anggota->status_keanggotaan = $input->role_id;
        //     $anggota->status_aktifasi = '0';
        //     $anggota->users_id = $user->id;

        //     //input pict file ktp
        //     $file = $input->file('file_ktp');
        //     $pic = 'ktp'.time().'.'.$file->getClientOriginalExtension();
        //     $path = public_path('images/anggota/'.$pic);
        //     Image::make($file->getRealPath())->resize(836, 498)->save($path);
        //     $anggota->file_ktp = 'images/anggota/'.$pic;

        //     //input pict file foto
        //     $file_profil = $input->file('file_foto');
        //     $pic = 'foto'.time().'.'.$file_profil->getClientOriginalExtension();
        //     $path = public_path('images/anggota/'.$pic);
        //     Image::make($file_profil->getRealPath())->resize(836, 498)->save($path);
        //     $anggota->file_foto = 'images/anggota/'.$pic;
        // }

        // //dapatkan angka dari tanggal untuk nomor anggota di komen berikutnya
        // $tgl_now = date('dm');
        // $tahun_now = date('Y');
        // $nomor = substr($tahun_now, 2);
        // if ($anggota->save()) {
        //     //update nomor anggota sesuai urutan id dan sesuai format nomor anggota
        //     DB::table('anggota')->where('id', $anggota->id)->update([
        //         'nomor_anggota' => '212'.'.'.$tgl_now.$nomor.'.'.sprintf('%06d', $anggota->id), ]);
        //     //input pembayaran foreach jenis pembayaran yang tersedia
        //     foreach ($input->pembayaran as $jenis_pembayaran_id => $value) {
        //         $tmp = $jenis_pembayaran_id;
        //         $jenis_pembayaran = JenisPembayaranModel::where('id', '=', $tmp)->first();
        //         $lama_tempo = $jenis_pembayaran->lama_tempo;
        //         $bayar = $input->tanggal_pembayaran;
        //         $tgl = date('Y-m-d', strtotime("$bayar+$lama_tempo months"));
        //         if ($value != 0) {
        //             $file = $input->file('file_resi');
        //             $pic = 'resi'.time().'.'.$file->getClientOriginalExtension();
        //             $path = public_path('images/pembayaran/'.$pic);
        //             Image::make($file->getRealPath())->resize(836, 498)->save($path);
        //             $anggota->pembayaran()->save(new PembayaranModel([
        //                 'jumlah' => $value,
        //                 'tanggal_tempo' => $tgl,
        //                 'tanggal_pembayaran' => $bayarpembayaran,
        //                 'status' => '0',
        //                 'jenis_pembayaran_id' => $jenis_pembayaran_id,
        //                 'file_resi' => 'images/pembayaran/'.$pic,
        //             ]));
        //         }
        //     }

        //     //jika status keanggotaan anggota sekaligus investor maka jalankan ini
        //     if ($anggota->status_keanggotaan == 3) {
        //         foreach ($input->investasi as $jenis_investasi_id => $value) {
        //             //jika ada investasi yang diiputkan
        //             if ($value != 0) {
        //                 //format tanggal dari datepicker ke database format
        //                 $tgl_inves = date('d-m-Y', strtotime($input->tanggal_investasi[$jenis_investasi_id]));
        //                 $bayarinvestasi = date('Y-m-d', strtotime($tgl_inves));

        //                 //pengaturan path foto
        //                 $file = $input->file('foto_resi')[$jenis_investasi_id];
        //                 $pic = 'fresi'.time().'.'.$file->getClientOriginalExtension();
        //                 $path = public_path('images/investasi/'.$pic);
        //                 Image::make($file->getRealPath())->resize(836, 498)->save($path);
        //                 $anggota->investasi()->save(new InvestasiModel([
        //                     'jumlah' => $value,
        //                     'tanggal_pembayaran' => $bayarinvestasi,
        //                     'status' => '0',
        //                     'jenis_investasi_id' => $jenis_investasi_id,
        //                     'foto_resi' => 'images/investasi/'.$pic,
        //                 ]));
        //             }
        //         }
        //     }
        // }

        // $informasi = 'Selamat Anda Berhasil Registrasi. Silahkan Tunggu SMS Notifikasi!';
        // $message = 'Terima kasih telah melakukan registrasi. Data anda akan kami proses selambat-lambatnya 1x24 Jam. Bila dalam 1 x 24 Jam anda tidak menerima informasi aktivasi akun silahkan menghubungi call center kami. 212 Mart Samarinda';

        // //sms notif hanya jika production
        // if (env('APP_ENV') != 'production') {
        //     $sms = new \App\Helpers\Zenziva();
        //     $sms->send($anggota->no_telepon, $message);
        // }

        // return redirect()->route('login')->with(['informasi' => $informasi]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\PembayaranModel;
use App\Model\JenisPembayaranModel;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use \App\Notifications\NewPembayaran;
use App\Http\Requests\InvestorPembayaranRequest;
use App\Model\NotificationsModel;
use Intervention\Image\ImageManagerStatic as Image;
use \Carbon\Carbon;

class InvestorPembayaranController extends Controller
{
    public function index()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();

        //notifikasi verifikasi pembayaran dihapus dari navbar
        $del_verif_bayar= NotificationsModel::where('type','=','App\Notifications\VerifikasiPembayaran')
                    ->Where('notifiable_id','=',Auth::id())->delete();

        //list semua pembayaran dia
        $semuaPembayaran = PembayaranModel::where('anggota_id', '=', $anggota->id)->get()->sortByDesc('id');

        $tmp= new PembayaranModel();
        if($tmp->status == 0){
            $tmp->status = "Belum diverifikasi";
        }else{
            $tmp->status= "Sudah diverifikasi";
        }
        
        $total= PembayaranModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');
       
        return view('only_investor.simpanan.awal', compact('anggota','semuaPembayaran','tmp','total'));
    }
    public function create()
    {
        $anggota = AnggotaModel::where('users_id', '=', Auth::id())->first();
        $jenisPembayaranPluck = JenisPembayaranModel::where('id', '!=', '1')->pluck('nama', 'id');
        $pembayaran= PembayaranModel::where('jenis_pembayaran_id', '=', '2')->orderBy('id', 'DESC')->first();
        $jenisPokok= JenisPembayaranModel::where('id','=','1')->first();
        $jenisWajib= JenisPembayaranModel::where('id','=','2')->first();
        return view('only_investor.simpanan.tambah', compact('anggota','jenisPembayaranPluck',
        'jenisPokok','jenisWajib','pembayaran'));
    }

    public function store(InvestorPembayaranRequest $input)
    {
        $input->validated();
        $pembayaran = new PembayaranModel();

        //format tanggal pembayaran dari datepicker ke database format
        $tgl_bayar = date('d-m-Y', strtotime($input->tanggal_pembayaran));
        $bayarpembayaran = date('Y-m-d', strtotime($tgl_bayar));

        $pembayaran->jenis_pembayaran_id= $input->jbayar;
        $jenis_pembayaran=  JenisPembayaranModel::where('id', '=', $input->jbayar)->first();

        //tempo
        $lama_tempo=$jenis_pembayaran->lama_tempo;
        $tgl=date('Y-m-d', strtotime("+$lama_tempo months"));

        //pengaturan path file
        $file = $input->file('file_resi');
        $pic = time().'.'.$file->getClientOriginalExtension();
        $path = public_path('images/pembayaran/'.$pic);
        Image::make($file->getRealPath())->resize(468,249)->save($path);
       
        
        $anggota = AnggotaModel::where('users_id','=', Auth::id())->first();
        $pembayaran->anggota_id= $anggota->id;
        if($input->jbayar== 1){
                 $pembayaran->jumlah = $input->jumlahP;
             }else if ($input->jbayar== 2){
                 $pembayaran->jumlah = $input->jumlahW;
             }else {
                    $validator = \Validator::make($input->all(), [
                        'jumlah' => 'numeric|required',

                    ]);
                    if ($validator->fails()) {

                        $informasi = 'Jumlah Harus Angka';
                        return redirect()->back()->with(['informasi' => $informasi]);

                    }
                 $pembayaran->jumlah = $input->jumlah;
        }
        $pembayaran->tanggal_tempo = $tgl;
        $pembayaran->tanggal_pembayaran = $bayarpembayaran;
        $pembayaran->file_resi = 'images/pembayaran/'.$pic;
        $pembayaran->status = '0';
        
        $informasi=$pembayaran->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        $anggota= AnggotaModel::where('id','=',$pembayaran->anggota_id)->first();       
        $user= UserModel::where('id','=',$anggota->users_id)->first();

        //notifikasi ke navbar admin bahwa ada pembayaran baru
        $user->notify(new NewPembayaran());
        return redirect()->route('simpanan.index')->with(['informasi'=>$informasi]);
    }

}

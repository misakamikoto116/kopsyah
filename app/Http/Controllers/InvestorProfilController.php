<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Model\AnggotaModel;
use App\Model\UserModel;
use Intervention\Image\ImageManagerStatic as Image;

use Auth;

class InvestorProfilController extends Controller
{
    public function index()
    {
        $anggota = AnggotaModel::where('users_id','=',Auth::id())->first();

        return view('only_investor.profil.awal', compact('anggota'));
    }

    public function edit($id){
        
        $anggota= AnggotaModel::findOrFail($id);
        if ($anggota->users_id == Auth::user()->id) {
        return view('only_investor.profil.edit',compact('anggota'))->with(array('anggota'=>$anggota));
        }else {
            return view('errors.request_error');
        }
    }

    public function update($id, Request $input){
        if ($input->no_ktp != null) {
            $validator = \Validator::make($input->all(), [
                'no_ktp' => 'min:16',
            ]);
            if ($validator->fails()) {

                $informasi = 'Nomor KTP harus 16 digit';
                return redirect()->back()->with(['informasi' => $informasi]);

            }

        }


        // validasi jika kondisi file ktp, foto, tanggal lahir tidak null
        if ($input->file_ktp != null && $input->file_foto != null && $input->tanggal_lahir != null) {
            $validator = \Validator::make($input->all(), [
                'file_ktp' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                'file_foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                'tanggal_lahir' => 'date_format:"d-m-Y"|required',
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }
            
            //validasi jika kondisi file ktp dan tanggal lahir tidak null
        } else if ($input->file_ktp != null && $input->tanggal_lahir != null) {
            $validator = \Validator::make($input->all(), [
                'file_ktp' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                'tanggal_lahir' => 'date_format:"d-m-Y"|required'
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }

            //validasi jika kondisi file foto dan tanggal lahir tidak null
        } else if ($input->file_foto != null && $input->tanggal_lahir != null) {
            $validator = \Validator::make($input->all(), [
                'file_foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                'tanggal_lahir' => 'date_format:"d-m-Y"|required'
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }

            //validasi jika kondisi file foto dan file ktp yang tidak null
        } else if ($input->file_ktp != null && $input->file_foto != null) {
            $validator = \Validator::make($input->all(), [
                'file_ktp' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
                'file_foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }
            
            //validasi jika kondisi tanggal lahir saja yang tidak null
        } else if ($input->file_foto != null) {
            $validator = \Validator::make($input->all(), [
                'file_foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }
            
            //validasi jika kondisi file foto saja yang tidak null
        } else if ($input->file_ktp != null) {
            $validator = \Validator::make($input->all(), [
                'file_ktp' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }
            
            //validasi jika kondisi tanggal lahir saja yang tidak null
        } else if ($input->tanggal_lahir != null) {
            $validator = \Validator::make($input->all(), [
                'tanggal_lahir' => 'date_format:"d-m-Y"|required'
            ]);
            if ($validator->fails()) {

                $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
                return redirect()->back()->with(['informasi' => $informasi]);

            }
        }

        $anggota = AnggotaModel::findOrFail($id);
            $anggota->no_ktp = $input->no_ktp;
            $anggota->nama = $input->nama;
            $anggota->tempat_lahir = $input->tempat_lahir;
            $anggota->no_telepon = $input->no_telepon;
            $anggota->jenis_kelamin = $input->jenis_kelamin;
            $anggota->kota = $input->kota;
            $anggota->alamat = $input->alamat;
            $anggota->penghasilan = $input->penghasilan;
            $anggota->pekerjaan = $input->pekerjaan;

        if ($input->file_ktp != null) {

            //pengaturan path file
            $file = $input->file('file_ktp');
            $pic = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/anggota/' . $pic);
            Image::make($file->getRealPath())->resize(836, 498)->save($path);
            $anggota->file_ktp = 'images/anggota/' . $pic;
        }

        if ($input->file_foto != null) {

            //pengaturan path file
            $file_profil = $input->file('file_foto');
            $pic = time() . '.' . $file_profil->getClientOriginalExtension();
            $path = public_path('images/anggota/' . $pic);
            Image::make($file_profil->getRealPath())->resize(836, 498)->save($path);
            $anggota->file_foto = 'images/anggota/' . $pic;
        }
        if ($input->tanggal_lahir != null) {

            //pengaturan format tanggal dari datepicker ke format date database
            $tgl_lhr = date('d-m-Y', strtotime($input->tanggal_lahir));
            $ini = date('Y-m-d', strtotime($tgl_lhr));
            $anggota->tanggal_lahir = $ini;
        }

        if ($anggota->save()) {
            $informasi = 'Berhasil update Data';
        } else {
            $informasi = 'Gagal update Data';
        }

        return redirect()->route('profil.index')->with(['informasi' => $informasi]);

    
    }
    use PerformaAnggota;
        
    

  

}
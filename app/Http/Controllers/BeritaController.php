<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TentangModel;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\BeritaRequest;
use DB;
use File;

class BeritaController extends Controller
{
    public function index(Request $request){
        // $semuaTentang= TentangModel::where('jenis', '=', '5')->get();
        $semuaTentang= TentangModel::where('jenis','=','5')->Cari($request)->paginate(5);
        return view('berita.awal',compact('semuaTentang'));
    }

    public function create(){
        return view('berita.tambah');
    }

    public function store(BeritaRequest $input){

        // $validator = \Validator::make($input->all(), [
        //     'judul' => 'required',
        //     'isi' => 'required',
        //     'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
        // ]);
        // if ($validator->fails()) {

        //     $informasi = 'Format gambar anda tidak didukung. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,PNG ';
        //     return redirect()->back()->with(['informasi' => $informasi]);

        // }

        try {
            
            DB::beginTransaction();

            $tentang= new TentangModel();
            $tentang->judul=$input->judul;
            $tentang->isi=$input->isi;
            $tentang->jenis='5';
            
            $file = $input->file('foto');
            $pic = time().'.'.$file->getClientOriginalExtension();
            $path = storage_path('app/public/images/tentang/'.$pic);
            Image::make($file->getRealPath())->resize(836,498)->save($path);
            $tentang->foto = $pic;
            $informasi=$tentang->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

            DB::commit();

            return redirect()->route('berita.index')->with(['informasi'=>$informasi]);

        } catch (Exception $e) {
            
            DB::rollback();

            return redirect()->back->withInput()->withError($e->getMessage());

        }
    }

    public function edit($id){
        $tentang= TentangModel::findOrFail($id);
        return view('berita.edit')->with(array('tentang'=>$tentang));
    }

    public function update($id, BeritaRequest $input)
    {

        try {
            
            DB::beginTransaction();

            $tentang= TentangModel::findOrFail($id);
            if ($input->foto != null ){
                $tentang->judul = $input->judul;
                $tentang->isi   = $input->isi;
                $file           = $input->file('foto');
                $pic            = time().'.'.$file->getClientOriginalExtension();
                $path           = storage_path('app/public/images/tentang/'.$pic);
                Image::make($file->getRealPath())->resize(836,498)->save($path);
                $tentang->foto  = $pic;
                if ($tentang->save()) {
                    $informasi = 'Berhasil update Data';
                } else {
                    $informasi = 'Gagal update Data';
                }
            } else {
                $tentang->judul = $input->judul;
                $tentang->isi   = $input->isi;
                if ($tentang->save()) {
                    $informasi = 'Berhasil update Data';
                } else {
                    $informasi = 'Gagal update Data';
                }
            }

            DB::commit();

            return redirect()->route('berita.index')->with(['informasi'=>$informasi]);

        } catch (Exception $e) {
            
            DB::rollback();

            return redirect()->back->withInput()->withError($e->getMessage());

        } 
}


    public function destroy($id)
    {
        try {
     
            DB::beginTransaction();

            $tentang        = TentangModel::findOrFail($id);
            $this->delImage($tentang->foto, 'images/tentang');
            $informasi      = $tentang->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';

            DB::commit();

            return redirect()->route('berita.index')->with(['informasi'=>$informasi]);

        } catch (Exception $e) {
            
            DB::rollback();

            return redirect()->back->withInput()->withError($e->getMessage());

        }
    }

    public function delImage($filename, $lokasi)
    {
        $path = storage_path().'/app/public/'.$lokasi.'/';
        return File::delete($path.$filename);
    }
}

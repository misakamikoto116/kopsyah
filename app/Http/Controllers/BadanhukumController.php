<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TentangModel;
use App\Http\Requests\BadanhukumRequest;

class BadanhukumController extends Controller
{
    public function index(Request $request){
        // $semuaTentang= TentangModel::where('jenis','=','2')->get();;
        $semuaTentang= TentangModel::where('jenis','=','2')->Cari($request)->paginate(5);
        return view('badanhukum.awal',compact('semuaTentang'));
    }

    public function create(){
        return view('badanhukum.tambah');
    }

    public function store(BadanhukumRequest $input){
            $tentang= new TentangModel();
            $tentang->judul=$input->judul;
            $tentang->isi=$input->isi;
            $tentang->jenis = '2';
            $informasi=$tentang->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
            return redirect()->route('badanhukum.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $tentang= TentangModel::findOrFail($id);
        return view('badanhukum.edit')->with(array('tentang'=>$tentang));
    }

    public function update($id, BadanhukumRequest $input){
        $tentang= TentangModel::findOrFail($id);
        $tentang->judul=$input->judul;
        $tentang->isi=$input->isi;
        $informasi=$tentang->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('badanhukum.index')->with(['informasi'=>$informasi]);
    }

    public function destroy($id){
        $tentang= TentangModel::findOrFail($id);
        $informasi=$tentang->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('badanhukum.index')->with(['informasi'=>$informasi]);

    }
}

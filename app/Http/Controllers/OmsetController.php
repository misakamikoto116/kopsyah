<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\OmsetModel;

class OmsetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semuaOmset= OmsetModel::all();
        return view('omset.awal', compact('semuaOmset'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('omset.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $input)
    {
        $validator = \Validator::make($input->all(), [
            'tanggal' => 'date_format:"Y-m-d"|required'
        ]);

        if ($validator->fails()) {

            $informasi = 'Format tanggal salah';
            return redirect()->back()->with(['informasi' => $informasi]);

        }
        $omset = new OmsetModel($input->only('tanggal', 'omset'));
        $informasi = $omset->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('omset.index')->with(['informasi' => $informasi]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $omset = OmsetModel::findOrFail($id);
        return view('omset.lihat')->with(array('omset' => $omset));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $omset = OmsetModel::findOrFail($id);
        return view('omset.edit', compact('omset'))->with(array('omset' => $omset));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $input, $id)
    {
        $omset = OmsetModel::findOrFail($id);
        $omset->tanggal= $input->tanggal;
        $omset->omset = $input->omset;
        $informasi = $omset->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('omset.index')->with(['informasi' => $informasi]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $omset = OmsetModel::findOrFail($id);
        $informasi = $omset->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('omset.index')->with(['informasi' => $informasi]);
    }
}

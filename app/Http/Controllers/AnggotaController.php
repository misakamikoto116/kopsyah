<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelpers;
use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use App\Model\InvestasiModel;
use App\Model\PembayaranModel;
use App\Model\JenisInvestasiModel;
use App\Model\JenisPembayaranModel;
use App\Model\NotificationsModel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AnggotaRequest;
use App\Notifications\NewRegistrasi;
use GuzzleHttp\Client;
use Intervention\Image\ImageManagerStatic as Image;
use Hash;
use App\Http\Resources\Api\Anggota\AnggotaCollection;

class AnggotaController extends Controller
{
    use PerformaAnggota;

    public function index(Request $request)
    {
        $semuaAnggotaTes = AnggotaModel::Cari($request)->get();
        $semuaAnggota = AnggotaModel::Cari($request)->paginate(10);

        $urutan = [];

        foreach ($semuaAnggotaTes as $key => $data) {
            $urutan[] = $key + 1;
        }
        $reverse_urutan = array_reverse($urutan);
        // dd($semuaAnggota);
        // hapus peringatan notifikasi pendaftaran anggota di navbar
        $notif = NotificationsModel::where('type', '=', 'App\Notifications\NewRegistrasi')->delete();

        return view('anggota.awal', compact('semuaAnggota', 'reverse_urutan'));
    }

    public function fullAnggota(Request $request)
    {
        return new AnggotaCollection(AnggotaModel::get());
    }

    public function create()
    {
        $jenis_investasi = JenisInvestasiModel::all();
        $jenis_pembayaran = JenisPembayaranModel::all();

        return view('anggota.tambah', compact('jenis_investasi', 'jenis_pembayaran'));
    }

    public function store(AnggotaRequest $input)
    {
        try {

            DB::beginTransaction();

            $data_user = [
                'role_id'       => $input->role_id,
                'username'      => $input->email,
                'password'      => Hash::make($input->password),
            ];

            $user = new UserModel();
            $user->fill($data_user);
            $user->save();
            $user->notify(new NewRegistrasi());

            // formatting
            $tgl_lhr    = date('d-m-Y', strtotime($input->tanggal_lahir));
            $tgllhr     = date('Y-m-d', strtotime($tgl_lhr));

            // foto
            // pengatuan path penyimpanan gambar
            $file       = $input->file('file_ktp');
            $pic_ktp    = 'ktp' . time() . '.' . $file->getClientOriginalExtension();
            $path       = public_path('images/anggota/' . $pic_ktp);
            Image::make($file->getRealPath())->resize(836, 498)->save($path);

            // pengatuan path penyimpanan gambar
            $file_profil    = $input->file('file_foto');
            $pic_profil     = 'foto' . time() . '.' . $file_profil->getClientOriginalExtension();
            $path           = public_path('images/anggota/' . $pic_profil);
            Image::make($file_profil->getRealPath())->resize(836, 498)->save($path);

            // get anggota sebelum nya
            $last_anggota = AnggotaModel::orderBy('id', 'DESC')->first();

            // get last nomor anggota
            $last_nomor   = explode('.', $last_anggota->nomor_anggota);

            // Dapatkan angka dari tanggal untuk nomor anggota di komen berikutnya
            $tgl_now        = date('dm');
            $tahun_now      = date('Y');
            $nomor          = substr($tahun_now, 2);
            $new_id         = (int)(end($last_nomor)) + 1;
            $nomor_anggota  = '212' . '.' . $tgl_now . $nomor . '.' . sprintf('%06d', $new_id);

            $data_anggota = [
                'no_ktp'                => $input->no_ktp,
                'nama'                  => $input->nama,
                'agama'                 => 'Islam',
                'tempat_lahir'          => $input->tempat_lahir,
                'tanggal_lahir'         => $tgllhr,
                'email'                 => $input->email,
                'no_telepon'            => $input->no_telepon,
                'jenis_kelamin'         => $input->jenis_kelamin,
                'kota'                  => $input->kota,
                'alamat'                => $input->alamat,
                'penghasilan'           => $input->penghasilan,
                'pekerjaan'             => $input->pekerjaan,
                'status_keanggotaan'    => $input->role_id,
                'status_aktifasi'       => '0',
                'users_id'              => $user->id,
                'file_foto'             => 'images/anggota/' . $pic_profil,
                'file_ktp'              => 'images/anggota/' . $pic_ktp,
                'nomor_anggota'         => $nomor_anggota,
            ];

            $anggota = new AnggotaModel();
            $anggota->fill($data_anggota);
            $anggota->save();

            //data to API Pos
            //proses ke API pos
            $data = [
                'no_pelanggan'       => $anggota->nomor_anggota,
                'nama'               => $anggota->nama,
                'alamat'             => $anggota->alamat,
                'alamat_pajak'       => $anggota->alamat,
                'kota'               => $anggota->kota,
                'telepon'            => $anggota->no_telepon,
                'personal_kontak'    => $anggota->no_telepon,
                'email'              => $anggota->email,
                'nama'               => $anggota->nama,
                'kota'               => $anggota->kota,
                'nik'                => $anggota->no_ktp,
            ];

            $apiPos = new ApiHelpers();
            $apiPos->sendPostAnggota($data);

            //end proses ke API pos

            foreach ($input->investasi as $jenis_investasi_id => $value) {
                // jika ada investasi
                if ($value != 0) {
                    //pengaturan path penyimpanan gambar
                    $file   = $input->file('foto_resi')[$jenis_investasi_id];
                    $pic    = 'fresi' . time() . '.' . $file->getClientOriginalExtension();
                    $path   = public_path('images/investasi/' . $pic);
                    Image::make($file->getRealPath())->resize(836, 498)->save($path);

                    // pengaturan tanggal dari format datepiker ke format database
                    $tgl_inv                    = date('d-m-Y', strtotime($input->tanggal_investasi[$jenis_investasi_id]));
                    $tglinv                     = date('Y-m-d', strtotime($tgl_inv));

                    $anggota->investasi()->save(new InvestasiModel([
                        'jumlah'                => $value,
                        'biaya_cetak_investasi' => $input->biaya_cetak_investasi[$jenis_investasi_id],
                        'tanggal_pembayaran'    => $tglinv,
                        'status'                => '0',
                        'jenis_investasi_id'    => $jenis_investasi_id,
                        'foto_resi'             => 'images/investasi/' . $pic,
                    ]));
                }
            }

            DB::commit();

            $informasi = 'Berhasil simpan data';

            return redirect()->route('anggota.index')->with(['informasi' => $informasi]);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show($id)
    {
        $anggota = AnggotaModel::findOrFail($id);

        return view('anggota.lihat')->with(array('anggota' => $anggota));
    }

    public function edit($id)
    {
        $anggota = AnggotaModel::findOrFail($id);

        return view('anggota.edit', compact('anggota'))->with(array('anggota' => $anggota));
    }

    public function update($id, Request $input)
    {
        $anggota = AnggotaModel::findOrFail($id);
        $oldNoAnggota = $anggota->nomor_anggota;
        $validator = \Validator::make($input->all(), [
            'no_ktp' => 'required|min:16|max:16',
            'nama' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
        ]);

        if ($validator->fails()) {
            $informasi = 'Nomor KTP harus 16 digit.';

            return redirect()->back()->with(['informasi' => $informasi]);
        }
        $anggota->nomor_anggota = $input->nomor_anggota;
        $anggota->no_ktp = $input->no_ktp;
        $anggota->nama = $input->nama;
        $anggota->tempat_lahir = $input->tempat_lahir;
        $anggota->no_telepon = $input->no_telepon;
        $anggota->jenis_kelamin = $input->jenis_kelamin;
        $anggota->kota = $input->kota;
        $anggota->alamat = $input->alamat;
        $anggota->penghasilan = $input->penghasilan;
        $anggota->pekerjaan = $input->pekerjaan;
        $anggota->status_keanggotaan = $input->role_id;

        // jika inputan update tanggal lahir tidak null
        if ($input->tanggal_lahir != null) {
            $validator = \Validator::make($input->all(), [
                'tanggal_lahir' => 'date_format:"d-m-Y"|required',
            ]);

            if ($validator->fails()) {
                $informasi = 'Tanggal lahir salah';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            // pengaturan format tanggal datepiker sesuai tanggal database
            $tgl_lhr = date('d-m-Y', strtotime($input->tanggal_lahir));
            $tgllhr = date('Y-m-d', strtotime($tgl_lhr));
            $anggota->tanggal_lahir = $tgllhr;
        }

        // jika inputan file ktp tidak null
        if ($input->file_ktp != null) {
            $validator = \Validator::make($input->all(), [
                'file_ktp' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);

            if ($validator->fails()) {
                $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,atau PNG ';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            //pengaturan path file ktp
            $file = $input->file('file_ktp');
            $pic = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/anggota/' . $pic);
            Image::make($file->getRealPath())->resize(836, 498)->save($path);
            $anggota->file_ktp = 'images/anggota/' . $pic;
        }

        // jika inputan file foto tidak null
        if ($input->file_foto != null) {
            $validator = \Validator::make($input->all(), [
                'file_foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            ]);

            if ($validator->fails()) {
                $informasi = 'Format file tidak didukung atau ukuran file lebih dari 2Mb. Pastikan format file adalah jpeg,png,jpg,JPEG,JPG,atau PNG ';

                return redirect()->back()->with(['informasi' => $informasi]);
            }
            //pengaturan path file foto
            $file_profil = $input->file('file_foto');
            $pic = time() . '.' . $file_profil->getClientOriginalExtension();
            $path = public_path('images/anggota/' . $pic);
            Image::make($file_profil->getRealPath())->resize(836, 498)->save($path);
            $anggota->file_foto = 'images/anggota/' . $pic;
        }

        // jika update anggota save lanjut update user
        if ($anggota->save()) {
            $validator = \Validator::make($input->all(), [
                'role_id' => 'required',
            ]);

            if ($validator->fails()) {
                $informasi = 'Status keanggotaan harus disi';

                return redirect()->back()->with(['informasi' => $informasi]);
            }


            //data to API Pos
            //proses ke API pos
            $data = [
                'oldNoAnggota'       => $oldNoAnggota,
                'no_pelanggan'       => $anggota->nomor_anggota,
                'nama'               => $anggota->nama,
                'alamat'             => $anggota->alamat,
                'alamat_pajak'       => $anggota->alamat,
                'kota'               => $anggota->kota,
                'telepon'            => $anggota->no_telepon,
                'personal_kontak'    => $anggota->no_telepon,
                'nama'               => $anggota->nama,
                'kota'               => $anggota->kota,
                'nik'                => $anggota->no_ktp,
            ];

            $apiPos = new ApiHelpers();
            $apiPos->sendUpdateAnggota($data);

            //end proses ke API pos

            $users_id = $anggota->users_id;
            $role = $input->role_id;
            $user = UserModel::findOrFail($users_id);
            $user->role_id = $role;
            $user->save();
            $informasi = 'Berhasil update Data';
        } else {
            $informasi = 'Gagal update Data';
        }

        return redirect()->route('anggota.index')->with(['informasi' => $informasi]);
    }

    public function destroy($id)
    {
        $anggota = AnggotaModel::with('investasi')->findOrFail($id);
        $noAnggota = $anggota->no_anggota;
        if ($anggota->delete()) {
            $informasi = 'Berhasil hapus data';

            //proses ke API pos
            $apiPos = new ApiHelpers();
            $apiPos->sendDeleteAnggota($noAnggota);

            //end proses ke API pos
        } else {
            $informasi = 'Gagal hapus data';
        }

        return redirect()->route('anggota.index')->with(['informasi' => $informasi]);
    }

    public function status($id, $status)
    {
        // mengubah status aktifasi anggota dari klik tombol aktifasi
        $anggota = AnggotaModel::findOrFail($id);
        $update = $anggota->where('id', $id)->firstOrFail()->update(['status_aktifasi' => $status]);
        if ($status == 1) {
            $informasi = 'Akun berhasil di aktifkan';

            // Send confirmation sms
            $message = 'Assalamualaikum wr. wb , Selamat ' . $anggota->nama . ', Akun anda telah di aktivasi. Anda telah resmi bergabung di 212 Mart Samarinda dengan Nomor Anggota : ' . $anggota->nomor_anggota . '. Silahkan login ke halaman web kami www.koperasisyariahsms.com dengan menggunakan alamat email dan password anda ';
            $sms = new \App\Helpers\Zenziva();
            $sms->send($anggota->no_telepon, $message);

            $this->create($anggota);

            //Send confirmation mail
            $email = $anggota['email'];
            $data = ['pesan' => $message];
            Mail::send('emails.hello', $data, function ($mail) use ($email) {
                $mail->to($email, 'Akun');

                $mail->subject('Akun Sudah Diaktifkan!');
            });
        } else {
            $informasi = 'Akun berhasil di Non Aktifkan';

            //Send confirmation sms
            $message = 'Assalamualaikum wr. wb , Maaf ' . $anggota->nama . ', Akun anda dengan Nomor Anggota : ' . $anggota->nomor_anggota . ' telah di non-aktifkan. Info lebih lanjut silahkan menghubungi call center kami. 212 Mart Samarinda 
                        ';
            $sms = new \App\Helpers\Zenziva();
            $sms->send($anggota->no_telepon, $message);

            $this->create($anggota);

            //Send confirmation mail
            $email = $anggota['email'];
            $data = ['pesan' => $message];
            Mail::send('emails.hello', $data, function ($mail) use ($email) {
                $mail->to($email, 'Akun');

                $mail->subject('Akun Sudah Dinonaktifkan!');
            });
        }

        return redirect()->route('anggota.index')->with(['informasi' => $informasi]);
    }
}

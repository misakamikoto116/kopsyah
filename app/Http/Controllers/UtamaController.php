<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UserModel;
use App\Model\JenisInvestasiModel;
use App\Model\TentangModel;
use App\Model\DewanPengurusModel;
use App\Model\KepengurusanModel;
use App\Model\DokumenModel;

class UtamaController extends Controller
{
    public function welcome()
    {
        $view = [
            'title'        => 'Beranda',
            'jml_anggota'  => UserModel::whereHas('anggota', function ($a) {
                                $a->where('status_aktifasi', '=', '1');
                            })
                                ->where('role_id', '!=', '1')
                                ->count(),
            'jml_investor' => UserModel::whereHas('anggota', function ($a) {
                                    $a->where('status_aktifasi', '=', '1');
                                })
                                    ->where('role_id', '=', '3')
                                    ->count(),
            'jml_usaha'    => JenisInvestasiModel::all()->count(),
            'berita'       => TentangModel::where('jenis', '=', 5)->orderBy('created_at', 'DESC')->limit(3)->get(),
        ];

        return view('welcome')->with($view);
    }

    public function tentang()
    {
        $view = [
            'title'         => 'Profil',
            'visis'         => TentangModel::where('jenis', '=', '1')->first(),
            'badanhukum'    => TentangModel::where('jenis', '=', '2')->first(),
            'ketentuan'     => TentangModel::where('jenis', '=', '3')->get(),
            'breadcrumbs'   => ['Beranda','Tentang Kami'],
            'bar_profil'    => 'active',
        ];

        return view('pages.profil')->with($view);
    }

    public function struktur()
    {   
        $ketua_Penasehat = DewanPengurusModel::where('struktur', '=', '3')->where('jabatan', '=', '1')->where('status_kepengurusan', '=', '1')->first();
        if ($ketua_Penasehat != null) {
            $dewan_Penasehat = DewanPengurusModel::where('struktur', '=', '3')->where('id', '!=', $ketua_Penasehat->id)->where('status_kepengurusan', '=', '1')->get();
        } else {
            $dewan_Penasehat = null;
        }

        $ketua_Operasional = DewanPengurusModel::where('struktur', '=', '2')->where('jabatan', '=', '1')->where('status_kepengurusan', '=', '1')->first();
        if ($ketua_Operasional != null) {
            $dewan_Operasional = DewanPengurusModel::where('struktur', '=', '2')->where('id', '!=', $ketua_Operasional->id)->where('status_kepengurusan', '=', '1')->get();
        } else {
            $dewan_Operasional = null;
        }

        $ketua_Syariah = DewanPengurusModel::where('struktur', '=', '1')->where('jabatan', '=', '1')->where('status_kepengurusan', '=', '1')->first();
        if ($ketua_Syariah != null) {
            $dewan_Syariah = DewanPengurusModel::where('struktur', '=', '1')->where('id', '!=', $ketua_Syariah->id)->where('status_kepengurusan', '=', '1')->get();
        } else {
            $dewan_Syariah = null;
        }

        $ketua_Pengurus = KepengurusanModel::where('jabatan_id', '=', '1')->where('status_kepengurusan', '=', '1')->first();
        if ($ketua_Pengurus != null) {
            $dewan_Pengurus = KepengurusanModel::where('id', '!=', $ketua_Pengurus->id)->where('status_kepengurusan', '=', '1')->get();
        } else {
            $dewan_Pengurus = null;
        }

        $view = [
            // 'semua_Dewan'       => , 
            'title'             => 'Struktur', 
            'ketua_Penasehat'   => $ketua_Penasehat, 
            'dewan_Penasehat'   => $dewan_Penasehat, 
            'ketua_Operasional' => $ketua_Operasional, 
            'dewan_Operasional' => $dewan_Operasional, 
            'ketua_Syariah'     => $ketua_Syariah, 
            'dewan_Syariah'     => $dewan_Syariah, 
            'ketua_Pengurus'    => $ketua_Pengurus, 
            'dewan_Pengurus'    => $dewan_Pengurus,
            'breadcrumbs'       => ['Beranda','Struktur'],
            'bar_struktur'      => 'active',
        ];

        return view('pages.struktur')->with($view);
    }

    public function faq()
    {
        $view = [
            'title'         => 'FAQ',
            'faq'           => TentangModel::where('jenis', '=', '4')->orderBy('id', 'ASC')->get(),
            'breadcrumbs'   => ['Beranda','FAQ'],
            'bar_faq'       => 'active',
        ];

        return view('pages.faq')->with($view);
    }

    public function blog()
    {
        $terkini = TentangModel::where('jenis', '=', 5)->orderBy('created_at', 'DESC')->first();
        if ($terkini !== null) {
            $terbaru = TentangModel::where('jenis', '=', 5)->where('id', '<>', $terkini->id)->orderBy('created_at', 'DESC')->limit(3)->get();
            $listberita = TentangModel::where('jenis', '=', 5)
                ->where('id', '<>', $terkini->id)
                ->orderBy('created_at', 'DESC')
                ->limit(15)
                ->get();
        } else {
            $terbaru = null;
            $listberita = null;
        }

        if ($terkini !== null) {
            $tgl = \Carbon\Carbon::parse($terkini->created_at)->format('F j, Y, g:i a');
        } else if ($terkini == null) {
            $tgl = \Carbon\Carbon::now()->format('F j, Y, g:i a');
        }

        $view = [
            'title'         => 'Blog',
            'terkini'       => $terkini,
            'terbaru'       => $terbaru,
            'tgl'           => $tgl,
            'listberita'    => $listberita,
            'breadcrumbs'   => ['Beranda','Blog'],
            'bar_blog'      => 'active',
        ];

        return view('pages.blog')->with($view);
    }

    public function blogOne($id)
    {
        $terkini = TentangModel::findOrFail($id);
        if ($terkini !== null) {
            $terbaru = TentangModel::where('jenis', '=', 5)->where('id', '<>', $terkini->id)->orderBy('created_at', 'DESC')->limit(3)->get();
            $listberita = TentangModel::where('jenis', '=', 5)
                ->where('id', '<>', $terkini->id)
                ->orderBy('created_at', 'DESC')
                ->limit(15)
                ->get();
        } else {
            $terbaru = null;
            $listberita = null;
        }

        if ($terkini !== null) {
            $tgl = \Carbon\Carbon::parse($terkini->created_at)->format('F j, Y, g:i a');
        } else if ($terkini == null) {
            $tgl = \Carbon\Carbon::now()->format('F j, Y, g:i a');
        }

        $view = [
            'title'         => 'Blog',
            'terkini'       => $terkini,
            'terbaru'       => $terbaru,
            'tgl'           => $tgl,
            'listberita'    => $listberita,
            'breadcrumbs'   => ['Beranda','Blog'],
            'bar_blog'      => 'active',
        ];

        return view('pages.blog_one')->with($view);
    }

    public function page_doc(Request $request)
    {
        $view = [
            'title'         => 'Dokumen',
            'semuaDokumen'  => DokumenModel::orderBy('id', 'DESC')->get(),
            'breadcrumbs'   => ['Beranda','Dokumen'],
            'bar_page_doc'  => 'active',
        ];

        return view('pages.doc')->with($view);
    }
    public function download(Request $request)
    {
        return response()->download(public_path('/' . $request['file']));
    }
}

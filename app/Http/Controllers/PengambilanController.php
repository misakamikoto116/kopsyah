<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PengambilanModel;
use \App\Http\Requests\PengambilanRequest;
use App\Model\UserModel;
use App\Model\AnggotaModel;
use Auth;

class PengambilanController extends Controller
{
    public function index(){
        $semuaPengambilan= PengambilanModel::all();
        return view('pengambilan.awal',compact('semuaPengambilan'));
    }

    public function create(){
        //$anggotaPluck = AnggotaModel::pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_aktifasi', '=', '1')->get();
        return view('pengambilan.tambah', compact('anggotaPluck'));
    }

    public function store(PengambilanRequest $input){
        $pengambilan = new PengambilanModel();
        $tgl_ambl = date('d-m-Y', strtotime($input->tanggal));
        $ambil = date('Y-m-d', strtotime($tgl_ambl));
        $pengambilan = new PengambilanModel($input->only('anggota_id'));
        $pengambilan->users_id = Auth::id();
        $pengambilan->jumlah = $input->jumlah;
        $pengambilan->tanggal = $ambil;
        $pengambilan->deskripsi = $input->deskripsi;
        $pengambilan->status = $input->status;        
        
        $informasi=$pengambilan->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
        return redirect()->route('pengambilan.index')->with(['informasi'=>$informasi]);

    }

    public function edit($id){
        $pengambilan= PengambilanModel::findOrFail($id);
        // $anggotaPluck = AnggotaModel::pluck('nama', 'id');
        $anggotaPluck = AnggotaModel::where('status_aktifasi', '=', '1')->get();
        return view('pengambilan.edit',compact('pengambilan','anggotaPluck'))->with(array('pengambilan'=>$pengambilan));
    }

    public function update($id, PengambilanRequest $input){
        $pengambilan = PengambilanModel::findOrFail($id);
        // if($input->jumlah != null){
        //     $validator = \Validator::make($input->all(), [
        //         'jumlah' => 'numeric|required',

        //     ]);
        //     if ($validator->fails()) {

        //         $informasi = 'Jumlah Pengambilan Harus Angka';
        //         return redirect()->back()->with(['informasi' => $informasi]);

        //     }
        // }
            // if ($input->tanggal== null) {
            
            //     $pengambilan->anggota_id = $input->anggota_id;
            //     $pengambilan->users_id = Auth::id();
            //     $pengambilan->jumlah = $input->jumlah;
            //     $pengambilan->deskripsi = $input->deskripsi;        
            // }else{
            //     $validator = \Validator::make($input->all(), [
            //         'tanggal' => 'date_format:"d-m-Y"|required',

            //     ]);
            //     if ($validator->fails()) {

            //         $informasi = 'Format tanggal salah';
            //         return redirect()->back()->with(['informasi' => $informasi]);

            //     }

            // }

                $tgl_ambl = date('d-m-Y', strtotime($input->tanggal));
                $ambil = date('Y-m-d', strtotime($tgl_ambl));
                $pengambilan->anggota_id = $input->anggota_id;
                $pengambilan->users_id = Auth::id();
                $pengambilan->jumlah = $input->jumlah;
                $pengambilan->tanggal = $ambil;
                $pengambilan->deskripsi = $input->deskripsi;

                if ($pengambilan->save()) {
                    $informasi = 'Berhasil update Data';
                } else {
                    $informasi = 'Gagal update Data';
                }
                return redirect()->route('pengambilan.index')->with(['informasi'=>$informasi]);
    }

    public function show($id){
        $pengambilan = PengambilanModel::findOrFail($id);
        return view('pengambilan.lihat')->with(array('pengambilan'=>$pengambilan));
    }

    public function destroy($id){
        $pengambilan= PengambilanModel::findOrFail($id);
        $informasi=$pengambilan->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('pengambilan.index')->with(['informasi'=>$informasi]);

    }

    public function status($id,$status) {
        $pengambilan = new PengambilanModel;
        $update = $pengambilan->where('id',$id)->firstOrFail()->update(['status'=>$status]);
        if($status == 1) {
            $informasi = 'Berhasil Aktifkan';
        }
        else {
            $informasi = 'Berhasil Non Aktifkan';
        }
        return redirect()->route('pengambilan.index')->with(['informasi'=>$informasi]);
    }


}

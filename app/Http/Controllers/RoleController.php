<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Model\RoleModel;

class RoleController extends Controller
{
    public function index(){
        $semuaRole= RoleModel::all();
        return view('role.awal',compact('semuaRole'));
    }
    
    public function edit($id){
        $role= RoleModel::findOrFail($id);
        return view('role.edit')->with(array('role'=>$role));
    }

    public function update($id, Request $input){
        $role= RoleModel::findOrFail($id);
        $role->nama=$input->nama;
        $informasi=$role->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('role.index')->with(['informasi'=>$informasi]);
    }

    public function show($id){
        $role = RoleModel::findOrFail($id);
        return view('role.lihat')->with(array('role'=>$role));
    }
    
}

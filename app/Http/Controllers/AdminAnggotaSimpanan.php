<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\PembayaranModel;

class AdminAnggotaSimpanan extends Controller
{
    
    public function awal($id)
    {
        $anggota= AnggotaModel::findOrFail($id);
        $pembayaranAnda= PembayaranModel::where('anggota_id','=',$anggota->id)->get();
        $total= PembayaranModel::where('anggota_id', '=', $anggota->id)->sum('jumlah');

        return view('detail_simpanan.awal',compact('anggota','pembayaranAnda','total'));
    }
}

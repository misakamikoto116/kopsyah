<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TentangModel;
use App\Http\Requests\VisimisiRequest;

class VisimisiController extends Controller
{
    public function index(Request $request){
        // $semuaTentang= TentangModel::where('jenis','=','1')->get();
        $semuaTentang= TentangModel::where('jenis','=','1')->Cari($request)->paginate(5);
        
        return view('visi_misi.awal',compact('semuaTentang'));
    }

    public function create(){
        return view('visi_misi.tambah');
    }

    public function store(VisimisiRequest $input){
            $tentang= new TentangModel();
            $tentang->judul=$input->judul;
            $tentang->isi=$input->isi;
            $tentang->jenis = '1';
            $informasi=$tentang->save() ? 'Berhasil simpan data' : 'Gagal simpan data';
            return redirect()->route('visimisi.index')->with(['informasi'=>$informasi]);
    }

    public function edit($id){
        $tentang= TentangModel::findOrFail($id);
        return view('visi_misi.edit')->with(array('tentang'=>$tentang));
    }

    public function update($id, VisimisiRequest $input){
        $tentang= TentangModel::findOrFail($id);
        $tentang->judul=$input->judul;
        $tentang->isi=$input->isi;
        $informasi=$tentang->save() ? 'Berhasil update data' : 'Gagal update data';
        return redirect()->route('visimisi.index')->with(['informasi'=>$informasi]);
    }

    public function destroy($id){
        $tentang= TentangModel::findOrFail($id);
        $informasi=$tentang->delete() ? 'Berhasil hapus data' : 'Gagal hapus data';
        return redirect()->route('visimisi.index')->with(['informasi'=>$informasi]);

    }
}

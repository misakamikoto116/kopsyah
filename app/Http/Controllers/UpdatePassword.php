<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnggotaModel;
use App\Model\UserModel;
use Hash;
use Auth;
use App\Http\Requests\UpdatePasswordRequest;
class UpdatePassword extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota= AnggotaModel::findOrFail($id);
        $user= UserModel::where('id','=',$anggota->users_id)->first();
        if ($user->id == Auth::user()->id) {
            if ($user->role_id == '3') {
                return view('only_investor.profil.editPassword', compact('anggota'))->with(array('anggota' => $anggota));
            } else {
                return view('only_anggota.profil.editPassword', compact('anggota'))->with(array('anggota' => $anggota));
            }
        }else {
            return view('errors.request_error');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePasswordRequest $input, $id)
    {
        
        $user= UserModel::findOrFail($id);
        
             if($input->password != null){

                //jika konfirmasi password sudah sama
                if(password_verify($input->password_old, $user->password)){
                $user->password = Hash::make($input->password);
                if ($user->save()) {
                        $informasi = 'Berhasil Ubah Password';
                    } else {
                        $informasi = 'Gagal Ubah Password';
                    }
                    if($user->role_id == '3'){  
                return redirect()->route('profil.index')->with(['informasi'=>$informasi]);
                    }elseif($user->role_id == '2'){
                        return redirect()->route('profil_anggota.index')->with(['informasi'=>$informasi]);
                    }
                }else{
                    $informasi= 'Password tidak cocok';
                    return redirect()->back()->with(['informasi'=>$informasi]);
                }
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

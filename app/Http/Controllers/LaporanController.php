<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use PDF;
use \App\Model\JenisInvestasiModel;
use App\Model\InvestasiModel;
class LaporanController extends Controller
{
    public function iniLaporan(Request $request)
    {
        $jenisInvestasi= JenisInvestasiModel::CariUsaha($request)->get();

        return view('iniLaporan',compact('jenisInvestasi'));
    }
    
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfview($id, Request $request)
    {
        
        $items = InvestasiModel::with('anggota','jenis_investasi')
                ->where('jenis_investasi_id','=',$id)
                ->get();

        return view('pdfview',compact('items','id'));
    }

    public function download($jenis, $id)
    {
        $items = InvestasiModel::with('anggota', 'jenis_investasi')
                ->where('jenis_investasi_id', '=', $id)
                ->get();
        if($jenis === 'pdf'){
            $pdf = PDF::loadView('pdfview',compact('id','items'));
            return $pdf->download('LaporanUnitUsaha.pdf');

        }
        return redirect()->back();
        // return view('pdfview',compact('items', 'id'));
    }

    function rupiah($nilai)
    {
        return 'Rp. ' . number_format($nilai, 0, ',', '.');
    }

}

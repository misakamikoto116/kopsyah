<?php

namespace App\Http\Controllers;

use App\PenjualanModel;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PenjualanModel $penjualanModel)
    {
        $this->validate($request, [
                'tanggal' => 'required',
                'total' => 'required',
                'jml_umkm' => 'required',
                'jml_non' => 'required',
                'nomor_anggota' => 'required',
            ]);
        $anggota = AnggotaModel::where('nomor_anggota', $request->nomor_anggota)->first();
        $penjualan = new PenjualanModel();
        $penjualan->tanggal = $request->tanggal;
        $penjualan->total = $request->total;
        $penjualan->jml_umkm = $request->jml_umkm;
        $penjualan->jml_non = $request->jml_non;
        $penjualan->nomor_anggota = $request->nomor_anggota;
        $penjualan->anggota_id = $request->anggota_id;
        $informasi = $penjualan->save() ? 'Berhasil simpan data' : 'Gagal simpan data';

        return [
            'metadata' => [
                'status' => 'OK',
                'pesan' => $informasi,
            ],
        ];
        // return redirect()->route('jabatan.index')->with(['informasi' => $informasi]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\PenjualanModel      $penjualanModel
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenjualanModel $penjualanModel)
    {
        $penjualan = PenjualanModel::findOrFail($id);
        $penjualan->tanggal = $request->tanggal;
        $penjualan->total = $request->total;
        $penjualan->jml_umkm = $request->jml_umkm;
        $penjualan->jml_non = $request->jml_non;

        $informasi = $penjualan->save() ? 'Berhasil update data' : 'Gagal update data';
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class AnggotaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            if (\Auth::user()->role_id == '2') return $next($request);
        }
        return redirect('login');
    
    }
}

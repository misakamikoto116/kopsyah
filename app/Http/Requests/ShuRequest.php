<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'deskripsi' => 'required',
            'persentase' => 'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'persentase.numeric' => 'Persentasi harus angka',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PengambilanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'date_format:"d-m-Y"|required',
            'jumlah' => 'required|numeric',
            'deskripsi' => 'required',
            'status' => 'numeric',
            'anggota_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'tanggal.required' => 'Tanggal harus diisi',
            'tanggal.date_format' => 'Format tanggal menjabat salah',
            'jumlah.numeric' => 'Jumlah harus diisi dengan angka',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'status.numeric' => 'Status harus diisi',
            'angggota_id.required' => 'Anggota harus diisi',
        ];
    }
}
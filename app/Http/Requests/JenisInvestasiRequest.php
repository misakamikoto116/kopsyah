<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JenisInvestasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'jumlah_investasi' => 'required|numeric',
            'status' => 'required',
            'deskripsi' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi',
            'jumlah_investasi.numeric' => 'modal harus angka',
            'jumlah_investasi.required' => 'modal harus diisi',
            
            'status.required' => 'Status harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
        ];
    }
}


<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'role_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username Harus Diisi',
            'role_id.required' => 'Role Harus Diisi',
            'password.required' => 'Password Baru Harus Diisi',
            'password.min' => 'Password Baru Minimal 6 digit',
            'password.confirmed' => 'Password Konfirmasi tidak sama',
            'password.different' => 'Password Baru Harus Berbeda dengan Password Lama',
            'password_confirmation.required' => 'Password Konfirmasi Harus Diisi',
            'password_confirmation.min' => 'Password Konfirmasi Minimal 6 digit',
        ];
    }
}

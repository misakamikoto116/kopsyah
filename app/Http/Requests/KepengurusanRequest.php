<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KepengurusanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_menjabat' => 'date_format:"d-m-Y"|required',
            'sk_pengurus' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048',
            'status_kepengurusan' => 'required|numeric',
            'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048',
            'jabatan_id' => 'required|numeric',
            
        ];
    }

    public function messages()
    {
        return [
            'tanggal_menjabat.required' => 'Tanggal menjabat harus diisi',
            'tanggal_menjabat.date_format' => 'Format tanggal menjabat salah',
            'sk_pengurus.mimes' => 'SK pengurus harus jpeg,png,jpg,JPEG,JPG,PNG, atau pdf,doc,docx,ppt,pptx,xls,xlsx',
            'sk_pengurus.max' => 'SK pengurus maksimal 2Mb',
            'status_kepengurusan.required' => 'Status kepengurusan harus diisi',
            'status_kepengurusan.numeric' => 'Status kepengurusan harus diisi',
            'foto.mimes' => 'Foto harus jpeg,png,jpg,JPEG,JPG,atau PNG',
            'foto.max' => 'Foto maksimal 2Mb',
            'jabatan_id.numeric' => 'Jabatan harus diisi',
            'jabatan_id.required' => 'Jabatan harus diisi',
        ];
    }

}

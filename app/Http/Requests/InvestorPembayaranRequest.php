<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvestorPembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            'tanggal_pembayaran' => 'date_format:"d-m-Y"|required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file_resi.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
        ];
    }
}

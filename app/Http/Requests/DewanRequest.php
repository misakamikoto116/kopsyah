<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DewanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_menjabat' => 'date_format:"d-m-Y"|required',
            'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048',
            'sk_pengurus' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG,pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048',
            'struktur' => 'required|numeric',
            'jabatan' => 'required|numeric',
            'status_kepengurusan' => 'required|numeric',
            'deskripsi' => 'required',
            'profil' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'tanggal_menjabat.required' => 'Tanggal menjabat harus diisi',
            'tanggal_menjabat.date_format' => 'Format tanggal menjabat salah',
            'foto.required' => 'Foto harus diisi',
            'foto.mimes' => 'Foto harus jpeg,png,jpg,JPEG,JPG,atau PNG',
            'foto.max' => 'Foto maksimal 2Mb',
            'sk_pengurus.required' => 'SK pengurus harus diisi bisa berupa File atau Gambar(Misal diScan)',
            'sk_pengurus.mimes' => 'SK pengurus harus jpeg,png,jpg,JPEG,JPG,PNG, atau pdf,doc,docx,ppt,pptx,xls,xlsx',
            'sk_pengurus.max' => 'SK pengurus maksimal 2Mb',
            'struktur.numeric' => 'Struktur harus diisi',
            'struktur.required' => 'Struktur harus diisi',
            'jabatan.numeric' => 'Jabatan harus diisi',
            'jabatan.required' => 'Jabatan harus diisi',
            'status_kepengurusan.required' => 'Status kepengurusan harus diisi',
            'status_kepengurusan.numeric' => 'Status kepengurusan harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'profil.required' => 'Profil harus diisi',
        ];
    }
}

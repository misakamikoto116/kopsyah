<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DokumenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'judul' => 'required',
            'deskripsi' => 'required',
            'file_dokumen' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx|max:2048|required',
        ];
    }
    public function messages()
    {
        return [
            'judul.required' => 'Judul harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'file_dokumen.required' => 'File Dokumen harus diisi',
            'file_dokumen.mimes' => 'File Dokumen harus pdf,doc,docx,ppt,pptx,xls atau,xlsx',
            'file_dokumen.max' => 'File Dokumen maksimal 2Mb',
        ];
    }
}

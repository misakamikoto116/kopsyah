<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvestorInvestasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foto_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            'tanggal_pembayaran' => 'date_format:"d-m-Y"|required',
        ];
    }
    public function messages()
    {
        return [
            'foto_resi.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
        ];
    }
}

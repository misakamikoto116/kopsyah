<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'              => 'unique:users',
            'password'              => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'role_id'               => 'required',
            // 'g-recaptcha-response' => 'required|captcha'
            'no_ktp'                => 'required|min:16|max:16',
            'nama'                  => 'required',
            'alamat'                => 'required',
            'file_ktp'              => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            'file_foto'             => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            'tanggal_lahir'         => 'date_format:"d-m-Y"|required',
            'email'                 => 'required|unique:anggota',
            // 'file_resi'             => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
            // 'tanggal_pembayaran'    => 'date_format:"d-m-Y"|required',
            //jika role_id =3 makarequire
            'foto_resi.*'           => 'required_if:role_id,3|mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048',
            // 'tanggal_pembayaran.*'  => 'required_if:role_id,3|date_format:"d-m-Y"|required',
            'no_telepon'            => 'required|unique:anggota',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'no_ktp.min' => 'Nomor KTP Anda Kurang Dari 16 Digit.',
            'no_ktp.max' => 'Nomor KTP Anda Lebih Dari 16 Digit.',
            'username.unique' => 'Email Sudah Digunakan. Anda Tidak Diperbolehkan Menggunakannya Lagi',
            'password.min' => 'Password Minimal 16 digit',
            'password.confirmed' => 'Password dan Password Konfirmasi Harus Sama',
            'password_confirmation.min' => 'Password Konfirmasi Minimal 6 digit',
            'no_telepon.unique' => 'Nomor Telepon Sudah Digunakan. Anda Tidak Diperbolehkan Menggunakannya Lagi',
            'file_ktp.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
            'file_foto.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
            // 'file_resi.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
            // 'foto_resi.mimes' => 'Format foto harus tepat. Pastikan hanya jpeg,png,jpg,JPEG,JPG,atau PNG',
            'nama.required' => 'Nama harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'email.unique' => 'Email sudah digunakan.Anda Tidak Diperbolehkan Menggunakannya Lagi',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old' => 'required|min:6',
            'password' => 'required|min:6|confirmed|different:password_old',
            'password_confirmation' => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'password_old.required' => 'Password Lama Harus Diisi',
            'password_old.min' => 'Password Baru Minimal 6 digit',
            'password.required' => 'Password Baru Harus Diisi',
            'password.min' => 'Password Baru Minimal 6 digit',
            'password.confirmed' => 'Password Konfirmasi tidak sama',
            'password.different' => 'Password Baru Harus Berbeda dengan Password Lama',
            'password_confirmation.required' => 'Password Konfirmasi Harus Diisi',
            'password_confirmation.min' => 'Password Konfirmasi Minimal 6 digit dan Harus Sama dengan Password Baru',
        ];
    }
}

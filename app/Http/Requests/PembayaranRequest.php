<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_pembayaran' => 'date_format:"d-m-Y"|required',
            'status' => 'numeric',
            'jbayar' => 'numeric',
            'file_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048',
            'jumlah' =>'numeric',
        ];
    }
    public function messages()
    {
        return [
            'tanggal_pembayaran.required' => 'Tanggal Pembayaran harus diisi',
            'status.numeric' => 'Status harus diisi',
            'jbayar.numeric' => 'Status harus diisi',
            'tanggal_pembayaran.date_format' => 'Format tanggal salah',
            'file_resi.mimes' => 'File resi harus jpeg,png,jpg,JPEG,JPG, atau PNG',
            'file_resi.max' => 'File resi max 2Mb',
            'jumlah.numeric' => 'Jumlah harus diisi dengan angka',
        ];
    }
}

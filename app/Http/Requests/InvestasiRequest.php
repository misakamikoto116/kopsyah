<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvestasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_pembayaran' => 'date_format:"d-m-Y"|required',
            'status' => 'numeric',
            'jumlah' => 'required|numeric',
            'foto_resi' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'status.required' => 'Status harus diisi',
            'status.numeric' => 'Status harus diisi',
            'jumlah.required' => 'jumlah harus diisi',
            'jumlah.numeric' => 'jumlah harus angka',
            'tanggal_pembayaran.date_format' => 'Format tanggal salah',
            'foto_resi.required' => 'File resi harus diisi',
            'foto_resi.mimes' => 'File resi harus jpeg,png,jpg,JPEG,JPG, atau PNG',
            'foto_resi.max' => 'File resi max 2Mb',
        ];
    }
}

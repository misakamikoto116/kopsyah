<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BeritaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'judul' => 'required',
            'isi' => 'required',
            'foto' => 'mimes:jpeg,png,jpg,JPEG,JPG,PNG|max:2048|required',
        ];
    }
    public function messages()
    {
        return [
            'judul.required' => 'Judul harus diisi',
            'isi.required' => 'Isi harus diisi',
            'foto.required' => 'Foto harus diisi',
            'foto.mimes' => 'Foto harus jpeg,png,jpg,JPEG,JPG,atau PNG',
            'foto.max' => 'Foto maksimal 2Mb',
        ];
    }
}

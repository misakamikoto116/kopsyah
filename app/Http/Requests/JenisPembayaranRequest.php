<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JenisPembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'nominal' => 'required|numeric',
            'lama_tempo' => 'required|numeric',
            'deskripsi' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi',
            'nominal.required' => 'Nominal harus diisi',
            'nominal.numeric' => 'Nominal harus angka',
            'lama_tempo.required' => 'Lama tempo harus diisi',
            'lama_tempo.numeric' => 'Lama tempo harus angka (menujukkan jumlah bulan misal 12 bulan maka 12)',
            'deskripsi.required' => 'Deskripsi harus diisi',
        ];
    }
}

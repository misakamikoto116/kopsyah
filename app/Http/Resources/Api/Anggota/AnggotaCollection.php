<?php

namespace App\Http\Resources\Api\Anggota;

use App\Model\AnggotaModel;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AnggotaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection->transform(function (AnggotaModel $AnggotaModel) {
            return (new AnggotaResource($AnggotaModel));
        });

        return [
            'status' => true,
            'message' => 'Anggota',
            'data' => $collection,
        ];
    }
}

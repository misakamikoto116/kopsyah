<?php

namespace App\Http\Resources\Api\Anggota;

use Illuminate\Http\Resources\Json\Resource;

class AnggotaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $data = [
        'id'                 => $this->id,
        'no_pelanggan'       => $this->nomor_anggota,
        'nama'               => $this->nama,
        'alamat'             => $this->alamat,
        'alamat_pajak'       => $this->alamat,
        'kota'               => $this->kota,
        'telepon'            => $this->no_telepon,
        'personal_kontak'    => $this->no_telepon,
        'email'              => $this->email,
        'nama'               => $this->nama,
        'kota'               => $this->kota,
        'nik'                => $this->no_ktp,
      ];

      return $data;
    }
}

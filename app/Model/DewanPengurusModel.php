<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DewanPengurusModel extends Model
{
    protected $table='dewan';
    protected $guarded=['id'];

    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->where('nama', 'like', '%' . $request->nama . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OmsetModel extends Model
{
    protected $table = 'omset';
    protected $guarded = ['id'];
}

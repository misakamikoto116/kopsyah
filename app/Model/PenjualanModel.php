<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PenjualanModel extends Model
{
    protected $table = 'penjualan';
    protected $guarded = ['id'];
    protected $fillable = ['tanggal', 'total', 'jml_umkm', 'jml_non', 'nomor_anggota', 'anggota_id'];   
}

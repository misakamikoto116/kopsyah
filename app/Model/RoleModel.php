<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    protected $table='role';
    protected $fillable=['nama'];

    public function user(){
        return $this->hasOne(UserModel::class);
    }
    //untuk role
    public function menu(){
        return $this->belongsToMany(Menu::class);
    }

}

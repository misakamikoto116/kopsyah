<?php
//untuk role
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function role(){
        return $this->belongsToMany(Role::class);
    }

    public function child(){
        return $this->hasMany(self::class);
    }
    public function parent(){
        
    }
}

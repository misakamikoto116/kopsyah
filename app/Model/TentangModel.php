<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TentangModel extends Model
{
    protected $table='tentang';
    protected $protected=['id'];

    public function ScopeCari($query, $request)
    {
        if ($request->has('judul')) {
            $query->where('judul', 'like', '%' . $request->judul . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

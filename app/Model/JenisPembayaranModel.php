<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisPembayaranModel extends Model
{
    protected $table='jenis_pembayaran';
    protected $guarded=['id'];

    public function pembayaran(){
        return $this->hasMany(PembayaranModel::class);
    }
    public function listjumlahPokok(){
            $out = [];
            
                foreach($this->all() as $inv){
                
                    $out[$inv->id] = "Rp {$inv->nominal},-";
            
			
                
			}
			return $out;
        }
        public function listjumlahWajib(){
			$out = [];
			foreach($this->all() as $inv){
                
                    $out[$inv->id] = "Rp {$inv->nominal},-";
                
			}
			return $out;
        }
        
    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->where('nama', 'like', '%' . $request->nama . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

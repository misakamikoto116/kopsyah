<?php

namespace App\Model;
use \Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class PembayaranModel extends Model
{
    use Notifiable;
    protected $table='pembayaran';
    protected $guarded=['id'];
    //protected $dates=['tanggal_pembayaran'];
    public function jenis_pembayaran(){
        return $this->belongsTo(JenisPembayaranModel::class);
    }

    public function anggota(){
        return $this->belongsTo(AnggotaModel::class,'anggota_id');
    }
    public function notifikasi(){
        return $this->hasMany(NotificationModel::class,'pembayaran_id');
    }
    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->whereHas('anggota',function($query) use ($request){
                $query->where('nama', 'like', '%' . $request->nama . '%')
                    ->orWhere('nomor_anggota', 'like', '%' . $request->nama . '%');
            });
                
        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DokumenModel extends Model
{
    protected $table = 'dokumen';
    protected $guarded = ['id'];

    public function ScopeCari($query, $request)
    {
        if ($request->has('judul')) {
            $query->where('judul', 'like', '%' . $request->judul . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

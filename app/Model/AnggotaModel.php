<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnggotaModel extends Model
{
    use SoftDeletes;
    protected $table='anggota';
    protected $guarded=['id'];

    public function user(){
        return $this->belongsTo(UserModel::class,'users_id');
    }

    public function kepengurusan(){
        return $this->hasOne(KepengurusanModel::class,'anggota_id');
    }

    public function investasi(){
        return $this->hasMany(InvestasiModel::class,'anggota_id');
    }

    public function pembayaran(){
        return $this->hasMany(PembayaranModel::class,'anggota_id');
    }

    public function pengambilan(){
        return $this->hasMany(PengambilanModel::class,'anggota_id');
    }
    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
                $query->where('nama', 'like', '%' . $request->nama . '%')
                    ->orWhere('nomor_anggota', 'like', '%' . $request->nama . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

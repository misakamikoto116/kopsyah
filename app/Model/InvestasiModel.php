<?php

namespace App\Model;
use \Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class InvestasiModel extends Model
{
    use Notifiable;
    protected $table='investasi';
    protected $guarded=['id'];

    public function jenis_investasi()
    {
        return $this->belongsTo(JenisInvestasiModel::class,'jenis_investasi_id');
    }

    public function anggota()
    {
        return $this->belongsTo(AnggotaModel::class);
    }
    
    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->whereHas('anggota', function ($query) use ($request) {
                $query->where('nama', 'like', '%' . $request->nama . '%')
                    ->orWhere('nomor_anggota', 'like', '%' . $request->nama . '%');
            });

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

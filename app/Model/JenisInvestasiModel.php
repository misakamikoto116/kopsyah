<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisInvestasiModel extends Model
{
    protected $table='jenis_investasi';
    protected $guarded=['id'];

    public function investasi(){
        return $this->hasMany(InvestasiModel::class,'jenis_investasi_id');
    }
    public function listNamadanJumlah(){
			$out = [];
			foreach($this->all() as $inv){
				$out[$inv->id] = "{$inv->nama}";
			}
			return $out;
        }

    public function ScopeCariUsaha($query, $request)
    {
        if ($request->has('usaha')) {
            $query->where('id', 'like', '%' . $request->usaha . '%');
        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }

    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->where('nama', 'like', '%' . $request->nama . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

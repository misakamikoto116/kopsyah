<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationsModel extends Model
{
    protected $table='notifications';
    protected $guarded=['id'];

    public function pembayaran(){
        return $this->belongsTo(PembayaranModel::class,'pembayaran_id');
    }
}

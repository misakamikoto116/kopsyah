<?php

namespace App\Model;

use \Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BelanjaModel extends Model
{
    use Notifiable;
    protected $table = 'belanja';
    protected $guarded = ['id'];
    protected $fillable = ['nama_barang', 'merek', 'jumlah','periode','anggota_id','created_at','updated_at'];

    public function anggota()
    {
        return $this->belongsTo(AnggotaModel::class, 'anggota_id');
    }

}

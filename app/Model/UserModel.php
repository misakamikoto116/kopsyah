<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use \Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class UserModel extends Model implements AuthenticatableContract
{
    use Authenticatable;
    use Notifiable;
    protected $table='users';
    protected $guarded=['id'];
    // public function getAuthIdentifier()
    // {
    //     return $this->username;
    // }
    // public function getAuthPassword()
    // {
    //     return $this->password;
    // }
    public function role()
    {
        return $this->belongsTo(RoleModel::class);
    }


    public function anggota(){
        return $this->hasOne(AnggotaModel::class,'users_id');
    }

    public function pengambilan(){
        return $this->hasMany(PengambilanModel::class,'users_id');

    }
    public function ScopeCari($query, $request)
    {
        if ($request->has('user')) {
            //$query->where('username', 'like', '%' . $request->user . '%');
            $query->whereHas('role', function ($query) use ($request) {
                $query->where('username', 'like', '%' . $request->user . '%')
                    ->orWhere('nama', 'like', '%' . $request->user . '%');
            });

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KepengurusanModel extends Model
{
    protected $table='kepengurusan';
    protected $guarded=['id'];

    public function jabatan(){
        return $this->belongsTo(JabatanModel::class);
    }

    // public function anggota(){
    //     return $this->belongsTo(AnggotaModel::class);
    // }

    public function anggota()
    {
        return $this->belongsTo(AnggotaModel::class, 'anggota_id');
    }

    public function ScopeCari($query, $request)
    {
        if ($request->has('nama')) {
            $query->whereHas('anggota',function($query) use ($request){
                $query->where('nama', 'like', '%' . $request->nama . '%');
            });
        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengambilanModel extends Model
{
    protected $table='pengambilan';
    protected $guarded=['id'];

    public function user(){
        return $this->belongsTo(UserModel::class, 'users_id');
    }
    
    public function anggota()
    {
        return $this->belongsTo(AnggotaModel::class, 'anggota_id');
    }
}

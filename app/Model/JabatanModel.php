<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JabatanModel extends Model
{
    protected $table='jabatan';
    protected $guarded=['id'];

    public function jabatan(){
        return $this->hasMany(KepengurusanModel::class);
    }

    public function ScopeCari($query, $request)
    {
        if ($request->has('posisi')) {
            $query->where('posisi', 'like', '%' . $request->posisi . '%');

        }
        $query
            ->orderBy('id', 'DESC');
        return $query;
    }
}
